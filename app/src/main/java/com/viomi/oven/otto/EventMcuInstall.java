package com.viomi.oven.otto;

/**
 * Created by Ljh on 18/5/9
 * 安装状态
 */
public class EventMcuInstall extends Event {
    private int status;//0-安装中 1-安装成功  2-安装失败
    private int progress = 0;

    public EventMcuInstall(String from, int status) {
        setBusFrom(from);
        this.status = status;
    }
    public EventMcuInstall(String from, int status, int progress) {
        setBusFrom(from);
        this.status = status;
        this.progress = progress;
    }

    public int getSuccessStatus() {
        return status;
    }

    public int getProgress() {
        return progress;
    }
}
