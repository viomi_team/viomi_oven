package com.viomi.oven.otto;

import android.os.Handler;
import android.os.Looper;

import com.viomi.oven.util.LogUtils;
import com.squareup.otto.Bus;

/**
 * Created by Ljh on 16/12/19.
 */
public class BusProvider extends Bus {
    private static final String TAG = "BusProvider";

    private static final BusProvider bus = new BusProvider();
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    public static BusProvider getInstance() {
        return bus;
    }

    public BusProvider() {
    }

    /**
     * 全部切换到主线程发送
     *
     * @param event
     */
    public void postEvent(final Event event) {
        if (event == null) return;
        LogUtils.d(TAG + "   the event from:" + event.getBusFrom() + "   the event is:" + event.toString());
        if (Looper.myLooper() == Looper.getMainLooper()) {
            bus.post(event);
        } else {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    bus.post(event);
                }
            });
        }
    }
}