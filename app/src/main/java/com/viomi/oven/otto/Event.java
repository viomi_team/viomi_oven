package com.viomi.oven.otto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ljh on 16/12/19.
 */
public class Event {
    private String busFrom = "";
    private String busTo = "";
    private List<String> mListTo = new ArrayList<>();

    public Event() {
    }

    public String getBusFrom() {
        return busFrom;
    }

    public void setBusFrom(String busFrom) {
        this.busFrom = busFrom;
    }

    public String getBusTo() {
        return busTo;
    }

    public void setBusTo(String busTo) {
        this.busTo = busTo;
    }

    public List<String> getListTo() {
        return mListTo;
    }

    public void setListTo(List<String> listTo) {
        mListTo = listTo;
    }
}
