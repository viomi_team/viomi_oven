package com.viomi.oven.otto;

/**
 * Created by Ljh on 18/1/26.
 * 更新曲线用
 */
public class Event2PauseStatus extends Event {
    private int pauseStatus;

    public Event2PauseStatus(String from, int pauseStatus) {
        setBusFrom(from);
        this.pauseStatus = pauseStatus;
    }

    public int getPauseStatus() {
        return pauseStatus;
    }
}
