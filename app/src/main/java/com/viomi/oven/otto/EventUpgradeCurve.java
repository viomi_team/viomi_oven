package com.viomi.oven.otto;

import com.viomi.oven.bean.CurveRunStep;

/**
 * Created by Ljh on 18/1/26.
 * 更新曲线用
 */
public class EventUpgradeCurve extends Event {
    private CurveRunStep mCurveRunStep;

    public EventUpgradeCurve(String from, CurveRunStep mCurveRunStep) {
        setBusFrom(from);
        this.mCurveRunStep = mCurveRunStep;
    }

    public CurveRunStep getCurveRunStep() {
        return mCurveRunStep;
    }
}
