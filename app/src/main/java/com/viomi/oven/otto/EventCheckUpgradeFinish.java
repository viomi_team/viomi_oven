package com.viomi.oven.otto;

/**
 * Created by Ljh on 18/3/23
 * 检测版本
 */
public class EventCheckUpgradeFinish extends Event {
    private boolean success;

    public EventCheckUpgradeFinish(String from, boolean success) {
        setBusFrom(from);
        this.success = success;
    }

    public boolean getSuccessStatus() {
        return success;
    }
}
