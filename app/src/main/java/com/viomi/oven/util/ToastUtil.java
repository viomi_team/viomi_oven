package com.viomi.oven.util;

import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.viomi.oven.R;
import com.viomi.oven.ViomiApplication;


/**
 * Created by viomi on 2016/10/24.
 * toast封装
 */

public class ToastUtil {
    public static void show(String result) {
        if (Global.isMainThread()) {
            Toast toast = new Toast(ViomiApplication.getContext());
            TextView view = new TextView(ViomiApplication.getContext());
            view.setBackgroundResource(R.color.black_ap80);
            view.setTextColor(Color.WHITE);
            view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28);
            view.setText(result);
            view.setPadding(20, 20, 20, 20);
            toast.setGravity(Gravity.CENTER, 0, 40);
            toast.setView(view);
            toast.show();
        }
        //Toast.makeText(ViomiApplication.getContext(), result, Toast.LENGTH_SHORT).show();
    }

    public static void showCenter(String result) {
        if (Global.isMainThread()) {
            Toast toast = new Toast(ViomiApplication.getContext());
            TextView view = new TextView(ViomiApplication.getContext());
            view.setBackgroundResource(R.color.black_ap80);
            view.setTextColor(Color.WHITE);
            view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28);
            view.setText(result);
            view.setPadding(20, 20, 20, 20);
            toast.setGravity(Gravity.CENTER, 0, 40);
            toast.setView(view);
            toast.show();
        }
    }
}
