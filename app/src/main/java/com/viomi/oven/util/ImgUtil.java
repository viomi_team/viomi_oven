package com.viomi.oven.util;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.request.RequestOptions;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Ljh on 17/12/12.
 */
public class ImgUtil {
    private static final String TAG = "ImgUtil";

    public static void showImage(String url, ImageView iv) {
        LogUtils.d(TAG + " the url is:" + url);
        RequestOptions options = new RequestOptions()
                .placeholder(android.R.color.white)
                .error(android.R.color.white);
        Glide.with(iv.getContext()).load(url).apply(options).into(iv);
    }

    public static void showImage(int url, ImageView iv) {
        LogUtils.d(TAG + " the url is:" + url);
        RequestOptions options = new RequestOptions()
                .placeholder(android.R.color.white)
                .error(android.R.color.white);
        Glide.with(iv.getContext()).load(url).apply(options).into(iv);
    }

    public static void showCircleImage(String url, ImageView iv) {
        LogUtils.d(TAG + " the url is:" + url);
        RequestOptions options = new RequestOptions()
                .placeholder(android.R.color.white)
                .error(android.R.color.white).bitmapTransform(new
                        CropCircleTransformation());
        Glide.with(iv.getContext()).load(url).apply(options).into(iv);
    }

    public static void showCircleImage(int url, ImageView iv) {
        LogUtils.d(TAG + " the url is:" + url);
        RequestOptions options = new RequestOptions()
                .placeholder(android.R.color.white)
                .error(android.R.color.white).bitmapTransform(new
                        CropCircleTransformation());
        Glide.with(iv.getContext()).load(url).apply(options).into(iv);
    }

    public static void showDefinedImage(String url, ImageView iv, int resouceId) {
        LogUtils.d(TAG + " the url is:" + url);
        RequestOptions options = new RequestOptions()
                .placeholder(resouceId)
                .error(resouceId);
        Glide.with(iv.getContext()).load(url).apply(options).into(iv);
    }

    public static void showDefinedImage(int url, ImageView iv, int resouceId) {
        LogUtils.d(TAG + " the url is:" + url);
        RequestOptions options = new RequestOptions()
                .placeholder(resouceId)
                .error(resouceId);
        Glide.with(iv.getContext()).load(url).apply(options).into(iv);
    }

    public static void showDefinedCircleImage(String url, ImageView iv, int resouceId) {
        LogUtils.d(TAG + " the url is:" + url);
        RequestOptions options = new RequestOptions()
                .placeholder(resouceId)
                .error(resouceId).bitmapTransform(new
                        CropCircleTransformation());
        Glide.with(iv.getContext()).load(url).apply(options).into(iv);
    }

    public static void showDefinedTransform(int url, ImageView iv, Transformation<Bitmap> transformation) {
        LogUtils.d(TAG + " the url is:" + url);
        RequestOptions options = new RequestOptions()
                .placeholder(android.R.color.white)
                .error(android.R.color.white).bitmapTransform(transformation);
        Glide.with(iv.getContext()).load(url).apply(options).into(iv);
    }

    public static void showDefinedTransform(String url, ImageView iv, Transformation<Bitmap> transformation) {
        LogUtils.d(TAG + " the url is:" + url);
        RequestOptions options = new RequestOptions()
                .placeholder(android.R.color.white)
                .error(android.R.color.white).bitmapTransform(transformation);
        Glide.with(iv.getContext()).load(url).apply(options).into(iv);
    }
}
