package com.viomi.oven.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.viomi.oven.AppParam;
import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.ViomiApplication;
import com.viomi.oven.activity.MainActivity_H;
import com.viomi.oven.activity.MainActivity_V_NEW;
import com.viomi.oven.bean.BasicKeyInfo;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.enumType.FromType;
import com.viomi.oven.enumType.McuStatus;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.manager.SerialManager;

import java.util.ArrayList;
import java.util.List;

/**
 * 全局公共类, 封装一些公共公能
 */
public class Global {

    public static Context mContext;

    public static float mDensity;

    public static float mScreenWidth;

    public static float mScreenHeight;


    public static void init(Context context) {
        mContext = context;
        initScreenSize();
    }

    public static Context getContext() {
        return mContext;
    }

    public static Resources getResources() {
        return mContext.getResources();
    }

    private static void initScreenSize() {
        DisplayMetrics dm = mContext.getResources().getDisplayMetrics();
        mDensity = dm.density;
        mScreenHeight = dm.heightPixels;
        mScreenWidth = dm.widthPixels;
    }

    /**
     * dp --> px
     *
     * @param dp
     * @return
     */
    public static int dp2px(int dp) {
        return (int) (dp * mDensity);
    }

    /**
     * px --> dp
     *
     * @param px
     * @return
     */
    public static int px2dp(int px) {
        //dp和px的转换关系比例值
        return (int) (px / mDensity + 0.5);
    }

    /**
     * px转sp
     *
     * @param px
     * @return
     */
    public static float px2sp(float px) {
        return (float) (px / mContext.getResources().getDisplayMetrics().scaledDensity + 0.5);
    }

    /**
     * sp转px
     *
     * @param sp
     * @return
     */
    public static int sp2px(float sp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                sp, mContext.getResources().getDisplayMetrics());
    }


    public static View inflate(int layoutResID, ViewGroup parent) {
        return LayoutInflater.from(mContext).inflate(layoutResID, parent, false);
    }

    public static View inflate(int layoutResID) {
        return inflate(layoutResID, null);
    }

    private static Handler mHandler = new Handler(Looper.getMainLooper());

    public static Handler getMainHandler() {
        return mHandler;
    }

    /**
     * 判断当前线程是否是主线程
     *
     * @return true表示当前是在主线程中运行
     */
    public static boolean isUIThread() {
        return Looper.getMainLooper() == Looper.myLooper();
    }

    public static void runOnUIThread(Runnable run) {
        if (isUIThread()) {
            run.run();
        } else {
            mHandler.post(run);
        }
    }

    /**
     * 隐藏输入法面板
     */
    public static void hideInputMethod(View view) {
        Context context = view.getContext();
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * 进入Activity界面时不弹出输入法面板
     *
     * @param activity
     */
    public static void hideInputMethod(Activity activity) {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    /**
     * 用来记录打开了多少个Activity界面
     */
    private static List<Activity> activityList = new ArrayList<Activity>();

    /**
     * 进入一个新的Activity时调用此方法，记录Activity
     *
     * @param activity
     */
    public static void pushActivity(Activity activity) {
        if (!activityList.contains(activity)) {
            activityList.add(activity);
        }
    }

    /**
     * 退出一个Activity时调用此方法,移除Activity
     */
    public static void popActivity(Activity activity) {
        activityList.remove(activity);
    }

    /**
     * 是否在正在运行的Activity
     *
     * @return true表示程序在前台运行，false表示在后台进行
     */
    public static boolean hasForegroundActivies() {
        return activityList.size() > 0;
    }

    /**
     * dip2px
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 获取屏幕宽度
     */
    public static int getScreenWidth(Context context) {
        return ((Activity) context).getWindowManager().getDefaultDisplay()
                .getWidth();
    }

    public static void jump2MainActivity(Context mContext) {
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            mContext.startActivity(new Intent(mContext, MainActivity_V_NEW.class));
        else
            mContext.startActivity(new Intent(mContext, MainActivity_H.class));
    }

    public static void jump2MainActivityNewTask(Context mContext) {
        Intent bootStartIntent = new Intent(mContext, MainActivity_H.class);
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) {
            bootStartIntent = new Intent(mContext, MainActivity_V_NEW.class);
        }
        bootStartIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(bootStartIntent);
    }

    public static void jump2MainActivity() {
        Intent intent = new Intent();
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            intent.setClass(ViomiApplication.getContext(), MainActivity_V_NEW.class);
        else
            intent.setClass(ViomiApplication.getContext(), MainActivity_H.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ViomiApplication.getContext().startActivity(intent);
    }

    public static void backMainInitHawkParams() {
        HawkParams.setWorkStatus(WorkStatus.STATUS_IDLE);
        HawkParams.setPauseStatus(0);
        HawkParams.setCookFrom(FromType.FROM_RECIPE.value);
        HawkParams.setC_totletime(0);
        HawkParams.setC_time(0);
        HawkParams.setS_temp(0);
        HawkParams.setC_temps("");
        HawkParams.setMode(0);
        HawkParams.setNutritionInfos(new ArrayList<BasicKeyInfo>());
        HawkParams.setBespeakdish("");
        HawkParams.setAppointTime(0);
        HawkParams.setFoodName("");
        HawkParams.setFoodId("");
        HawkParams.setDescaling(0);
        if (AppParam.mcuStatus == McuStatus.MCU_WORKING || AppParam.mcuStatus == McuStatus.MCU_SUSPENDED)
            SerialManager.getInstance().sendCmdOver();
    }

    public static boolean isMainThread() {
        return Looper.getMainLooper() == Looper.myLooper();
    }

    public static boolean isPrepareOk(List<DeviceRunStep> mSteps) {
        if (AppParam.isDoorOpen) {
            ToastUtil.show("请关门后再启动烹饪");
            return false;
        }
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) {
            for (DeviceRunStep step : mSteps) {
                if (AppParam.isNoWater) {
                    if (step.getMode() == SteamBakeType.STEAM.value || step.getMode() == SteamBakeType.STEAM_BAKE.value
                            || step.getMode() == SteamBakeType.STEAM_BAKE_1.value || step.getMode() == SteamBakeType.STEAM_BAKE_2.value
                            || step.getMode() == SteamBakeType.STEAM_BAKE_3.value || step.getMode() == SteamBakeType.STEAM_BAKE_4.value
                            || step.getMode() == SteamBakeType.STEAM_BAKE_5.value || step.getMode() == SteamBakeType.STEAM_BAKE_6.value
                            || step.getMode() == SteamBakeType.DISINFECTION.value || step.getMode() == SteamBakeType.THAW.value
                            || step.getMode() == SteamBakeType.FERMENTATION.value || step.getMode() == SteamBakeType.HOT_FOOD.value
                            || step.getMode() == SteamBakeType.CLEAN.value || step.getMode() == SteamBakeType.CHUGOU.value) {
                        ToastUtil.show("水箱缺水");
                        return false;
                        //return true;
                    }
                }
            }
        }
        return true;
    }

    /**
     * @param step
     * @return
     */
    public static DeviceRunStep compatibleChange(DeviceRunStep step) {
        int mode = step.getMode();
        int time = step.getTime();
        int temp = step.getTemp();
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) {//嵌入式菜谱转换成台面式
            if (mode >= SteamBakeType.BAKE_1.value && mode <= SteamBakeType.BAKE_7.value) {
                step.setMode(SteamBakeType.BAKE.value);
                if (temp > 220 && time > 40)
                    time = 40;
                step.setTime(time);
                step.setTemp(temp);
            } else if (mode >= SteamBakeType.STEAM_BAKE_1.value && mode <= SteamBakeType.STEAM_BAKE_6.value) {
                step.setMode(SteamBakeType.STEAM_BAKE.value);
                if (temp > 220 && time > 40)
                    time = 40;
                step.setTime(time);
                step.setTemp(temp);
            }
        } else {//台面式菜谱转换成嵌入式
            if (mode == SteamBakeType.BAKE.value) {
                step.setMode(SteamBakeType.BAKE_6.value);
                step.setTime(time);
                step.setTemp(temp);
            } else if (mode == SteamBakeType.STEAM_BAKE.value) {
                step.setMode(SteamBakeType.STEAM_BAKE_2.value);
                step.setTime(time);
                step.setTemp(temp);
            }
        }
        return step;
    }

    public static List<DeviceRunStep> compatibleChangeList(List<DeviceRunStep> stepList) {
        for (DeviceRunStep step : stepList) {
            compatibleChange(step);
        }
        return stepList;
    }

    public static String verString(int ver) {
        if (ver >= 100)
            return "v" + ver;
        else if (ver >= 10)
            return "v0" + ver;
        else return "v00" + ver;
    }
}
