package com.viomi.oven.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * 数字相关工具类
 */

public class NumberUtil {
    public static String formatFloat2decimals(float number) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(); // 获得格式化类对象
        df.applyPattern("0.00");// 设置小数点位数(两位) 余下的会四舍五入
        return df.format(number);
    }

    public static String formatFloat2decimals(double number) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(); // 获得格式化类对象
        df.applyPattern("0.00");// 设置小数点位数(两位) 余下的会四舍五入
        return df.format(number);
    }

    public static String formatFloat2decimals(String number) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(); // 获得格式化类对象
        df.applyPattern("0.00");// 设置小数点位数(两位) 余下的会四舍五入
        return df.format(Float.parseFloat(number));
    }

    public static String formatFloat2decimalsUp(double number){
        DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(); // 获得格式化类对象
        df.applyPattern("0.00");// 设置小数点位数(两位) 余下的会四舍五入
        df.setRoundingMode(RoundingMode.UP);
        return df.format(number);
    }

    public static String formatFloat3decimals(double number) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(); // 获得格式化类对象
        df.applyPattern("0.000");// 设置小数点位数(两位) 余下的会四舍五入
        return df.format(number);
    }
}
