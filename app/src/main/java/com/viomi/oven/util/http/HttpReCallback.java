package com.viomi.oven.util.http;

import com.google.gson.Gson;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.NetworkUtils;
import com.viomi.oven.util.ToastUtil;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import okhttp3.Call;

/**
 * Created by Ljh on 2018/3/2
 */

public abstract class HttpReCallback<T> extends StringCallback {

    @Override
    public void onResponse(String response, int d) {
        String responceResult = response.substring(response.indexOf("{", 1), response.lastIndexOf("}"));
        LogUtils.d("HttpReceiver", System.currentTimeMillis() + "");
        LogUtils.d("HttpReceiver", responceResult);
        T result = parseResponse(responceResult);
        LogUtils.d("HttpReceiver", System.currentTimeMillis() + "");
        postResult(result);
    }

    @Override
    public void onError(Call call, Exception e, int id) {
        postError(e);
    }

    public abstract void onSuccess(T result);

    public abstract void onFail(int errorCode);

    private T parseResponse(String response) {
        Type type;
        Class<?> clazz = this.getClass();
        try {
            ParameterizedType mType = (ParameterizedType) clazz.getGenericInterfaces()[0];
            type = mType.getActualTypeArguments()[0];
        } catch (Throwable e) {
            type = ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments()[0];
        }

        T result = null;
        if (response == null) {
            postError(new HttpException("null response"));
            return null;
        }
        try {
            Gson gson = new Gson();
            result = gson.fromJson(response, type);

        } catch (Exception e) {
            postError(e);
        }
        return result;
    }

    private void postError(Exception e) {
        //LogUtils.e(e);
        int errorCode = HttpErrors.EXCEPTION;

        if (!NetworkUtils.isAvailable()) {
            errorCode = HttpErrors.NETWORK_DISABLE;
            ToastUtil.show("网络异常");
            LogUtils.d("HttpReceiver", "errorCode -9996 网络错误");
        } else {
            if (e instanceof IOException) {
                errorCode = HttpErrors.IO_EXCEPTION;
                ToastUtil.show("服务器异常");
                LogUtils.d("HttpReceiver", "errorCode -9997 读写错误");
            } else if (e instanceof ParseDataException) {
                errorCode = HttpErrors.PARSE_EXCEPTION;
                LogUtils.d("HttpReceiver", "errorCode -9998 解析数据错误");
            }
        }
        onFail(errorCode);
    }

    private void postResult(T result) {
        if (result != null) {
            onSuccess(result);
        }
    }
}
