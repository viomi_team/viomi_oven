package com.viomi.oven.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.viomi.oven.ViomiApplication;

import java.util.ArrayList;
import java.util.Arrays;

public class AppSharePreferences {
    // / name
    private static final String CONFIG_PREFERENCE_NAME = "com.viomi.oven.preference";

    // / remove interface
    public static void remove(String key) {
        SharedPreferences pref = ViomiApplication.getContext().getSharedPreferences(CONFIG_PREFERENCE_NAME, Context.MODE_MULTI_PROCESS);
        pref.edit().remove(key).commit();
    }

    public static String getString(String key) {
        return getString(key, null);
    }

    public static String getString(String key, String def) {
        SharedPreferences pref = ViomiApplication.getContext().getSharedPreferences(CONFIG_PREFERENCE_NAME, Context.MODE_MULTI_PROCESS);
        String s = pref.getString(key, def);
        return s;
    }

    public static int getInt(String key) {
        return getInt(key, -1);
    }

    public static int getInt(String key, int def) {
        SharedPreferences pref = ViomiApplication.getContext().getSharedPreferences(CONFIG_PREFERENCE_NAME, Context.MODE_MULTI_PROCESS);
        int value = pref.getInt(key, def);
        return value;
    }

    public static boolean getBoolean(String key) {
        return getBoolean(key, false);
    }

    public static boolean getBoolean(String key, boolean def) {
        SharedPreferences pref = ViomiApplication.getContext().getSharedPreferences(CONFIG_PREFERENCE_NAME, Context.MODE_MULTI_PROCESS);
        boolean value = pref.getBoolean(key, def);
        return value;
    }

    public static long getLong(String key) {
        return getLong(key, -1);
    }

    public static long getLong(String key, long def) {
        SharedPreferences pref = ViomiApplication.getContext().getSharedPreferences(CONFIG_PREFERENCE_NAME, Context.MODE_MULTI_PROCESS);
        long value = pref.getLong(key, def);
        return value;
    }

    public static boolean setValue(String key, boolean value) {
        SharedPreferences pref = ViomiApplication.getContext().getSharedPreferences(CONFIG_PREFERENCE_NAME, Context.MODE_MULTI_PROCESS);
        return pref.edit().putBoolean(key, value).commit();
    }

    public static boolean setValue(String key, int value) {
        SharedPreferences pref = ViomiApplication.getContext().getSharedPreferences(CONFIG_PREFERENCE_NAME, Context.MODE_MULTI_PROCESS);
        return pref.edit().putInt(key, value).commit();
    }

    public static boolean setValue(String key, String value) {
        SharedPreferences pref = ViomiApplication.getContext().getSharedPreferences(CONFIG_PREFERENCE_NAME, Context.MODE_MULTI_PROCESS);
        return pref.edit().putString(key, value).commit();
    }

    public static boolean setValue(String key, Long value) {
        SharedPreferences pref = ViomiApplication.getContext().getSharedPreferences(CONFIG_PREFERENCE_NAME, Context.MODE_MULTI_PROCESS);
        return pref.edit().putLong(key, value).commit();
    }

    public static SharedPreferences.Editor getEditor() {
        SharedPreferences pref = ViomiApplication.getContext().getSharedPreferences(CONFIG_PREFERENCE_NAME, Context.MODE_MULTI_PROCESS);
        return pref.edit();
    }

    public static void setValueAsync(String key, boolean value) {
        SharedPreferences pref = ViomiApplication.getContext().getSharedPreferences(CONFIG_PREFERENCE_NAME, Context.MODE_MULTI_PROCESS);
        pref.edit().putBoolean(key, value).apply();
    }

    public static void setValueAsync(String key, int value) {
        SharedPreferences pref = ViomiApplication.getContext().getSharedPreferences(CONFIG_PREFERENCE_NAME, Context.MODE_MULTI_PROCESS);
        pref.edit().putInt(key, value).apply();
    }

    public static void setValueAsync(String key, String value) {
        SharedPreferences pref = ViomiApplication.getContext().getSharedPreferences(CONFIG_PREFERENCE_NAME, Context.MODE_MULTI_PROCESS);
        pref.edit().putString(key, value).apply();
    }

    public static void setValueAsync(String key, Long value) {
        SharedPreferences pref = ViomiApplication.getContext().getSharedPreferences(CONFIG_PREFERENCE_NAME, Context.MODE_MULTI_PROCESS);
        pref.edit().putLong(key, value).apply();
    }

    public static void setValueAsync(String key, ArrayList<String> values, String divider) {
        StringBuilder sb = new StringBuilder();
        for (String value : values) {
            sb.append(value);
            sb.append(divider);
        }
        setValueAsync(key, sb.substring(0, sb.length() - divider.length()));
    }

    public static void setValueAsync(String key, String[] values, String divider) {
        StringBuilder sb = new StringBuilder();
        for (String value : values) {
            sb.append(value);
            sb.append(divider);
        }
        setValueAsync(key, sb.substring(0, sb.length() - divider.length()));
    }

    public static String[] getStringArray(String key, String divider) {
        String value = getString(key);
        if (value == null) {
            return null;
        }

        String[] values = value.split(divider);
        return values;
    }

    public static ArrayList<String> getStringArrayList(String key, String divider) {
        String[] array = getStringArray(key, divider);
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(array));
        return list;
    }
}
