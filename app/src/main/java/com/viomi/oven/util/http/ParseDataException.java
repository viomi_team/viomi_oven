package com.viomi.oven.util.http;

public class ParseDataException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -7028384902901084501L;

    public ParseDataException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public ParseDataException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
        // TODO Auto-generated constructor stub
    }

    public ParseDataException(String detailMessage) {
        super(detailMessage);
        // TODO Auto-generated constructor stub
    }

    public ParseDataException(Throwable throwable) {
        super(throwable);
        // TODO Auto-generated constructor stub
    }


}
