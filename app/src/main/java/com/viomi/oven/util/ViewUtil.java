package com.viomi.oven.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.oven.R;
import com.viomi.oven.view.HorizonListView;

/**
 * Created by Ljh on 2018/1/24.
 */

public class ViewUtil {
    private static final String TAG = "ViewUtil";
    //
    public final static float BASE_SCREEN_WIDTH = 720f;
    public final static float BASE_SCREEN_HEIGHT = 1280f;
    public final static float BASE_SCREEN_DENSITY = 2f;
    public static Float sScaleW, sScaleH;
    private static final int DP_TO_PX = TypedValue.COMPLEX_UNIT_DIP;
    private static final int SP_TO_PX = TypedValue.COMPLEX_UNIT_SP;
    private static final int PX_TO_DP = TypedValue.COMPLEX_UNIT_MM + 1;
    private static final int PX_TO_SP = TypedValue.COMPLEX_UNIT_MM + 2;
    private static final int DP_TO_PX_SCALE_H = TypedValue.COMPLEX_UNIT_MM + 3;
    private static final int DP_SCALE_H = TypedValue.COMPLEX_UNIT_MM + 4;
    private static final int DP_TO_PX_SCALE_W = TypedValue.COMPLEX_UNIT_MM + 5;

    private static float applyDimension(Context context, int unit, float value, DisplayMetrics metrics) {
        switch (unit) {
            case DP_TO_PX:
            case SP_TO_PX:
                return TypedValue.applyDimension(unit, value, metrics);
            case PX_TO_DP:
                return value / metrics.density;
            case PX_TO_SP:
                return value / metrics.scaledDensity;
            case DP_TO_PX_SCALE_H:
                return TypedValue.applyDimension(DP_TO_PX, value * getScaleFactorH(context), metrics);
            case DP_SCALE_H:
                return value * getScaleFactorH(context);
            case DP_TO_PX_SCALE_W:
                return TypedValue.applyDimension(DP_TO_PX, value * getScaleFactorW(context), metrics);
        }
        return 0;
    }

    public static void setGridViewWidth(HorizonListView gv) {
        ///*
        ListAdapter listAdapter = gv.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalWidth = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, gv);
            listItem.measure(0, 0);
            LogUtils.d(TAG, "the setGridViewWidth item width is:" + listItem.getMeasuredWidth());
            totalWidth += listItem.getMeasuredWidth();
        }
        ViewGroup.LayoutParams params = gv.getLayoutParams();
        params.width = totalWidth + gv.getPaddingLeft()
                + gv.getPaddingRight();
        //int interval = (listAdapter.getCount() > 0) ? gv.getHorizontalSpacing() * (listAdapter.getCount() - 1) : 0;
        //LogUtils.d(TAG, "the setGridViewWidth interval is:" + interval);
        //params.width += (interval+5);// if without this statement,the listview will be a
        LogUtils.d(TAG, "the setGridViewWidth width is:" + params.width);
//*/
        //ViewGroup.LayoutParams paramsParent = parent.getLayoutParams();
        //paramsParent.width = params.width;
        //parent.setLayoutParams(paramsParent);
        // little short
        gv.setLayoutParams(params);
    }

    public static void llAddLeftSpaceRightView(Context context, LinearLayout ll, String tv1, String tv2) {
        RelativeLayout rl_item = (RelativeLayout) ((Activity) context).getLayoutInflater()
                .inflate(R.layout.layout_left_space_right_text, null, false);
        ((TextView) rl_item.findViewById(R.id.tv1)).setText(tv1);
        ((TextView) rl_item.findViewById(R.id.tv2)).setText(tv2);
        ll.addView(rl_item);
    }

    public static void llAddLeftSpaceRightView(Context context, LinearLayout ll, String tv1, String tv2, int color) {
        RelativeLayout rl_item = (RelativeLayout) ((Activity) context).getLayoutInflater()
                .inflate(R.layout.layout_left_space_right_text, null, false);
        if (color != 0)
            rl_item.findViewById(R.id.root).setBackgroundColor(color);
        ((TextView) rl_item.findViewById(R.id.tv1)).setText(tv1);
        ((TextView) rl_item.findViewById(R.id.tv2)).setText(tv2);
        ll.addView(rl_item);
    }

    public static void llAddLeftSpaceRightView(Context context, LinearLayout ll, String tv1, SpannableStringBuilder tv2) {
        RelativeLayout rl_item = (RelativeLayout) ((Activity) context).getLayoutInflater()
                .inflate(R.layout.layout_left_space_right_text, null, false);
        ((TextView) rl_item.findViewById(R.id.tv1)).setText(tv1);
        ((TextView) rl_item.findViewById(R.id.tv2)).setText(tv2);
        ll.addView(rl_item);
    }

    /**
     * Drawable转bitmap
     *
     * @param drawableId
     * @return
     */
    public static Bitmap Drawable2Bitmap(Context c, int drawableId) {
        Resources r = c.getResources();
        Drawable drawable = r.getDrawable(drawableId);
        BitmapDrawable bd_b = (BitmapDrawable) drawable;
        Bitmap icon = bd_b.getBitmap();
        return icon;
    }

    public static Bitmap ScaledBitmap(Bitmap bitmap, float scaleX, float scaleY) {
        Matrix matrix = new Matrix();
        matrix.postScale(scaleX, scaleY);  //长和宽放大缩小的比例
        Bitmap resizeBmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return resizeBmp;
    }

    /**
     * 按比例缩放图片
     *
     * @param origin 原图
     * @param ratio  比例
     * @return 新的bitmap
     */
    public static Bitmap ScaledBitmap(Bitmap origin, float ratio) {
        if (origin == null) {
            return null;
        }
        int width = origin.getWidth();
        int height = origin.getHeight();
        Matrix matrix = new Matrix();
        matrix.preScale(ratio, ratio);
        Bitmap newBM = Bitmap.createBitmap(origin, 0, 0, width, height, matrix, false);
        if (newBM.equals(origin)) {
            return newBM;
        }
        //origin.recycle();
        return newBM;
    }

    /**
     * 根据给定的宽和高进行拉伸
     *
     * @param origin    原图
     * @param newWidth  新图的宽
     * @param newHeight 新图的高
     * @return new Bitmap
     */
    public static Bitmap ScaledBitmap(Bitmap origin, int newWidth, int newHeight) {
        if (origin == null) {
            return null;
        }
        int height = origin.getHeight();
        int width = origin.getWidth();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);// 使用后乘
        Bitmap newBM = Bitmap.createBitmap(origin, 0, 0, width, height, matrix, true);
        //if (!origin.isRecycled()) {
        //    origin.recycle();
        //}
        return newBM;
    }

    public static int getDeviceWith(Activity context) {
        DisplayMetrics dm = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        return width;
    }

    public static int getDeviceHeight(Activity context) {
        DisplayMetrics dm = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int height = dm.heightPixels;
        return height;
    }

    public static float getDensity(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    /**
     * 如果要计算的值已经经过dip计算，则使用此结果，如果没有请使用getScaleFactorWithoutDip
     */
    public static float getScaleFactorW(Context context) {
        if (sScaleW == null) {
            sScaleW = (getDeviceWith((Activity) context) * BASE_SCREEN_DENSITY) / (getDensity(context) * BASE_SCREEN_WIDTH);
        }
        return sScaleW;
    }

    public static float getScaleFactorH(Context context) {
        if (sScaleH == null) {
            sScaleH = (getDeviceHeight((Activity) context) * BASE_SCREEN_DENSITY)
                    / (getDensity(context) * BASE_SCREEN_HEIGHT);
        }
        return sScaleH;
    }

    public static int dp2px(Context context, float value) {
        return (int) applyDimension(context, DP_TO_PX, value, context.getResources().getDisplayMetrics());
    }

    public static int sp2px(Context context, float value) {
        return (int) applyDimension(context, SP_TO_PX, value, context.getResources().getDisplayMetrics());
    }

    public static int px2dp(Context context, float value) {
        return (int) applyDimension(context, PX_TO_DP, value, context.getResources().getDisplayMetrics());
    }

    public static int px2sp(Context context, float value) {
        return (int) applyDimension(context, PX_TO_SP, value, context.getResources().getDisplayMetrics());
    }
}
