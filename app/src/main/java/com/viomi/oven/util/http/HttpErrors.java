package com.viomi.oven.util.http;

public interface HttpErrors {
    int EXCEPTION = -9999;
    int PARSE_EXCEPTION = -9998;//解析数据错误
    int IO_EXCEPTION = -9997;//读写错误
    int NETWORK_DISABLE = -9996;//网络错误
}
