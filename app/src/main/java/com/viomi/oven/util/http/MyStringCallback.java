package com.viomi.oven.util.http;

import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.NetworkUtils;
import com.viomi.oven.util.ToastUtil;
import com.zhy.http.okhttp.callback.Callback;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Ljh on 2018/3/21
 */
public abstract class MyStringCallback extends Callback<String> {
    @Override
    public String parseNetworkResponse(Response response, int id) throws IOException {
        return response.body().string();
    }

    @Override
    public void onError(Call call, Exception e, int id) {
        postError(e);
    }

    @Override
    public void onResponse(String response, int id) {
        onSuccess(response);
    }

    private void postError(Exception e) {
        //LogUtils.e(e);
        int errorCode = HttpErrors.EXCEPTION;

        if (!NetworkUtils.isAvailable()) {
            errorCode = HttpErrors.NETWORK_DISABLE;
            ToastUtil.show("网络异常");
            LogUtils.d("HttpReceiver", "errorCode -9996 网络错误");
        } else {
            if (e instanceof IOException) {
                errorCode = HttpErrors.IO_EXCEPTION;
                ToastUtil.show("服务器异常");
                LogUtils.d("HttpReceiver", "errorCode -9997 读写错误");
            } else if (e instanceof ParseDataException) {
                errorCode = HttpErrors.PARSE_EXCEPTION;
                LogUtils.d("HttpReceiver", "errorCode -9998 解析数据错误");
            }
        }
        onFail(errorCode);
    }

    public abstract void onSuccess(String result);

    public abstract void onFail(int errorCode);
}
