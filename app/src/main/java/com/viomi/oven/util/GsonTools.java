package com.viomi.oven.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created whith  Android Studio.
 * Author:Ljh
 * Date: 2016/6/27.
 */
public class GsonTools {
    private static final String TAG = "GsonTools";

    public GsonTools() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param <T>
     * @param jsonString
     * @param cls
     * @return
     */
    public static <T> T getClassItem(String jsonString, Class<T> cls) {
        T t = null;
        try {
            Gson gson = new Gson();
            //Gson gson  = new GsonBuilder().registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory()).create();
            t = gson.fromJson(jsonString, cls);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return t;
    }

    /**
     * @param <T>
     * @param jsonString
     * @param cls
     * @return
     */
    public static <T> T ReGetClassItem(String jsonString, Class<T> cls) {
        T t = null;
        try {
            String responce = jsonString.substring(jsonString.indexOf("{", 1), jsonString.lastIndexOf("}"));//截取
            LogUtils.d(TAG, responce);
            Gson gson = new Gson();
            t = gson.fromJson(responce, cls);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return t;
    }

    /**
     * 使用Gson进行解析 List<person>
     *
     * @param <T>
     * @param json
     * @param clazz
     * @return
     */
    public static <T> List<T> getClassItemList(String json, Class<T> clazz) throws Exception {
        List<T> lst = new ArrayList<T>();
        JsonArray array = new JsonParser().parse(json).getAsJsonArray();
        for (final JsonElement elem : array) {
            Gson gson = new Gson();
            //Gson gson  = new GsonBuilder().registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory()).create();
            lst.add(gson.fromJson(elem, clazz));
            //lst.add(new Gson().fromJson(elem, clazz));
        }
        return lst;
    }

    /**
     * @param jsonString
     * @return
     */
    public static List<String> getStringItemList(String jsonString) {
        List<String> list = new ArrayList<String>();
        try {
            Gson gson = new Gson();
            //Gson gson  = new GsonBuilder().registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory()).create();
            list = gson.fromJson(jsonString, new TypeToken<List<String>>() {
            }.getType());
        } catch (Exception e) {
            // TODO: handle exception
        }
        return list;
    }

    public static List<Map<String, Object>> getListKeyMaps(String jsonString) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        try {
            Gson gson = new Gson();
            //Gson gson  = new GsonBuilder().registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory()).create();
            list = gson.fromJson(jsonString,
                    new TypeToken<List<Map<String, Object>>>() {
                    }.getType());
        } catch (Exception e) {
            // TODO: handle exception
        }
        return list;
    }

    public static String listToJsonString(List<?> list) {
        String s2 = "";
        try {
            Gson gson = new Gson();
            //Gson gson  = new GsonBuilder().registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory()).create();
            s2 = gson.toJson(list);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return s2;
    }

    public static String objectToJson(Object object) {
        String s2 = "";
        try {
            Gson gson = new Gson();
            //Gson gson  = new GsonBuilder().registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory()).create();
            s2 = gson.toJson(object);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return s2;
    }


}
