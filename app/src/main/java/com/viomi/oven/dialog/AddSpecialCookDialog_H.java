package com.viomi.oven.dialog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.oven.R;
import com.viomi.oven.activity.ModeInstructionsActivity;
import com.viomi.oven.adapter.TubatuAdapter;
import com.viomi.oven.bean.ModelWayItem;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.view.ClipViewPager;
import com.viomi.oven.view.ScalePageTransformer;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ljh on 2018/4/3
 * 专业模式参数
 */
public class AddSpecialCookDialog_H extends BaseDialog implements View.OnClickListener {
    private static final String TAG = "AddSpecialCookDialog_H";
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgBigMode)
    ImageView imgBigMode;
    @BindView(R.id.imgLineBg)
    ImageView imgLineBg;
    @BindView(R.id.imgMask)
    ImageView imgMask;
    @BindView(R.id.tvOk)
    TextView tvOk;
    @BindView(R.id.tvCurModeName)
    TextView tvCurModeName;
    @BindView(R.id.tvModeInfo)
    TextView tvModeInfo;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.viewpager)
    ClipViewPager viewpager;
    @BindView(R.id.rlContainer)
    RelativeLayout rlContainer;
    @BindView(R.id.rlContainer1)
    RelativeLayout rlContainer1;
    //参数
    Context context;
    private String flag = "";
    public boolean isAdd = false;
    public SteamBakeType curType = null;
    AddSpecialCookDialog_HCallBack callBack;
    //参数
    List<ModelWayItem> mModelWayItems = new ArrayList<>();
    private TubatuAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_special_cook_h);
        ButterKnife.bind(this);
        imgBack.setOnClickListener(this);
        tvOk.setOnClickListener(this);
        mModelWayItems.clear();
        mModelWayItems.add(new ModelWayItem(SteamBakeType.STEAM));
        mModelWayItems.add(new ModelWayItem(SteamBakeType.STEAM_BAKE_1));
        mModelWayItems.add(new ModelWayItem(SteamBakeType.STEAM_BAKE_2));
        mModelWayItems.add(new ModelWayItem(SteamBakeType.BAKE_1));
        mModelWayItems.add(new ModelWayItem(SteamBakeType.BAKE_2));
        mModelWayItems.add(new ModelWayItem(SteamBakeType.BAKE_3));
        mModelWayItems.add(new ModelWayItem(SteamBakeType.BAKE_4));
        mModelWayItems.add(new ModelWayItem(SteamBakeType.BAKE_5));
        mModelWayItems.add(new ModelWayItem(SteamBakeType.BAKE_6));
        mModelWayItems.add(new ModelWayItem(SteamBakeType.BAKE_7));
        //
        rlContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return viewpager.dispatchTouchEvent(event);
            }
        });
        viewpager.setDisAbleOtherClick(true);
        viewpager.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                LogUtils.d(TAG,"viewpager onLongClick");
                context.startActivity(new Intent(context, ModeInstructionsActivity.class));
                return false;
            }
        });
        viewpager.setPageTransformer(true, new ScalePageTransformer());
        viewpager.setPageMargin(0);
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                LogUtils.d(TAG, " onPageScrolled position is:" + position + " positionOffset is:" + positionOffset + " positionOffsetPixels is:" + positionOffsetPixels);
                imgMask.setVisibility(positionOffsetPixels == 0?View.VISIBLE:View.INVISIBLE);
            }

            @Override
            public void onPageSelected(int position) {
                LogUtils.d(TAG, "the onPageSelected is:" + position);
                initShow(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                LogUtils.d(TAG, " onPageScrollStateChanged is:" + state);
                //imgMask.setVisibility(View.VISIBLE);

            }
        });
        //
        mPagerAdapter = new TubatuAdapter(context);
        viewpager.setAdapter(mPagerAdapter);

        List<Integer> list = new ArrayList<>();
        for (ModelWayItem temp : mModelWayItems) {
            list.add(temp.getSteamBakeType().h_drawable);
        }
        //设置预加载OffscreenPageLimit
        viewpager.setOffscreenPageLimit(10);
        mPagerAdapter.addAll(list);
        viewpager.setCurrentItem(0);
    }

    public AddSpecialCookDialog_H(Context context) {
        super(context, R.style.Dialog_FS);
        this.context = context;
    }

    public void setSelectType(SteamBakeType type) {
        if (type == null) {
            tvTitle.setText("添加模式");
            isAdd = true;
            curType = SteamBakeType.STEAM;
        } else {
            tvTitle.setText("更换当前模式");
            isAdd = false;
            curType = type;
        }
        for (int i = 0; i < mModelWayItems.size(); i++) {
            if (curType == mModelWayItems.get(i).getSteamBakeType()) {
                viewpager.setCurrentItem(i);
                initShow(i);
                break;
            }
        }
    }

    public void setCallBack(AddSpecialCookDialog_HCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void dealTheMsg(Message msg) {

    }

    public void initShow(int position) {
        imgMask.setVisibility(View.VISIBLE);
        tvCurModeName.setText(mModelWayItems.get(position).getSteamBakeType().name);
        imgBigMode.setImageResource(mModelWayItems.get(position).getSteamBakeType().l_drawable);
        tvModeInfo.setText(mModelWayItems.get(position).getSteamBakeType().name + "模式     预设温度" + mModelWayItems.get(position).getSteamBakeType()
                .defTemp + "℃  " + mModelWayItems.get(position).getSteamBakeType().defTime + "分钟");
        curType = mModelWayItems.get(position).getSteamBakeType();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                dismiss();
                break;
            case R.id.tvOk:
                if (curType != null && callBack != null)
                    callBack.setCurType(isAdd, curType);
                dismiss();
                break;
            default:
                break;
        }
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public interface AddSpecialCookDialog_HCallBack {
        void setCurType(boolean isAdd, SteamBakeType curType);
    }
}
