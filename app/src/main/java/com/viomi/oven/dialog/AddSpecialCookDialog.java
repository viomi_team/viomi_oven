package com.viomi.oven.dialog;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.IdRes;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ljh on 2018/1/10
 * 专业模式参数
 */
public class AddSpecialCookDialog extends BaseDialog implements View.OnClickListener {
    private static final String TAG = "AddSpecialCookDialog";
    //UI
    Context context;
    @BindView(R.id.tvBack)
    TextView tvBack;
    @BindView(R.id.tvOk)
    TextView tvOk;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.rbModel1)
    RadioButton rbModel1;
    @BindView(R.id.rbModel2)
    RadioButton rbModel2;
    @BindView(R.id.rbModel3)
    RadioButton rbModel3;
    @BindView(R.id.rgModel)
    RadioGroup rgModel;
    //参数
    private String flag = "";
    public boolean isAdd = false;
    public SteamBakeType curType = null;
    AddSpecialCookDialogCallBack callBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_special_cook);
        ButterKnife.bind(this);
        tvBack.setOnClickListener(this);
        tvOk.setOnClickListener(this);

        rgModel.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.rbModel1) {
                    curType = SteamBakeType.STEAM;
                } else if (checkedId == R.id.rbModel2) {
                    curType = SteamBakeType.BAKE;
                } else if (checkedId == R.id.rbModel3) {
                    curType = SteamBakeType.STEAM_BAKE;
                    if(DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V2))
                        curType = SteamBakeType.STEAM_BAKE_1;
                } else curType = null;
            }
        });
    }

    public AddSpecialCookDialog(Context context) {
        super(context, R.style.Dialog_FS);
        this.context = context;
    }

    public void setSelectType(SteamBakeType type) {
        if (type == null) {
            tvTitle.setText("添加模式");
            rbModel2.setChecked(false);
            rbModel3.setChecked(false);
            rgModel.clearCheck();
            rbModel1.setChecked(true);
            isAdd = true;
            curType = SteamBakeType.STEAM;
        } else {
            tvTitle.setText("更换当前模式");
            rbModel1.setChecked(type == SteamBakeType.STEAM);
            rbModel2.setChecked(type == SteamBakeType.BAKE);
            rbModel3.setChecked(type == SteamBakeType.STEAM_BAKE);
            if(DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V2))
                rbModel3.setChecked(type == SteamBakeType.STEAM_BAKE_1);
            isAdd = false;
            curType = type;
        }
    }

    public void setCallBack(AddSpecialCookDialogCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void dealTheMsg(Message msg) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvBack:
                dismiss();
                break;
            case R.id.tvOk:
                if (curType == null)
                    ToastUtil.show("请选择一项");
                else if (callBack != null) {
                    callBack.setCurType(isAdd, curType);
                    dismiss();
                }
                break;
            default:
                break;
        }
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public interface AddSpecialCookDialogCallBack {
        void setCurType(boolean isAdd, SteamBakeType curType);
    }
}
