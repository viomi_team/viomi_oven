package com.viomi.oven.dialog;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.view.PickerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ljh on 2018/3/26
 * 选择待机时间
 */
public class SelectTimeDialog extends BaseDialog implements View.OnClickListener {
    private static final String TAG = "SelectTimeDialog";
    //UI
    @BindView(R.id.tvBtn1)
    TextView tvBtn1;
    @BindView(R.id.tvBtn2)
    TextView tvBtn2;
    @BindView(R.id.pvSelect)
    PickerView pvSelect;
    @BindView(R.id.root)
    LinearLayout root;
    //参数
    Context context;
    private int pos = 0;
    private String flag = "";
    SelectTimeDialogCallBack callBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            setContentView(R.layout.dialog_select_time);
        else setContentView(R.layout.dialog_select_time_h);
        ButterKnife.bind(this);
        tvBtn1.setOnClickListener(this);
        tvBtn2.setOnClickListener(this);
        //root.setOnClickListener(this);
        List<String> data = new ArrayList<String>();
        for (int i = 10; i <= 30; i++) {
            data.add(i + "");
        }

        pvSelect.setData(data);
        pvSelect.setOnSelectListener(new PickerView.onSelectListener() {

            @Override
            public void onSelect(String text) {
                flag = text;
                LogUtils.d(TAG, "onSelect :" + text);
            }
        });
    }

    public SelectTimeDialog(Context context) {
        super(context, R.style.Dialog_FS);
        this.context = context;
    }

    public SelectTimeDialog(Context context, SelectTimeDialogCallBack callBack) {
        super(context, R.style.Dialog_FS);
        this.context = context;
        this.callBack = callBack;
    }

    public void setCallBack(SelectTimeDialogCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void dealTheMsg(Message msg) {

    }

    public void show(int selectItem) {
        flag = selectItem + "";
        show();
        pvSelect.moveToSelectItem(flag);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvBtn1:
                dismiss();
                break;
            case R.id.tvBtn2:
                if (callBack != null) {
                    LogUtils.d(TAG, "tvBtn2 callBack flag=" + flag);
                    callBack.btnOk(Integer.parseInt(flag));
                }
                dismiss();
                break;
            case R.id.root:
                dismiss();
                break;
            default:
                break;
        }
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public interface SelectTimeDialogCallBack {
        void btnOk(int time);
    }
}
