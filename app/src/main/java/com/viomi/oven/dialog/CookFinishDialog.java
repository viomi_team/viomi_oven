package com.viomi.oven.dialog;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.TextView;

import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.bean.BasicKeyInfo;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.enumType.FromType;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.manager.DeviceManager;
import com.viomi.oven.manager.VoiceManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ljh on 2018/1/10
 * 完成烹饪
 */
public class CookFinishDialog extends BaseDialog implements View.OnClickListener {
    private static final String TAG = "CookFinishDialog";
    //UI
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tvEnergy)
    TextView tvEnergy;
    @BindView(R.id.tvBack)
    TextView tvBack;
    //参数
    Context context;
    private int pos = 0;
    private String flag = "";
    SteamBakeDialogCallBack callBack;
    List<BasicKeyInfo> mBasicKeyInfos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_cook_finish);
        ButterKnife.bind(this);
        tvBack.setOnClickListener(this);
    }

    public CookFinishDialog(Context context) {
        super(context, R.style.Dialog_FS);
        this.context = context;
    }

    public void setCallBack(SteamBakeDialogCallBack callBack) {
        this.callBack = callBack;
    }

    public void setState(String info, boolean haveOldSignId) {
        //mTv2.setText(info);
        //mTvBtn0.setVisibility(haveOldSignId ? View.GONE : View.VISIBLE);
    }

    @Override
    public void dealTheMsg(Message msg) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvBack:
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void show() {
        super.show();
        HawkParams.setWorkStatus(WorkStatus.STATUS_FINISH);
        HawkParams.setCookStep(new ArrayList<DeviceRunStep>());//清空步骤
        HawkParams.setS_temp(0);
        if (HawkParams.getMode() == SteamBakeType.STEAM.value
                || HawkParams.getMode() == SteamBakeType.BAKE.value
                || HawkParams.getMode() == SteamBakeType.STEAM_BAKE.value) {
            //tvFinishInfo.setText("烹饪完成");
            VoiceManager.getInstance().startSpeak("上菜啦，上菜啦，上菜啦，重要的事情说三遍，上菜啦");
        } else {
            for (SteamBakeType temp : SteamBakeType.values()) {
                if (temp.value == HawkParams.getMode()) {
                    //tvFinishInfo.setText(temp.name + "完成");
                    VoiceManager.getInstance().startSpeak(temp.name + "完成");
                }
            }
        }

        mBasicKeyInfos = HawkParams.getNutritionInfos();
        if (HawkParams.getCookFrom() == FromType.FROM_RECIPE.value && mBasicKeyInfos != null && mBasicKeyInfos.size() > 0) {//显示营养信息
            //llNutritionInfo.setVisibility(View.VISIBLE);
            //for (int i = 0; i < mBasicKeyInfos.size(); i++) {
            //    LinearLayout ll_item = (LinearLayout) ((Activity) mContext).getLayoutInflater()
            //            .inflate(R.layout.item_nutrition_layout, null, false);
            //    ((TextView) ll_item.findViewById(R.id.tvValue)).setText(mBasicKeyInfos.get(i).getValue());
            //    ((TextView) ll_item.findViewById(R.id.tvUnit)).setText(mBasicKeyInfos.get(i).getUnit());
            //    ((TextView) ll_item.findViewById(R.id.tvName)).setText(mBasicKeyInfos.get(i).getName());
            //    llNutritionInfo.addView(ll_item);
            //    if (i % 4 == 0 || i % 4 == 3) {
            //        ll_item.findViewById(R.id.llContent).setBackgroundResource(R.drawable.nutrition_bg1);
            //    } else if (i % 4 == 1) {
            //        ll_item.findViewById(R.id.llContent).setBackgroundResource(R.drawable.nutrition_bg2);
            //    } else if (i % 4 == 2) {
            //        ll_item.findViewById(R.id.llContent).setBackgroundResource(R.drawable.nutrition_bg3);
            //    }
            //}
            DeviceManager.getInstance().sendOnceRecord(HawkParams.getCookeStartTime()/1000, HawkParams.getFoodName(), HawkParams.getC_totletime(), HawkParams
                    .getNutritionInfos2String(), HawkParams.getC_temps(),HawkParams.getWeight());
        }
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public interface SteamBakeDialogCallBack {
        void useSign();

        void goSign();
    }
}
