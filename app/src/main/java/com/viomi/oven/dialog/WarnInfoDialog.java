package com.viomi.oven.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.activity.ChugouActivity;
import com.viomi.oven.activity.VersionManagerActivity;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.manager.VoiceManager;
import com.viomi.oven.util.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ljh on 2018/1/10
 * 完成烹饪
 */
public class WarnInfoDialog extends BaseDialog implements View.OnClickListener {
    private static final String TAG = "WarnInfoDialog";
    //UI
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvInfo)
    TextView tvInfo;
    @BindView(R.id.tvOk)
    TextView tvOk;
    @BindView(R.id.tvCancle)
    TextView tvCancle;
    @BindView(R.id.root)
    RelativeLayout root;
    @BindView(R.id.v1)
    View v1;
    //参数
    Context context;
    private int pos = 0;
    private String flag = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            setContentView(R.layout.dialog_warn_info_v);
        else
            setContentView(R.layout.dialog_warn_info);
        ButterKnife.bind(this);
        root.setOnClickListener(this);
        tvOk.setOnClickListener(this);
        tvCancle.setOnClickListener(this);
    }

    public WarnInfoDialog(Context context) {
        super(context, R.style.Dialog_FS);
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.root:
                if (tvOk.getVisibility() == View.GONE)
                    dismiss();
                else if (tvOk.getText().toString().equals("打开面板") || tvOk.getText().toString().equals("开始除垢"))
                    dismiss();
                break;
            case R.id.tvOk:
                if (tvOk.getText().toString().equals("确定"))
                    context.startActivity(new Intent(context, VersionManagerActivity.class));
                else if (tvOk.getText().toString().equals("打开面板")) {
                    if (SerialManager.getInstance().sendCmdControlPanelSwitch(true))
                        dismiss();
                } else if (tvOk.getText().toString().equals("开始除垢")) {
                    //context.startActivity(new Intent(context, ChugouActivity.class));

                    List<DeviceRunStep> mDeviceRunSteps = new ArrayList<>();
                    mDeviceRunSteps.add(new DeviceRunStep(SteamBakeType.CHUGOU));
                    if (Global.isPrepareOk(mDeviceRunSteps)) {
                        if (SerialManager.getInstance().sendCmdStart(mDeviceRunSteps)) {
                            VoiceManager.getInstance().startSpeak("开始除垢");
                            Intent intent = new Intent(context,ChugouActivity.class);
                            intent.putExtra("step",1);
                            context.startActivity(intent);
                        }
                    }
                }
                break;
            case R.id.tvCancle:
                HawkParams.setEvaporatorRunningTime(0);
                dismiss();
                break;
            default:
                break;
        }
    }

    public void showInfo(String msg) {
        if(((Activity)context).isFinishing())
            return;
        show();
        tvTitle.setText("警报");
        tvInfo.setText(msg);
        if (msg.contains("水箱水量不足")) {
            tvOk.setText("打开面板");
            tvOk.setVisibility(View.VISIBLE);
            v1.setVisibility(View.GONE);
        } else if (msg.contains("除垢")) {
            tvOk.setText("开始除垢");
            tvOk.setVisibility(View.VISIBLE);
            tvCancle.setVisibility(View.VISIBLE);
            v1.setVisibility(View.GONE);
            tvTitle.setText("提醒");
        } else {
            tvOk.setVisibility(View.GONE);
            v1.setVisibility(View.VISIBLE);
        }
    }

    public void showUpgrade() {
        if(((Activity)context).isFinishing())
            return;
        show();
        tvInfo.setText("蒸烤箱处于升级状态，请进入设置页点击固件版本完成固件升级");
        tvOk.setVisibility(View.VISIBLE);
        v1.setVisibility(View.GONE);
        tvOk.setText("确定");
    }

    @Override
    public void dealTheMsg(Message msg) {

    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
