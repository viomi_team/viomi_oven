package com.viomi.oven.dialog;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.oven.R;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.view.NumberPickerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ljh on 2018/3/26
 * 选择待机时间
 */
public class NumberPickerDialog extends BaseDialog implements View.OnClickListener {
    private static final String TAG = "NumberPickerDialog";
    //UI
    @BindView(R.id.tvBtn1)
    TextView tvBtn1;
    @BindView(R.id.tvBtn2)
    TextView tvBtn2;
    @BindView(R.id.tvUnit)
    TextView tvUnit;
    @BindView(R.id.picker)
    NumberPickerView picker;
    @BindView(R.id.root)
    LinearLayout root;
    //参数
    Context context;
    private int pos = 0;
    private String flag = "";
    String[] toShow;
    public boolean isChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_number_picker);
        ButterKnife.bind(this);
        tvBtn1.setOnClickListener(this);
        tvBtn2.setOnClickListener(this);

        List<String> data = new ArrayList<String>();
        for (int i = 1; i <= 180; i++) {
            data.add(i + "");
        }
        //toShow = new String[data.size()];
        //data.toArray(toShow);
        //
        picker.setOnValueChangedListener(new NumberPickerView.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPickerView picker, int oldVal, int newVal) {
                LogUtils.d(TAG, "the oldVal = " + oldVal + " the newVal = " + newVal);
                pos = newVal;
            }
        });
        picker.setFriction(2 * ViewConfiguration.get(picker.getContext()).getScrollFriction());//设置滑动阻力
        //picker.refreshByNewDisplayedValues(toShow);
    }

    public void update(List<String> data) {
        toShow = new String[data.size()];
        data.toArray(toShow);
    }

    public NumberPickerDialog(Context context) {
        super(context, R.style.Dialog_FS);
        this.context = context;
    }

    @Override
    public void dealTheMsg(Message msg) {

    }

    public void show(String selectItem, String unit) {
        for (int i = 0; i < toShow.length; i++) {
            if (selectItem.equals(toShow[i])) {
                pos = i;
                break;
            }
        }
        show();
        tvUnit.setText(unit);
        picker.refreshByNewDisplayedValues(toShow);
        picker.setValue(pos);
        isChanged = false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvBtn1:
                isChanged = false;
                dismiss();
                break;
            case R.id.tvBtn2:
                isChanged = true;
                dismiss();
                break;
            case R.id.root:
                dismiss();
                break;
            default:
                break;
        }
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

}
