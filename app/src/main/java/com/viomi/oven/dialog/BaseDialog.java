package com.viomi.oven.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.view.Window;

/**
 * Created by Ljh on 2016/6/28.
 */
public abstract class BaseDialog extends Dialog {

    private Loading loading;

    public BaseDialog(Context context) {
        super(context);
        //去掉系统对话框的title和边框背景
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading = new Loading(context);
        loading.setCancelable(true);
    }

    public BaseDialog(Context context, int style) {
        super(context, style);
        //去掉系统对话框的title和边框背景
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading = new Loading(context);
        loading.setCancelable(true);
    }

    protected void showLoading() {
        loading.setCancelable(true);
        if (!loading.isShowing())
            loading.show();
    }

    protected void showUnCancelLoading() {
        loading.setCancelable(false);
        if (!loading.isShowing())
            loading.show();
    }

    protected void hideLoading() {
        if (loading.isShowing())
            loading.cancel();
    }

    public boolean getLoadingStatus() {
        return loading.isShowing();
    }

    public void sendToHandler(int what, String msg) {
        Message message = handler.obtainMessage();
        message.obj = msg;
        message.what = what;
        handler.sendMessage(message);
    }

    /**
     * *处理消息循环
     **/
    public abstract void dealTheMsg(Message msg);

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            dealTheMsg(msg);
        }
    };
}
