package com.viomi.oven.dialog;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.enumType.DownFileType;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.interfaces.ProgressCallback;
import com.viomi.oven.manager.UpgradeManager;
import com.viomi.oven.otto.BusProvider;
import com.viomi.oven.otto.EventMcuInstall;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ljh on 2018/3/26
 * 升级弹窗
 */
public class UpLoadDialog extends BaseDialog implements View.OnClickListener {
    private static final String TAG = "UpLoadDialog";
    //UI
    @BindView(R.id.tvNewVer)
    TextView tvNewVer;
    @BindView(R.id.tvVerInfo)
    TextView tvVerInfo;
    @BindView(R.id.tvDown)
    TextView tvDown;
    @BindView(R.id.llDownInfo)
    LinearLayout llDownInfo;
    @BindView(R.id.llDowning)
    LinearLayout llDowning;
    @BindView(R.id.tvDownStatus)
    TextView tvDownStatus;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.imgClose)
    ImageView imgClose;
    //参数
    Context context;
    private int pos = -1;
    private String flag = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_upload);
        ButterKnife.bind(this);
        imgClose.setOnClickListener(this);
        tvDown.setOnClickListener(this);
    }

    public UpLoadDialog(Context context) {
        super(context, R.style.Dialog_FS);
        this.context = context;
    }


    @Override
    public void dealTheMsg(Message msg) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgClose:
                dismiss();
                break;
            case R.id.tvDown:
                llDownInfo.setVisibility(View.GONE);
                imgClose.setVisibility(View.GONE);
                llDowning.setVisibility(View.VISIBLE);
                if (flag.equals(DownFileType.APP.valueString)) {//下载APP
                    UpgradeManager.getInstance().downLoadApp(DownFileType.APP, new ProgressCallback() {
                        @Override
                        public void onProgress(int progress) {
                            progressBar.setProgress(progress);
                            LogUtils.d(TAG, "upgradeApp onProgress is:" + progress);
                        }

                        @Override
                        public void onSuccess(Object data) {
                            LogUtils.d(TAG, "upgradeApp onSuccess");
                            tvDownStatus.setText("正在安装");
                            dismiss();
                        }

                        @Override
                        public void onFail(int errorCode, String msg) {
                            LogUtils.d(TAG, "upgradeApp onFail");
                            ToastUtil.show("升级失败！");
                            dismiss();
                        }
                    });
                } else if (flag.equals(DownFileType.MCU.valueString)) {//下载MCU
                    UpgradeManager.getInstance().downLoadApp(DownFileType.MCU, new ProgressCallback() {
                        @Override
                        public void onProgress(int progress) {
                            progressBar.setProgress(progress);
                            LogUtils.d(TAG, "upgradeMcu onProgress is:" + progress);
                        }

                        @Override
                        public void onSuccess(Object data) {
                            tvDownStatus.setText("正在安装");
                            progressBar.setProgress(0);
                            LogUtils.d(TAG, "upgradeMcu onSuccess");
                        }

                        @Override
                        public void onFail(int errorCode, String msg) {
                            LogUtils.d(TAG, "upgradeMcu onFail");
                            BusProvider.getInstance().postEvent(new EventMcuInstall(TAG, 2));
                        }
                    });
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void show() {
        HawkParams.setWorkStatus(WorkStatus.STATUS_UPGRADE);
        super.show();
        if (flag.equals(DownFileType.APP.valueString)) {
            tvNewVer.setText("检测到最新APP版本v" + HawkParams.getOnlineAppVer().getCode());
            tvVerInfo.setText(HawkParams.getOnlineAppVer().getDetail());
        } else {
            tvNewVer.setText("检测到最新固件版本" + Global.verString(HawkParams.getOnlineMcuVer().getCode()));
            tvVerInfo.setText(HawkParams.getOnlineMcuVer().getDetail());
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        HawkParams.setWorkStatus(WorkStatus.STATUS_IDLE);
    }

    public void updateProgress(int progress) {
        this.pos = progress;
        progressBar.setProgress(pos);
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
