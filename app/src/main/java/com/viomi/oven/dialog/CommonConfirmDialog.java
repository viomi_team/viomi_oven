package com.viomi.oven.dialog;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ljh on 2018/1/10
 * 再次确认
 */
public class CommonConfirmDialog extends BaseDialog implements View.OnClickListener {
    private static final String TAG = "CommonConfirmDialog";
    //UI
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvBtn1)
    TextView tvBtn1;
    @BindView(R.id.tvBtn2)
    TextView tvBtn2;
    //参数
    Context context;
    private int pos = 0;
    private String flag = "";
    String title = "是否确认删除该模式？";
    CommonConfirmDialogCallBack callBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            setContentView(R.layout.dialog_common_confirm1);
        else
            setContentView(R.layout.dialog_common_confirm);
        ButterKnife.bind(this);
        tvBtn1.setOnClickListener(this);
        tvBtn2.setOnClickListener(this);
    }

    public CommonConfirmDialog(Context context) {
        super(context, R.style.Dialog_FS);
        this.context = context;
    }

    public void setCallBack(CommonConfirmDialogCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void dealTheMsg(Message msg) {

    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvBtn1:
                dismiss();
                break;
            case R.id.tvBtn2:
                if (callBack != null)
                    callBack.btnOk();
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void show() {
        super.show();
        tvBtn2.setSelected(true);
        tvTitle.setText(title);
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public interface CommonConfirmDialogCallBack {
        void btnOk();
    }
}
