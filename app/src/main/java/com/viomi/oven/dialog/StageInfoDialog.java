package com.viomi.oven.dialog;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.oven.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ljh on 2018/3/1
 * 再次确认
 */
public class StageInfoDialog extends BaseDialog implements View.OnClickListener {
    private static final String TAG = "StageInfoDialog";
    //UI
    @BindView(R.id.tvMode)
    TextView tvMode;
    @BindView(R.id.tvTemp)
    TextView tvTemp;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tvStage)
    TextView tvStage;
    @BindView(R.id.llContent)
    LinearLayout llContent;
    @BindView(R.id.root)
    RelativeLayout root;
    //参数
    Context context;
    private int pos = 0;
    private String flag = "";
    String mode = "蒸汽模式";
    String temp = "";
    String time = "";
    int stage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_stage_info);
        ButterKnife.bind(this);
        llContent.setOnClickListener(this);
        root.setOnClickListener(this);
    }

    public StageInfoDialog(Context context) {
        super(context, R.style.Dialog_FS);
        this.context = context;
    }

    @Override
    public void dealTheMsg(Message msg) {

    }

    public void setInfo(String mode, String time, String temp, int stage) {
        this.mode = mode;
        this.time = time;
        this.temp = temp;
        this.stage = stage;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llContent:
                break;
            case R.id.root:
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void show() {
        super.show();
        tvMode.setText(mode + "模式");
        tvTemp.setText(temp);
        tvTime.setText(time);
        if (stage == 0)
            tvStage.setText("第一阶段");
        else if (stage == 1)
            tvStage.setText("第二阶段");
        else if (stage == 2)
            tvStage.setText("第三阶段");
        else if (stage == 3)
            tvStage.setText("第四阶段");
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
