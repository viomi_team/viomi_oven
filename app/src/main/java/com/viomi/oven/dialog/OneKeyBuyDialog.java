package com.viomi.oven.dialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mylhyl.zxing.scanner.encode.QREncode;
import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.util.LogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ljh on 2018/6/4
 * 扫码购
 */
public class OneKeyBuyDialog extends BaseDialog implements View.OnClickListener {
    private static final String TAG = "OneKeyBuyDialog";
    //UI
    @BindView(R.id.root)
    RelativeLayout root;
    @BindView(R.id.imgBuyQr)
    ImageView imgBuyQr;
    @BindView(R.id.imgClose)
    ImageView imgClose;
    @BindView(R.id.llContent)
    LinearLayout llContent;
    //参数
    Context context;
    private int pos = 0;
    private String flag = "";//做SkuId用

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            setContentView(R.layout.dialog_one_key_buy_v);
        else
            setContentView(R.layout.dialog_one_key_buy);
        ButterKnife.bind(this);
        root.setOnClickListener(this);
        llContent.setOnClickListener(this);
        imgClose.setOnClickListener(this);
    }

    public OneKeyBuyDialog(Context context) {
        super(context, R.style.Dialog_FS);
        this.context = context;
    }

    @Override
    public void dealTheMsg(Message msg) {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.root:
                dismiss();
                break;
            case R.id.imgClose:
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void show() {
        super.show();
        LogUtils.d(TAG, "the skuUrl = " + AppStaticConfig.getSkuIdUrl(flag));
        Bitmap bitmap = new QREncode.Builder(context)
                .setColor(context.getResources().getColor(R.color.black))//二维码颜色
                //.setParsedResultType(ParsedResultType.TEXT)//默认是TEXT类型
                .setContents(AppStaticConfig.getSkuIdUrl(flag))//二维码内容
                .build().encodeAsBitmap();
        imgBuyQr.setImageBitmap(bitmap);
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public interface CommonConfirmDialogCallBack {
        void btnOk();
    }
}
