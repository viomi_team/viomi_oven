package com.viomi.oven.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.viomi.oven.R;

public class Loading extends Dialog implements View.OnClickListener {
    private Context mContext;
    private LayoutInflater inflater;
    private LayoutParams lp;
    private RelativeLayout root;
    private ImageView imgLoading;
    boolean mCancelable = true;

    public Loading(Context context) {
        super(context, R.style.Dialog);
        this.mContext = context;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.loading1, null);
        root = (RelativeLayout) layout.findViewById(R.id.root);
        root.setOnClickListener(this);
        imgLoading = (ImageView) layout.findViewById(R.id.imgLoading);
        setContentView(layout);
        lp = getWindow().getAttributes();
        lp.gravity = Gravity.CENTER;
        lp.dimAmount = 0;
        lp.alpha = 1.0f;
        getWindow().setAttributes(lp);
        getWindow().setLayout(android.view.ViewGroup.LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.MATCH_PARENT);
        this.setCancelable(false);
    }

    @Override
    public void setCancelable(boolean flag) {
        mCancelable = flag;
        super.setCancelable(flag);
    }

    @Override
    public void onClick(View v) {
        if (mCancelable)
            this.cancel();
    }

    @Override
    public void show() {
        super.show();
        Glide.with(imgLoading.getContext()).load(R.drawable.loading_g) //去掉显示动画
                .into(imgLoading);
    }
}