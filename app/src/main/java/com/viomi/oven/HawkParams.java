package com.viomi.oven;

import com.orhanobut.hawk.Hawk;
import com.viomi.oven.bean.AppUpgradeMsg;
import com.viomi.oven.bean.BasicKeyInfo;
import com.viomi.oven.bean.FoodDetail;
import com.viomi.oven.bean.ModesBean;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.enumType.ModelType;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.util.AppSharePreferences;
import com.viomi.oven.util.GsonTools;
import com.viomi.oven.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ljh on 2017/12/20.
 */

public class HawkParams {
    private static final String TAG = "HawkParams";

    //////////////////---------------------------------------------------------------------------------------------------------------------------
    //保存烹饪步骤(同时保存总烹饪时间)
    public static boolean setCookStep(List<DeviceRunStep> steps) {
        int timeLengthMin = 0;
        for (DeviceRunStep temp : steps) {
            timeLengthMin += temp.getTime();
        }
        setC_totletime(timeLengthMin * 60);
        //LogUtils.d(TAG,"setCookStep:"+GsonTools.listToJsonString(steps));
        return Hawk.put("RUN_STEP", steps);
    }

    public static List<DeviceRunStep> getCookStep() {
        return Hawk.get("RUN_STEP");
    }

    //烹饪步骤JSON
    public static String getCookModesBeanString() {
        List<ModesBean> beans = new ArrayList<>();
        List<DeviceRunStep> steps = getCookStep();
        if (steps != null && steps.size() > 0) {
            for (DeviceRunStep step : steps) {
                beans.add(new ModesBean(step.getMode(), step.getTime(), step.getTemp()));
            }
        }
        LogUtils.d(TAG, "getCookModesBeanString = " + beans.toString());
        return GsonTools.objectToJson(beans);
    }

    public static boolean delCookStep() {
        return Hawk.delete("RUN_STEP");
    }

    //保存最新的MCU版本,0b0xxxxxxx为台面式版本号0b1xxxxxxx为嵌入式版本号
    public static boolean setMcuVer(int mcuVer) {
        return AppSharePreferences.setValue("mcuVer", mcuVer);
    }

    public static int getMcuVer() {
        return AppSharePreferences.getInt("mcuVer", 0);
    }

    public static boolean setOnlineMcuVer(AppUpgradeMsg steps) {
        return Hawk.put("ONLINE_MCU", steps);
    }

    public static AppUpgradeMsg getOnlineMcuVer() {
        return Hawk.get("ONLINE_MCU");
    }

    //保存线上的APP版本
    public static boolean setAppVer(int appVer) {
        return AppSharePreferences.setValue("appVer", appVer);
    }

    public static int getAppVer() {
        return AppSharePreferences.getInt("appVer", 0);
    }

    public static boolean setOnlineAppVer(AppUpgradeMsg steps) {
        return Hawk.put("ONLINE_APP", steps);
    }

    public static AppUpgradeMsg getOnlineAppVer() {
        return Hawk.get("ONLINE_APP");
    }

    //菜谱版本(我的菜谱)
    public static boolean setOnLineMySetDishVer(int dishsVer) {
        return AppSharePreferences.setValue("ONLINE_DISHVER", dishsVer);
    }

    public static int getOnLineMySetDishVer() {
        return AppSharePreferences.getInt("ONLINE_DISHVER", 0);
    }

    public static boolean setOffLineMySetDishVer(int dishsVer) {
        return AppSharePreferences.setValue("OFFLINE_DISHVER", dishsVer);
    }

    public static int getOffLineMySetDishVer() {
        return AppSharePreferences.getInt("OFFLINE_DISHVER", 0);
    }

    //智能菜谱
    public static boolean setSmartFoodList(List<FoodDetail> foodInfos) {
        return Hawk.put("SmartFoodList", foodInfos);
    }

    public static List<FoodDetail> getSmartFoodList() {
        return Hawk.get("SmartFoodList", new ArrayList<FoodDetail>());
    }

    //我的菜谱
    public static boolean setMySetFoodList(List<FoodDetail> foodInfos) {
        return Hawk.put("MySetFoodList", foodInfos);
    }

    public static List<FoodDetail> getMySetFoodList() {
        return Hawk.get("MySetFoodList", new ArrayList<FoodDetail>());
    }

    //从菜谱列表选中的菜谱
    public static boolean setSelectFoodPos(int pos) {
        return AppSharePreferences.setValue("curSelectFoodPos", pos);
    }

    public static int getSelectFoodPos() {
        return AppSharePreferences.getInt("curSelectFoodPos", 0);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //当前模式
    public static boolean setMode(int mode) {
        LogUtils.d(TAG, "setMode=" + mode);
        return AppSharePreferences.setValue("MODE", mode);
    }

    public static boolean setMode(SteamBakeType type) {
        return AppSharePreferences.setValue("MODE", type.value);
    }

    public static int getMode() {
        return AppSharePreferences.getInt("MODE", SteamBakeType.NO_MODE.value);
    }

    //工作状态
    public static boolean setWorkStatus(WorkStatus status) {
        return AppSharePreferences.setValue("WORK_STATUS", status.value);
    }

    public static int getWorkStatus() {
        return AppSharePreferences.getInt("WORK_STATUS", WorkStatus.STATUS_IDLE.value);
    }

    //暂停状态   0:正常，1:暂停
    public static boolean setPauseStatus(int pauseStatus) {
        return AppSharePreferences.setValue("PAUSE_STATUS", pauseStatus);
    }

    public static int getPauseStatus() {
        return AppSharePreferences.getInt("PAUSE_STATUS", 0);
    }

    //来源  0:菜谱、1:专业烹饪、2:其他模式
    public static boolean setCookFrom(int from) {
        return AppSharePreferences.setValue("from", from);
    }

    public static int getCookFrom() {
        return AppSharePreferences.getInt("from", 0);
    }

    //菜品ID(现在只在获取菜谱详情的语音提示时使用)
    public static boolean setFoodId(String foodId) {
        return AppSharePreferences.setValue("foodId", foodId);
    }

    public static String getFoodId() {
        return AppSharePreferences.getString("foodId", "");
    }

    //烹饪菜名
    public static boolean setFoodName(String foodName) {
        return AppSharePreferences.setValue("foodName", foodName);
    }

    public static String getFoodName() {
        return AppSharePreferences.getString("foodName", "");
    }

    //营养信息
    //public static boolean setDishNutrinfo(String nutrition) {
    //    return AppSharePreferences.setValue("nutrition", nutrition);
    //}

    public static String getNutritionInfos2String() {
        List<BasicKeyInfo> mInfos = getNutritionInfos();
        if (mInfos == null || mInfos.size() == 0)
            return "";
        else {
            return "{\"info\":" + GsonTools.listToJsonString(mInfos) + "}";
        }
    }

    public static boolean setNutritionInfos(List<BasicKeyInfo> mNutritionInfos) {
        return Hawk.put("mNutritionInfos", mNutritionInfos);
    }

    public static List<BasicKeyInfo> getNutritionInfos() {
        return Hawk.get("mNutritionInfos");
    }

    //预约时间,时间戳,秒级别
    public static boolean setAppointTime(int appointTime) {
        LogUtils.d(TAG, "setAppointTime:" + appointTime);
        return AppSharePreferences.setValue("appointTime", appointTime);
    }

    public static int getAppointTime() {
        return AppSharePreferences.getInt("appointTime", 0);
    }

    //第一次扫码烹饪时间
    public static boolean setFirstScanTime(int firstScan) {
        LogUtils.d(TAG, "setFirstScanTime:" + firstScan);
        return AppSharePreferences.setValue("firstScan", firstScan);
    }

    public static int getFirstScanTime() {
        return AppSharePreferences.getInt("firstScan", 0);
    }

    //温度曲线采集点
    public static boolean addC_temps(String add_c_temps) {
        String curC_temps = AppSharePreferences.getString("c_temps", "");
        return AppSharePreferences.setValue("c_temps", curC_temps + "-" + add_c_temps);
    }

    public static boolean setC_temps(String c_temps) {
        return AppSharePreferences.setValue("c_temps", c_temps);
    }

    public static String getC_temps() {
        return AppSharePreferences.getString("c_temps", "");
    }

    //当前(上次)已烹饪时间,单位s
    public static boolean setC_time(int c_time) {
        return AppSharePreferences.setValue("c_time", c_time);
    }

    public static int getC_time() {
        return AppSharePreferences.getInt("c_time", 0);
    }

    //当前(上次)总烹饪时间,单位s
    public static boolean setC_totletime(int c_totletime) {
        return AppSharePreferences.setValue("c_totletime", c_totletime);
    }

    public static int getC_totletime() {
        return AppSharePreferences.getInt("c_totletime", 0);
    }

    //当前(上次)温度
    public static boolean setC_temp(int c_temp) {
        return AppSharePreferences.setValue("c_temp", c_temp);
    }

    public static int getC_temp() {
        return AppSharePreferences.getInt("c_temp", 0);
    }

    //当前工作模式设置的温度
    public static boolean setS_temp(int s_temp) {
        return AppSharePreferences.setValue("s_temp", s_temp);
    }

    public static int getS_temp() {
        return AppSharePreferences.getInt("s_temp", 0);
    }

    //语音开关
    public static boolean setVoiceOpenStatus(boolean status) {
        return AppSharePreferences.setValue("VOICE_OPEN", status);
    }

    public static boolean getVoiceOpenStatus() {
        return AppSharePreferences.getBoolean("VOICE_OPEN", true);
    }

    //锁屏开关
    public static boolean setLockScreenStatuc(boolean status) {
        return AppSharePreferences.setValue("LockScreen", status);
    }

    public static boolean getLockScreenStatus() {
        return AppSharePreferences.getBoolean("LockScreen", false);
    }

    //预约的菜谱信息
    public static boolean setBespeakdish(String Bespeakdish) {
        return AppSharePreferences.setValue("BespeakdishBean", Bespeakdish);
    }

    public static String getBespeakdish() {
        return AppSharePreferences.getString("BespeakdishBean", "");
    }

    //扫码登录的手机类型
    public static boolean setAndroidScanLogin(boolean isAndroid) {
        return AppSharePreferences.setValue("androidScanLogin", isAndroid);
    }

    public static boolean getAndroidScanLogin() {
        return AppSharePreferences.getBoolean("androidScanLogin", true);
    }

    //屏保弹出时间,单位s
    public static boolean setScreenTime(int screenTime) {
        return AppSharePreferences.setValue("screenTime", screenTime);
    }

    public static int getScreenTime() {
        //return 60;
        return AppSharePreferences.getInt("screenTime", 60);
    }

    // 扫描登录手机类型，0：android；1：ios
    public static boolean setScanPhoneType(int type) {
        return AppSharePreferences.setValue("Key_Scan_Phone_Type", type);
    }

    public static int getScanPhoneType() {
        return AppSharePreferences.getInt("Key_Scan_Phone_Type", 0);
    }

    //App启动时间
    public static void setAppStartTime(long startTimie) {//单位：秒
        AppSharePreferences.setValue("Key_App_Start_Time", startTimie);
    }

    public static long getAppStartTime() {
        return AppSharePreferences.getLong("Key_App_Start_Time", 0);
    }

    //第一次启动app或登录云米帐号时间
    public static boolean setViomiLoginTime(long time) {
        return AppSharePreferences.setValue("Key_Viomi_Login_Time", time);
    }

    public static long getViomiLoginTime() {
        return AppSharePreferences.getLong("Key_Viomi_Login_Time", 0);
    }

    //选择的入口模式
    public static void setModelType(ModelType type) {
        AppSharePreferences.setValue("ModelType", type.value);
    }

    public static int getModelType() {
        return AppSharePreferences.getInt("ModelType");
    }

    //UserId
    public static boolean setUserId(int userId) {
        return AppSharePreferences.setValue("Key_UserId", userId);
    }

    public static int getUserId() {
        return AppSharePreferences.getInt("Key_UserId", 0);
    }

    //设备绑定
    public static boolean setDeviceBindFlag(boolean enable) {
        return AppSharePreferences.setValue("Key_Device_Bind_Flag", enable);
    }

    public static boolean isDeviceBindFlag() {
        return AppSharePreferences.getBoolean("Key_Device_Bind_Flag", false);
    }

    //蒸发盘运行时间统计 单位秒(蒸发盘累计运行100h后，需除垢提醒)
    public static int getEvaporatorRunningTime() {
        return AppSharePreferences.getInt("evaporator", 0);
    }

    public static boolean setEvaporatorRunningTime(int sec) {
        return AppSharePreferences.setValue("evaporator", sec);
    }

    public static boolean evaporatorRunningTimeAdd(int sec) {
        if (getEvaporatorRunningTime() <= 360000) {
            return setEvaporatorRunningTime(AppSharePreferences.getInt("evaporator", 0) + sec);
        }
        return true;
    }

    public static boolean isNeedChugou() {
        //return true;
        return getEvaporatorRunningTime() > 360000;
    }

    //烹饪开始时间
    public static boolean setCookStartTime(long cookStartTimeMs) {
        return AppSharePreferences.setValue("cookStartTimeMs", cookStartTimeMs);
    }

    public static long getCookeStartTime() {
        return AppSharePreferences.getLong("cookStartTimeMs", 0);
    }

    //预热中
    public static boolean setPreheating(boolean preHeating) {
        return AppSharePreferences.setValue("preHeating", preHeating);
    }

    public static boolean getPreheating() {
        return AppSharePreferences.getBoolean("preHeating", false);
    }

    //食材重量
    public static boolean setWeight(String weight) {
        return AppSharePreferences.setValue("weight", weight);
    }

    public static String getWeight() {
        return AppSharePreferences.getString("weight", "");
    }

    //除垢工作进度
    public static boolean setDescaling(int descaling) {
        return AppSharePreferences.setValue("descaling", descaling);
    }

    public static int getDescaling() {
        return AppSharePreferences.getInt("descaling", 0);
    }

}
