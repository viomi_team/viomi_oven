package com.viomi.oven.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Administrator on 2016-06-15.
 */
public abstract class ConnectionChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifiNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        try {
            if (!wifiNetInfo.isConnected()) {
                // 无网操作
                hasNoNetwork();
            } else {
                // 有网操作
                hasNetwork();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public abstract void hasNoNetwork();

    public abstract void hasNetwork();

}
