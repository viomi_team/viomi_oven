package com.viomi.oven.broadcast;

/**
 * Created by Ljh on 2018/2/6.
 */

public class BroadcastAction {

    public static final String ACTION_LOCATION = "com.viomi.oven.action.ACTION_LOCATION";//定位

    public static final String ACTION_START_APP_UPGRADE = "com.viomi.oven.action.ACTION_START_APP_UPGRADE";//启动app升级


    public static final String ACTION_FAULT_HAPPEN= "com.viomi.oven.action.ACTION_FAULT_HAPPEN";//异常发生
    public static final String ACTION_FAULT_REMOVE= "com.viomi.oven.action.ACTION_REMOVE";//异常解除

    public static final String ACTION_PUSH_MESSAGE= "com.viomi.oven.action.ACTION_PUSH_MESSAGE";//推送到通知栏的

    public static final String ACTION_DEVICE_BIND = "com.viomi.oven.action.ACTION_DEVICE_BIND";//设备被小米账号绑定
    public static final String ACTION_DEVICE_UNBIND = "com.viomi.oven.action.ACTION_DEVICE_UNBIND";//设备被解绑


}
