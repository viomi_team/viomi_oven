package com.viomi.oven.broadcast;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.viomi.oven.activity.WelcomeActivity;
import com.viomi.oven.service.TimeSyncService;
import com.viomi.oven.util.LogUtils;

import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Ljh on 2018/1/9.
 */

public class MyBroadcastReceiver extends android.content.BroadcastReceiver {
    private final static String TAG = MyBroadcastReceiver.class.getSimpleName();
    private static final String action_boot = "android.intent.action.BOOT_COMPLETED";
    private static final String action_replace = "android.intent.action.PACKAGE_REPLACED";

    private long getWlanTime() {
        URL url = null;//取得资源对象
        try {
            url = new URL("http://www.baidu.com");
            URLConnection uc = url.openConnection();//生成连接对象
            uc.connect(); //发出连接
            return uc.getDate(); //取得网站日期时间
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        LogUtils.d(TAG, "" + intent.getAction());
        if (intent.getAction().equals(action_boot)) {
            //Global.jump2MainActivityNewTask(context);
            Intent bootStartIntent = new Intent(context, WelcomeActivity.class);
            bootStartIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(bootStartIntent);
        } else if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {//wifi连接上与否

            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo wifiNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            try {
                if (!wifiNetInfo.isConnected()) {
                    LogUtils.i(TAG, "wifi disconnect ");
                    // 无网操作
                } else {
                    //同步时间
                    LogUtils.i(TAG, "wifi connect ");
                    //同步时间
                    Intent intentService = new Intent(context, TimeSyncService.class);
                    context.startService(intentService);
                    //Intent locationIntent = new Intent(BroadcastAction.ACTION_LOCATION);
                    //LocalBroadcastManager.getInstance(context).sendBroadcast(locationIntent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (intent.getAction().equals(action_replace)) {//静默升级完成

        }
    }
}
