package com.viomi.oven.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.R;
import com.viomi.oven.bean.CartItem;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.util.ImgUtil;
import com.viomi.oven.util.LogUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 购物车食物
 */
public class AdapterFoodCart extends BaseAdapter {
    private static final String TAG = "AdapterFoodCart";
    private Context mContext;
    private List<CartItem> items;
    public int curPos = 0;
    AdapterFoodCartCallBack mAdapterFoodCartCallBack;

    public AdapterFoodCart(Context context, List<CartItem> items) {
        this.mContext = context;
        this.items = items;
    }

    public void setCallBack(AdapterFoodCartCallBack callBack) {
        mAdapterFoodCartCallBack = callBack;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = View.inflate(mContext, DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? R.layout.adapter_food_cart_v : R.layout.adapter_food_cart,
                    null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }
        LogUtils.d(TAG, " the pos is:" + position);
        holder.tvFoodName.setText(items.get(position).getSkuInfoBean().getName());
        holder.tvFoodNum.setText(items.get(position).getQuantity() + "");
        holder.tvFoodPrice.setText("￥" + items.get(position).getSkuInfoBean().getPrice());
        holder.tvDel.setTag(R.id.tagPos, position);
        holder.tvAdd.setTag(R.id.tagPos, position);
        holder.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAdapterFoodCartCallBack != null)
                    mAdapterFoodCartCallBack.numberChange(Integer.parseInt(v.getTag(R.id.tagPos).toString()), true);
            }
        });
        holder.tvDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAdapterFoodCartCallBack != null)
                    mAdapterFoodCartCallBack.numberChange(Integer.parseInt(v.getTag(R.id.tagPos).toString()), false);

            }
        });
        ImgUtil.showDefinedImage(items.get(position).getSkuInfoBean().getMainImg(), holder.imgFood, R.drawable.default_onekey_buy);
        return view;
    }

    public void setCurPos(int curPos) {
        this.curPos = curPos;
        notifyDataSetChanged();
    }

    public void update(List<CartItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    static class ViewHolder {
        @BindView(R.id.imgFood)
        ImageView imgFood;
        @BindView(R.id.tvFoodName)
        TextView tvFoodName;
        @BindView(R.id.tvFoodNum)
        TextView tvFoodNum;
        @BindView(R.id.tvFoodPrice)
        TextView tvFoodPrice;
        @BindView(R.id.tvAdd)
        TextView tvAdd;
        @BindView(R.id.tvDel)
        TextView tvDel;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public interface AdapterFoodCartCallBack {
        void numberChange(int pos, boolean isAdd);
    }
}
