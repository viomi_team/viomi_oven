package com.viomi.oven.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ljh on 2018/1/3
 */
public class RAdapterMoreModel extends RecyclerView.Adapter<RAdapterMoreModel.MyViewHolder> implements View.OnClickListener {
    private static final String TAG = "RAdapterMoreModel";
    //
    private Context mContext;
    List<SteamBakeType> datas = new ArrayList<>();
    MoreModelCallBack mCallBack;

    public RAdapterMoreModel(Context mContext, List<SteamBakeType> datas) {
        this.mContext = mContext;
        this.datas = datas;
    }

    private SteamBakeType getItem(int position) {
        if (datas == null) {
            return null;
        }
        return datas.get(position);
    }

    public void setCallBack(MoreModelCallBack callBack) {
        this.mCallBack = callBack;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(mContext, DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? R.layout.item_more_model_v : R.layout.item_more_model, null);
        return new MyViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        LogUtils.d(TAG + " the pos is:" + position);
        holder.root.setTag(R.id.tagPos, position);
        holder.root.setOnClickListener(this);
        holder.root.setImageResource(datas.get(position).drawable);
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    @Override
    public void onClick(View v) {
        if (v == null) {
            return;
        }
        switch (v.getId()) {
            case R.id.root://选中
                if (mCallBack != null)
                    mCallBack.clicked(Integer.parseInt(v.getTag(R.id.tagPos).toString()));
                break;
        }
    }

    protected static class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView root;
        public View.OnClickListener onClickListener;

        public void setOnClickListener(View.OnClickListener onClickListener) {
            this.onClickListener = onClickListener;
        }

        public MyViewHolder(View view, View.OnClickListener onClickListener) {
            super(view);
            root = (ImageView) view.findViewById(R.id.root);
            root.setOnClickListener(onClickListener);
        }
    }

    public interface MoreModelCallBack {
        void clicked(int pos);
    }
}
