package com.viomi.oven.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.oven.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ljh on 2018/1/31.
 */

public class TubatuAdapter1 extends RecyclingPagerAdapter {
    private static final String TAG = "TubatuAdapter1";
    private List<Integer> mList = new ArrayList<>();
    private List<String> names = new ArrayList<>();
    private final Context mContext;
    private int curPos = -1;

    public TubatuAdapter1(Context context) {
        mList = new ArrayList<>();
        mContext = context;
    }

    public void addAll(List<Integer> list,List<String> names) {
        this.mList.addAll(list);
        this.names.addAll(names);
        notifyDataSetChanged();
    }

    public void setCurPos(int curPos){
        this.curPos = curPos;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup container) {
        LinearLayout llTemp = null;
        //ImageView imageView = null;
        if (convertView == null) {
            llTemp = (LinearLayout) ((Activity) mContext).getLayoutInflater()
                    .inflate(R.layout.item_main_h, null, false);
            //imageView = new ImageView(mContext);
        } else {
            llTemp = (LinearLayout) convertView;
        }
        ((ImageView)llTemp.findViewById(R.id.img)).setImageResource(mList.get(position));
        ((TextView)llTemp.findViewById(R.id.tvName)).setText(names.get(position));
        ((TextView)llTemp.findViewById(R.id.tvName)).getPaint().setFakeBoldText(curPos == position);
        llTemp.setTag(position);
        //imageView.setImageResource(mList.get(position));
        return llTemp;
    }

    @Override
    public int getCount() {
        return mList.size();
    }
}
