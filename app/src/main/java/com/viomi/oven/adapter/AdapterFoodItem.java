package com.viomi.oven.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.R;
import com.viomi.oven.bean.FoodCard;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.util.ImgUtil;
import com.viomi.oven.util.StringUtil;

import java.util.List;

/**
 * Created by Ljh on 2018/5/31.
 */

public class AdapterFoodItem extends PagerAdapter {

    private List<FoodCard> cardList;
    private AdapterFoodItemCallBack mAdapterFoodItemCallBack;

    public AdapterFoodItem(List<FoodCard> cardList) {
        this.cardList = cardList;
    }

    public void setCallBack(AdapterFoodItemCallBack callBack) {
        mAdapterFoodItemCallBack = callBack;
    }

    @Override
    public int getCount() {
        return cardList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LinearLayout itemView = (LinearLayout) LayoutInflater.from(container.getContext()).inflate(DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)
                ? R.layout.item_cart_v : R.layout.item_cart, null, false);
        ((TextView) itemView.findViewById(R.id.tvFoodName)).setText(cardList.get(position).getName());
        ((TextView) itemView.findViewById(R.id.tvFoodPrice)).setText("￥" + cardList.get(position).getPrice());
        ((TextView) itemView.findViewById(R.id.tvWeight)).setText(cardList.get(position).getWeight());
        itemView.findViewById(R.id.tvWeight).setVisibility(StringUtil.isEmpty(cardList.get(position).getWeight()) ? View.GONE : View.VISIBLE);
        //ImgUtil.showDefinedImage(cardList.get(position).getMainImg(), ((ImageView) itemView.findViewById(R.id.imgFood)), R.color.transparent);
        ImgUtil.showDefinedImage(cardList.get(position).getMainImg(), ((ImageView) itemView.findViewById(R.id.imgFood)), R.drawable.default_onekey_buy);

        itemView.findViewById(R.id.tvAdd2ShopCart).setTag(R.id.tagPos, position);
        itemView.findViewById(R.id.tvAdd2ShopCart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAdapterFoodItemCallBack != null) {
                    mAdapterFoodItemCallBack.onBuyClick(Integer.parseInt(v.getTag(R.id.tagPos).toString()), false);
                }
            }
        });
        itemView.findViewById(R.id.tvBuy).setTag(R.id.tagPos, position);
        itemView.findViewById(R.id.tvBuy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAdapterFoodItemCallBack != null) {
                    mAdapterFoodItemCallBack.onBuyClick(Integer.parseInt(v.getTag(R.id.tagPos).toString()), true);
                }
            }
        });
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public interface AdapterFoodItemCallBack {
        void onBuyClick(int pos, boolean isBuy);
    }
}
