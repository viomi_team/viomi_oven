package com.viomi.oven.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.oven.R;
import com.viomi.oven.bean.ModeInfo;
import com.viomi.oven.util.LogUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 专业模式
 */
public class AdapterModeInstructions extends BaseAdapter {
    private static final String TAG = "AdapterModeInstructions";
    private Context mContext;
    private List<ModeInfo> items;
    public int curPos = 0;

    public AdapterModeInstructions(Context context, List<ModeInfo> items) {
        this.mContext = context;
        this.items = items;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = View.inflate(mContext, R.layout.adapter_mode_instruction, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }
        LogUtils.d(TAG, " the pos is:" + position);
        holder.imgMode.setImageResource(items.get(position).getSteamBakeType().h_drawable);
        holder.tvModeName.setText(items.get(position).getSteamBakeType().name);
        holder.tvFunction.setText("功能说明：" + items.get(position).getFunction());
        holder.tvFood.setText("推荐烹饪食物：" + items.get(position).getFood());
        return view;
    }

    public void setCurPos(int curPos) {
        this.curPos = curPos;
        notifyDataSetChanged();
    }

    public void update(List<ModeInfo> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    static class ViewHolder {
        @BindView(R.id.imgMode)
        ImageView imgMode;
        @BindView(R.id.tvModeName)
        TextView tvModeName;
        @BindView(R.id.tvFunction)
        TextView tvFunction;
        @BindView(R.id.tvFood)
        TextView tvFood;
        @BindView(R.id.root)
        LinearLayout root;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
