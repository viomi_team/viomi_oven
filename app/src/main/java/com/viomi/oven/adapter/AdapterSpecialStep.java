package com.viomi.oven.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.device.DeviceRunStep;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 专业模式
 */
public class AdapterSpecialStep extends BaseAdapter {
    private Context mContext;
    private List<DeviceRunStep> items;
    public int curPos = 0;
    public boolean editStatus = false;//是否处于编辑状态

    public AdapterSpecialStep(Context context, List<DeviceRunStep> items) {
        this.mContext = context;
        this.items = items;
    }

    public void setEditStatus(boolean editStatus) {
        this.editStatus = editStatus;
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) {
            if (items.size() == 0 || items.size() == 1)
                view = View.inflate(mContext, R.layout.adapter_special_step_v, null);
            else if (items.size() == 2)
                view = View.inflate(mContext, R.layout.adapter_special_step_2, null);
            else if (items.size() == 3 || items.size() == 4)
                view = View.inflate(mContext, R.layout.adapter_special_step_3, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            if (view != null) {
                holder = (ViewHolder) view.getTag();
            } else {
                view = View.inflate(mContext, R.layout.adapter_special_step, null);
                holder = new ViewHolder(view);
                view.setTag(holder);
            }
        }

//        if (view != null) {
//            holder = (ViewHolder) view.getTag();
//        } else {
//            if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) {
//                if (items.size() == 0 || items.size() == 1)
//                    view = View.inflate(mContext, R.layout.adapter_special_step_v, null);
//                else if (items.size() == 2)
//                    view = View.inflate(mContext, R.layout.adapter_special_step_2, null);
//                else if (items.size() == 3 || items.size() == 4)
//                    view = View.inflate(mContext, R.layout.adapter_special_step_3, null);
//            } else
//                view = View.inflate(mContext, R.layout.adapter_special_step, null);
//            holder = new ViewHolder(view);
//            view.setTag(holder);
//        }

        holder.tvTime.setText(items.get(position).getTime() + (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? "" : "min"));
        holder.tvTemp.setText(items.get(position).getTemp() + (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? "" : "℃"));
        holder.tvModel.setText(items.get(position).getType().name);
        holder.tvModel.setSelected(position == curPos);
        holder.imgModel.setImageResource(items.get(position).getType().s_drawable);
        holder.imgModel.setSelected(position == curPos);
        holder.llContent.setSelected(position == curPos);
        holder.imgDel.setVisibility(editStatus ? View.VISIBLE : View.INVISIBLE);
        return view;
    }

    public void setCurPos(int curPos) {
        this.curPos = curPos;
        notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        if (curPos >= items.size())
            curPos = items.size() - 1;
        super.notifyDataSetChanged();
    }

    public void update(List<DeviceRunStep> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    static class ViewHolder {
        @BindView(R.id.tvModel)
        TextView tvModel;
        @BindView(R.id.tvTemp)
        TextView tvTemp;
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.llContent)
        LinearLayout llContent;
        @BindView(R.id.imgDel)
        ImageView imgDel;
        @BindView(R.id.imgModel)
        ImageView imgModel;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
