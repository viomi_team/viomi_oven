package com.viomi.oven.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ljh on 2018/1/31.
 */

public class TubatuAdapter extends RecyclingPagerAdapter {
    private static final String TAG = "TubatuAdapter";
    private final List<Integer> mList;
    private final Context mContext;

    public TubatuAdapter(Context context) {
        mList = new ArrayList<>();
        mContext = context;
    }

    public void addAll(List<Integer> list) {
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup container) {
        ImageView imageView = null;
        if (convertView == null) {
            imageView = new ImageView(mContext);
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setTag(position);
        imageView.setImageResource(mList.get(position));
        return imageView;
    }

    @Override
    public int getCount() {
        return mList.size();
    }
}
