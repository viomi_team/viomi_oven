package com.viomi.oven.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.R;
import com.viomi.oven.bean.FoodDetail;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.util.LogUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ljh on 2018/1/3
 */
public class RAdapterFoodManager extends RecyclerView.Adapter<RAdapterFoodManager.MyViewHolder> implements View.OnClickListener {
    private static final String TAG = "RAdapterFoodManager";
    //
    private Context mContext;
    private List<FoodDetail> datas = new ArrayList<>();
    private FoodManageAdapterCallback mCallback;

    public RAdapterFoodManager(Context mContext) {
        this.mContext = mContext;
    }

    public RAdapterFoodManager(Context mContext, List<FoodDetail> datas) {
        this.mContext = mContext;
        this.datas = datas;
    }

    public List<FoodDetail> getDatas() {
        return datas;
    }

    public void update(List<FoodDetail> items) {
        this.datas = items;
        notifyDataSetChanged();
    }

    private FoodDetail getItem(int position) {
        if (datas == null) {
            return null;
        }
        return datas.get(position);
    }

    private int getItemRunTime(List<DeviceRunStep> steps) {
        int time = 0;
        for (DeviceRunStep step : steps) {
            time += step.getTime();
        }
        return time;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = R.layout.item_food_manage_h;
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            layout = R.layout.item_food_manage_v;
        View view = View.inflate(mContext, layout, null);
        return new MyViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        LogUtils.d(TAG + " the pos is:" + position);
        FoodDetail bean = getItem(position);
        if (bean == null) {
            return;
        }
        holder.root.setTag(position);
        holder.tvName.setText(bean.getName());
        holder.tvNameHead.setText(bean.getName().charAt(0) + "");
        holder.tvTime.setText("：" + getItemRunTime(bean.getRecipeDirectionList()) + " min");
        if (position % 5 == 0) {
            holder.imgHead.setImageResource(DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? R.drawable.food_circle1 : R.drawable.food_circle1_h);
            holder.imgBanner.setImageResource(R.drawable.food_banner1);
        } else if (position % 5 == 1) {
            holder.imgHead.setImageResource(DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? R.drawable.food_circle2 : R.drawable.food_circle2_h);
            holder.imgBanner.setImageResource(R.drawable.food_banner2);
        } else if (position % 5 == 2) {
            holder.imgHead.setImageResource(DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? R.drawable.food_circle3 : R.drawable.food_circle3_h);
            holder.imgBanner.setImageResource(R.drawable.food_banner3);
        } else if (position % 5 == 3) {
            holder.imgHead.setImageResource(DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? R.drawable.food_circle4 : R.drawable.food_circle4_h);
            holder.imgBanner.setImageResource(R.drawable.food_banner4);
        } else if (position % 5 == 4) {
            holder.imgHead.setImageResource(DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? R.drawable.food_circle5 : R.drawable.food_circle5_h);
            holder.imgBanner.setImageResource(R.drawable.food_banner5);
        }
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    @Override
    public void onClick(View v) {
        if (v == null) {
            return;
        }
        switch (v.getId()) {
            case R.id.root://选中
                if (mCallback != null)
                    mCallback.select(Integer.parseInt(v.getTag().toString()));
                break;
        }
    }

    public void setCallback(FoodManageAdapterCallback callback) {
        mCallback = callback;
    }

    protected static class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout root;
        private TextView tvName, tvNameHead, tvTime;
        private ImageView imgHead, imgBanner;
        public View.OnClickListener onClickListener;

        public void setOnClickListener(View.OnClickListener onClickListener) {
            this.onClickListener = onClickListener;
        }

        public MyViewHolder(View view, View.OnClickListener onClickListener) {
            super(view);
            root = (RelativeLayout) view.findViewById(R.id.root);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvNameHead = (TextView) view.findViewById(R.id.tvNameHead);
            tvTime = (TextView) view.findViewById(R.id.tvTime);
            imgBanner = (ImageView) view.findViewById(R.id.imgBanner);
            imgHead = (ImageView) view.findViewById(R.id.imgHead);
            root.setOnClickListener(onClickListener);
        }
    }

    public interface FoodManageAdapterCallback {
        void select(int position);//
    }
}
