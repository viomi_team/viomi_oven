package com.viomi.oven.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viomi.oven.dialog.Loading;
import com.viomi.oven.otto.BusProvider;
import com.viomi.oven.util.LogUtils;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/*
*   Created by Ljh
 */
public abstract class BaseFragment extends Fragment{
    public final String TAG = this.getClass().getSimpleName();
    //
    protected Context mContext;
    private Unbinder unbinder;
    private Loading loading;
    /**
     * Fragment显示的视图
     */
    protected View mRoot;

    @Override
    public void onAttach(Activity activity) {
        LogUtils.d(TAG+" onAttach");
        super.onAttach(activity);
        mContext = getActivity();
    }

    @Override
    public void onDetach() {
        LogUtils.d(TAG + " onDetach");
        super.onDetach();
    }

    @Override
    public void onStart() {
        LogUtils.d(TAG + " onStart");
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LogUtils.d(TAG + " onCreateView");
        if (mRoot == null) {
            mRoot = inflater.inflate(getLayoutRes(), container, false);
            unbinder = ButterKnife.bind(this, mRoot);
            BusProvider.getInstance().register(this);
            initView(mRoot);
            initData();
        } else {
            // 避免mRoot重复添加到多个父控件导致出错
            ViewGroup parent = (ViewGroup) mRoot.getParent();
            if (parent != null) {
                parent.removeView(mRoot);
            }
        }
        return mRoot;
    }

    protected abstract int getLayoutRes();

    protected abstract void initView(View root);

    protected abstract void initData();

    @Override
    public void onDestroyView() {
        LogUtils.d(TAG + " onDestroyView");
        hideLoading();
        BusProvider.getInstance().unregister(this);
        unbinder.unbind();
        super.onDestroyView();
    }

    public void initLoading() {
        if (loading == null) {
            loading = new Loading(mContext);
        }
    }

    public void showLoading() {
        if (getActivity().isFinishing())
            return;
        initLoading();
        loading.setCancelable(true);
        if (!loading.isShowing()) {
            loading.show();
        }
    }

    public void showUnCancelLoading() {
        if (getActivity().isFinishing())
            return;
        initLoading();
        loading.setCancelable(false);
        if (!loading.isShowing()) {
            loading.show();
        }
    }

    public void hideLoading() {
        if (loading != null && loading.isShowing())
            loading.cancel();
    }

    public boolean getLoadingStatus() {
        initLoading();
        return loading.isShowing();
    }

    /**
     * 查找子控件，可省强转
     */
    public <T> T findView(int id) {
        T view = (T) mRoot.findViewById(id);
        return view;
    }

    public void sendToHandler(int what, String msg) {
        Message message = handler.obtainMessage();
        message.obj = msg;
        message.what = what;
        handler.sendMessage(message);
    }

    public void sendToHandlerDelayed(int what, String msg, long delayMillis) {
        Message message = handler.obtainMessage();
        message.obj = msg;
        message.what = what;
        handler.sendMessageDelayed(message, delayMillis);
    }

    /**
     * *处理消息循环
     **/
    public void dealTheMsg(Message msg) {
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (mContext != null && !((Activity) mContext).isFinishing())
                dealTheMsg(msg);
        }
    };

    @Override
    public void onDestroy() {
        LogUtils.d(TAG + " onDestroy");
        super.onDestroy();
    }

    @Override
    public void onPause() {
        LogUtils.d(TAG + " onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        LogUtils.d(TAG + " onStop");
        super.onStop();
        //BusProvider.getInstance().unregister(this);
    }
}


















