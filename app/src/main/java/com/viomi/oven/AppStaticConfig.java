package com.viomi.oven;

import com.viomi.oven.manager.AccountManager;
import com.viomi.oven.util.PhoneUtil;

/**
 * Created by Ljh on 2017/12/20.
 * 不可变静态参数
 */

public class AppStaticConfig {
    public static boolean TYPE_DEBUG = false;//环境切换和log输出调试模式
    //
    public static final String VIOMI_OVEN_V1 = "viomi.oven.v1";//台面式,竖屏
    public static final String VIOMI_OVEN_V2 = "viomi.oven.v2";//嵌入式,横屏
    //
    public static int COOKING_FINISH_STAGE = 11;

    //HTTP RESULT
    public static final int HTTP_SUCCESS = 100;

    //URL  登录
    private static final String vmall_url_debug = "https://auth.mi-ae.net/services";// 扫码登陆商城测试环境
    private static final String vmall_url_real = "https://vmall-auth.mi-ae.net/services";// 扫码登陆商城正式环境
    private static final String vmall_url = TYPE_DEBUG ? vmall_url_debug : vmall_url_real;// 商城正式环境
    //private static final String vmall_url = TYPE_DEBUG ? vmall_url_real : vmall_url_real;// 商城正式环境

    //菜谱
    private static final String URL_DEBUG = "http://mstest.viomi.com.cn/housekeeper";//智能菜谱类
    private static final String URL_REAL = "http://ms.viomi.com.cn/housekeeper";
    private static final String URL = TYPE_DEBUG ? URL_DEBUG : URL_REAL;
    //private static final String URL = TYPE_DEBUG ? URL_REAL : URL_REAL;

    //一键购
    private static final String URL_SHOP_CART_DEBUG = "https://vj.viomi.com.cn/services";
    private static final String URL_SHOP_CART_REAL = "https://s.viomi.com.cn/services";
    private static final String URL_SHOP = TYPE_DEBUG ? URL_SHOP_CART_DEBUG : URL_SHOP_CART_REAL;
    //private static final String URL_SHOP = TYPE_DEBUG ? URL_SHOP_CART_REAL : URL_SHOP_CART_REAL;
    //
    public static final String URL_CHECK_APP_VER = "https://app.mi-ae.com.cn/getdata" + "?type=version&p=1&l=1&package=";
    public static final String URL_CREATOR_QR = vmall_url + "/vmall/login/QRCode.json";// 生成商城登录二维码
    public static final String URL_CHECK_LOGIN_STATUS = vmall_url + "/vmall/login/QRCode.json";// 轮询获取二维码登录状态

    public static final String URL_FOOD_DETAIL = URL + "/recipe/recipeId/";//菜谱详情
    public static final String URL_FOOD_QR_DETAIL = URL + "/recipe/barCodeRecipe/";//扫码食材详情

    public static final String getUrlFoodLlist(String relationType) {//relationType=1,设备专用（12项菜谱） relationType=2,用户自定义(我的菜谱)
        return URL + "/oven/recipe/did/" + PhoneUtil.getMiIdentify().did + "/relationType/" + relationType + "?pageNum=0&pageSize=12";
    }

    public static final String URL_ONE_KEY_SHOP_LIST = URL_SHOP + "/hardware/oven/prod/list.json";//一键购食材列表

    //购物车列表
    public static final String getShopCartList() {
        return URL_SHOP + "/user/" + AccountManager.getViomiUser(ViomiApplication.getContext()).getUserCode() + "/cart/1.json" + "?sourc=" + 2 +
                "&token=" + AccountManager.getViomiUser(ViomiApplication.getContext()).getToken();
    }

    public static final String addFoodItem(String skuId) {
        return URL_SHOP + "/user/" + AccountManager.getViomiUser(ViomiApplication.getContext()).getUserCode() + "/cart/add/" + skuId + "" +
                ".json?token=" + AccountManager.getViomiUser(ViomiApplication.getContext()).getToken();
    }

    public static final String reduceFoodItem(String skuId) {
        return URL_SHOP + "/user/" + AccountManager.getViomiUser(ViomiApplication.getContext()).getUserCode() + "/cart/remove/" + skuId + "" +
                ".json?token=" + AccountManager.getViomiUser(ViomiApplication.getContext()).getToken();
    }

    public static final String delFoodItem(String skuId) {
        return URL_SHOP + "/user/" + AccountManager.getViomiUser(ViomiApplication.getContext()).getUserCode() + "/cart/delete/" + skuId + "" +
                ".json?token=" + AccountManager.getViomiUser(ViomiApplication.getContext()).getToken();
    }

    public static final String getSkuIdUrl(String skuId) {
        if (TYPE_DEBUG)
            return "https://yunmi-wechat.mi-ae.com.cn/v1.0/views/vmall/index.html?referee=&source_channel=&#!/proDetails?skuId=" + skuId;
        else
            return "https://vmall.viomi.com.cn/v1.0/views/vmall/index.html?referee=&source_channel=&#!/proDetails?skuId=" + skuId;
    }

    //public static final String getSkuIdUrl(String skuId) {
    //    return "https://vmall.viomi.com.cn/v1.0/views/vmall/index.html?referee=&source_channel=&#!/proDetails?skuId=" + skuId;
    //}
}


