package com.viomi.oven.bean;

import java.util.List;

/**
 * Created by Ljh on 2018/1/17.
 */
public class CheckScanResult {
    public int code;
    public String desc;
    public String token;
    public String appendAttr;
    public LoginData loginData;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAppendAttr() {
        return appendAttr;
    }

    public void setAppendAttr(String appendAttr) {
        this.appendAttr = appendAttr;
    }

    public LoginData getLoginData() {
        return loginData;
    }

    public void setLoginData(LoginData loginData) {
        this.loginData = loginData;
    }

    public class LoginData {
        public int accountType;
        public int loginType;
        public int userId;
        public String userCode;
        public List<String> roles;

        public int getAccountType() {
            return accountType;
        }

        public void setAccountType(int accountType) {
            this.accountType = accountType;
        }

        public int getLoginType() {
            return loginType;
        }

        public void setLoginType(int loginType) {
            this.loginType = loginType;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getUserCode() {
            return userCode;
        }

        public void setUserCode(String userCode) {
            this.userCode = userCode;
        }

        public List<String> getRoles() {
            return roles;
        }

        public void setRoles(List<String> roles) {
            this.roles = roles;
        }
    }

    public boolean checkDataOk() {
        return (token != null && appendAttr != null && loginData != null && loginData.getUserCode() != null);
    }
}
