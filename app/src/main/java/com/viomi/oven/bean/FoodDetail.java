package com.viomi.oven.bean;

import com.viomi.oven.device.DeviceRunStep;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ljh on 2018/2/26.
 */

public class FoodDetail {
    private String name = "";
    private String material = "";
    private Manufacture manufacture;
    private List<String> imgUrl = new ArrayList<>();
    private List<Integer> imgsId = new ArrayList<>();
    private List<Integer> logoId = new ArrayList<>();
    private List<RecipeGuide> recipeGuideList = new ArrayList<>();
    private List<DeviceRunStep> recipeDirectionList = new ArrayList<>();
    private List<BasicKeyInfo> nutritionInfo = new ArrayList<>();
    private List<BasicKeyInfo> ingredients = new ArrayList<>();
    private List<BasicKeyInfo> basicInfo = new ArrayList<>();

    public List<String> getImgUrl() {
        if (imgUrl == null)
            imgUrl = new ArrayList<>();
        return imgUrl;
    }

    public void setImgUrl(List<String> imgs) {
        this.imgUrl = imgs;
    }

    public List<RecipeGuide> getRecipeGuideList() {
        if (recipeGuideList == null)
            recipeGuideList = new ArrayList<>();
        return recipeGuideList;
    }

    public void setRecipeGuideList(List<RecipeGuide> recipeGuideList) {
        this.recipeGuideList = recipeGuideList;
    }

    public List<BasicKeyInfo> getNutritionInfo() {
        if (nutritionInfo == null)
            nutritionInfo = new ArrayList<>();
        return nutritionInfo;
    }

    public void setNutritionInfo(List<BasicKeyInfo> nutritionInfo) {
        this.nutritionInfo = nutritionInfo;
    }

    public List<DeviceRunStep> getRecipeDirectionList() {
        if (recipeDirectionList == null)
            recipeDirectionList = new ArrayList<>();
        return recipeDirectionList;
    }

    public void setRecipeDirectionList(List<DeviceRunStep> recipeDirectionList) {
        this.recipeDirectionList = recipeDirectionList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BasicKeyInfo> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<BasicKeyInfo> ingredients) {
        this.ingredients = ingredients;
    }

    public List<BasicKeyInfo> getBasicInfo() {
        if (basicInfo == null)
            basicInfo = new ArrayList<>();
        return basicInfo;
    }

    public void setBasicInfo(List<BasicKeyInfo> basicInfo) {
        this.basicInfo = basicInfo;
    }

    public Manufacture getManufacture() {
        if (manufacture == null)
            manufacture = new Manufacture();
        return manufacture;
    }

    public void setManufacture(Manufacture manufacture) {
        this.manufacture = manufacture;
    }

    public List<Integer> getImgsId() {
        return imgsId;
    }

    public void setImgsId(List<Integer> imgsId) {
        this.imgsId = imgsId;
    }

    public List<Integer> getLogoId() {
        return logoId;
    }

    public void setLogoId(List<Integer> logoId) {
        this.logoId = logoId;
    }

    public String getMaterial() {
        if (material == null)
            material = "";
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }


    public class Manufacture {
        private String address = "黑龙江省齐齐哈尔市";
        private String name = "黑龙江恒阳牛业";

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
