package com.viomi.oven.bean;

/**
 * Created by Ljh on 2018/3/29.
 */
public class RecipeGuide {
    private int guideOrder;
    private String guideDesc;

    public RecipeGuide(int guideOrder, String guideDesc) {
        this.guideDesc = guideDesc;
        this.guideOrder = guideOrder;
    }

    public int getGuideOrder() {
        return guideOrder;
    }

    public void setGuideOrder(int guideOrder) {
        this.guideOrder = guideOrder;
    }

    public String getGuideDesc() {
        return guideDesc;
    }

    public void setGuideDesc(String guideDesc) {
        this.guideDesc = guideDesc;
    }
}
