package com.viomi.oven.bean;

import com.viomi.oven.device.DeviceRunStep;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ljh on 2018/1/3.
 */

public class FoodInfo {
    private String id = "";
    private String name = "";
    private List<BasicKeyInfo> info = new ArrayList<>();
    //private String info = "";
    private List<DeviceRunStep> modes = new ArrayList<>();

    boolean selected = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public List<DeviceRunStep> getModes() {
        return modes;
    }

    public void setModes(List<DeviceRunStep> modes) {
        if (modes == null)
            modes = new ArrayList<>();
        this.modes = modes;
    }

    public List<BasicKeyInfo> getInfo() {
        return info;
    }

    public void setInfo(List<BasicKeyInfo> info) {
        this.info = info;
    }
}
