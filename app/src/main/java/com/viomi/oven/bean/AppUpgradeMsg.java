package com.viomi.oven.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by young2 on 2016/5/20.
 */
public class AppUpgradeMsg implements Parcelable {
    public String id = "";
    public int code;//app版本
    public String detail = "";//平台固件描述
    public String url = "";//apk下载地址
    public String createtime = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public AppUpgradeMsg() {

    }

    protected AppUpgradeMsg(Parcel in) {
        id = in.readString();
        code = in.readInt();
        detail = in.readString();
        url = in.readString();
        createtime = in.readString();
    }

    public static final Creator<AppUpgradeMsg> CREATOR = new Creator<AppUpgradeMsg>() {
        @Override
        public AppUpgradeMsg createFromParcel(Parcel in) {
            return new AppUpgradeMsg(in);
        }

        @Override
        public AppUpgradeMsg[] newArray(int size) {
            return new AppUpgradeMsg[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(code);
        dest.writeString(detail);
        dest.writeString(url);
        dest.writeString(createtime);
    }
}
