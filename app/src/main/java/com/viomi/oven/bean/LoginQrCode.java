package com.viomi.oven.bean;

/**
 * Created by Ljh on 2018/1/17.
 */

public class LoginQrCode {
    String desc;
    String result;
    int code;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
