package com.viomi.oven.bean;

import com.viomi.oven.enumType.SteamBakeType;

/**
 * Created by Ljh on 2018/4/8
 */

public class ModeInfo {
    private SteamBakeType mSteamBakeType;
    private String food = "";
    private String function = "";

    public ModeInfo(SteamBakeType type, String function,String food){
        this.mSteamBakeType = type;
        this.function = function;
        this.food = food;
    }

    public SteamBakeType getSteamBakeType() {
        return mSteamBakeType;
    }

    public void setSteamBakeType(SteamBakeType steamBakeType) {
        mSteamBakeType = steamBakeType;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }
}
