package com.viomi.oven.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ljh on 2018/3/1.
 */

public class BespeakdishBean {

    /**
     * name : 螃蟹
     * modes : [{"mode":"0","time":"200","temp":"100"},{"mode":"1","time":"300","temp":"110"}]
     */

    private String name = "";
    private List<ModesBean> modes = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ModesBean> getModes() {
        return modes;
    }

    public void setModes(List<ModesBean> modes) {
        this.modes = modes;
    }

}
