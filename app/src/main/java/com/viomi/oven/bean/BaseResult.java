package com.viomi.oven.bean;

import java.io.Serializable;

/**
 * Created by Ljh on 18/1/17.
 */
public class BaseResult<T> implements Serializable{
    private static final long serialVersionUID = -4222520829449056781L;

    private String desc = "";
    private int code = 0;
    private T result;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String jht_error_msg) {
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int jht_error_code) {
        this.code = code;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T data) {
        this.result = result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BaseResult [msg=");
        sb.append(desc);
        sb.append(", status=");
        sb.append(code);
        sb.append(", data=");
        sb.append(result);
        sb.append("]");
        return sb.toString();
    }
}
