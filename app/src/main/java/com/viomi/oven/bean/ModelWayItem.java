package com.viomi.oven.bean;

import com.viomi.oven.enumType.SteamBakeType;

/**
 * Created by Ljh on 2018/1/4
 */

public class ModelWayItem {

    private SteamBakeType mSteamBakeType;
    private boolean select = false;

    public ModelWayItem(SteamBakeType steamBakeType){
        this.mSteamBakeType = steamBakeType;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public SteamBakeType getSteamBakeType() {
        return mSteamBakeType;
    }

    public void setSteamBakeType(SteamBakeType steamBakeType) {
        mSteamBakeType = steamBakeType;
    }
}
