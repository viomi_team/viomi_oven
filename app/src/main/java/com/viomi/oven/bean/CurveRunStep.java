package com.viomi.oven.bean;

/**
 * Created by Ljh on 2018/1/26.
 */

public class CurveRunStep {
    private int timeSec = 0;//运行时间
    public double temp = 0;//运行温度
    public float floatX = 0;
    public float floatY = 0;
    //
    private int stage = 0;//温度曲线中的各个阶段，值范围为0~5，0指还未启动前，1~4分别是指4个阶段，11是指结束蒸烤动作了。
    //private boolean connectOk = true, waterOk = true;
    //private SteamBakeType mSteamBakeType = SteamBakeType.STEAM;//

    public CurveRunStep(double temp) {
        this.temp = temp;
    }

    public CurveRunStep(int stage, int timeSec, int temp/*, boolean connectOk, boolean waterOk*/) {
        this.stage = stage;
        this.timeSec = timeSec;
        this.temp = temp;
        //this.connectOk = connectOk;
        //this.waterOk = waterOk;
    }

    public int getTimeSec() {
        return timeSec;
    }

    public int getTimeMinute() {
        return (int) (timeSec / 60);
    }

    public void setTimeSec(int timeSec) {
        this.timeSec = timeSec;
    }

    public double getTemp() {
        return temp;
    }

    public int getIntTemp(){
        return (int) temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    //public SteamBakeType getSteamBakeType() {
    //    return mSteamBakeType;
    //}
    //
    //public void setSteamBakeType(SteamBakeType steamBakeType) {
    //    mSteamBakeType = steamBakeType;
    //}


    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    //public boolean isConnectOk() {
    //    return connectOk;
    //}
    //
    //public void setConnectOk(boolean connectOk) {
    //    this.connectOk = connectOk;
    //}
    //
    //public boolean isWaterOk() {
    //    return waterOk;
    //}
    //
    //public void setWaterOk(boolean waterOk) {
    //    this.waterOk = waterOk;
    //}
}
