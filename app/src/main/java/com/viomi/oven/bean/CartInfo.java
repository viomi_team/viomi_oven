package com.viomi.oven.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ljh on 2018/6/5.
 */

public class CartInfo {
    private String provider = "";
    private List<CartItem> cartItems = new ArrayList<>();

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
    }
}
