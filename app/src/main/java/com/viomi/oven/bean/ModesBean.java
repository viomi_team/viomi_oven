package com.viomi.oven.bean;

/**
 * Created by Ljh on 2018/3/1.
 */
public class ModesBean {
    /**
     * mode : 0
     * time : 200
     * temp : 100
     */

    private int mode = 1;
    private int time = 0;
    private int temp = 0;

    public ModesBean() {

    }

    public ModesBean(int mode, int time, int temp) {
        setMode(mode);
        setTemp(temp);
        setTime(time);
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }
}
