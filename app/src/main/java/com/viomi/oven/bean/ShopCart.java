package com.viomi.oven.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ljh on 2018/6/5.
 */

public class ShopCart {
    private int totalAmount = 0;
    private List<CartInfo> cartInfos = new ArrayList<>();
    //
    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<CartInfo> getCartInfos() {
        return cartInfos;
    }

    public void setCartInfos(List<CartInfo> cartInfos) {
        this.cartInfos = cartInfos;
    }

}
