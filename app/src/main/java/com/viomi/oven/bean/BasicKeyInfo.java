package com.viomi.oven.bean;

/**
 * Created by Ljh on 2018/2/10
 */

public class BasicKeyInfo {

    private String name = "";
    private String value = "";
    private String unit = "";

    public BasicKeyInfo(String name, String value, String unit) {
        setName(name);
        setValue(value);
        setUnit(unit);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
