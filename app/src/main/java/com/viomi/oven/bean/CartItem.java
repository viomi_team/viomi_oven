package com.viomi.oven.bean;

/**
 * Created by Ljh on 2018/6/5.
 */

public class CartItem {
    private int quantity = 0;
    private String providerName = "";
    private FoodCard skuInfoBean;

    public int getQuantity() {
        return quantity;
    }

    public void QuantityAdd() {
        quantity++;
    }

    public void QuantityDel() {
        quantity--;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public FoodCard getSkuInfoBean() {
        return skuInfoBean;
    }

    public void setSkuInfoBean(FoodCard skuInfoBean) {
        this.skuInfoBean = skuInfoBean;
    }
}
