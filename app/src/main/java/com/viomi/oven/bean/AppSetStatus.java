package com.viomi.oven.bean;

/**
 * Created by Ljh on 2018/22/22
 */

public class AppSetStatus {
    private int status;
    private int from;
    private int prepareTime = 0;//
    private FoodInfo dish;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getPrepareTime() {
        return prepareTime;
    }

    public void setPrepareTime(int prepareTime) {
        this.prepareTime = prepareTime;
    }

    //public int getAppointTime() {//prepareTime进行转换,只提取时分值
    //    return 600;
    //}

    public FoodInfo getDish() {
        return dish;
    }

    public void setDish(FoodInfo dish) {
        this.dish = dish;
    }
}
