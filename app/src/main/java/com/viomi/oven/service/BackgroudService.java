package com.viomi.oven.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.tencent.bugly.crashreport.CrashReport;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.ViomiApplication;
import com.viomi.oven.bean.ViomiUser;
import com.viomi.oven.broadcast.BroadcastAction;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.interfaces.AppCallback;
import com.viomi.oven.manager.AccountManager;
import com.viomi.oven.manager.DeviceManager;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.manager.UpgradeManager;
import com.viomi.oven.util.LogUtils;

import java.text.SimpleDateFormat;

public class BackgroudService extends Service {
    private final static String TAG = BackgroudService.class.getSimpleName();
    SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("HHmm");
    //private Clockhread mClockThread;

    private MediaPlayer mMediaPlayer;
    private MediaPlayer mMediaPlayer1;
    private AudioManager audioManager;
    private BraodcastReceiver mBraodcastReceiver;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        init();
        return START_REDELIVER_INTENT;
    }

    private void init() {
        register();
        SerialManager.getInstance().open(DeviceConfig.MODEL);
        HawkParams.setAppStartTime(System.currentTimeMillis() / 1000);
        initDevice();
        initView();
        UpgradeManager.getInstance().init(this);
    }

    public static class BraodcastReceiver extends BroadcastReceiver {
        private BackgroudService mService;

        public BraodcastReceiver() {
        }

        public BraodcastReceiver(BackgroudService service) {
            mService = service;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.d(TAG, "onReceive,action=" + intent.getAction());
            switch (intent.getAction()) {
                case BroadcastAction.ACTION_START_APP_UPGRADE:
                    mService.startAppUpgrade();
                    break;
                case BroadcastAction.ACTION_FAULT_HAPPEN:
                    //mService.mMessageDialog.setVisibility(View.VISIBLE);
                    break;
                case BroadcastAction.ACTION_FAULT_REMOVE:
                    //mService.mMessageDialog.setVisibility(View.GONE);
                    break;
                case Intent.ACTION_DATE_CHANGED:///日期发生变化，作凌晨时升级判断

                    break;
            }
        }
    }

    private void register() {
        mBraodcastReceiver = new BraodcastReceiver(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BroadcastAction.ACTION_START_APP_UPGRADE);
        intentFilter.addAction(BroadcastAction.ACTION_FAULT_HAPPEN);
        intentFilter.addAction(BroadcastAction.ACTION_FAULT_REMOVE);
        intentFilter.addAction(BroadcastAction.ACTION_PUSH_MESSAGE);
        intentFilter.addAction(Intent.ACTION_DATE_CHANGED);
        LocalBroadcastManager.getInstance(ViomiApplication.getContext()).registerReceiver(mBraodcastReceiver, intentFilter);
    }

    private void unRegister() {
        LocalBroadcastManager.getInstance(ViomiApplication.getContext()).unregisterReceiver(mBraodcastReceiver);
    }

    private void initView() {
        mMediaPlayer = MediaPlayer.create(this, R.raw.alert);
        mMediaPlayer1 = MediaPlayer.create(this, R.raw.bubbles);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
    }

    /***
     * 设备初始化
     */
    private void initDevice() {
        ViomiUser viomiUser = AccountManager.getViomiUser(ViomiApplication.getContext());
        String miId = null;
        if (viomiUser != null) {
            miId = viomiUser.getMiId();
            CrashReport.setUserId(viomiUser.getAccount() + "_" + viomiUser.getMobile());
            LogUtils.d(TAG, "miId=" + miId);
        }
        //在其他手机上测试显示效果需屏蔽initDevice，不然会奔溃
        try {
            DeviceManager.getInstance().initDevice(ViomiApplication.getContext(), miId, new AppCallback<String>() {
                @Override
                public void onSuccess(String data) {
                    LogUtils.e(TAG, "initDevice success");
                }

                @Override
                public void onFail(int errorCode, String msg) {
                    LogUtils.e(TAG, "initDevice fail,err=" + errorCode + ",msg=" + msg);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * app升级启动
     */
    private void startAppUpgrade() {
        LogUtils.i(TAG, "startAppUpgrade");
        UpgradeManager.getInstance().httpCheckAppVersion(true, UpgradeManager.OPERATE_AUTO);
    }

    @Override
    public void onDestroy() {
        SerialManager.getInstance().close();
        unRegister();
        //if (mClockThread != null) {
        //    mClockThread.interrupt();
        //    mClockThread = null;
        //}
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer = null;
        }
        if (mMediaPlayer1 != null) {
            mMediaPlayer1.stop();
            mMediaPlayer1 = null;
        }
        super.onDestroy();
    }

    /**
     * 升级检测，考虑弃用

     class Clockhread extends Thread {
    @Override public synchronized void run() {
    super.run();

    while (!isInterrupted()) {
    try {
    Thread.sleep(1000 * 60);
    String timeStr = mSimpleDateFormat.format(new Date());
    try {
    int hour = Integer.parseInt(timeStr);
    if (hour == 0) {//00:00 先检测MCU
    if (HawkParams.getWorkStatus() == WorkStatus.STATUS_IDLE.value)
    UpgradeManager.getInstance().httpCheckAppVersion(false, UpgradeManager.OPERATE_AUTO);
    } else if (hour == 100) {//01:00检测App
    if (HawkParams.getWorkStatus() == WorkStatus.STATUS_IDLE.value)
    UpgradeManager.getInstance().httpCheckAppVersion(true, UpgradeManager.OPERATE_AUTO);
    }
    } catch (Exception e) {
    e.printStackTrace();
    }
    } catch (Exception e) {
    LogUtils.e(TAG, "Clockhread fail!msg: " + e.getMessage());
    e.printStackTrace();
    }
    }
    }
    }
     */
}