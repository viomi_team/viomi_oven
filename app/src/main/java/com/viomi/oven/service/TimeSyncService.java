package com.viomi.oven.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.viomi.oven.util.LogUtils;

import java.net.URL;
import java.net.URLConnection;

/**
 * Created by young2 on 2017/7/24.
 */

public class TimeSyncService extends IntentService {
    private final static String TAG=TimeSyncService.class.getSimpleName();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public TimeSyncService(String name) {
        super(name);
    }

    public TimeSyncService() {
        super("TimeSyncService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        long time= getWlanTime();
        if(time<=1500885892000L){
            Log.e(TAG,"time error,time="+time);
            return;
        }
        LogUtils.d(TAG,"sync time="+time);
        Intent intentTime = new Intent();
        intentTime.setAction("android.intent.action.USER_SET_TIME");
        intentTime.putExtra("time", time);
        sendBroadcast(intentTime);
    }

    private long getWlanTime(){
        URL url = null;//取得资源对象
        try {
            url = new URL("http://www.baidu.com");
            URLConnection uc = url.openConnection();//生成连接对象
            uc.connect(); //发出连接
            return  uc.getDate(); //取得网站日期时间
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}