package com.viomi.oven.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.bean.BaseResult;
import com.viomi.oven.bean.BasicKeyInfo;
import com.viomi.oven.bean.FoodDetail;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.enumType.FromType;
import com.viomi.oven.enumType.ModelType;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.manager.AccountManager;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.ImgUtil;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.StringUtil;
import com.viomi.oven.util.ToastUtil;
import com.viomi.oven.util.ViewUtil;
import com.viomi.oven.util.http.HttpReCallback;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Ljh on 2018/1/3
 * 一键烹饪
 */
public class OneKeyCookingActivity_V extends BaseActivity {
    //UI
    @BindView(R.id.imgWifiV)
    ImageView imgWifiV;
    @BindView(R.id.imgFood)
    ImageView imgFood;
    @BindView(R.id.tvBuy)
    TextView tvBuy;
    @BindView(R.id.tvComposition)
    TextView tvComposition;
    @BindView(R.id.tvFoodName)
    TextView tvFoodName;
    @BindView(R.id.tvTemp)
    TextView tvTemp;
    @BindView(R.id.llRecipe)
    LinearLayout llRecipe;
    @BindView(R.id.tvMakeStep)
    TextView tvMakeStep;
    @BindView(R.id.llFood)
    LinearLayout llFood;
    @BindView(R.id.imgBasicArrow)
    ImageView imgBasicArrow;
    @BindView(R.id.llBasicInfo)
    LinearLayout llBasicInfo;
    @BindView(R.id.llBasicContent)
    LinearLayout llBasicContent;
    @BindView(R.id.imgNutrientsArrow)
    ImageView imgNutrientsArrow;
    @BindView(R.id.llNutrientsInfo)
    LinearLayout llNutrientsInfo;
    @BindView(R.id.llNutrientsContent)
    LinearLayout llNutrientsContent;
    @BindView(R.id.imgIngredientsArrow)
    ImageView imgIngredientsArrow;
    @BindView(R.id.llIngredientsInfo)
    LinearLayout llIngredientsInfo;
    @BindView(R.id.llIngredientsContent)
    LinearLayout llIngredientsContent;
    @BindView(R.id.tvOk)
    TextView tvOk;
    @BindView(R.id.llTime)
    LinearLayout llTime;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tvInfo)
    TextView tvInfo;
    @BindView(R.id.llInfo)
    LinearLayout llInfo;
    @BindView(R.id.tvFactoryName)
    TextView tvFactoryName;
    @BindView(R.id.llFactoryName)
    LinearLayout llFactoryName;
    @BindView(R.id.tvFactoryAddr)
    TextView tvFactoryAddr;
    @BindView(R.id.llFactoryAddr)
    LinearLayout llFactoryAddr;
    //参数
    FoodDetail mFoodDetail = new FoodDetail();
    List<DeviceRunStep> preHeating = new ArrayList<>();
    List<DeviceRunStep> mDeviceRunSteps = new ArrayList<>();
    String foodId = "";
    //boolean isScan = true;

    public static void actionStart(Context context, String foodId) {
        Intent intent = new Intent(context, OneKeyCookingActivity_V.class);
        intent.putExtra("foodId", foodId);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_one_key_cooking_v;
    }

    @Override
    protected void initView() {
        imgWifiV.setVisibility(View.VISIBLE);
        llBasicInfo.setSelected(true);
        imgBasicArrow.setSelected(true);
        llBasicContent.setVisibility(View.VISIBLE);
        llIngredientsInfo.setSelected(true);
        imgIngredientsArrow.setSelected(true);
        llIngredientsContent.setVisibility(View.VISIBLE);
        llNutrientsInfo.setSelected(true);
        imgNutrientsArrow.setSelected(true);
        llNutrientsContent.setVisibility(View.VISIBLE);
    }

    @Override
    protected void initData() {
        foodId = getIntent().getStringExtra("foodId");
        if (StringUtil.isEmpty(foodId)) {
            if (HawkParams.getModelType() == ModelType.MODEL_MY_SET.value)
                mFoodDetail = HawkParams.getMySetFoodList().get(HawkParams.getSelectFoodPos());
            else
                mFoodDetail = HawkParams.getSmartFoodList().get(HawkParams.getSelectFoodPos());
            llFood.setVisibility(View.GONE);
            llRecipe.setVisibility(View.VISIBLE);
            tvBuy.setVisibility(View.INVISIBLE);
            initShowAndData();
        } else {
            llFood.setVisibility(View.VISIBLE);
            llRecipe.setVisibility(View.GONE);
            httpGetFoodDetail(foodId);
        }
    }

    public void httpGetFoodDetail(String foodId) {
        showLoading();
        OkHttpUtils
                .get()
                .url(AppStaticConfig.URL_FOOD_QR_DETAIL + foodId)
                .tag(this)
                .build()
                .execute(new HttpReCallback<BaseResult<FoodDetail>>() {
                    @Override
                    public void onSuccess(BaseResult<FoodDetail> result) {
                        if (result.getCode() == 100) {
                            mFoodDetail = result.getResult();
                            if (mFoodDetail == null)
                                ToastUtil.show("数据解析异常");
                            initShowAndData();
                        }
                        hideLoading();
                    }

                    @Override
                    public void onFail(int errorCode) {
                        hideLoading();
                    }
                });
    }

    public void initShowAndData() {
        if (mFoodDetail == null)
            return;
        if (mFoodDetail.getImgUrl().size() > 0)
            ImgUtil.showDefinedImage(mFoodDetail.getImgUrl().get(0), imgFood, R.color.transparent);
        else if (mFoodDetail.getImgsId().size() > 0) {
            ImgUtil.showDefinedImage(mFoodDetail.getImgsId().get(0), imgFood, R.color.transparent);
        } else
            ImgUtil.showDefinedImage(R.drawable.default_food, imgFood, R.drawable.default_food);
        tvFoodName.setText(mFoodDetail.getName());
        tvComposition.setText(mFoodDetail.getMaterial());
        //生产商和生产地址
        tvFactoryName.setText(mFoodDetail.getManufacture().getName());
        tvFactoryAddr.setText(mFoodDetail.getManufacture().getAddress());
        //烹饪方式
        String info = "";
        for (int i = 0; i < mFoodDetail.getRecipeGuideList().size(); i++) {
            if (i == 0)
                info = mFoodDetail.getRecipeGuideList().get(i).getGuideOrder() + "、" + mFoodDetail.getRecipeGuideList().get(i).getGuideDesc();
            else info += "\n\n" + mFoodDetail.getRecipeGuideList().get(i).getGuideOrder() + "、" + mFoodDetail.getRecipeGuideList().get(i).getGuideDesc();
        }
        tvInfo.setText(info);
        tvMakeStep.setText(info);
        //基本信息
        List<BasicKeyInfo> mBasicInfos = mFoodDetail.getBasicInfo();
        llBasicInfo.setVisibility(mBasicInfos.size() > 0 ? View.VISIBLE : View.GONE);
        llBasicContent.setVisibility(mBasicInfos.size() > 0 ? View.VISIBLE : View.GONE);
        for (int i = 0; i < mBasicInfos.size(); i++) {//商品基本信息
            ViewUtil.llAddLeftSpaceRightView(mContext, llBasicContent, mBasicInfos.get(i).getName(),
                    mBasicInfos.get(i).getValue() + mBasicInfos.get(i).getUnit(), (i % 2 == 0) ? getResources().getColor(R.color.black_2B) :
                            getResources().getColor(R.color.black));
            if (mBasicInfos.get(i).getName().equals("产品重量")) {
                HawkParams.setWeight(mBasicInfos.get(i).getValue());
            }
        }
        //营养信息
        List<BasicKeyInfo> mNutritionInfos = mFoodDetail.getNutritionInfo();
        llNutrientsInfo.setVisibility(mNutritionInfos.size() > 0 ? View.VISIBLE : View.GONE);
        llNutrientsContent.setVisibility(mNutritionInfos.size() > 0 ? View.VISIBLE : View.GONE);
        for (int i = 0; i < mNutritionInfos.size(); i++) {//商品营养信息
            ViewUtil.llAddLeftSpaceRightView(mContext, llNutrientsContent, mNutritionInfos.get(i).getName(),
                    mNutritionInfos.get(i).getValue() + mNutritionInfos.get(i).getUnit(), (i % 2 == 0) ? getResources().getColor(R.color.black_2B) :
                            getResources().getColor(R.color.black));
        }
        HawkParams.setNutritionInfos(mNutritionInfos);
        LogUtils.d(TAG, HawkParams.getNutritionInfos2String());
        //配料名称
        List<BasicKeyInfo> mIngredients = mFoodDetail.getIngredients();
        llIngredientsInfo.setVisibility(mIngredients.size() > 0 ? View.VISIBLE : View.GONE);
        llIngredientsContent.setVisibility(mIngredients.size() > 0 ? View.VISIBLE : View.GONE);
        for (int i = 0; i < mIngredients.size(); i++) {//商品营养信息
            String unit = "";
            if (!mIngredients.get(i).getValue().equals("0"))
                unit = mIngredients.get(i).getValue() + mIngredients.get(i).getUnit();
            ViewUtil.llAddLeftSpaceRightView(mContext, llIngredientsContent, mIngredients.get(i).getName(),
                    unit, (i % 2 == 0) ? getResources().getColor(R.color.black_2B) :
                            getResources().getColor(R.color.black));
        }
        //
        mDeviceRunSteps.clear();
        preHeating.clear();
        for (DeviceRunStep temp : mFoodDetail.getRecipeDirectionList()) {
            DeviceRunStep step = new DeviceRunStep(SteamBakeType.STEAM);
            step.setMode(temp.getMode());
            step.setTemp(temp.getTemp());
            step.setTime(temp.getTime());
            step.setStop(temp.getStop());
            step.setStoptip(temp.getStoptip());
            if (temp.getMode() == SteamBakeType.PREHEATING.value) {
                preHeating.add(step);
            } else {
                mDeviceRunSteps.add(Global.compatibleChange(step));
            }
        }
    }

    @OnClick({R.id.llBasicInfo, R.id.llIngredientsInfo, R.id.llNutrientsInfo, R.id.tvBuy, R.id.tvOk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llBasicInfo:
                boolean toSelectStatus = !view.isSelected();//true为展开，false为隐藏
                view.setSelected(toSelectStatus);
                imgBasicArrow.setSelected(toSelectStatus);
                llBasicContent.setVisibility(toSelectStatus ? View.VISIBLE : View.GONE);
                break;
            case R.id.llIngredientsInfo:
                boolean toSelectStatus1 = !view.isSelected();//true为展开，false为隐藏
                view.setSelected(toSelectStatus1);
                imgIngredientsArrow.setSelected(toSelectStatus1);
                llIngredientsContent.setVisibility(toSelectStatus1 ? View.VISIBLE : View.GONE);
                break;
            case R.id.llNutrientsInfo:
                boolean toSelectStatus2 = !view.isSelected();//true为展开，false为隐藏
                view.setSelected(toSelectStatus2);
                imgNutrientsArrow.setSelected(toSelectStatus2);
                llNutrientsContent.setVisibility(toSelectStatus2 ? View.VISIBLE : View.GONE);
                break;
            case R.id.tvBuy:
                if (AccountManager.getViomiUser(mContext) == null) {
                    ToastUtil.show("请先登录");
                    startActivity(new Intent(mContext, ScanLoginActivity.class));
                } else {
                    startActivity(new Intent(mContext, OneKeyBuyActivity.class));
                }
                break;
            case R.id.tvOk:
                if (preHeating.size() > 0) {//有预热
                    if (Global.isPrepareOk(preHeating)) {
                        if (SerialManager.getInstance().sendCmdStart(preHeating)) {
                            HawkParams.setFoodName(mFoodDetail.getName());
                            HawkParams.setCookFrom(FromType.FROM_RECIPE.value);
                            HawkParams.setCookStep(mDeviceRunSteps);
                            PreHeatingActivity.actionStart(mContext, preHeating.get(0).getTemp());
                            finish();
                        }
                    }
                } else if (mDeviceRunSteps.size() > 0) {//直接烹饪
                    if (Global.isPrepareOk(mDeviceRunSteps)) {
                        if (SerialManager.getInstance().sendCmdStart(mDeviceRunSteps)) {
                            HawkParams.setFoodName(mFoodDetail.getName());
                            HawkParams.setCookFrom(FromType.FROM_RECIPE.value);
                            //VoiceManager.getInstance().startSpeak("现在开始烹饪" + mFoodDetail.getName());
                            startActivity(new Intent(mContext, CookRunningActivity.class));
                        }
                    }
                } else
                    showToast("缺乏烹饪数据！");
                break;
        }
    }
}
