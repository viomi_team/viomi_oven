package com.viomi.oven.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.tencent.bugly.crashreport.CrashReport;
import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.bean.ViomiUser;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.dialog.CommonConfirmDialog;
import com.viomi.oven.dialog.NumberPickerDialog;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.interfaces.AppCallback;
import com.viomi.oven.manager.AccountManager;
import com.viomi.oven.manager.DeviceManager;
import com.viomi.oven.util.ApkUtil;
import com.viomi.oven.util.ImgUtil;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.NetworkUtils;
import com.viomi.oven.util.ToastUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

public class SettingActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgLogo)
    ImageView imgLogo;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvNikName)
    TextView tvNikName;
    @BindView(R.id.tvAccount)
    TextView tvAccount;
    @BindView(R.id.imgBtnLogin)
    ImageView imgBtnLogin;
    @BindView(R.id.tvWifi)
    TextView tvWifi;
    @BindView(R.id.tvStandbyTime)
    TextView tvStandbyTime;
    @BindView(R.id.imgSwVoice)
    ImageView imgSwVoice;
    @BindView(R.id.imgSwLock)
    ImageView imgSwLock;
    @BindView(R.id.tvNoLogin)
    TextView tvNoLogin;
    @BindView(R.id.llLogined)
    LinearLayout llLogined;
    @BindView(R.id.llDateTime)
    LinearLayout llDateTime;
    @BindView(R.id.tvDateTime)
    TextView tvDateTime;
    @BindView(R.id.tvVersion)
    TextView tvVersion;
    @BindView(R.id.llVersion)
    LinearLayout llVersion;
    @BindView(R.id.llMiLogin)
    LinearLayout llMiLogin;
    @BindView(R.id.tvReStart)
    TextView tvReStart;
    @BindView(R.id.tvXunFei)
    TextView tvXunFei;
    @BindView(R.id.llLockScreen)
    LinearLayout llLockScreen;
    @BindView(R.id.llWifi)
    LinearLayout llWifi;
    @BindView(R.id.llStandbyTime)
    LinearLayout llStandbyTime;
    CommonConfirmDialog mCommonConfirmDialog;
    NumberPickerDialog mNumberPickerDialog;
    TimePickerView pvCustomTime;
    //参数
    ViomiUser user;
    List<String> timeData = new ArrayList<>();
    List<String> timeDataHead = Arrays.asList("1","2","5","8");
    private static final int REQ_LOGIN = 1000;

    @Override
    protected int getLayoutRes() {
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            return R.layout.activity_setting_v;
        else
            return R.layout.activity_setting;
    }

    @Override
    protected void initView() {
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V2)) {
            tvTitle.setText("设置");
            tvXunFei.setVisibility(View.GONE);
        }
        mCommonConfirmDialog = new CommonConfirmDialog(mContext);
        mCommonConfirmDialog.setCallBack(new CommonConfirmDialog.CommonConfirmDialogCallBack() {
            @Override
            public void btnOk() {
                Intent intent = new Intent("com.unilife.fridge.action.dameon.reboot");
                sendBroadcast(intent);
            }
        });
        imgSwVoice.setSelected(HawkParams.getVoiceOpenStatus());
        imgSwLock.setSelected(HawkParams.getLockScreenStatus());
        tvVersion.setText("v" + ApkUtil.getVersionCode());
        tvStandbyTime.setText(HawkParams.getScreenTime() / 60 + "分钟");
        mNumberPickerDialog = new NumberPickerDialog(mContext);
        mNumberPickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (mNumberPickerDialog.isChanged) {
                    int time = Integer.parseInt(timeData.get(mNumberPickerDialog.getPos()).toString());
                    HawkParams.setScreenTime(time * 60);
                    tvStandbyTime.setText(HawkParams.getScreenTime() / 60 + "分钟");
                    LogUtils.d(TAG, " the time is:" + time);
                }
            }
        });
        initCustomTimePicker();
        showTime(System.currentTimeMillis());
    }

    private void initCustomTimePicker() {
        // 注意：自定义布局中，id为 optionspicker 或者 timepicker 的布局以及其子控件必须要有，否则会报空指针
        //因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
        //控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
        Calendar selectedDate = Calendar.getInstance();//系统当前时间
        Calendar startDate = Calendar.getInstance();
        startDate.set(2017, 0, 1);
        Calendar endDate = Calendar.getInstance();
        endDate.set(2027, 2, 28);
        pvCustomTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                setSystemTime(date.getTime());
            }
        })
                .setTextColorCenter(Color.BLACK)
                .setTextColorOut(Color.GRAY)
                .setContentSize(26)
                .setTitleSize(26)
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                .setLayoutRes(R.layout.system_time_setting_layout, new CustomListener() {

                    @Override
                    public void customLayout(View v) {
                        final TextView tvSubmit = (TextView) v.findViewById(R.id.yes_set);
                        TextView ivCancel = (TextView) v.findViewById(R.id.no_set);
                        tvSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvCustomTime.returnData();
                                //sendBroadcast(new Intent(AppConfig.ACTION_FOOD_MANAGE_UPDATE));
                            }
                        });
                        ivCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvCustomTime.dismiss();
                            }
                        });
                    }
                })
                .setDividerColor(getResources().getColor(R.color.cyan))
                .build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        HawkParams.setWorkStatus(WorkStatus.STATUS_IDLE);
        WifiInfo info = ((WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE)).getConnectionInfo();
        if (info != null && info.getBSSID() != null && NetworkUtils.isAvailable())
            tvWifi.setText(info.getSSID().replace("\"", ""));
        else
            tvWifi.setText("未连接");
    }

    @Override
    protected void initData() {
        initUser();
    }

    public void initUser() {
        user = AccountManager.getViomiUser(this);
        if (user == null) {
            imgBtnLogin.setSelected(false);
            tvNoLogin.setVisibility(View.VISIBLE);
            llLogined.setVisibility(View.GONE);
            imgLogo.setImageResource(R.drawable.logo_viomi_oven);
        } else {
            imgBtnLogin.setSelected(true);
            tvNoLogin.setVisibility(View.GONE);
            llLogined.setVisibility(View.VISIBLE);
            tvNikName.setText(user.getNickname());
            tvAccount.setText(getString(R.string.title_viomi_account) + user.getAccount());
            CrashReport.setUserId(user.getAccount() + "_" + user.getMobile());
            ImgUtil.showDefinedCircleImage(user.getHeadImg(), imgLogo, R.drawable.logo_viomi_oven_active);
        }
    }

    public void loginOut() {
        CommonConfirmDialog logoutDialog = new CommonConfirmDialog(mContext);
        logoutDialog.setCallBack(new CommonConfirmDialog.CommonConfirmDialogCallBack() {
            @Override
            public void btnOk() {
                DeviceManager.getInstance().unBindDevice(mContext, new AppCallback<String>() {
                    @Override
                    public void onSuccess(String data) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                initUser();
                                LogUtils.d(TAG, "loginOut success");
                                ToastUtil.show(getString(R.string.text_device_unbind_success));
                            }
                        });
                    }

                    @Override
                    public void onFail(int errorCode, String msg) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                LogUtils.d(TAG, "loginOut fail");
                                ToastUtil.show(getString(R.string.text_device_unbind_fail));
                            }
                        });
                    }
                });
            }
        });
        logoutDialog.setTitle("确定退出登录吗？");
        logoutDialog.show();
    }

    @OnClick({R.id.imgBtnLogin, R.id.llWifi, R.id.llStandbyTime, R.id.imgSwVoice, R.id.imgSwLock, R.id.llVersion, R.id.tvReStart, R.id.llMiLogin, R.id.llDateTime})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBtnLogin:
                if (!view.isSelected())
                    ScanLoginActivity.actionStartForResult(mContext, REQ_LOGIN);
                else
                    loginOut();
                break;
            case R.id.llWifi:
                startActivity(new Intent(mContext, WifiScanActivity.class));
                break;
            case R.id.llStandbyTime://待机时间
                timeData.clear();
                timeData.addAll(timeDataHead);
                for (int i = 10; i <= 30; i++) {
                    timeData.add(i + "");
                }
                mNumberPickerDialog.update(timeData);
                mNumberPickerDialog.show(HawkParams.getScreenTime() / 60 + "", "分");
                break;
            case R.id.imgSwVoice://语音播报
                HawkParams.setVoiceOpenStatus(!HawkParams.getVoiceOpenStatus());
                imgSwVoice.setSelected(HawkParams.getVoiceOpenStatus());
                break;
            case R.id.imgSwLock://锁屏
                HawkParams.setLockScreenStatuc(!HawkParams.getLockScreenStatus());
                imgSwLock.setSelected(HawkParams.getLockScreenStatus());
                break;
            case R.id.llVersion://版本
                startActivity(new Intent(mContext, VersionManagerActivity.class));
                break;
            case R.id.tvReStart://重启
                mCommonConfirmDialog.setTitle("是否确认重启系统");
                mCommonConfirmDialog.show();
                break;
            case R.id.llMiLogin:
                break;
            case R.id.llDateTime:
                pvCustomTime.show();
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQ_LOGIN) {//登录成功
                initUser();
            }
        }
    }

    private void showTime(long time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        Date date = new Date(time);
        tvDateTime.setText(simpleDateFormat.format(date));
    }

    private void setSystemTime(long time) {
        showTime(time);
        Intent intent = new Intent();
        intent.setAction("android.intent.action.USER_SET_TIME");
        intent.putExtra("time", time);
        sendBroadcast(intent);
    }
}
