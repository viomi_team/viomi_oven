package com.viomi.oven.activity;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.otto.Subscribe;
import com.viomi.oven.AppParam;
import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.device.SerialInfo;
import com.viomi.oven.dialog.CommonConfirmDialog;
import com.viomi.oven.enumType.McuStatus;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.otto.EventUpgradeCurve;
import com.viomi.oven.util.ApkUtil;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.LogUtils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 老化模式界面
 */
public class AgingModeActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgLight)
    ImageView imgLight;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvAgingTest)
    TextView tvAgingTest;
    @BindView(R.id.tvPaishui)
    TextView tvPaishui;
    @BindView(R.id.tvResume)
    TextView tvResume;
    @BindView(R.id.tvAgingStatus)
    TextView tvAgingStatus;
    @BindView(R.id.tvType)
    TextView tvType;
    @BindView(R.id.tvApkVer)
    TextView tvApkVer;
    @BindView(R.id.tvMcuVer)
    TextView tvMcuVer;
    @BindView(R.id.tvOvenTemp)
    TextView tvOvenTemp;
    @BindView(R.id.tvBakeTemp)
    TextView tvBakeTemp;
    @BindView(R.id.tvSteamTemp)
    TextView tvSteamTemp;
    @BindView(R.id.tvBoilTemp)
    TextView tvBoilTemp;
    @BindView(R.id.tvElectronic)
    TextView tvElectronic;
    @BindView(R.id.llProgress)
    LinearLayout llProgress;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.tvER_DOOR)
    TextView tvERDOOR;
    @BindView(R.id.tvER_CONNECT)
    TextView tvER_CONNECT;
    @BindView(R.id.tvER_OVEN_NTC)
    TextView tvER_OVEN_NTC;
    @BindView(R.id.tvER_EVAPORATING_HIGH_TEMP)
    TextView tvER_EVAPORATING_HIGH_TEMP;
    @BindView(R.id.tvER_EVAPORATING_NTC)
    TextView tvER_EVAPORATING_NTC;
    @BindView(R.id.tvER_EVAPORATING_LOW_TEMPER)
    TextView tvER_EVAPORATING_LOW_TEMPER;
    @BindView(R.id.tvER_BOIL_NTC)
    TextView tvER_BOIL_NTC;
    @BindView(R.id.tvER_BOIL_HEATING)
    TextView tvER_BOIL_HEATING;
    @BindView(R.id.tvER_BAKE_NTC)
    TextView tvER_BAKE_NTC;
    @BindView(R.id.tvER_LACK_WATER)
    TextView tvER_LACK_WATER;
    @BindView(R.id.tvER_OVEN_LOW_TEMPER)
    TextView tvER_OVEN_LOW_TEMPER;
    @BindView(R.id.tvER_OVEN_HIGH_TEMPER)
    TextView tvER_OVEN_HIGH_TEMPER;
    CommonConfirmDialog mCommonConfirmDialog;
    //参数
    int timeLengthSec = 3600;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_aging_mode;
    }

    @Override
    protected void initView() {
        tvTitle.setText("工厂老化测试");
        mCommonConfirmDialog = new CommonConfirmDialog(mContext);
        mCommonConfirmDialog.setCallBack(new CommonConfirmDialog.CommonConfirmDialogCallBack() {
            @Override
            public void btnOk() {
                if (SerialManager.getInstance().sendCmdOver())
                    finish();
            }
        });
        imgBack.setSelected(true);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (HawkParams.getWorkStatus() == WorkStatus.STATUS_WORKING.value) {
                    mCommonConfirmDialog.setTitle("是否确认退出老化测试");
                    mCommonConfirmDialog.show();
                } else {
                    if (SerialManager.getInstance().sendCmdOver())
                        finish();
                }
            }
        });
        tvType.setText("型号:" + (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? "台面式" : "嵌入式"));
        tvApkVer.setText("APK版本:v" + ApkUtil.getVersionCode());
        tvMcuVer.setText("固件版本:" + Global.verString(HawkParams.getMcuVer()));
    }

    @Override
    protected void initData() {
    }

    @Override
    protected void onDestroy() {
        HawkParams.setWorkStatus(WorkStatus.STATUS_IDLE);
        super.onDestroy();
    }

    @OnClick({R.id.tvAgingTest, R.id.tvPaishui, R.id.tvResume})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvAgingTest:
                if (!tvAgingTest.isSelected()) {
                    if (SerialManager.getInstance().sendCmdAgingTest()) {
                        tvPaishui.setVisibility(View.GONE);
                        tvAgingTest.setText("退出老化测试");
                        tvAgingTest.setSelected(true);
                    }
                    //else
                    //    showToast("设置失败");
                } else {//退出老化
                    if (HawkParams.getWorkStatus() == WorkStatus.STATUS_WORKING.value) {
                        mCommonConfirmDialog.setTitle("是否确认退出老化测试");
                        mCommonConfirmDialog.show();
                    } else {
                        if (SerialManager.getInstance().sendCmdOver())
                            finish();
                    }
                }
                break;
            case R.id.tvPaishui:
                SerialManager.getInstance().sendCmdAgingDrainage();
                break;
            case R.id.tvResume:
                if (SerialManager.getInstance().sendCmdResumeRun()) {
                    tvResume.setVisibility(View.GONE);
                    tvPaishui.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Subscribe
    public void receivedEvent(EventUpgradeCurve event) {
        LogUtils.d("receiver event from is:" + event.getBusFrom() + " the time is:" + event.getCurveRunStep().getTimeSec());
        imgLight.setSelected(AppParam.isLightOn);

        if (event.getCurveRunStep() != null) {//更新曲线
            llProgress.setVisibility(View.VISIBLE);
            progressBar.setProgress((int) (event.getCurveRunStep().getTimeSec() * 100 * 1.0 / timeLengthSec));
            if (AppParam.mcuStatus == McuStatus.MCU_WORKING) {
                tvAgingStatus.setTextColor(Color.WHITE);
                if (timeLengthSec / 60 > event.getCurveRunStep().getTimeMinute())
                    tvAgingStatus.setText("老化状态(剩余：" + (timeLengthSec / 60 - event.getCurveRunStep().getTimeMinute()) + "分钟)");
                else
                    tvAgingStatus.setText("老化状态(剩余：1分钟)");
            }
            //判断烹饪是否完成
            if (event.getCurveRunStep().getStage() == AppStaticConfig.COOKING_FINISH_STAGE) {
                tvAgingStatus.setTextColor(Color.YELLOW);
                tvAgingStatus.setText("老化完成");
                progressBar.setProgress(100);
                showToast("老化测试完成");
            } else {//除垢中
                tvOvenTemp.setText("当前腔体温度：" + SerialManager.getInstance().getDataReceiveInfo().getOvenTemp() + "℃");
                tvBakeTemp.setText("当前底部烤温度：" + SerialManager.getInstance().getDataReceiveInfo().getBakeTemp() + "℃");
                tvSteamTemp.setText("当前蒸发盘温度：" + SerialManager.getInstance().getDataReceiveInfo().getSteamTemp() + "℃");
                tvBoilTemp.setText("当前煮水盘度：" + SerialManager.getInstance().getDataReceiveInfo().getBoilTemp() + "℃");
                tvElectronic.setText("当前电流值：" + String.format("%.2f", SerialManager.getInstance().getDataReceiveInfo().getElectricityValue()) + "A");
                if (SerialManager.getInstance().latestErrInfo.contains(SerialInfo.ErrCode.ER_DOOR.valueString))
                    tvERDOOR.setVisibility(View.VISIBLE);
                if (SerialManager.getInstance().latestErrInfo.contains(SerialInfo.ErrCode.ER_CONNECT.valueString))
                    tvER_CONNECT.setVisibility(View.VISIBLE);
                if (SerialManager.getInstance().latestErrInfo.contains(SerialInfo.ErrCode.ER_OVEN_NTC.valueString))
                    tvER_OVEN_NTC.setVisibility(View.VISIBLE);
                if (SerialManager.getInstance().latestErrInfo.contains(SerialInfo.ErrCode.ER_EVAPORATING_HIGH_TEMP.valueString))
                    tvER_EVAPORATING_HIGH_TEMP.setVisibility(View.VISIBLE);
                if (SerialManager.getInstance().latestErrInfo.contains(SerialInfo.ErrCode.ER_EVAPORATING_NTC.valueString))
                    tvER_EVAPORATING_NTC.setVisibility(View.VISIBLE);
                if (SerialManager.getInstance().latestErrInfo.contains(SerialInfo.ErrCode.ER_EVAPORATING_LOW_TEMPER.valueString))
                    tvER_EVAPORATING_LOW_TEMPER.setVisibility(View.VISIBLE);
                if (SerialManager.getInstance().latestErrInfo.contains(SerialInfo.ErrCode.ER_BOIL_NTC.valueString))
                    tvER_BOIL_NTC.setVisibility(View.VISIBLE);
                if (SerialManager.getInstance().latestErrInfo.contains(SerialInfo.ErrCode.ER_BOIL_HEATING.valueString))
                    tvER_BOIL_HEATING.setVisibility(View.VISIBLE);
                if (SerialManager.getInstance().latestErrInfo.contains(SerialInfo.ErrCode.ER_BAKE_NTC.valueString))
                    tvER_BAKE_NTC.setVisibility(View.VISIBLE);
                if (SerialManager.getInstance().latestErrInfo.contains(SerialInfo.ErrCode.ER_LACK_WATER.valueString))
                    tvER_LACK_WATER.setVisibility(View.VISIBLE);
                if (SerialManager.getInstance().latestErrInfo.contains(SerialInfo.ErrCode.ER_OVEN_LOW_TEMPER.valueString))
                    tvER_OVEN_LOW_TEMPER.setVisibility(View.VISIBLE);
                if (SerialManager.getInstance().latestErrInfo.contains(SerialInfo.ErrCode.ER_OVEN_HIGH_TEMPER.valueString))
                    tvER_OVEN_HIGH_TEMPER.setVisibility(View.VISIBLE);


                ////运行过程中出现异常(平板烹饪中但MCU未工作)
                if ((HawkParams.getPauseStatus() == 0 && AppParam.mcuStatus == McuStatus.MCU_SUSPENDED)) {
                    if (SerialManager.getInstance().sendCmdStop()) {
                        tvResume.setVisibility(View.VISIBLE);
                        tvAgingStatus.setTextColor(Color.YELLOW);
                        tvAgingStatus.setText("老化暂停");
                        //tvPaishui.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }
}
