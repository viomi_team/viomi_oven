package com.viomi.oven.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.otto.Subscribe;
import com.viomi.oven.AppParam;
import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.dialog.CommonConfirmDialog;
import com.viomi.oven.dialog.WarnInfoDialog;
import com.viomi.oven.enumType.McuStatus;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.manager.VoiceManager;
import com.viomi.oven.otto.Event2PauseStatus;
import com.viomi.oven.otto.EventUpgradeCurve;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.ToastUtil;
import com.viomi.oven.view.BubbleLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Ljh on 2018/2/1
 * 预热中
 */
public class PreHeatingActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgLight)
    ImageView imgLight;
    @BindView(R.id.tvTemp)
    TextView tvTemp;
    @BindView(R.id.tvWarn)
    TextView tvWarn;
    @BindView(R.id.tvInfo)
    TextView tvInfo;
    @BindView(R.id.tvOver)
    TextView tvOver;
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    @BindView(R.id.tvbtnPause)
    TextView tvbtnPause;
    @BindView(R.id.tvBtnOver)
    TextView tvBtnOver;
    @BindView(R.id.llPauseStop)
    LinearLayout llPauseStop;
    @BindView(R.id.imgStart)
    ImageView imgStart;
    @BindView(R.id.rlStart)
    RelativeLayout rlStart;
    @BindView(R.id.bubble)
    BubbleLayout bubble;
    CommonConfirmDialog mCommonConfirmDialog;
    WarnInfoDialog mWarnInfoDialog;
    //参数
    int will2Temp = 0;
    boolean heatingFinish = false;
    private String stopSound = "预热结束，请放入食材，开始你的表演";
    String latestErrInfo = "";
    private final int RESUME_START = 1000;
    private boolean clicked2Pause = false;

    public static void actionStart(Context context, int will2Temp) {
        Intent intent = new Intent(context, PreHeatingActivity.class);
        intent.putExtra("will2Temp", will2Temp);
        context.startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        imgLight.setSelected(true);
    }

    @Override
    protected int getLayoutRes() {
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            return R.layout.activity_preheating_v;
        else
            return R.layout.activity_preheating;
    }

    @Override
    protected void initView() {
        mWarnInfoDialog = new WarnInfoDialog(mContext);
        mCommonConfirmDialog = new CommonConfirmDialog(mContext);
        mCommonConfirmDialog.setTitle("是否确认结束预热");
        mCommonConfirmDialog.setCallBack(new CommonConfirmDialog.CommonConfirmDialogCallBack() {
            @Override
            public void btnOk() {
                if (SerialManager.getInstance().sendCmdOver()) {
                    Global.jump2MainActivity(mContext);
                }
            }
        });
        VoiceManager.getInstance().startSpeak("开始预热，请不要放入你的食材哦");
        imgBack.setSelected(true);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.open_pannel);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SerialManager.getInstance().sendCmdControlPanelSwitch(true);
            }
        });
        bubble.setBubblesRun(true);
        bubble.setTemp(70);
    }

    @Override
    protected void initData() {
        HawkParams.setWorkStatus(WorkStatus.STATUS_WORKING);
        HawkParams.setPauseStatus(0);
        HawkParams.setMode(SteamBakeType.PREHEATING);
        will2Temp = getIntent().getIntExtra("will2Temp", 0);
        tvInfo.setText("目标温度：" + will2Temp + "℃");
    }

    @OnClick({R.id.tvbtnPause, R.id.tvBtnOver, R.id.imgStart, R.id.tvOver})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvbtnPause:
                if (view.isSelected()) {//恢复预热
                    if (AppParam.isDoorOpen) {
                        ToastUtil.show("请关门后再启动烹饪");
                    } else {
                        if (SerialManager.getInstance().sendCmdResumeRun()) {
                            view.setSelected(false);
                            clicked2Pause = false;
                            tvStatus.setText("预热中");
                        }
                    }
                } else {//停止运行
                    if (SerialManager.getInstance().sendCmdStop()) {
                        clicked2Pause = true;
                        view.setSelected(true);
                        tvStatus.setText("预热暂停");
                    }
                }
                break;
            case R.id.tvBtnOver:
                mCommonConfirmDialog.show();
                break;
            case R.id.imgStart:
                List<DeviceRunStep> mSteps = HawkParams.getCookStep();
                if (Global.isPrepareOk(mSteps)) {
                    if (SerialManager.getInstance().sendCmdStart(mSteps)) {
                        //VoiceManager.getInstance().startSpeak("现在开始烹饪" + HawkParams.getFoodName());
                        startActivity(new Intent(mContext, CookRunningActivity.class));
                        finish();
                    }
                }
                break;
            case R.id.tvOver:
                Global.jump2MainActivity(mContext);
                break;
        }
    }

    @Override
    public void dealTheMsg(Message msg) {
        super.dealTheMsg(msg);
        if (msg.what == RESUME_START) {
            if (latestErrInfo.length() == 0 && AppParam.mcuStatus != McuStatus.MCU_WORKING && tvbtnPause.isSelected() && !clicked2Pause)
                tvbtnPause.performClick();
        }
    }

    @Subscribe
    public void receivedEvent(EventUpgradeCurve event) {
        LogUtils.d("receiver event from is" + event.getBusFrom());
        imgLight.setSelected(AppParam.isLightOn);
        if (event.getCurveRunStep() != null) {//更新温度
            tvTemp.setText((int) event.getCurveRunStep().getTemp() + "");
            if(event.getCurveRunStep().getTemp() > 300)
                tvTemp.setText("---");
            if (/*will2Temp <= (int) event.getCurveRunStep().getTemp() ||*/ event.getCurveRunStep().getStage() == AppStaticConfig.COOKING_FINISH_STAGE) {//完成预热
                //HawkParams.setS_temp(0);
                tvTemp.setText(will2Temp + "");
                if (SerialManager.getInstance().sendCmdOver()) {
                    HawkParams.setWorkStatus(WorkStatus.STATUS_FINISH);
                    if (HawkParams.getCookStep() == null || HawkParams.getCookStep().size() == 0) {//单纯的预热
                        //Global.jump2MainActivity(mContext);
                        //ToastUtil.show("预热完成");
                        //VoiceManager.getInstance().startSpeak("预热完成");
                        heatingFinish = true;
                        llPauseStop.setVisibility(View.GONE);
                        tvOver.setVisibility(View.VISIBLE);
                        tvStatus.setText("预热完成");
                        VoiceManager.getInstance().startSpeak("预热结束");
                        tvWarn.setText("预热完成啦");
                        tvInfo.setText("已达到目标温度");


                    } else {//预热完成后进入下一步烹饪
                        heatingFinish = true;
                        llPauseStop.setVisibility(View.GONE);
                        rlStart.setVisibility(View.VISIBLE);
                        tvStatus.setText("预热完成");
                        VoiceManager.getInstance().startSpeak(stopSound);
                        tvWarn.setText("预热完成啦，请放入食材");
                        tvInfo.setText("已达到目标温度");
                    }
                }
            } else {//预热中
                if (!SerialManager.getInstance().latestErrInfo.equals(latestErrInfo)) {
                    if (SerialManager.getInstance().latestErrInfo.length() == 0) {
                        mWarnInfoDialog.cancel();
                    } else
                        mWarnInfoDialog.showInfo(SerialManager.getInstance().latestErrInfo);
                    if (latestErrInfo.equals("门打开") && SerialManager.getInstance().latestErrInfo.length() == 0 && AppParam.isDoorOpen == false) {//
                        sendToHandlerDelayed(RESUME_START, 800);
                    }
                    latestErrInfo = SerialManager.getInstance().latestErrInfo;
                }
                //运行过程中出现异常(平板烹饪中但MCU未工作)
                if ((HawkParams.getPauseStatus() == 0 && AppParam.mcuStatus == McuStatus.MCU_SUSPENDED)) {
                    if (SerialManager.getInstance().sendCmdStop()) {
                        tvbtnPause.setSelected(true);
                        tvStatus.setText("预热暂停");
                    }
                }
            }
        }
    }

    @Subscribe
    public void receiveEvent(Event2PauseStatus event) {
        LogUtils.d("receiver event from is" + event.getBusFrom() + " the time is:");
        if (HawkParams.getWorkStatus() == WorkStatus.STATUS_WORKING.value) {
            if (event.getPauseStatus() == 1) {//暂停指令
                if (tvbtnPause.isSelected() == false)//当前为正常状态
                    tvbtnPause.performClick();
                else
                    LogUtils.e(TAG, "Event2PauseStatus指令不匹配");
            } else if (event.getPauseStatus() == 0) {//恢复运行指令
                if (tvbtnPause.isSelected() == true)
                    tvbtnPause.performClick();
                else
                    LogUtils.e(TAG, "Event2PauseStatus指令不匹配");
            } else if (event.getPauseStatus() == 2) {//结束运行指令
                if (SerialManager.getInstance().sendCmdOver()) {
                    Global.jump2MainActivity(mContext);
                    finish();
                }
            }
            // else if (event.getPauseStatus() == 3) {//进入下一步，开始烹饪
            //    if (heatingFinish)
            //        imgStart.performClick();
            //}
        } else if (HawkParams.getWorkStatus() == WorkStatus.STATUS_FINISH.value) {
            if (event.getPauseStatus() == 3) {//进入下一步，开始烹饪
                if (heatingFinish)
                    imgStart.performClick();
            }else if (event.getPauseStatus() == 2){
                Global.jump2MainActivity(mContext);
            }
        }
    }
}
