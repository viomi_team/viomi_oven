package com.viomi.oven.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.otto.Subscribe;
import com.viomi.oven.AppParam;
import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.bean.BaseResult;
import com.viomi.oven.bean.BasicKeyInfo;
import com.viomi.oven.bean.CurveRunStep;
import com.viomi.oven.bean.FoodDetail;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.dialog.CommonConfirmDialog;
import com.viomi.oven.dialog.StageInfoDialog;
import com.viomi.oven.dialog.WarnInfoDialog;
import com.viomi.oven.enumType.McuStatus;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.manager.VoiceManager;
import com.viomi.oven.otto.Event2PauseStatus;
import com.viomi.oven.otto.EventUpgradeCurve;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.StringUtil;
import com.viomi.oven.util.http.HttpNoToastReCallback;
import com.viomi.oven.view.CurveRunningView;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Ljh on 2018/1/3
 * 运行曲线
 */
public class CookRunningActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgLight)
    ImageView imgLight;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvSmallTitle)
    TextView tvSmallTitle;
    @BindView(R.id.tvTemp)
    TextView tvTemp;
    @BindView(R.id.tvSetTemp)
    TextView tvSetTemp;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.curveView)
    CurveRunningView curveView;
    @BindView(R.id.tvTotalTime)
    TextView tvTotalTime;
    //@BindView(R.id.tvTotalTime1)
    //TextView tvTotalTime1;
    @BindView(R.id.tvbtnPause)
    TextView tvbtnPause;
    @BindView(R.id.tvBtnOver)
    TextView tvBtnOver;
    @BindView(R.id.tvMode)
    TextView tvMode;
    @BindView(R.id.root)
    LinearLayout root;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.rlPointContent)
    RelativeLayout rlPointContent;
    @BindView(R.id.llModeContent)
    LinearLayout llModeContent;
    @BindView(R.id.tvStopTip)
    TextView tvStopTip;
    //
    CommonConfirmDialog mCommonConfirmDialog;
    //CookFinishDialog mCookFinishDialog;
    WarnInfoDialog mWarnInfoDialog;
    StageInfoDialog mStageInfoDialog;//阶段信息
    //参数
    List<CurveRunStep> mCurveRunSteps = new ArrayList<>();
    List<DeviceRunStep> mDeviceRunSteps = new ArrayList<>();
    private int timeLengthSec = 0;//单位s
    float collectInteval = 0;//每隔多少秒采集一个点
    boolean needStop = false;
    String latestErrInfo = "";
    private CurveRunStep lastCurveRunStep;
    private final int RESUME_START = 1000;
    private boolean clicked2Pause = false;
    private String specialModeName = "";

    @Override
    protected void onResume() {
        super.onResume();
        imgLight.setSelected(true);
    }

    @Override
    protected int getLayoutRes() {
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            return R.layout.activity_cook_running_v;
        else
            return R.layout.activity_cook_running;
    }

    @Override
    protected void initView() {
        mStageInfoDialog = new StageInfoDialog(mContext);
        imgBack.setSelected(true);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.open_pannel);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SerialManager.getInstance().sendCmdControlPanelSwitch(true);
            }
        });
        mWarnInfoDialog = new WarnInfoDialog(mContext);
        curveView.setShowBgPoint(true);
        mCommonConfirmDialog = new CommonConfirmDialog(mContext);
        mCommonConfirmDialog.setCallBack(new CommonConfirmDialog.CommonConfirmDialogCallBack() {
            @Override
            public void btnOk() {
                if (mCommonConfirmDialog.getFlag().equals("0")) {//停止烹饪
                    if (SerialManager.getInstance().sendCmdStop()) {
                        clicked2Pause = true;
                        tvbtnPause.setSelected(true);
                        tvSmallTitle.setText("已暂停");
                        tvSmallTitle.setVisibility(View.VISIBLE);
                    }
                    //else {
                    //    ToastUtil.show("通讯异常");
                    //}
                } else {//结束烹饪
                    if (SerialManager.getInstance().sendCmdOver()) {//结束烹饪
                        Global.jump2MainActivity(mContext);
                        finish();
                    }
                    //else {
                    //    ToastUtil.show("通讯异常");
                    //}
                }
            }
        });
        llModeContent.setTag(0);
    }

    @Override
    protected void initData() {
        HawkParams.setCookStartTime(System.currentTimeMillis());
        HawkParams.setWorkStatus(WorkStatus.STATUS_WORKING);
        HawkParams.setPauseStatus(0);
        HawkParams.setC_temps("");
        timeLengthSec = HawkParams.getC_totletime();
        mDeviceRunSteps = HawkParams.getCookStep();
        tvMode.setText(mDeviceRunSteps.get(0).getType().name + "模式");
        collectInteval = ((float) timeLengthSec) / curveView.getShowMaxxCount();//s为单位
        //
        tvTotalTime.setText("共" + (timeLengthSec / 60) + " min");
        //tvTotalTime1.setText(timeLengthSec / 60 + "");
        String foodName = HawkParams.getFoodName();
        tvTitle.setText(HawkParams.getFoodName());
        showPoint();
        if (needStop && !StringUtil.isEmpty(HawkParams.getFoodId()))
            httpGetVoiceInfo(HawkParams.getFoodId());
        for (SteamBakeType type : SteamBakeType.values()) {
            if (type.name.equals(foodName)) {
                specialModeName = foodName;
                VoiceManager.getInstance().startSpeak("现在开始" + specialModeName);
                return;
            }
        }
        if (!foodName.equals("专业烹饪")) {
            VoiceManager.getInstance().startSpeak("现在开始烹饪" + foodName);
        } else
            VoiceManager.getInstance().startSpeak("现在开始烹饪");
    }

    public void httpGetVoiceInfo(String foodId) {
        OkHttpUtils
                .get()
                .url((foodId.length() > 8 ? AppStaticConfig.URL_FOOD_QR_DETAIL : AppStaticConfig.URL_FOOD_DETAIL) + foodId)
                .tag(this)
                .build()
                .execute(new HttpNoToastReCallback<BaseResult<FoodDetail>>() {
                    @Override
                    public void onSuccess(BaseResult<FoodDetail> result) {
                        if (result.getCode() == 100) {
                            FoodDetail foodDetail = result.getResult();
                            if (foodDetail != null) {
                                if (foodDetail.getRecipeDirectionList().size() > 0) {
                                    if (foodDetail.getRecipeDirectionList().get(0).getMode() == SteamBakeType.PREHEATING.value)
                                        foodDetail.getRecipeDirectionList().remove(0);
                                    for (int i = 0; i < foodDetail.getRecipeDirectionList().size(); i++) {
                                        mDeviceRunSteps.get(i).setStoptip(foodDetail.getRecipeDirectionList().get(i).getStoptip());
                                    }
                                }
                                for (BasicKeyInfo keyInfo : foodDetail.getBasicInfo()) {
                                    if (keyInfo.getName().equals("产品重量")) {
                                        HawkParams.setWeight(keyInfo.getValue());
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onFail(int errorCode) {

                    }
                });
    }

    //添加阶段点
    public void showPoint() {
        rlPointContent.removeAllViews();
        int timeStageLeng = 0;
        for (int i = 0; i < mDeviceRunSteps.size(); i++) {
            if (mDeviceRunSteps.get(i).getStop() == 1 && StringUtil.isEmpty(mDeviceRunSteps.get(i).getStoptip()))
                needStop = true;
            RelativeLayout rl_item = (RelativeLayout) ((Activity) mContext).getLayoutInflater()
                    .inflate(R.layout.item_step, null, false);
            ((TextView) rl_item.findViewById(R.id.tvStep)).setText((i + 1) + "");
            rl_item.setTag(i + "");
            rl_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int stage = Integer.parseInt(v.getTag().toString());
                    showStageDetail(stage);
                }
            });
            timeStageLeng += mDeviceRunSteps.get(i).getTime() * 60;
            LogUtils.d(TAG, "showPoint timeLeng:" + timeStageLeng + " timeLengthSec:" + timeLengthSec);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(60, 60);
            int marginLeft = (int) (600 * 1.0 / timeLengthSec * timeStageLeng - 20);
            LogUtils.d(TAG, "showPoint marginLeft:" + marginLeft);
            layoutParams.setMargins(marginLeft, 0, 0, 0);
            rlPointContent.addView(rl_item, layoutParams);
        }
    }

    public void showStageDetail(int stage) {
        mStageInfoDialog.setInfo(mDeviceRunSteps.get(stage).getType().name, mDeviceRunSteps.get(stage).time + "", mDeviceRunSteps.get(stage).temp + "", stage);
        mStageInfoDialog.show();
    }

    @Override
    public void dealTheMsg(Message msg) {
        super.dealTheMsg(msg);
        if (msg.what == RESUME_START) {
            if (latestErrInfo.length() == 0 && AppParam.mcuStatus != McuStatus.MCU_WORKING && tvbtnPause.isSelected() && !clicked2Pause && tvbtnPause != null)
                tvbtnPause.performClick();
        }
    }

    @OnClick({R.id.tvbtnPause, R.id.tvBtnOver, R.id.llModeContent})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvbtnPause:
                if (view.isSelected() == false) {//当前为正常运行状态，则暂停
                    if (StringUtil.isEmpty(specialModeName))
                        mCommonConfirmDialog.setTitle("是否确认暂停烹饪");
                    else
                        mCommonConfirmDialog.setTitle("是否确认暂停" + specialModeName);
                    mCommonConfirmDialog.setFlag("0");
                    mCommonConfirmDialog.show();
                } else {//恢复运行
                    if (Global.isPrepareOk(mDeviceRunSteps)) {
                        if (SerialManager.getInstance().sendCmdResumeRun()) {
                            tvStopTip.setVisibility(View.GONE);
                            llModeContent.setVisibility(View.VISIBLE);
                            clicked2Pause = false;
                            tvSmallTitle.setVisibility(View.GONE);
                            view.setSelected(false);
                        }
                    }
                }
                break;
            case R.id.tvBtnOver:
                if (StringUtil.isEmpty(specialModeName))
                    mCommonConfirmDialog.setTitle("是否确认结束烹饪");
                else
                    mCommonConfirmDialog.setTitle("是否确认结束" + specialModeName);
                mCommonConfirmDialog.setFlag("1");
                mCommonConfirmDialog.show();
                break;
            case R.id.llModeContent:
                showStageDetail(Integer.parseInt(view.getTag().toString()));
                break;
        }
    }

    @Subscribe
    public void receivedEvent(EventUpgradeCurve event) {
        LogUtils.d("receiver event from is:" + event.getBusFrom() + " the time is:" + event.getCurveRunStep().getTimeSec());
        imgLight.setSelected(AppParam.isLightOn);
        if (event.getCurveRunStep() != null) {//更新曲线
            if (mCurveRunSteps.size() == 0) {//第一个点
                mCurveRunSteps.add(event.getCurveRunStep());
                curveView.updata(mCurveRunSteps);
                HawkParams.setC_temps(event.getCurveRunStep().getIntTemp() + "");
                progressBar.setProgress(1);
            } else {//共采集mShownMaxCount = 40个点
                if (event.getCurveRunStep().getTimeSec() >= collectInteval * mCurveRunSteps.size() && mCurveRunSteps
                        .size() < (curveView.getShowMaxxCount() + 3)) {//采集点更新
                    mCurveRunSteps.add(event.getCurveRunStep());
                    curveView.updata(mCurveRunSteps);
                    HawkParams.addC_temps("" + event.getCurveRunStep().getIntTemp());
                    progressBar.setProgress((int) (event.getCurveRunStep().getTimeSec() * 100 * 1.0 / timeLengthSec));
                }
            }
            tvTemp.setText((int) event.getCurveRunStep().getTemp() + "");
            if (event.getCurveRunStep().getTemp() > 300)
                tvTemp.setText("---");
            tvTime.setText((timeLengthSec / 60 - event.getCurveRunStep().getTimeMinute()) + "");//剩余时间
        }
        LogUtils.d(TAG, "mCurveRunSteps size is:" + mCurveRunSteps.size());
        //判断烹饪是否完成
        if (event.getCurveRunStep().getStage() == AppStaticConfig.COOKING_FINISH_STAGE) {
            if (SerialManager.getInstance().sendCmdOver()) {
                HawkParams.setWorkStatus(WorkStatus.STATUS_FINISH);
                startActivity(new Intent(mContext, CookFinishActivity.class));
                finish();
            }
        } else {
            if (event.getCurveRunStep().getStage() >= 1 && event.getCurveRunStep().getStage() <= 4) {//第一到第四阶段
                tvMode.setText(mDeviceRunSteps.get(event.getCurveRunStep().getStage() - 1).getType().name + "模式");
                tvSetTemp.setText(mDeviceRunSteps.get(event.getCurveRunStep().getStage() - 1).getTemp() + "");
                llModeContent.setTag(event.getCurveRunStep().getStage() - 1);
                cumsumEvaporator(mDeviceRunSteps.get(event.getCurveRunStep().getStage() - 1).getType());
                //报警提示
                if (!SerialManager.getInstance().latestErrInfo.equals(latestErrInfo)) {
                    if (SerialManager.getInstance().latestErrInfo.length() == 0) {
                        mWarnInfoDialog.cancel();
                    } else
                        mWarnInfoDialog.showInfo(SerialManager.getInstance().latestErrInfo);
                    if (latestErrInfo.equals("门打开") && SerialManager.getInstance().latestErrInfo.length() == 0 && AppParam.isDoorOpen == false) {//
                        sendToHandlerDelayed(RESUME_START, 800);
                    }
                    latestErrInfo = SerialManager.getInstance().latestErrInfo;
                }
                //烹饪阶段切换时需要中途暂停并进行语音提示
                if (lastCurveRunStep != null && lastCurveRunStep.getStage() != event.getCurveRunStep().getStage()
                        && mDeviceRunSteps.get(lastCurveRunStep.getStage() - 1).getStop() == 1) {//
                    if (SerialManager.getInstance().sendCmdStop()) {
                        clicked2Pause = true;
                        tvStopTip.setText("烹饪提示：" + mDeviceRunSteps.get(lastCurveRunStep.getStage() - 1).getStoptip());
                        tvStopTip.setVisibility(View.VISIBLE);
                        llModeContent.setVisibility(View.GONE);
                        VoiceManager.getInstance().startSpeak(mDeviceRunSteps.get(lastCurveRunStep.getStage() - 1).getStoptip());
                        tvbtnPause.setSelected(true);
                        tvSmallTitle.setText("已暂停");
                        tvSmallTitle.setVisibility(View.VISIBLE);
                    }
                }
                lastCurveRunStep = event.getCurveRunStep();
                //运行过程中出现异常(平板烹饪中但MCU未工作)
                if ((HawkParams.getPauseStatus() == 0 && AppParam.mcuStatus == McuStatus.MCU_SUSPENDED)) {
                    //if (HawkParams.getPauseStatus() == 0 && latestErrInfo.length() > 0) {
                    if (SerialManager.getInstance().sendCmdStop()) {
                        tvbtnPause.setSelected(true);
                        tvSmallTitle.setText("已暂停");
                        tvSmallTitle.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    /**
     * 统计蒸发盘运行时间
     *
     * @param type
     */
    public void cumsumEvaporator(SteamBakeType type) {
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V2)) {
            if (type == SteamBakeType.STEAM || type == SteamBakeType.STEAM_BAKE_1 || type == SteamBakeType.STEAM_BAKE_2
                    || type == SteamBakeType.DISINFECTION || type == SteamBakeType.THAW || type == SteamBakeType.CLEAN
                    || type == SteamBakeType.HOT_FOOD || type == SteamBakeType.FERMENTATION) {
                HawkParams.evaporatorRunningTimeAdd(1);
            }
        }
    }

    @Subscribe
    public void receiveEvent(Event2PauseStatus event) {
        LogUtils.d("receiver event from is:" + event.getBusFrom() + " the time is:");
        if (HawkParams.getWorkStatus() == WorkStatus.STATUS_WORKING.value) {
            if (event.getPauseStatus() == 1) {//暂停指令
                if (tvbtnPause.isSelected() == false) {//当前为正常状态
                    if (SerialManager.getInstance().sendCmdStop()) {
                        tvbtnPause.setSelected(true);
                        clicked2Pause = true;
                    }
                } else
                    LogUtils.e(TAG, "Event2PauseStatus指令不匹配");
            } else if (event.getPauseStatus() == 0) {//恢复运行指令
                if (tvbtnPause.isSelected() == true)
                    tvbtnPause.performClick();
                else
                    LogUtils.e(TAG, "Event2PauseStatus指令不匹配");
            } else if (event.getPauseStatus() == 2) {//结束运行指令
                if (SerialManager.getInstance().sendCmdOver()) {//结束烹饪
                    Global.jump2MainActivity(mContext);
                    finish();
                }
            }
        }
    }
}
