//package com.viomi.oven.activity;
//
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.net.ConnectivityManager;
//import android.net.wifi.WifiManager;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.google.zxing.activity.CaptureActivity;
//import com.viomi.oven.PrefParams;
//import com.viomi.oven.R;
//import com.viomi.oven.adapter.TubatuAdapter;
//import com.viomi.oven.broadcast.BroadcastAction;
//import com.viomi.oven.broadcast.ConnectionChangeReceiver;
//import com.viomi.oven.enumType.ModelType;
//import com.viomi.oven.service.BackgroudService;
//import com.viomi.oven.util.Global;
//import com.viomi.oven.util.LogUtils;
//
//import butterknife.BindView;
//import butterknife.OnClick;
//
//public class MainActivity_V extends BaseActivity implements View.OnTouchListener {
//    //UI
//    @BindView(R.id.imgBack)
//    ImageView imgBack;
//    @BindView(R.id.imgWifiV)
//    ImageView imgWifiV;
//    @BindView(R.id.imgLight)
//    ImageView imgLight;
//    @BindView(R.id.img1)
//    TextView img1;
//    @BindView(R.id.img2)
//    TextView img2;
//    @BindView(R.id.img3)
//    TextView img3;
//    @BindView(R.id.img4)
//    TextView img4;
//    @BindView(R.id.img5)
//    TextView img5;
//    @BindView(R.id.img6)
//    TextView img6;
//    //参数
//    private boolean isConnectNetwork;
//    private WifiManager wifiManager;
//    private BroadcastReceiver mReceiver;
//    private BroadcastReceiver updateReceiver;
//    private Intent backgroudServiceintent;
//    private TubatuAdapter mPagerAdapter;
//    private Animation animation;
//    //
//    public static final int MSG_SHOW_WAVE = 1000;
//
//    @Override
//    protected int getLayoutRes() {
//        return R.layout.activity_main_v;
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        initParams();
//        sendToHandlerDelayed(MSG_SHOW_WAVE, 1000);
//    }
//
//    public void initParams() {
//        Global.backMainInitHawkParams();
//    }
//
//    @Override
//    protected void initView() {
//        imgWifiV.setVisibility(View.VISIBLE);
//        imgBack.setVisibility(View.GONE);
//        img1.setOnTouchListener(this);
//        img2.setOnTouchListener(this);
//        img3.setOnTouchListener(this);
//        img4.setOnTouchListener(this);
//        img5.setOnTouchListener(this);
//        img6.setOnTouchListener(this);
//    }
//
//    @Override
//    protected void initData() {
//        //开机自动连wifi
//        wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
//        wifiManager.setWifiEnabled(true);
//        registerReceiver();
//        initService();
//    }
//
//    private void registerReceiver() {
//        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        filter.addAction(Intent.ACTION_TIME_CHANGED);// 时间改变,例如手动修改设置里的时间
//        mReceiver = new ConnectionChangeReceiver() {
//            @Override
//            public void hasNoNetwork() {
//                isConnectNetwork = false;
//            }
//
//            @Override
//            public void hasNetwork() {
//                isConnectNetwork = true;
//            }
//        };
//
//        IntentFilter filter8 = new IntentFilter();
//        filter8.addAction(BroadcastAction.ACTION_APP_UPGRADE_CHECK);
//        filter8.addAction(BroadcastAction.ACTION_VERSION_UPDATE_CLICK);
//        updateReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                if (BroadcastAction.ACTION_APP_UPGRADE_CHECK.equals(intent.getAction())) {
//                    LogUtils.w(TAG, "ACTION_APP_UPGRADE");
//                    //updateIcon_update(true);
//                }
//
//                if (BroadcastAction.ACTION_VERSION_UPDATE_CLICK.equals(intent.getAction())) {
//                    LogUtils.w(TAG, "ACTION_APP_UPGRADE_CLICK");
//                    //updateIcon_update(false);
//                }
//            }
//        };
//
//        registerReceiver(mReceiver, filter);
//        registerReceiver(updateReceiver, filter8);
//    }
//
//    private void unregisterReceiver() {
//        unregisterReceiver(mReceiver);
//        unregisterReceiver(updateReceiver);
//    }
//
//    private void initService() {
//        backgroudServiceintent = new Intent(this, BackgroudService.class);
//        startService(backgroudServiceintent);
//    }
//
//    private void stopService() {
//        stopService(backgroudServiceintent);
//    }
//
//    @Override
//    protected void onDestroy() {
//        unregisterReceiver();
//        stopService();
//        super.onDestroy();
//    }
//
//    @OnClick({R.id.img1, R.id.img2, R.id.img3, R.id.img4, R.id.img5, R.id.img6})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.img1:
//                PrefParams.setModelType(ModelType.MODEL_SCAN);
//                startActivity(new Intent(mContext, CaptureActivity.class));
//                break;
//            case R.id.img2:
//                PrefParams.setModelType(ModelType.MODEL_SMART);
//                startActivity(new Intent(mContext, FoodMenuActivity.class));
//                //startActivity(new Intent(mContext,CircleActivity.class));
//                break;
//            case R.id.img3:
//                PrefParams.setModelType(ModelType.MODEL_SPECIAL);
//                startActivity(new Intent(mContext, SpecialCookingActivity_V_New.class));
//                break;
//            case R.id.img4:
//                PrefParams.setModelType(ModelType.MODEL_MY_SET);
//                startActivity(new Intent(mContext, FoodMenuActivity.class));
//                break;
//            case R.id.img5:
//                startActivity(new Intent(mContext, SettingActivity.class));
//                break;
//            case R.id.img6:
//                //startActivity(new Intent(mContext, MoreActivity.class));
//                startActivity(new Intent(mContext,MainActivity_V_NEW.class));
//                break;
//        }
//    }
//
//    @Override
//    public boolean onTouch(View view, MotionEvent event) {
//        if (event.getAction() == MotionEvent.ACTION_DOWN) {
//            animation = AnimationUtils.loadAnimation(mContext, R.anim.ani_press_down);
//            view.startAnimation(animation);
//        }
//        //抬起操作
//        if (event.getAction() == MotionEvent.ACTION_UP) {
//            animation = AnimationUtils.loadAnimation(mContext, R.anim.ani_press_up);
//            view.startAnimation(animation);
//        }
//        //移动操作
//        if (event.getAction() == MotionEvent.ACTION_MOVE) {
//        }
//        return false;
//    }
//}
