//package com.viomi.oven.activity;
//
//import android.app.ProgressDialog;
//import android.content.Intent;
//import android.graphics.Paint;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.viomi.oven.AppStaticConfig;
//import com.viomi.oven.R;
//import com.viomi.oven.bean.LoginQrCode;
//import com.viomi.oven.device.DeviceConfig;
//import com.viomi.oven.interfaces.AppCallback;
//import com.viomi.oven.util.DataProcessUtil;
//import com.viomi.oven.util.GsonTools;
//import com.viomi.oven.util.PhoneUtil;
//import com.zhy.http.okhttp.OkHttpUtils;
//import com.zhy.http.okhttp.callback.StringCallback;
//
//import butterknife.BindView;
//import butterknife.OnClick;
//import okhttp3.Call;
//
///**
// * Created by Ljh on 2018/2/1
// * 食材和商城二维码
// */
//
//public class FoodQrActivity extends BaseActivity {
//    //UI
//    @BindView(R.id.imgWifiV)
//    ImageView imgWifiV;
//    @BindView(R.id.tvUseInfo)
//    TextView tvUseInfo;
//    @BindView(R.id.tvRefresh)
//    TextView tvRefresh;
//    @BindView(R.id.imgQr)
//    ImageView imgQr;
//    @BindView(R.id.tvDowmInfo)
//    TextView tvDowmInfo;
//    private ProgressDialog progressDialog = null;
//    //参数
//    private AppCallback<String> mBindCallback;
//
//    private String mClientId;
//
//    @Override
//    protected int getLayoutRes() {
//        return R.layout.activity_food_qr;
//    }
//
//    @Override
//    protected void initView() {
//        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
//            imgWifiV.setVisibility(View.VISIBLE);
//        tvDowmInfo.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
//        tvUseInfo.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
//    }
//
//    @Override
//    protected void initData() {
//        httpGetQr();
//    }
//
//    public void httpGetQr() {

//    }
//
//    @OnClick({R.id.tvDowmInfo, R.id.tvRefresh, R.id.tvUseInfo})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.tvDowmInfo:
//                startActivity(new Intent(mContext,DownAppShopActivity.class));
//                break;
//            case R.id.tvRefresh:
//                httpGetQr();
//                break;
//            case R.id.tvUseInfo:
//                //undo
//                break;
//        }
//    }
//}
