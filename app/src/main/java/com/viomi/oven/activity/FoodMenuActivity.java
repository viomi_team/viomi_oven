package com.viomi.oven.activity;

import android.content.DialogInterface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.adapter.RAdapterFoodManager;
import com.viomi.oven.bean.BasicKeyInfo;
import com.viomi.oven.bean.FoodDetail;
import com.viomi.oven.bean.RecipeGuide;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.enumType.FromType;
import com.viomi.oven.enumType.ModelType;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.util.ApkUtil;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.GsonTools;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.NetworkUtils;
import com.viomi.oven.util.http.MyStringCallback;
import com.zhy.http.okhttp.OkHttpUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Ljh on 2018/1/3
 * 菜谱
 */
public class FoodMenuActivity extends BaseActivity implements RAdapterFoodManager.FoodManageAdapterCallback {
    //UI
    //@BindView(R.id.imgWifi)
    //ImageView imgWifi;
    @BindView(R.id.imgWifiV)
    ImageView imgWifiV;
    @BindView(R.id.imgLight)
    ImageView imgLight;
    @BindView(R.id.recycleMenu)
    RecyclerView recycleMenu;
    @BindView(R.id.llContent)
    LinearLayout llContent;
    @BindView(R.id.rlNoFood)
    RelativeLayout rlNoFood;
    @BindView(R.id.tvWarn)
    TextView tvWarn;
    //参数
    List<FoodDetail> mFoodList = new ArrayList<>();
    RAdapterFoodManager mRAdapterFoodManager;
    ModelType mModelType;

    @Override
    protected int getLayoutRes() {
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            return R.layout.activity_food_menu;
        else
            return R.layout.activity_food_menu_h;
    }

    @Override
    protected void initView() {
        initLoading();
        loading.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                OkHttpUtils.getInstance().cancelTag(this);
            }
        });
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) {
            imgWifiV.setVisibility(View.VISIBLE);
            recycleMenu.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        } else {
            imgWifiV.setVisibility(View.VISIBLE);
            recycleMenu.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        }
    }

    @Override
    protected void initData() {
        HawkParams.setCookFrom(FromType.FROM_RECIPE.value);
        HawkParams.setAppointTime(0);
        if (HawkParams.getModelType() == ModelType.MODEL_MY_SET.value) {
            tvWarn.setVisibility(View.VISIBLE);
            mModelType = ModelType.MODEL_MY_SET;
        } else
            mModelType = ModelType.MODEL_SMART;
        initFoodList();
        mRAdapterFoodManager = new RAdapterFoodManager(mContext, mFoodList);
        mRAdapterFoodManager.setCallback(this);
        //
        recycleMenu.setAdapter(mRAdapterFoodManager);
        //
        if (NetworkUtils.isAvailable()) {
            if (mModelType == ModelType.MODEL_MY_SET) {
                httpUpdateDishVer(false);
            } else if (mModelType == ModelType.MODEL_SMART) {
                httpUpdateDishVer(true);
            }
        } else {
            if (mFoodList.size() == 0) {
                rlNoFood.setVisibility(View.VISIBLE);
                llContent.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 1为只能菜谱，2为我的菜谱
     */
    public void httpUpdateDishVer(final boolean isSmart) {
        showLoading();
        OkHttpUtils
                .get()
                .url(AppStaticConfig.getUrlFoodLlist(isSmart ? "1" : "2"))
                .tag(this)
                .build()
                .execute(new MyStringCallback() {
                    @Override
                    public void onSuccess(String result) {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getJSONObject("mobBaseRes").getInt("code") == 100) {
                                List<FoodDetail> tempList = GsonTools.getClassItemList(jsonObject.getJSONObject("mobBaseRes").getJSONObject("result")
                                        .getString("list"), FoodDetail.class);
                                if (tempList != null && tempList.size() >= 0) {
                                    mFoodList = tempList;
                                    for (FoodDetail tempDetail : mFoodList) {
                                        Global.compatibleChangeList(tempDetail.getRecipeDirectionList());
                                    }
                                    //更新成功
                                    mRAdapterFoodManager.update(mFoodList);
                                    if (isSmart) {
                                        HawkParams.setSmartFoodList(mFoodList);
                                    } else {
                                        HawkParams.setMySetFoodList(mFoodList);
                                        if (mFoodList.size() == 0) {
                                            rlNoFood.setVisibility(View.VISIBLE);
                                            llContent.setVisibility(View.GONE);
                                        }
                                    }
                                }
                            } else
                                showToast(jsonObject.getJSONObject("mobBaseRes").getString("desc"));
                        } catch (JSONException e) {
                            showToast("数据解析异常");
                            e.printStackTrace();
                        } catch (Exception e) {
                            showToast("数据解析异常");
                            e.printStackTrace();
                        }
                        hideLoading();
                    }

                    @Override
                    public void onFail(int errorCode) {
                        hideLoading();
                    }
                });
    }

    @Override
    public void select(int position) {
        LogUtils.d(TAG, " the pos is:" + position);
        HawkParams.setSelectFoodPos(position);
        HawkParams.setFoodName(mFoodList.get(position).getName());
        HawkParams.setCookStep(mFoodList.get(position).getRecipeDirectionList());//从网络获取或者世楼传送
        AppointPreviewActivity.actionStart(mContext, false);
    }

    public void initFoodList() {
        if (mModelType == ModelType.MODEL_MY_SET)
            mFoodList = HawkParams.getMySetFoodList();
        else {//智能菜谱
            //APK版本变更时需对缓存的菜谱重置
            if (HawkParams.getAppVer() != ApkUtil.getVersionCode()) {
                mFoodList = null;
                HawkParams.setAppVer(ApkUtil.getVersionCode());
            } else
                mFoodList = HawkParams.getSmartFoodList();

            if (mFoodList == null || mFoodList.size() == 0) {
                mFoodList = new ArrayList<>();
                FoodDetail temp;
                //
                temp = new FoodDetail();
                temp.setName("蒜香烤生蚝");
                temp.getImgsId().add(R.drawable.suanxiangkaoshenghao);
                //temp.getLogoId().add(R.drawable.lg_shengmao);
                temp.getRecipeGuideList().add(new RecipeGuide(1, "将生蚝洗净，大蒜剁碎，辣椒、生姜都切丝"));
                temp.getRecipeGuideList().add(new RecipeGuide(2, "将剁碎的大蒜放入烧开的油中煎炒至金黄，加入适量盐"));
                temp.getRecipeGuideList().add(new RecipeGuide(3, "将大蒜和油淋在生蚝上，放上切好的辣椒和生姜，放入电蒸炉内，选择热风烤模式，温度调为180℃，时间20分钟，听到鸣笛声取出即可食用"));
                temp.getRecipeDirectionList().add(new DeviceRunStep((DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? SteamBakeType.BAKE.value : SteamBakeType.BAKE_6.value), 20, 180));
                temp.setMaterial("生蚝、大蒜、辣椒、生姜、盐");
                //-----------------------------营养信息
                temp.getIngredients().add(new BasicKeyInfo("生蚝", "10", "个"));
                temp.getIngredients().add(new BasicKeyInfo("大蒜", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("辣椒", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("生姜", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("盐", "0", ""));
                //-----------------------------基本信息
                mFoodList.add(temp);
                //
                temp = new FoodDetail();
                temp.setName("酱醋蒸茄子");
                temp.getImgsId().add(R.drawable.jiangchuqiezi);
                //temp.getLogoId().add(R.drawable.lg_qiezi);
                temp.getRecipeGuideList().add(new RecipeGuide(1, "摘去茄子蒂，将茄子切成8等份，用盐水浸泡，除去涩味"));
                temp.getRecipeGuideList().add(new RecipeGuide(2, "整齐的排于盛菜盘中，浇上调好的调味汁（酱油、辣椒、米醋、花椒粉、香油、盐）"));
                temp.getRecipeGuideList().add(new RecipeGuide(3, "放入电蒸炉中，选择纯蒸模式，温度调为110℃，时间20分钟，听到鸣笛声取出，撒上葱花即可食用"));
                temp.getRecipeDirectionList().add(new DeviceRunStep(SteamBakeType.STEAM.value, 20, 110));
                temp.setMaterial("茄子、酱油、辣椒、醋、花椒粉、香油、盐、葱花");
                //-----------------------------营养信息
                temp.getIngredients().add(new BasicKeyInfo("茄子", "400", "克"));
                temp.getIngredients().add(new BasicKeyInfo("酱油", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("辣椒", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("醋", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("花椒粉", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("香油", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("盐", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("葱花", "0", ""));
                //-----------------------------基本信息
                mFoodList.add(temp);
                //
                temp = new FoodDetail();
                temp.setName("清蒸豆腐");
                //-----------------------------图片
                temp.getImgsId().add(R.drawable.qingzhendoufu);
                //temp.getLogoId().add(R.drawable.lg_doufu);
                temp.getRecipeGuideList().add(new RecipeGuide(1, "将豆腐洗净，沥干水分，切成小方块"));
                temp.getRecipeGuideList().add(new RecipeGuide(2, "取一深盘，加入色拉油、生抽、白糖拌匀，放入姜丝、辣椒，摆上上海青，放入电蒸炉中，选择纯蒸模式，温度调为100℃，时间8分钟，听到鸣笛声取出，撒上香菜即可食用"));
                temp.getRecipeDirectionList().add(new DeviceRunStep(SteamBakeType.STEAM.value, 8, 100));
                temp.setMaterial("豆腐、色拉油、生抽、白砂糖、香菜、辣椒、姜丝、上海青");
                //-----------------------------营养信息
                temp.getIngredients().add(new BasicKeyInfo("豆腐", "400", "g"));
                temp.getIngredients().add(new BasicKeyInfo("色拉油", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("生抽", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("白砂糖", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("香菜", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("辣椒", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("姜丝", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("上海青", "0", ""));
                //-----------------------------基本信息
                mFoodList.add(temp);
                //
                temp = new FoodDetail();
                temp.setName("粉蒸肉");
                temp.getImgsId().add(R.drawable.fenzhen);
                //temp.getLogoId().add(R.drawable.lg_fenzhenrou);
                temp.getRecipeGuideList().add(new RecipeGuide(1, "将五花肉洗净切好，调入盐、料酒、胡椒粉、糖、酱油拌匀，再加入蒸肉粉、香油拌匀"));
                temp.getRecipeGuideList().add(new RecipeGuide(2, "将五花肉装盘放入电蒸炉内，选择纯蒸模式，温度调为100℃，时间20分钟"));
                temp.getRecipeGuideList().add(new RecipeGuide(3, "听到鸣笛声取出，重新摆盘，放上萝卜花点缀"));
                temp.getRecipeDirectionList().add(new DeviceRunStep(SteamBakeType.STEAM.value, 20, 100));
                temp.setMaterial("五花肉、蒸肉粉、生粉、香油、盐、料酒、胡椒粉、糖、酱油");
                //-----------------------------营养信息
                temp.getIngredients().add(new BasicKeyInfo("五花肉", "300", "克"));
                temp.getIngredients().add(new BasicKeyInfo("蒸肉粉", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("生粉", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("香油", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("盐", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("料酒", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("胡椒粉", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("糖", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("酱油", "0", ""));
                //-----------------------------基本信息
                mFoodList.add(temp);
                //
                temp = new FoodDetail();
                temp.setName("蒜香蒸大虾");
                temp.getImgsId().add(R.drawable.suanxiandaxia);
                //temp.getLogoId().add(R.drawable.lg_zhengxia);
                temp.getRecipeGuideList().add(new RecipeGuide(1, "将虾去虾肠，洗净，蒜切碎"));
                temp.getRecipeGuideList().add(new RecipeGuide(2, "将生抽、蒜蓉、白糖放于碗内调成汁备用"));
                temp.getRecipeGuideList().add(new RecipeGuide(3, "将大虾放入电蒸炉中，选择纯蒸模式，温度调为105℃，时间12分钟，听到鸣笛声取出，撒上蒜蓉辣酱，蘸上调好的酱料即可食用"));
                temp.getRecipeDirectionList().add(new DeviceRunStep(SteamBakeType.STEAM.value, 12, 105));
                temp.setMaterial("大虾、大蒜、红辣椒、蒜蓉辣酱、糖、生抽");
                //-----------------------------营养信息
                temp.getIngredients().add(new BasicKeyInfo("大虾", "350", "克"));
                temp.getIngredients().add(new BasicKeyInfo("大蒜", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("红辣椒", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("蒜蓉辣酱", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("糖", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("生抽", "0", ""));
                //-----------------------------基本信息
                mFoodList.add(temp);
                //
                temp = new FoodDetail();
                temp.setName("清蒸大闸蟹");
                temp.getImgsId().add(R.drawable.dazhaxie);
                //temp.getLogoId().add(R.drawable.lg_dazhaxia);
                temp.getRecipeGuideList().add(new RecipeGuide(1, "将大闸蟹刷洗干净，葱、姜、蒜头洗净切碎"));
                temp.getRecipeGuideList().add(new RecipeGuide(2, "将生抽、醋、香油、葱姜蒜末放入碗中，混合均匀成蘸料备用"));
                temp.getRecipeGuideList().add(new RecipeGuide(3, "将洗净的大闸蟹装盘放入电蒸炉中，选择纯蒸模式，温度调为100℃，时间30分钟，听到鸣笛声取出，蘸上调好的调料即可食用"));
                temp.getRecipeDirectionList().add(new DeviceRunStep(SteamBakeType.STEAM.value, 30, 100));
                temp.setMaterial("大闸蟹、酱油、香油、醋、葱、姜、蒜头");
                //-----------------------------营养信息
                temp.getIngredients().add(new BasicKeyInfo("大闸蟹", "2", "只"));
                temp.getIngredients().add(new BasicKeyInfo("酱油", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("香油", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("醋", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("葱", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("姜", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("蒜头", "0", ""));
                //-----------------------------基本信息
                mFoodList.add(temp);
                //
                temp = new FoodDetail();
                temp.setName("清蒸鲈鱼");
                temp.getImgsId().add(R.drawable.qingzhenluyu);
                //temp.getLogoId().add(R.drawable.lg_luyu);
                temp.getRecipeGuideList().add(new RecipeGuide(1, "鲈鱼买好请商家处理干净，然后在背脊两边各划一刀，不要划断，用少许盐抹匀腌制5分钟左右"));
                temp.getRecipeGuideList().add(new RecipeGuide(2, "香葱切断，姜切片垫在盘底，鱼身也放些许葱段、姜片"));
                temp.getRecipeGuideList().add(new RecipeGuide(3, "放入电蒸炉的食盘上，选择纯蒸模式，温度调为95℃，时间12分钟"));
                temp.getRecipeGuideList().add(new RecipeGuide(4, "盛出移入另外一个盘子，撒上香葱丝，淋蒸鱼鼓油，辣椒、生姜末、蒜蓉、植物油浇上就好了"));
                temp.getRecipeDirectionList().add(new DeviceRunStep(SteamBakeType.STEAM.value, 12, 95));
                temp.setMaterial("鲈鱼、盐、小葱、蒸鱼鼓油、植物油、辣椒、生姜末、蒜蓉");
                //-----------------------------营养信息
                temp.getIngredients().add(new BasicKeyInfo("鲈鱼", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("盐", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("小葱", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("蒸鱼鼓油", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("植物油", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("辣椒", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("生姜末", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("蒜蓉", "0", ""));
                //-----------------------------基本信息
                mFoodList.add(temp);
                //
                temp = new FoodDetail();
                temp.setName("烤玉米");
                temp.getImgsId().add(R.drawable.kaoyumi);
                //temp.getLogoId().add(R.drawable.lg_yumi);
                temp.getRecipeGuideList().add(new RecipeGuide(1, "将玉米的外皮去掉"));
                temp.getRecipeGuideList().add(new RecipeGuide(2, "将玉米刷上油、蜂蜜，撒上适量胡椒粉"));
                temp.getRecipeGuideList().add(new RecipeGuide(3, "将玉米放入电蒸炉的烤架上，选择热风烤模式，温度调为200℃，时间12分钟，听到鸣笛声取出，再刷点胡椒粉即可食用"));
                temp.getRecipeDirectionList().add(new DeviceRunStep((DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? SteamBakeType.BAKE.value :
                        SteamBakeType.BAKE_6.value), 15, 100));
                temp.setMaterial("玉米、油、胡椒粉、蜂蜜");
                //-----------------------------营养信息
                temp.getIngredients().add(new BasicKeyInfo("玉米", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("油", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("胡椒粉", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("蜂蜜", "0", ""));
                //-----------------------------基本信息
                mFoodList.add(temp);
                //
                temp = new FoodDetail();
                temp.setName("青团子");
                temp.getImgsId().add(R.drawable.qingtuanzi);
                //temp.getLogoId().add(R.drawable.lg_qingtuanzi);
                temp.getRecipeGuideList().add(new RecipeGuide(1, "麦草去除硬梗，留叶片，冲洗干净，锅内放适量水煮开，放入麦草用开水焯烫一下"));
                temp.getRecipeGuideList().add(new RecipeGuide(2, "捞出后凉水，用剪刀将焯烫后的麦草剪碎，放入料理杯内，加入适量水搅打成泥，把打好的泥放入漏勺或纱布中，过滤出渣，留取青汁"));
                temp.getRecipeGuideList().add(new RecipeGuide(3, "粘米粉及糯米粉混合后放入盆中，青汁加热后倒入粉中，合成团，倒入色拉油继续揉，揉至面团光滑"));
                temp.getRecipeGuideList().add(new RecipeGuide(4, "揉好后的面团，分割成50克每份的剂子，豆沙分成15克每份，分别滚圆包好，放入电蒸炉中，选择纯蒸模式，温度调为110℃，时间15分钟，听到鸣笛声取出即可食用"));
                temp.getRecipeDirectionList().add(new DeviceRunStep(SteamBakeType.STEAM.value, 15, 110));
                temp.setMaterial("麦草、糯米粉、豆沙、色拉油");
                //-----------------------------营养信息
                temp.getIngredients().add(new BasicKeyInfo("麦草", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("糯米粉", "600", "克"));
                temp.getIngredients().add(new BasicKeyInfo("豆沙", "135", "克"));
                temp.getIngredients().add(new BasicKeyInfo("色拉油", "15", "克"));
                //-----------------------------基本信息
                mFoodList.add(temp);
                //
                temp = new FoodDetail();
                temp.setName("烤全鸡");
                temp.getImgsId().add(R.drawable.kaoquanji);
                //temp.getLogoId().add(R.drawable.lg_quanji);
                temp.getRecipeGuideList().add(new RecipeGuide(1, ("准备好一只新鲜的鸡，并将其里外彻底洗净")));
                temp.getRecipeGuideList().add(new RecipeGuide(2, "将鸡全身均匀抹上盐、胡椒、酱油、八角粉，在盆里腌制1小时"));
                temp.getRecipeGuideList().add(new RecipeGuide(3, "将鸡放置到烤盘上，选择蒸烤模式，温度调为180℃，时间30分钟，听到鸣笛声取出即可食用，烤出的鸡颜色金黄，皮骨肉都可分离，脆香可口"));
                temp.getRecipeDirectionList().add(new DeviceRunStep((DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? SteamBakeType.STEAM_BAKE.value : SteamBakeType.STEAM_BAKE_2.value), 30, 180));
                temp.setMaterial("鸡、盐、胡椒、酱油、八角粉");
                //-----------------------------营养信息
                temp.getIngredients().add(new BasicKeyInfo("鸡", "一", "只"));
                temp.getIngredients().add(new BasicKeyInfo("盐", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("胡椒", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("酱油", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("八角粉", "0", ""));
                //-----------------------------基本信息
                mFoodList.add(temp);
                //
                temp = new FoodDetail();
                temp.setName("香烤肉串");
                temp.getImgsId().add(R.drawable.xiankaorouchuan);
                //temp.getLogoId().add(R.drawable.lg_kaorouchuan);
                temp.getRecipeGuideList().add(new RecipeGuide(1, "把里脊肉切薄，然后用味料腌30分钟"));
                temp.getRecipeGuideList().add(new RecipeGuide(2, "再加入沙拉酱、砂糖，用竹签串起"));
                temp.getRecipeGuideList().add(new RecipeGuide(3, "将串好的肉串放到烤架上，择择热风模式200℃，时间20分钟，听到鸣笛声取出即可食用"));
                temp.getRecipeDirectionList().add(new DeviceRunStep((DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? SteamBakeType.BAKE.value : SteamBakeType.BAKE_6.value), 20, 200));
                temp.setMaterial("里脊肉、酱油、生粉、味精、沙拉酱、砂糖");
                //-----------------------------营养信息
                temp.getIngredients().add(new BasicKeyInfo("里脊肉", "300", "克"));
                temp.getIngredients().add(new BasicKeyInfo("酱油", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("生粉", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("味精", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("沙拉酱", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("砂糖", "0", ""));
                //-----------------------------基本信息
                mFoodList.add(temp);
                //
                temp = new FoodDetail();
                temp.setName("蒜蓉烤虾");
                temp.getImgsId().add(R.drawable.suanrongkaoxia);
                //temp.getLogoId().add(R.drawable.lg_kaoxia);
                temp.getRecipeGuideList().add(new RecipeGuide(1, "虾开背去肠线，洗净，擦干水分备用"));
                temp.getRecipeGuideList().add(new RecipeGuide(2, "大蒜、小葱、豆豉切碎（豆豉有咸味，所以不用加盐），加入色拉油、少许料酒，一点点糖拌匀，慢慢的塞进虾背。烤盘铺好锡纸，放整齐，蒙上保鲜膜冷藏腌制30分钟左右"));
                temp.getRecipeGuideList().add(new RecipeGuide(3, "放入电蒸炉内，选择热风烤模式，温度调为180℃，时间15分钟，听到鸣笛声取出即可食用"));
                temp.getRecipeDirectionList().add(new DeviceRunStep((DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? SteamBakeType.BAKE.value : SteamBakeType.BAKE_6.value), 15, 180));
                temp.setMaterial("虾、大蒜、小葱、色拉油、糖、料酒、豆豉");
                //-----------------------------营养信息
                temp.getIngredients().add(new BasicKeyInfo("虾", "400", "克"));
                temp.getIngredients().add(new BasicKeyInfo("大蒜", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("小葱", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("色拉油", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("糖", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("料酒", "0", ""));
                temp.getIngredients().add(new BasicKeyInfo("豆豉", "0", ""));
                //*/
                //-----------------------------基本信息
                mFoodList.add(temp);//
                HawkParams.setSmartFoodList(mFoodList);
            }
        }
    }
}
