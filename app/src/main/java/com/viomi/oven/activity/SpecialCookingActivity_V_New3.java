package com.viomi.oven.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.adapter.AdapterSpecialStep;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.dialog.AddSpecialCookDialog;
import com.viomi.oven.dialog.CommonConfirmDialog;
import com.viomi.oven.dialog.NumberPickerDialog;
import com.viomi.oven.enumType.FromType;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/***
 * Created by Ljh on 2018/5/5
 * 专业模式
 */

public class SpecialCookingActivity_V_New3 extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.unGvStep)
    ListView unGvStep;
    @BindView(R.id.rlImgAdd)
    RelativeLayout rlImgAdd;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.rlCurModel)
    RelativeLayout rlCurModel;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.llTime)
    LinearLayout llTime;
    @BindView(R.id.tvTemp)
    TextView tvTemp;
    @BindView(R.id.llTemp)
    LinearLayout llTemp;
    @BindView(R.id.imgAppoint)
    ImageView imgAppoint;
    @BindView(R.id.imgBtnStart)
    ImageView imgBtnStart;
    @BindView(R.id.llControl)
    LinearLayout llControl;
    @BindView(R.id.root)
    LinearLayout root;
    //
    AddSpecialCookDialog mAddSpecialCookDialog;
    CommonConfirmDialog mCommonConfirmDialog1;
    NumberPickerDialog mNumberPickerDialog;
    //参数
    List<String> tempData = new ArrayList<>();
    List<String> timeData = new ArrayList<>();
    List<DeviceRunStep> mDeviceRunSteps = new ArrayList<>();
    int curShowTemp = 0, curShowTime = 0;
    AdapterSpecialStep mAdapterSpecialStep;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_special_cooking_v_new3;
    }

    @Override
    protected void initView() {
        unGvStep.setVisibility(View.VISIBLE);
        mCommonConfirmDialog1 = new CommonConfirmDialog(mContext);
        mCommonConfirmDialog1.setCallBack(new CommonConfirmDialog.CommonConfirmDialogCallBack() {
            @Override
            public void btnOk() {
                mDeviceRunSteps.remove(mCommonConfirmDialog1.getPos());
                mAdapterSpecialStep.notifyDataSetChanged();
                updateContent();
                changeShowStatus();
            }
        });
        mAddSpecialCookDialog = new AddSpecialCookDialog(mContext);
        mAddSpecialCookDialog.setCallBack(new AddSpecialCookDialog.AddSpecialCookDialogCallBack() {
            @Override
            public void setCurType(boolean isAdd, SteamBakeType curType) {
                if (isAdd) {//添加模式
                    DeviceRunStep temp = new DeviceRunStep(curType);
                    mDeviceRunSteps.add(temp);
                    mAdapterSpecialStep.setCurPos(mDeviceRunSteps.size() - 1);
                    changeShowStatus();
                    updateContent();
                } else {//更改模式
                    mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setType(curType);
                    mAdapterSpecialStep.notifyDataSetChanged();
                    updateContent();
                }
            }
        });
        //
        unGvStep.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mAdapterSpecialStep.curPos != position) {
                    mAdapterSpecialStep.setCurPos(position);
                    updateContent();
                }
            }
        });
        unGvStep.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (mDeviceRunSteps.size() <= 1) {
                    //ToastUtil.show("至少保留一个");
                    return true;
                }
                mCommonConfirmDialog1.setPos(position);
                mCommonConfirmDialog1.show();
                return true;
            }
        });
        //
        mNumberPickerDialog = new NumberPickerDialog(mContext);
        mNumberPickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (mNumberPickerDialog.isChanged) {
                    if (mNumberPickerDialog.getFlag().equals("0")) {//选择温度
                        curShowTemp = Integer.parseInt(tempData.get(mNumberPickerDialog.getPos()).toString());
                        tvTemp.setText(curShowTemp + "");
                        mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setTemp(curShowTemp);
                        if (curShowTemp > 220 && curShowTime > 40 && mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType() != SteamBakeType.STEAM) {
                            curShowTime = 40;
                            mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setTime(curShowTime);
                            tvTime.setText(curShowTime + "");
                        }
                        mAdapterSpecialStep.notifyDataSetChanged();
                    } else {//选择时间
                        curShowTime = Integer.parseInt(timeData.get(mNumberPickerDialog.getPos()).toString());
                        tvTime.setText(curShowTime + "");
                        mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setTime(curShowTime);
                        mAdapterSpecialStep.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    @Override
    protected void initData() {
        HawkParams.setFoodName("专业烹饪");
        HawkParams.setCookFrom(FromType.FROM_SPECIAL.value);
        //
        DeviceRunStep temp = new DeviceRunStep(SteamBakeType.STEAM);
        mDeviceRunSteps.add(temp);
        mAdapterSpecialStep = new AdapterSpecialStep(mContext, mDeviceRunSteps);
        unGvStep.setAdapter(mAdapterSpecialStep);
        updateContent();
        changeShowStatus();
    }

    public void changeShowStatus() {
        if (mDeviceRunSteps.size() >= 4)
            rlImgAdd.setVisibility(View.GONE);
        else {
            rlImgAdd.setVisibility(View.VISIBLE);
            if (mDeviceRunSteps.size() == 1) {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) rlImgAdd.getLayoutParams();
                params.height = 200;
                rlImgAdd.setLayoutParams(params);
            } else if (mDeviceRunSteps.size() == 2) {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) rlImgAdd.getLayoutParams();
                params.height = 140;
                rlImgAdd.setLayoutParams(params);
            } else {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) rlImgAdd.getLayoutParams();
                params.height = 105;
                rlImgAdd.setLayoutParams(params);
            }
        }
    }

    public void updateContent() {
        curShowTemp = mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getTemp();
        tvName.setText(mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().name);
        tvTemp.setText(mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getTemp() + "");
        //
        curShowTime = mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getTime();
        tvTime.setText(mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getTime() + "");
        //imgAdd大小变化
    }

    @OnClick({R.id.rlImgAdd, R.id.rlCurModel, R.id.imgBtnStart, R.id.imgAppoint, R.id.llTime, R.id.llTemp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlImgAdd:
                mAddSpecialCookDialog.show();
                mAddSpecialCookDialog.setSelectType(null);
                break;
            case R.id.rlCurModel://选择蒸烤
                mAddSpecialCookDialog.show();
                mAddSpecialCookDialog.setSelectType(mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType());
                break;
            case R.id.imgAppoint:
                HawkParams.setCookStep(mDeviceRunSteps);
                startActivity(new Intent(mContext, SelectAppointTimeActivity.class));
                break;
            case R.id.imgBtnStart://确认开始
                if (isParamsOk()) {
                    if (SerialManager.getInstance().sendCmdStart(mDeviceRunSteps)) {
                        //VoiceManager.getInstance().startSpeak("现在开始烹饪");
                        startActivity(new Intent(mContext, CookRunningActivity.class));
                        finish();
                    }
                }
                break;
            case R.id.llTime:
                timeData.clear();
                mNumberPickerDialog.setFlag("1");
                int maxTime = mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().maxTime;
                if (mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType() != SteamBakeType.STEAM && curShowTemp > 220)
                    maxTime = 40;
                for (int i = mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().minTime; i <= maxTime; i++) {
                    timeData.add(i + "");
                }
                mNumberPickerDialog.update(timeData);
                mNumberPickerDialog.show(curShowTime + "", "分");
                break;
            case R.id.llTemp:
                tempData.clear();
                for (int i = mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().minTemp; i <= mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().maxTemp; i++) {
                    tempData.add(i + "");
                }
                mNumberPickerDialog.setFlag("0");
                mNumberPickerDialog.update(tempData);
                mNumberPickerDialog.show(curShowTemp + "", "℃");
                break;
        }
    }

    public boolean isParamsOk() {
        int timeLength = 0, highTempLength = 0;
        for (DeviceRunStep deviceRunStep : mDeviceRunSteps) {
            timeLength += deviceRunStep.getTime();
            if (deviceRunStep.getTemp() > 220 && (deviceRunStep.getMode() == SteamBakeType.BAKE.value || deviceRunStep.getMode() == SteamBakeType.STEAM_BAKE.value)) {
                highTempLength += deviceRunStep.getTime();
                //专业模式下（烤、蒸烤叠加），当设定温度在221-230℃，连续运行总时间不能超过40 min
                if (highTempLength > 40) {
                    ToastUtil.show("在大于220度高温状态不可以连续工作超过40min");
                    return false;
                }
            } else highTempLength = 0;
        }
        //判断门和水箱是否正常
        return Global.isPrepareOk(mDeviceRunSteps);
    }
}
