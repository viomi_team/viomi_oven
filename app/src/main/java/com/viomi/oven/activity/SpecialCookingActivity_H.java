package com.viomi.oven.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.adapter.AdapterSpecialStep;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.dialog.AddSpecialCookDialog_H;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.ToastUtil;
import com.viomi.oven.util.ViewUtil;
import com.viomi.oven.view.HorizonListView;
import com.viomi.oven.view.LongClickImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/***
 * Created by Ljh on 2018/1/3
 * 专业模式
 */
public class SpecialCookingActivity_H extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.unGvStep)
    HorizonListView unGvStep;
    @BindView(R.id.imgAdd)
    RelativeLayout imgAdd;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.llCurModel)
    LinearLayout llCurModel;
    @BindView(R.id.imgBtnStart)
    ImageView imgBtnStart;
    @BindView(R.id.imgDelTime)
    LongClickImageView imgDelTime;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.imgAddTime)
    LongClickImageView imgAddTime;
    @BindView(R.id.imgDelTemp)
    LongClickImageView imgDelTemp;
    @BindView(R.id.tvTemp)
    TextView tvTemp;
    @BindView(R.id.imgAddTemp)
    LongClickImageView imgAddTemp;
    @BindView(R.id.tvCancle)
    TextView tvCancle;
    @BindView(R.id.tvOk)
    TextView tvOk;
    @BindView(R.id.llConfirm)
    LinearLayout llConfirm;
    @BindView(R.id.root)
    RelativeLayout root;
    AddSpecialCookDialog_H mAddSpecialCookDialog_h;
    //参数
    List<DeviceRunStep> mDeviceRunSteps = new ArrayList<>();
    List<DeviceRunStep> mDeviceRunStepsBack = new ArrayList<>();
    AdapterSpecialStep mAdapterSpecialStep;
    int curShowTemp = 0, curShwoTime = 0;

    public static void actionStart(Context context) {
        Intent intent = new Intent(context, SpecialCookingActivity_H.class);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_special_cooking_h;
    }

    @Override
    protected void initView() {
        imgBack.setSelected(true);
        mAddSpecialCookDialog_h = new AddSpecialCookDialog_H(mContext);
        mAddSpecialCookDialog_h.setCallBack(new AddSpecialCookDialog_H.AddSpecialCookDialog_HCallBack() {
            @Override
            public void setCurType(boolean isAdd, SteamBakeType curType) {
                if (isAdd) {//添加模式
                    DeviceRunStep temp = new DeviceRunStep(curType);
                    mDeviceRunSteps.add(temp);
                    mAdapterSpecialStep.setCurPos(mDeviceRunSteps.size() - 1);
                    ViewUtil.setGridViewWidth(unGvStep);
                    imgAdd.setVisibility(mDeviceRunSteps.size() >= 4 ? View.GONE : View.VISIBLE);
                    updateContent();
                } else {//更改模式
                    mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setType(curType);
                    mAdapterSpecialStep.notifyDataSetChanged();
                    updateContent();
                }
            }
        });
        //
        unGvStep.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mAdapterSpecialStep.editStatus) {//删除
                    if (mDeviceRunSteps.size() <= 1) {
                        ToastUtil.show("至少保留一个");
                        return;
                    }
                    mDeviceRunSteps.remove(position);
                    mAdapterSpecialStep.notifyDataSetChanged();
                    ViewUtil.setGridViewWidth(unGvStep);
                } else {//选中
                    if (mAdapterSpecialStep.curPos != position) {
                        mAdapterSpecialStep.setCurPos(position);
                        updateContent();
                    }
                }
            }
        });

        unGvStep.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (!mAdapterSpecialStep.editStatus && mDeviceRunSteps.size() > 1) {//进入编辑模式
                    mAdapterSpecialStep.setEditStatus(true);
                    mDeviceRunStepsBack.clear();
                    for (DeviceRunStep deviceRunStep : mDeviceRunSteps) {
                        mDeviceRunStepsBack.add(deviceRunStep);
                    }
                    changeShowStatus();
                }
                return true;
            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mAdapterSpecialStep.editStatus) {
                    onBackPressed();
                }
            }
        });
    }

    @Override
    protected void initData() {
        DeviceRunStep temp = new DeviceRunStep(SteamBakeType.STEAM);
        mDeviceRunSteps.add(temp);
        mAdapterSpecialStep = new AdapterSpecialStep(mContext, mDeviceRunSteps);
        unGvStep.setAdapter(mAdapterSpecialStep);
        ViewUtil.setGridViewWidth(unGvStep);
        updateContent();
        changeShowStatus();
    }

    public void changeShowStatus() {
        llConfirm.setVisibility(mAdapterSpecialStep.editStatus ? View.VISIBLE : View.GONE);
        imgAdd.setVisibility((mAdapterSpecialStep.editStatus || mDeviceRunSteps.size() >= 4) ? View.GONE : View.VISIBLE);
    }

    public void updateContent() {
        tvName.setText(mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().name);
        curShowTemp = mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getTemp();
        tvTemp.setText(curShowTemp + "");
        curShwoTime = mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getTime();
        tvTime.setText(curShwoTime + "");
    }

    @OnClick({R.id.imgAdd, R.id.llCurModel, R.id.imgBtnStart, R.id.tvAppoint, R.id.tvCancle, R.id.tvOk, R.id.imgDelTime, R.id.imgAddTime, R.id.imgDelTemp, R.id.imgAddTemp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgAdd:
                mAddSpecialCookDialog_h.show();
                mAddSpecialCookDialog_h.setSelectType(null);
                break;
            case R.id.llCurModel://选择蒸烤
                if (mAdapterSpecialStep.editStatus)
                    return;
                mAddSpecialCookDialog_h.show();
                mAddSpecialCookDialog_h.setSelectType(mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType());
                break;
            case R.id.tvOk:
                mAdapterSpecialStep.setEditStatus(false);
                ViewUtil.setGridViewWidth(unGvStep);
                changeShowStatus();
                break;
            case R.id.tvCancle:
                mDeviceRunSteps.clear();
                for (DeviceRunStep temp : mDeviceRunStepsBack)
                    mDeviceRunSteps.add(temp);
                mAdapterSpecialStep.setEditStatus(false);
                ViewUtil.setGridViewWidth(unGvStep);
                changeShowStatus();
                break;
            case R.id.tvAppoint:
                HawkParams.setCookStep(mDeviceRunSteps);
                startActivity(new Intent(mContext, SelectAppointTimeActivity.class));
                break;
            case R.id.imgBtnStart://确认开始
                int timeLength = 0;
                for (DeviceRunStep deviceRunStep : mDeviceRunSteps) {
                    timeLength += deviceRunStep.getTime();
                }
                if (timeLength == 0) {
                    ToastUtil.show("参数异常");
                    return;
                }
                if (isParamsOk()) {
                    if (SerialManager.getInstance().sendCmdStart(mDeviceRunSteps)) {
                        HawkParams.setFoodName("专业烹饪");
                        startActivity(new Intent(mContext, CookRunningActivity.class));
                        finish();
                    }
                }
                break;
            case R.id.imgDelTime:
                timeDel();
                break;
            case R.id.imgAddTime:
                timeAdd();
                break;
            case R.id.imgDelTemp:
                tempDel();
                break;
            case R.id.imgAddTemp:
                tempAdd();
                break;
        }
    }

    public void timeAdd() {
        if (curShwoTime + 1 > mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().maxTime)
            return;
        else {
            curShwoTime++;
            tvTime.setText(curShwoTime + "");
            mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setTime(curShwoTime);
            mAdapterSpecialStep.notifyDataSetChanged();
        }
    }

    public void timeDel() {
        if (curShwoTime - 1 < mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().minTime)
            return;
        else {
            curShwoTime--;
            tvTime.setText(curShwoTime + "");
            mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setTime(curShwoTime);
            mAdapterSpecialStep.notifyDataSetChanged();
        }
    }

    public void tempAdd() {
        if (curShowTemp + 1 > mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().maxTemp)
            return;
        else {
            curShowTemp++;
            tvTemp.setText(curShowTemp + "");
            mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setTemp(curShowTemp);
            mAdapterSpecialStep.notifyDataSetChanged();
        }
    }

    public void tempDel() {
        if (curShowTemp - 1 < mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().minTemp)
            return;
        else {
            curShowTemp--;
            tvTemp.setText(curShowTemp + "");
            mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setTemp(curShowTemp);
            mAdapterSpecialStep.notifyDataSetChanged();
        }
    }

    public boolean isParamsOk() {
        //int timeLength = 0, highTempLength = 0;
        //for (DeviceRunStep deviceRunStep : mDeviceRunSteps) {
        //    timeLength += deviceRunStep.getTime();
        //    if (deviceRunStep.getTemp() > 220 && (deviceRunStep.getMode() != SteamBakeType.STEAM.value)) {
        //        highTempLength += deviceRunStep.getTime();
        //        //专业模式下（烤、蒸烤叠加），当设定温度在221-230℃，连续运行总时间不能超过40 min
        //        if (highTempLength > 40) {
        //            ToastUtil.show("在大于220度高温状态不可以连续工作超过40min");
        //            return false;
        //        }
        //    } else highTempLength = 0;
        //}
        //判断门和水箱是否正常
        return Global.isPrepareOk(mDeviceRunSteps);
    }
}
