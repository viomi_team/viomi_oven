package com.viomi.oven.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.R;
import com.viomi.oven.adapter.AdapterFoodCart;
import com.viomi.oven.bean.CartInfo;
import com.viomi.oven.bean.CartItem;
import com.viomi.oven.dialog.CommonConfirmDialog;
import com.viomi.oven.util.GsonTools;
import com.viomi.oven.util.ToastUtil;
import com.viomi.oven.util.http.MyStringCallback;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.RequestBody;

public class ShoppingCartActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.imgLight)
    ImageView imgLight;
    @BindView(R.id.lv)
    ListView lv;
    CommonConfirmDialog mCommonConfirmDialog;
    //参数
    AdapterFoodCart mAdapterFoodCart;
    List<CartItem> mCartItems = new ArrayList<>();

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_shopping_cart;
    }

    @Override
    protected void initView() {
        imgLight.setVisibility(View.GONE);
        tvTitle.setText("购物车");
        mCommonConfirmDialog = new CommonConfirmDialog(mContext);
        mCommonConfirmDialog.setCallBack(new CommonConfirmDialog.CommonConfirmDialogCallBack() {
            @Override
            public void btnOk() {
                httpDelShopCart(mCommonConfirmDialog.getPos());
            }
        });
    }

    @Override
    protected void initData() {
        mAdapterFoodCart = new AdapterFoodCart(mContext, mCartItems);
        mAdapterFoodCart.setCallBack(new AdapterFoodCart.AdapterFoodCartCallBack() {
            @Override
            public void numberChange(int pos, boolean isAdd) {
                if (isAdd) {
                    httpAddShopCart(pos);
                } else {
                    if (mCartItems.get(pos).getQuantity() > 1)
                        httpReduceShopCart(pos);
                    else {
                        mCommonConfirmDialog.setPos(pos);
                        mCommonConfirmDialog.setTitle("是否确定删除" + mCartItems.get(pos).getSkuInfoBean().getName());
                        mCommonConfirmDialog.show();
                    }
                }
            }
        });
        lv.setAdapter(mAdapterFoodCart);
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mCommonConfirmDialog.setPos(position);
                mCommonConfirmDialog.setTitle("是否确定删除" + mCartItems.get(position).getSkuInfoBean().getName());
                mCommonConfirmDialog.show();
                return true;
            }
        });
        httpGetShopCart();
    }

    public void httpGetShopCart() {
        showLoading();
        OkHttpUtils
                .get()
                .url(AppStaticConfig.getShopCartList())
                .tag(this)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        hideLoading();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        String result = response.substring(response.indexOf("{", 1), response.lastIndexOf("}"));
                        try {
                            int code = new JSONObject(result).getInt("code");
                            if (code == 100) {
                                String ShopCartString = new JSONObject(result).getString("result");
                                List<CartInfo> cartInfos = GsonTools.getClassItemList(new JSONObject(ShopCartString).getString("cartInfos"), CartInfo.class);
                                if (cartInfos == null)
                                    ToastUtil.show("数据解析异常");
                                else {
                                    mCartItems = cartInfos.get(0).getCartItems();
                                    mAdapterFoodCart.update(mCartItems);
                                }
                            } else showToast(new JSONObject(result).getString("desc"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            ToastUtil.show("数据解析异常");
                        } catch (Exception e) {
                            e.printStackTrace();
                            ToastUtil.show("数据解析异常");
                        }
                        hideLoading();
                    }
                });
    }

    public void httpAddShopCart(final int pos) {
        showLoading();
        OkHttpUtils
                .put()
                .url(AppStaticConfig.addFoodItem(mCartItems.get(pos).getSkuInfoBean().getSkuId()))
                .requestBody(RequestBody.create(null, ""))//
                .tag(this)
                .build()
                .execute(new MyStringCallback() {
                    @Override
                    public void onSuccess(String result) {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getJSONObject("mobBaseRes").getInt("code") == 100) {
                                mCartItems.get(pos).QuantityAdd();
                                mAdapterFoodCart.notifyDataSetChanged();
                            } else
                                showToast(jsonObject.getJSONObject("mobBaseRes").getString("desc"));
                        } catch (JSONException e) {
                            showToast("数据解析异常");
                            e.printStackTrace();
                        }
                        hideLoading();
                    }

                    @Override
                    public void onFail(int errorCode) {
                        hideLoading();
                    }
                });
    }

    public void httpReduceShopCart(final int pos) {
        showLoading();
        OkHttpUtils
                .put()
                .url(AppStaticConfig.reduceFoodItem(mCartItems.get(pos).getSkuInfoBean().getSkuId()))
                .requestBody(RequestBody.create(null, ""))//
                .tag(this)
                .build()
                .execute(new MyStringCallback() {
                    @Override
                    public void onSuccess(String result) {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getJSONObject("mobBaseRes").getInt("code") == 100) {
                                mCartItems.get(pos).QuantityDel();
                                if (mCartItems.get(pos).getQuantity() <= 0) {
                                    mCartItems.remove(pos);
                                }
                                mAdapterFoodCart.notifyDataSetChanged();
                            } else
                                showToast(jsonObject.getJSONObject("mobBaseRes").getString("desc"));
                        } catch (JSONException e) {
                            showToast("数据解析异常");
                            e.printStackTrace();
                        }
                        hideLoading();
                    }

                    @Override
                    public void onFail(int errorCode) {
                        hideLoading();
                    }
                });
    }

    public void httpDelShopCart(final int pos) {
        showLoading();
        OkHttpUtils
                .delete()
                .url(AppStaticConfig.delFoodItem(mCartItems.get(pos).getSkuInfoBean().getSkuId()))
                .requestBody(RequestBody.create(null, ""))//
                .tag(this)
                .build()
                .execute(new MyStringCallback() {
                    @Override
                    public void onSuccess(String result) {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getJSONObject("mobBaseRes").getInt("code") == 100) {
                                mCartItems.remove(pos);
                                mAdapterFoodCart.notifyDataSetChanged();
                            } else
                                showToast(jsonObject.getJSONObject("mobBaseRes").getString("desc"));
                        } catch (JSONException e) {
                            showToast("数据解析异常");
                            e.printStackTrace();
                        }
                        hideLoading();
                    }

                    @Override
                    public void onFail(int errorCode) {
                        hideLoading();
                    }
                });
    }
}
