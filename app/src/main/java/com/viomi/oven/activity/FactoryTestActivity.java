package com.viomi.oven.activity;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;

import com.viomi.oven.R;
import com.viomi.oven.util.ToastUtil;

import butterknife.BindView;
import butterknife.OnClick;
/**
 * Created by Ljh on 2018/1/3
 */
public class FactoryTestActivity extends BaseActivity {
    @BindView(R.id.btn1)
    Button btn1;
    @BindView(R.id.btn2)
    Button btn2;
    @BindView(R.id.btn3)
    Button btn3;
    @BindView(R.id.btn4)
    Button btn4;
    //UI

    //参数

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_factory_test;
    }

    @Override
    protected void initView() {
        ToastUtil.show("");
    }

    @Override
    protected void initData() {
    }

    @OnClick({R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4})
    public void onViewClicked(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {
            case R.id.btn1:
                intent.setAction("android.intent.action.WIFI_ADB");
                mContext.sendBroadcast(intent);
                break;
            case R.id.btn2:
                intent.setAction("android.provider.Telephony.SECRET_CODE");
                intent.setData(Uri.parse("android_secret_code://66"));
                mContext.sendBroadcast(intent);
                break;
            case R.id.btn3:


                break;
            case R.id.btn4:


                break;
        }
    }
}
