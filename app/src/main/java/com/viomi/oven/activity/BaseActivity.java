package com.viomi.oven.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.oven.AppParam;
import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.dialog.Loading;
import com.viomi.oven.dialog.WarnInfoDialog;
import com.viomi.oven.enumType.McuStatus;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.otto.BusProvider;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.NetworkUtils;
import com.viomi.oven.util.ToastUtil;
import com.zhy.http.okhttp.OkHttpUtils;

import java.lang.ref.WeakReference;
import java.util.Date;

import butterknife.ButterKnife;

/**
 * Created by Ljh
 */
public abstract class BaseActivity extends Activity /*implements View.OnClickListener*/ {
    public final String TAG = this.getClass().getSimpleName();
    //
    protected Loading loading;
    private ImageView wifi;
    //
    protected Context mContext = this;
    protected boolean isConnectNetwork;
    private int[] wifiIcons = new int[]{R.drawable.wifi_1, R.drawable.wifi_2, R.drawable.wifi_3, R.drawable.wifi_full};
    private IntentFilter wifiSigFilter;
    private BroadcastReceiver wifiSigFilterReceiver3;
    private ScreenThread screenThread;
    private WarnInfoDialog dialogConnectErr;
    private WarnInfoDialog mUpGradeDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LogUtils.v(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        initDirection();
        // 一进入界面不弹出输入法
        if (getLayoutRes() != 0)
            setContentView(getLayoutRes());
        ButterKnife.bind(this);
        BusProvider.getInstance().register(this);
        if (!TAG.equals("ScreenTimeActivity"))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        else
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        initView();
        initTitleBar();
        initData();
    }

    /**
     * 屏幕横竖屏
     */
    public void initDirection() {
        if (DeviceConfig.MODEL == AppStaticConfig.VIOMI_OVEN_V1) {///台面式竖屏
            if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                LogUtils.d(TAG, "change direction portait");
            }
        } else {//嵌入式横屏
            if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                LogUtils.d(TAG, "change direction landscape");
            }
        }
    }

    @Override
    protected void onResume() {
        LogUtils.v(TAG, "onResume" + " imgLight = " + AppParam.isLightOn);
        startScreenTimer();
        //刷新灯状态
        if (findViewById(R.id.imgLight) != null && findViewById(R.id.imgLight).getVisibility() == View.VISIBLE)
            findViewById(R.id.imgLight).setSelected(AppParam.isLightOn);
        if (findViewById(R.id.imgUnConnect) != null)
            findViewById(R.id.imgUnConnect).setVisibility(AppParam.isConnectErr ? View.VISIBLE : View.GONE);
        if (findViewById(R.id.tvTitle) != null && (TAG.equals("MainActivity_H") || TAG.equals("MainActivity_V_NEW") || TAG.equals("SelectAppointTimeActivity"))) {
            ((TextView) findViewById(R.id.tvTitle)).setText(DateFormat.format("HH:mm", new Date()).toString());
        }
        super.onResume();
    }

    protected abstract int getLayoutRes();

    protected abstract void initView();

    protected abstract void initData();

    public void showToast(String msg) {
        ToastUtil.show(msg);
    }

    private void initTitleBar() {
        //wifi
        if (findViewById(R.id.imgWifi) != null && findViewById(R.id.imgWifi).getVisibility() == View.VISIBLE)
            wifi = (ImageView) findViewById(R.id.imgWifi);
        else if (findViewById(R.id.imgWifiV) != null && findViewById(R.id.imgWifiV).getVisibility() == View.VISIBLE)
            wifi = (ImageView) findViewById(R.id.imgWifiV);

        if (wifi != null) {
            wifi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!v.isSelected())//
                        startActivity(new Intent(mContext, WifiScanActivity.class));
                }
            });
            //
            if (wifiSigFilter == null) {
                wifiSigFilter = new IntentFilter(WifiManager.RSSI_CHANGED_ACTION);
                wifiSigFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
                wifiSigFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            }
            if (wifiSigFilterReceiver3 == null) {
                wifiSigFilterReceiver3 = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (intent.getAction().equals(WifiManager.RSSI_CHANGED_ACTION)) {
                            //LogUtils.v(TAG, "wifiSigFilterReceiver3 receiver");
                            WifiInfo info = ((WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE)).getConnectionInfo();
                            if (info != null && info.getBSSID() != null && NetworkUtils.isAvailable()) {
                                if (wifi != null) {
                                    wifi.setImageResource(wifiIcons[WifiManager.calculateSignalLevel(info.getRssi(), wifiIcons.length)]);
                                }
                                isConnectNetwork = true;
                            } else {
                                if (wifi != null)
                                    wifi.setImageResource(R.drawable.wifi_0);
                                isConnectNetwork = false;
                            }
                        } else if (intent.getAction().equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                            LogUtils.d(TAG, WifiManager.WIFI_STATE_CHANGED_ACTION);
                            int wifistate = intent.getIntExtra(
                                    WifiManager.EXTRA_WIFI_STATE,
                                    WifiManager.WIFI_STATE_DISABLED);
                            if (wifistate == WifiManager.WIFI_STATE_DISABLED && wifi != null) {
                                isConnectNetwork = false;
                                wifi.setImageResource(R.drawable.wifi_0);
                            } else if (wifistate == WifiManager.WIFI_STATE_ENABLED) {
                                //updateWifiStrength();
                            }
                        } else if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                            LogUtils.d(TAG, ConnectivityManager.CONNECTIVITY_ACTION);
                            ConnectivityManager localConnectivityManager = (ConnectivityManager) context
                                    .getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo wifiNetInfo = localConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                            if (wifiNetInfo != null) {
                                if (wifiNetInfo.isConnected()) {
                                    //已连接
                                } else {
                                    if (wifi != null)
                                        wifi.setImageResource(R.drawable.wifi_0);
                                    isConnectNetwork = false;
                                }
                            } else {
                                if (wifi != null)
                                    wifi.setImageResource(R.drawable.wifi_0);
                                isConnectNetwork = false;
                            }
                        }
                    }
                };
            }
            registerReceiver(wifiSigFilterReceiver3, wifiSigFilter);
        }
        //灯光控制
        if (findViewById(R.id.imgLight) != null && findViewById(R.id.imgLight).getVisibility() == View.VISIBLE) {
            findViewById(R.id.imgLight).setSelected(AppParam.isLightOn);
            findViewById(R.id.imgLight).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LogUtils.d(TAG, "ceshi AppParam.isLightOn :" + AppParam.isLightOn + " is isSelected" + v.isSelected());
                    if (SerialManager.getInstance().sendCmdLightSwitch(!v.isSelected())) {//设置成功
                        //LogUtils.d("ceshi", "sendCmdLightSwitch result light = " + AppParam.isLightOn+" "+ System.currentTimeMillis()+" isMainThread =" + Global.isMainThread());
                        v.setSelected(AppParam.isLightOn);
                        //AppParam.isLightOn = !v.isSelected();
                        //v.setSelected(!v.isSelected());
                    }
                    //else
                    //    ToastUtil.show("设置失败");
                }
            });
        }
        //返回键
        if (findViewById(R.id.imgBack) != null && findViewById(R.id.imgBack).getVisibility() == View.VISIBLE
                && (!findViewById(R.id.imgBack).isSelected())) {//如果selected为true则事件交由子类
            findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LogUtils.d(TAG, "imgBack clicked");
                    onBackPressed();
                }
            });
        }
        //通讯状态
        if (findViewById(R.id.imgUnConnect) != null) {
            findViewById(R.id.imgUnConnect).setVisibility(AppParam.isConnectErr ? View.VISIBLE : View.GONE);
            findViewById(R.id.imgUnConnect).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showConnectErrDialog();
                }
            });
        }
    }

    private void showConnectErrDialog() {
        if(AppParam.mcuStatus == McuStatus.MCU_UPGRADE)
            return;
        if (dialogConnectErr == null) {
            dialogConnectErr = new WarnInfoDialog(mContext);
        }
        dialogConnectErr.setPos(dialogConnectErr.getPos() + 1);
        if (!dialogConnectErr.isShowing())
            dialogConnectErr.showInfo("通讯异常");
    }

    public void initLoading() {
        if (loading == null) {
            loading = new Loading(mContext);
        }
    }

    public void showLoading() {
        if (isFinishing())
            return;
        initLoading();
        loading.setCancelable(true);
        if (!loading.isShowing()) {
            loading.show();
        }
    }

    public void showUnCancelLoading() {
        if (isFinishing())
            return;
        initLoading();
        loading.setCancelable(false);
        if (!loading.isShowing()) {
            loading.show();
        }
    }

    public void hideLoading() {
        if (loading != null && loading.isShowing())
            loading.cancel();
    }

    public boolean getLoadingStatus() {
        initLoading();
        return loading.isShowing();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        startScreenTimer();
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onStop() {
        LogUtils.v(TAG, "onStop");
        if (loading != null)
            loading.cancel();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        OkHttpUtils.getInstance().cancelTag(this);
        BusProvider.getInstance().unregister(this);
        hideLoading();
        handler.removeCallbacksAndMessages(null);//参数为null时会清除所有消息
        if (wifiSigFilterReceiver3 != null)
            unregisterReceiver(wifiSigFilterReceiver3);
        super.onDestroy();
    }

    /**
     * 查找控件，可以省略强转
     *
     * @param id
     * @return
     */
    public <T> T findView(int id) {
        @SuppressWarnings("unchecked")
        T view = (T) findViewById(id);
        return view;
    }

    public void sendToHandler(int what) {
        sendToHandler(what, "");
    }

    public void sendToHandler(int what, String msg) {
        Message message = handler.obtainMessage();
        message.obj = msg;
        message.what = what;
        handler.sendMessage(message);
    }

    public void sendToHandlerDelayed(int what, long delayMillis) {
        sendToHandlerDelayed(what, "", delayMillis);
    }

    public void sendToHandlerDelayed(int what, String msg, long delayMillis) {
        Message message = handler.obtainMessage();
        message.obj = msg;
        message.what = what;
        handler.sendMessageDelayed(message, delayMillis);
    }

    public void removeMsg(int what) {
        if (null != handler) {
            handler.removeMessages(what);
        }
    }

    /**
     * *处理消息循环
     **/
    public void dealTheMsg(Message msg) {
        if (msg.what == 0) {
            if (HawkParams.getWorkStatus() == WorkStatus.STATUS_WORKING.value || HawkParams.getWorkStatus() == WorkStatus.STATUS_YUYUEING.value
                    || HawkParams.getWorkStatus() == WorkStatus.STATUS_UPGRADE.value)
                return;
            Intent intent = new Intent(mContext, ScreenTimeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        } else if (msg.what == 1) {
            if (findViewById(R.id.imgLight) != null && findViewById(R.id.imgLight).getVisibility() == View.VISIBLE)
                findViewById(R.id.imgLight).setSelected(AppParam.isLightOn);
            if (findViewById(R.id.imgUnConnect) != null)
                findViewById(R.id.imgUnConnect).setVisibility(AppParam.isConnectErr ? View.VISIBLE : View.GONE);
            if (AppParam.isConnectErr) {
                if (dialogConnectErr == null)
                    dialogConnectErr = new WarnInfoDialog(mContext);
                if (dialogConnectErr.getPos() == 0) {
                    showConnectErrDialog();
                }
            } else {
                if (dialogConnectErr != null && dialogConnectErr.isShowing())
                    dialogConnectErr.dismiss();
            }
            if (AppParam.mcuStatus == McuStatus.MCU_UPGRADE && !TAG.equals("VersionManagerActivity") && !TAG.equals("WifiScanActivity")) {
                if (mUpGradeDialog == null) {
                    mUpGradeDialog = new WarnInfoDialog(mContext);
                }
                mUpGradeDialog.showUpgrade();
            } else if (mUpGradeDialog != null && mUpGradeDialog.isShowing())
                mUpGradeDialog.cancel();
            if (TAG.equals("MainActivity_H") || TAG.equals("MainActivity_V_NEW") || TAG.equals("SelectAppointTimeActivity")) {
                ((TextView) findViewById(R.id.tvTitle)).setText(DateFormat.format("HH:mm", new Date()).toString());
                //((TextView)findViewById(R.id.tvTitle)).setText(DateFormat.format("yyyy-MM-dd HH:mm", new Date()).toString());
            }
        }
        //Global.showToast("该方法子类中继承实现");
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            dealTheMsg(msg);
        }
    };

    private static class ScreenThread extends Thread {

        private WeakReference<BaseActivity> weakReference;
        private BaseActivity mActivity;

        public ScreenThread(BaseActivity activity) {
            this.weakReference = new WeakReference<BaseActivity>(activity);
            mActivity = weakReference.get();
        }

        @Override
        public void run() {

            int screen_time = HawkParams.getScreenTime();
            for (int i = 0; i < screen_time; i++) {
                if (isInterrupted()) {
                    return;
                }
                SystemClock.sleep(1000);
                if (mActivity != null)
                    mActivity.sendToHandler(1);
            }
            if (isInterrupted()) {
                return;
            }
            if (mActivity != null)
                mActivity.sendToHandler(0);
        }
    }

    protected void startScreenTimer() {
        if (HawkParams.getWorkStatus() == WorkStatus.STATUS_WORKING.value || HawkParams.getWorkStatus() == WorkStatus.STATUS_YUYUEING.value
                || HawkParams.getWorkStatus() == WorkStatus.STATUS_UPGRADE.value)
            return;
        if (TAG.equals("ScreenTimeActivity"))
            return;
        if (screenThread != null) {
            screenThread.interrupt();
        }

        screenThread = new ScreenThread(this);
        screenThread.start();
    }

    @Override
    protected void onPause() {
        LogUtils.v(TAG, "onPause");
        if (screenThread != null) {
            screenThread.interrupt();
        }
        super.onPause();
    }
}
