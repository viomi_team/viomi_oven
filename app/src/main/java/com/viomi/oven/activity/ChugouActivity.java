package com.viomi.oven.activity;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.otto.Subscribe;
import com.viomi.oven.AppParam;
import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.dialog.CommonConfirmDialog;
import com.viomi.oven.dialog.WarnInfoDialog;
import com.viomi.oven.enumType.McuStatus;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.manager.VoiceManager;
import com.viomi.oven.otto.Event2PauseStatus;
import com.viomi.oven.otto.EventUpgradeCurve;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.view.CircleProgressView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ChugouActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvSmallTitle)
    TextView tvSmallTitle;
    @BindView(R.id.tvStep)
    TextView tvStep;
    @BindView(R.id.imgLight)
    ImageView imgLight;
    @BindView(R.id.tvRemainTime)
    TextView tvRemainTime;
    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.tvWarn)
    TextView tvWarn;
    @BindView(R.id.tvbtnPause)
    TextView tvbtnPause;
    @BindView(R.id.tvBtnOver)
    TextView tvBtnOver;
    @BindView(R.id.circleProgress)
    CircleProgressView circleProgress;
    WarnInfoDialog mWarnInfoDialog;
    CommonConfirmDialog mCommonConfirmDialog;
    //参数
    List<DeviceRunStep> mDeviceRunSteps = new ArrayList<>();
    SteamBakeType mSteamBakeType = SteamBakeType.CHUGOU;
    int timeLength = 60;
    int step = 0;//共两步,0为未开始运行，1为已经运行第一步，2为第一步运行完成后需要换水，3为已经运行第二步
    String latestErrInfo = "";
    private final String info2 = "请清理腔内水渍,水箱换清水后再启动进入第二步";

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_chugou;
    }

    @Override
    protected void initView() {
        tvTitle.setText("除垢");
        mWarnInfoDialog = new WarnInfoDialog(mContext);
        mCommonConfirmDialog = new CommonConfirmDialog(mContext);
        mCommonConfirmDialog.setCallBack(new CommonConfirmDialog.CommonConfirmDialogCallBack() {
            @Override
            public void btnOk() {
                if (mCommonConfirmDialog.getFlag().equals("0")) {//停止烹饪
                    if (SerialManager.getInstance().sendCmdStop()) {
                        tvbtnPause.setSelected(true);
                        tvSmallTitle.setText("已暂停");
                        tvSmallTitle.setVisibility(View.VISIBLE);
                    }
                } else {//结束烹饪
                    if (SerialManager.getInstance().sendCmdOver()) {//结束烹饪
                        Global.jump2MainActivity(mContext);
                    }
                }
            }
        });
        imgBack.setSelected(true);
        imgBack.setImageResource(R.drawable.open_pannel);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SerialManager.getInstance().sendCmdControlPanelSwitch(true);
            }
        });
    }

    @Override
    protected void initData() {
        step = getIntent().getIntExtra("step", 0);
        mDeviceRunSteps.add(new DeviceRunStep(mSteamBakeType));
        HawkParams.setWorkStatus(WorkStatus.STATUS_IDLE);
        initShowContent();
    }

    public void initShowContent() {
        if (step == 0) {//未除垢
            tvStep.setText("第一步");
            tvWarn.setVisibility(View.VISIBLE);
            tvWarn.setText("请将柠檬酸除垢剂（粉）与水按照20g:1L的比例加入水箱");
            tv1.setText("共");
            timeLength = 60;
            tvRemainTime.setText(timeLength + "");
            tvbtnPause.setSelected(true);
            tvbtnPause.setVisibility(View.VISIBLE);
            circleProgress.setProgressValue(0);
            HawkParams.setDescaling(0);
        } else if (step == 1) {//除垢进行时
            tvStep.setText("第一步");
            tvWarn.setVisibility(View.GONE);
            tv1.setText("剩余");
            tvbtnPause.setVisibility(View.GONE);
            tvBtnOver.setText("");
            tvBtnOver.setBackgroundResource(R.drawable.img_over_transparent);
            HawkParams.setWorkStatus(WorkStatus.STATUS_WORKING);
            HawkParams.setDescaling(1);
        } else if (step == 2) {//中途换水
            tvStep.setText("第二步");
            tvWarn.setVisibility(View.VISIBLE);
            //建议除垢模式第一步完成后增加把里面的水清理一下，因为一打开门后水就溢出来了
            tvWarn.setText(info2);
            tv1.setText("共");
            timeLength = 60;
            tvRemainTime.setText(timeLength + "");
            tvbtnPause.setVisibility(View.VISIBLE);
            tvbtnPause.setSelected(true);
            circleProgress.setProgressValue(0);
            HawkParams.setDescaling(2);
        } else if (step == 3) {//除垢进行时
            tvStep.setText("第二步");
            tvWarn.setVisibility(View.GONE);
            tv1.setText("剩余");
            tvbtnPause.setVisibility(View.GONE);
            HawkParams.setWorkStatus(WorkStatus.STATUS_WORKING);
            HawkParams.setDescaling(3);
        }
    }

    @OnClick({R.id.tvbtnPause, R.id.tvBtnOver})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvbtnPause:
                if (view.isSelected() == false) {//当前为正常运行状态，则暂停
                    mCommonConfirmDialog.setTitle("是否确认暂停除垢");
                    mCommonConfirmDialog.setFlag("0");
                    mCommonConfirmDialog.show();
                } else {//恢复运行或第二阶段运行
                    if (Global.isPrepareOk(mDeviceRunSteps)) {
                        if (step == 0 || step == 2) {//未除垢或除垢中途换水进入第二阶段
                            if (SerialManager.getInstance().sendCmdStart(mDeviceRunSteps)) {
                                if (step == 0)
                                    VoiceManager.getInstance().startSpeak("开始除垢");
                                step++;
                                tvSmallTitle.setVisibility(View.GONE);
                                view.setSelected(false);
                                initShowContent();
                            }
                        } else if (SerialManager.getInstance().sendCmdResumeRun()) {
                            tvSmallTitle.setVisibility(View.GONE);
                            view.setSelected(false);
                            initShowContent();
                        }
                    }
                }
                break;
            case R.id.tvBtnOver:
                if (AppParam.mcuStatus != McuStatus.MCU_IDLE) {
                    mCommonConfirmDialog.setTitle("是否确认结束除垢");
                    mCommonConfirmDialog.setFlag("1");
                    mCommonConfirmDialog.show();
                } else
                    Global.jump2MainActivity(mContext);
                break;
        }
    }

    @Subscribe
    public void receivedEvent(EventUpgradeCurve event) {
        LogUtils.d("receiver event from is:" + event.getBusFrom() + " the time is:" + event.getCurveRunStep().getTimeSec());
        imgLight.setSelected(AppParam.isLightOn);
        if (event.getCurveRunStep() != null) {//更新曲线
            if (timeLength - event.getCurveRunStep().getTimeMinute() < 0) {
                tvRemainTime.setText("0");//剩余时间
                circleProgress.setProgressValue(100);
            } else {
                tvRemainTime.setText(timeLength - event.getCurveRunStep().getTimeMinute() + "");//剩余时间
                circleProgress.setProgressValue(event.getCurveRunStep().getTimeMinute() * 100 / timeLength);
            }
        }
        if (!SerialManager.getInstance().latestErrInfo.equals(latestErrInfo)) {
            if (SerialManager.getInstance().latestErrInfo.length() == 0) {
                mWarnInfoDialog.cancel();
            } else
                mWarnInfoDialog.showInfo(SerialManager.getInstance().latestErrInfo);
            latestErrInfo = SerialManager.getInstance().latestErrInfo;
        }
        //判断烹饪是否完成
        if (event.getCurveRunStep().getStage() == AppStaticConfig.COOKING_FINISH_STAGE) {
            if (step == 1 || step == 3) {
                if (SerialManager.getInstance().sendCmdOver()) {
                    step++;
                    if (step == 2) {
                        VoiceManager.getInstance().startSpeak(info2);
                        //SerialManager.getInstance().sendCmdOver();
                    }
                    initShowContent();
                    if (step > 3) {//除垢完成
                        HawkParams.setEvaporatorRunningTime(0);//蒸发盘累计运行时间清零
                        //if (SerialManager.getInstance().sendCmdOver()) {
                        HawkParams.setDescaling(4);
                        HawkParams.setC_totletime(120 * 60);
                        startActivity(new Intent(mContext, CookFinishActivity.class));
                        finish();
                        //}
                    }
                }
            }

        } else {//除垢中
            //运行过程中出现异常(平板烹饪中但MCU未工作)
            if ((HawkParams.getPauseStatus() == 0 && AppParam.mcuStatus == McuStatus.MCU_SUSPENDED)) {
                if (SerialManager.getInstance().sendCmdStop()) {
                    tvbtnPause.setVisibility(View.VISIBLE);
                    tvbtnPause.setSelected(true);
                    tvSmallTitle.setText("已暂停");
                    tvSmallTitle.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Subscribe
    public void receiveEvent(Event2PauseStatus event) {
        LogUtils.d("receiver event from is:" + event.getBusFrom() + " the time is:");
        if (HawkParams.getWorkStatus() == WorkStatus.STATUS_WORKING.value) {
            //if (event.getPauseStatus() == 1) {//暂停指令
            //    if (tvbtnPause.isSelected() == false) {//当前为正常状态
            //        if (SerialManager.getInstance().sendCmdStop())
            //            tvbtnPause.setSelected(true);
            //    } else
            //        LogUtils.e(TAG, "Event2PauseStatus指令不匹配");
            //} else if (event.getPauseStatus() == 0) {//恢复运行指令
            //    if (tvbtnPause.isSelected() == true)
            //        tvbtnPause.performClick();
            //    else
            //        LogUtils.e(TAG, "Event2PauseStatus指令不匹配");
            //} else
            if (event.getPauseStatus() == 2) {//结束运行指令
                if (AppParam.mcuStatus != McuStatus.MCU_IDLE) {
                    if (SerialManager.getInstance().sendCmdOver()) {//结束烹饪
                        Global.jump2MainActivity(mContext);
                    }
                } else
                    Global.jump2MainActivity(mContext);
            }
        }
    }
}
