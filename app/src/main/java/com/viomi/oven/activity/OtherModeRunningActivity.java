//package com.viomi.oven.activity;
//
//import android.content.Context;
//import android.content.Intent;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.squareup.otto.Subscribe;
//import com.viomi.oven.AppParam;
//import com.viomi.oven.AppStaticConfig;
//import com.viomi.oven.HawkParams;
//import com.viomi.oven.R;
//import com.viomi.oven.bean.CurveRunStep;
//import com.viomi.oven.device.DeviceConfig;
//import com.viomi.oven.device.DeviceRunStep;
//import com.viomi.oven.dialog.CommonConfirmDialog;
//import com.viomi.oven.dialog.WarnInfoDialog;
//import com.viomi.oven.enumType.SteamBakeType;
//import com.viomi.oven.enumType.WorkStatus;
//import com.viomi.oven.manager.SerialManager;
//import com.viomi.oven.manager.VoiceManager;
//import com.viomi.oven.otto.Event2PauseStatus;
//import com.viomi.oven.otto.EventUpgradeCurve;
//import com.viomi.oven.util.Global;
//import com.viomi.oven.util.LogUtils;
//import com.viomi.oven.util.ToastUtil;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.BindView;
//import butterknife.OnClick;
//
///**
// * Created by Ljh on 2018/4/24
// */
//public class OtherModeRunningActivity extends BaseActivity {
//    //UI
//    @BindView(R.id.imgBack)
//    ImageView imgBack;
//    @BindView(R.id.tvTitle)
//    TextView tvTitle;
//    @BindView(R.id.tvSmallTitle)
//    TextView tvSmallTitle;
//    @BindView(R.id.imgWifi)
//    ImageView imgWifi;
//    @BindView(R.id.imgWifiV)
//    ImageView imgWifiV;
//    @BindView(R.id.imgLight)
//    ImageView imgLight;
//    @BindView(R.id.tvInfo)
//    TextView tvInfo;
//    @BindView(R.id.tvCurTemp)
//    TextView tvCurTemp;
//    @BindView(R.id.tvUnit)
//    TextView tvUnit;
//    @BindView(R.id.tvbtnPause)
//    TextView tvbtnPause;
//    @BindView(R.id.tvBtnOver)
//    TextView tvBtnOver;
//    @BindView(R.id.llPauseStop)
//    LinearLayout llPauseStop;
//    @BindView(R.id.tvFinish)
//    TextView tvFinish;
//    CommonConfirmDialog mCommonConfirmDialog;
//    WarnInfoDialog mWarnInfoDialog;
//    //参数
//    SteamBakeType mSteamBakeType = SteamBakeType.STEAM;
//    List<CurveRunStep> mCurveRunSteps = new ArrayList<>();
//    List<DeviceRunStep> mDeviceRunSteps = new ArrayList<>();
//    private int timeLengthSec = 0;//单位s
//    float collectInteval = 0;//每隔多少秒采集一个点
//    boolean needStop = false;
//    String latestErrInfo = "";
//    private CurveRunStep lastCurveRunStep;
//    int curTemp = 0, curTime = 0;
//
//    public static void actionStart(Context context, int type) {
//        Intent intent = new Intent(context, OtherModeRunningActivity.class);
//        intent.putExtra("type", type);
//        context.startActivity(intent);
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        imgLight.setSelected(true);
//    }
//
//    @Override
//    protected int getLayoutRes() {
//        return R.layout.activity_other_more_mode_running;
//    }
//
//    @Override
//    protected void initView() {
//        int type = getIntent().getIntExtra("type", 0);
//        for (SteamBakeType steamBakeType : SteamBakeType.values()) {
//            if (steamBakeType.value == type) {
//                mSteamBakeType = steamBakeType;
//                break;
//            }
//        }
//        tvTitle.setText(mSteamBakeType.name);
//        tvInfo.setText("当前温度");
//        tvCurTemp.setText(SerialManager.getInstance().getDataReceiveInfo().getOvenTemp() + "");
//        imgBack.setVisibility(View.GONE);
//        mWarnInfoDialog = new WarnInfoDialog(mContext);
//        mCommonConfirmDialog = new CommonConfirmDialog(mContext);
//        mCommonConfirmDialog.setCallBack(new CommonConfirmDialog.CommonConfirmDialogCallBack() {
//            @Override
//            public void btnOk() {
//                if (mCommonConfirmDialog.getFlag().equals("0")) {//停止烹饪
//                    if (SerialManager.getInstance().sendCmdStop()) {
//                        tvbtnPause.setSelected(true);
//                        tvSmallTitle.setText("已暂停");
//                        tvSmallTitle.setVisibility(View.VISIBLE);
//                    } else {
//                        ToastUtil.show("通讯异常");
//                    }
//                } else {//结束烹饪
//                    if (SerialManager.getInstance().sendCmdOver()) {//结束烹饪
//                        Global.jump2MainActivity(mContext);
//                        finish();
//                    } else {
//                        ToastUtil.show("通讯异常");
//                    }
//                }
//            }
//        });
//    }
//
//    @Override
//    protected void initData() {
//        HawkParams.setCookStartTime(System.currentTimeMillis());
//        HawkParams.setWorkStatus(WorkStatus.STATUS_WORKING);
//        HawkParams.setPauseStatus(0);
//        HawkParams.setC_temps("");
//        timeLengthSec = HawkParams.getC_totletime();
//        mDeviceRunSteps = HawkParams.getCookStep();
//    }
//
//
//    @OnClick({R.id.tvbtnPause, R.id.tvBtnOver, R.id.tvFinish})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.tvbtnPause:
//                if (view.isSelected() == false) {//当前为正常运行状态，则暂停
//                    mCommonConfirmDialog.setTitle("是否确认暂停");
//                    mCommonConfirmDialog.setFlag("0");
//                    mCommonConfirmDialog.show();
//                } else {//恢复运行
//                    if (SerialManager.getInstance().sendCmdResumeRun()) {
//                        tvSmallTitle.setVisibility(View.GONE);
//                        view.setSelected(false);
//                    } else
//                        ToastUtil.show("通讯异常");
//                }
//                break;
//            case R.id.tvBtnOver:
//                mCommonConfirmDialog.setTitle("是否确认结束");
//                mCommonConfirmDialog.setFlag("1");
//                mCommonConfirmDialog.show();
//                break;
//            case R.id.tvFinish:
//                Global.jump2MainActivityNewTask(mContext);
//                break;
//        }
//    }
//
//    @Subscribe
//    public void receivedEvent(EventUpgradeCurve event) {
//        LogUtils.d("receiver event from is:" + event.getBusFrom() + " the time is:" + event.getCurveRunStep().getTimeSec());
//        imgLight.setSelected(AppParam.isLightOn);
//        if (event.getCurveRunStep() != null) {//更新曲线
//            if (mCurveRunSteps.size() == 0) {//第一个点
//                mCurveRunSteps.add(event.getCurveRunStep());
//                HawkParams.setC_temps(event.getCurveRunStep().getIntTemp() + "");
//            } else {//共采集mShownMaxCount = 40个点
//                //if (event.getCurveRunStep().getTimeSec() >= collectInteval * mCurveRunSteps.size() && mCurveRunSteps
//                //        .size() < (curveView.getShowMaxxCount() + 3)) {//采集点更新
//                //    mCurveRunSteps.add(event.getCurveRunStep());
//                //    curveView.updata(mCurveRunSteps);
//                //    HawkParams.addC_temps("" + event.getCurveRunStep().getIntTemp());
//                //    progressBar.setProgress((int) (event.getCurveRunStep().getTimeSec() * 100 * 1.0 / timeLengthSec));
//                //}
//            }
//            //tvTemp.setText((int) event.getCurveRunStep().getTemp() + "");
//            //tvTime.setText((timeLengthSec / 60 - event.getCurveRunStep().getTimeMinute()) + "");//剩余时间
//        }
//        LogUtils.d(TAG, "mCurveRunSteps size is:" + mCurveRunSteps.size());
//        //判断烹饪是否完成
//        if (event.getCurveRunStep().getStage() == AppStaticConfig.COOKING_FINISH_STAGE) {
//            HawkParams.setWorkStatus(WorkStatus.STATUS_FINISH);
//            if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) {
//                tvFinish.setVisibility(View.VISIBLE);
//                llPauseStop.setVisibility(View.GONE);
//            }
//        } else {
//            if (event.getCurveRunStep().getStage() >= 1 && event.getCurveRunStep().getStage() <= 4) {//第一到第四阶段
//                //报警提示
//                if (!SerialManager.getInstance().latestErrInfo.equals(latestErrInfo)) {
//                    if (SerialManager.getInstance().latestErrInfo.length() == 0) {
//                        mWarnInfoDialog.cancel();
//                    } else
//                        mWarnInfoDialog.showInfo(SerialManager.getInstance().latestErrInfo);
//                    latestErrInfo = SerialManager.getInstance().latestErrInfo;
//                }
//                //中途暂停并进行语音提示
//                if (lastCurveRunStep != null && lastCurveRunStep.getStage() != event.getCurveRunStep().getStage()
//                        && mDeviceRunSteps.get(lastCurveRunStep.getStage() - 1).getStop() == 1) {//
//                    if (SerialManager.getInstance().sendCmdStop()) {
//                        VoiceManager.getInstance().startSpeak(mDeviceRunSteps.get(lastCurveRunStep.getStage() - 1).getStoptip());
//                    }
//                }
//                lastCurveRunStep = event.getCurveRunStep();
//            }
//        }
//    }
//
//    @Subscribe
//    public void receiveEvent(Event2PauseStatus event) {
//        LogUtils.d("receiver event from is:" + event.getBusFrom() + " the time is:");
//        if (HawkParams.getWorkStatus() == WorkStatus.STATUS_WORKING.value) {
//            if (event.getPauseStatus() == 1) {//暂停指令
//                if (tvbtnPause.isSelected() == false) {//当前为正常状态
//                    if (SerialManager.getInstance().sendCmdStop())
//                        tvbtnPause.setSelected(true);
//                } else
//                    LogUtils.e(TAG, "Event2PauseStatus指令不匹配");
//            } else if (event.getPauseStatus() == 0) {//恢复运行指令
//                if (tvbtnPause.isSelected() == true)
//                    tvbtnPause.performClick();
//                else
//                    LogUtils.e(TAG, "Event2PauseStatus指令不匹配");
//            } else if (event.getPauseStatus() == 2) {//结束运行指令
//                if (SerialManager.getInstance().sendCmdOver()) {//结束烹饪
//                    Global.jump2MainActivity(mContext);
//                    finish();
//                }
//            }
//        }
//    }
//}
