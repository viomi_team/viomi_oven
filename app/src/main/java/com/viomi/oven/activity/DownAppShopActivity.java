package com.viomi.oven.activity;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;

/**
 * 下载云米商城app
 */
public class DownAppShopActivity extends BaseActivity {
    //UI
    //参数

    @Override
    protected int getLayoutRes() {
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            return R.layout.activity_down_app_shop;
        else
            return R.layout.activity_down_app_shop_h;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
    }
}
