package com.viomi.oven.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.view.PickerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class SelectAppointTimeActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.tvAppointTime)
    TextView tvAppointTime;
    @BindView(R.id.tvCancle)
    TextView tvCancle;
    @BindView(R.id.tvConfirmAppoint)
    TextView tvConfirmAppoint;
    @BindView(R.id.pvHour)
    PickerView pvHour;
    @BindView(R.id.minute_tv)
    TextView minute_tv;
    @BindView(R.id.pvSec)
    PickerView pvSec;
    @BindView(R.id.second_tv)
    TextView second_tv;
    @BindView(R.id.tvWhichDay)
    TextView tvWhichDay;
    //参数
    int hour = 12, minute = 30;
    int timeStamp = 0;//时间戳

    @Override
    protected int getLayoutRes() {
        return DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? R.layout.activity_select_appoint_time_v : R.layout.activity_select_appoint_time;
    }

    @Override
    protected void initView() {
        imgBack.setVisibility(View.GONE);
    }

    @Override
    protected void initData() {
        hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        hour = (hour == 23 ? 0 : (hour + 1));
        tvWhichDay.setText(hour == 0 ? "明天" : "今天");
        minute = Calendar.getInstance().get(Calendar.MINUTE);
        tvAppointTime.setText(hour + " : " + (minute < 10 ? "0" + minute : minute));
        List<String> data = new ArrayList<String>();
        List<String> seconds = new ArrayList<String>();
        for (int i = 0; i < 24; i++) {
            data.add(i < 10 ? "0" + i : "" + i);
        }
        for (int i = 0; i < 60; i++) {
            seconds.add(i < 10 ? "0" + i : "" + i);
        }
        pvHour.setData(data);
        pvHour.setOnSelectListener(new PickerView.onSelectListener() {

            @Override
            public void onSelect(String text) {
                hour = Integer.parseInt(text);
                updateTimeView(hour, minute);
            }
        });
        pvHour.moveToSelectItem(hour < 10 ? "0" + hour : "" + hour);
        //
        pvSec.setData(seconds);
        pvSec.setOnSelectListener(new PickerView.onSelectListener() {

            @Override
            public void onSelect(String text) {
                minute = Integer.parseInt(text);
                updateTimeView(hour, minute);
            }
        });
        pvSec.moveToSelectItem(minute < 10 ? "0" + minute : "" + minute);
    }

    public void updateTimeView(int hour, int minute) {
        tvAppointTime.setText(hour + " : " + (minute < 10 ? ("0" + minute) : ("" + minute)));
        if (hour < Calendar.getInstance().get(Calendar.HOUR_OF_DAY) ||
                (hour == Calendar.getInstance().get(Calendar.HOUR_OF_DAY) && minute <= Calendar.getInstance().get(Calendar.MINUTE)))
            tvWhichDay.setText("明天");
        else
            tvWhichDay.setText("今天");
    }

    @OnClick({R.id.tvCancle, R.id.tvConfirmAppoint})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvCancle:
                onBackPressed();
                break;
            case R.id.tvConfirmAppoint:
                if (Global.isPrepareOk(HawkParams.getCookStep())) {
                    Date date = new Date();//取时间
                    if (tvWhichDay.getText().toString().equals("明天")) {
                        Calendar calendar = new GregorianCalendar();
                        calendar.setTime(date);
                        calendar.add(calendar.DATE, 1);//把日期往后增加一天.整数往后推,负数往前移动
                        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
                    }
                    date.setHours(hour);
                    date.setMinutes(minute);
                    ///
                    LogUtils.d(TAG, "Appoint2:" + date);
                    HawkParams.setAppointTime((int) (date.getTime() / 1000));
                    AppointPreviewActivity.actionStart(mContext, true);
                    setResult(RESULT_OK);
                    finish();
                }
                break;
        }
    }
}
