package com.viomi.oven.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;

import butterknife.BindView;

public class UseGuideActivity extends BaseActivity {
    //UI
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.imgWifiV)
    ImageView imgWifiV;
    @BindView(R.id.imgWifi)
    ImageView imgWifi;
    //参数

    @Override
    protected int getLayoutRes() {
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            return R.layout.activity_use_guide;
        else return R.layout.activity_use_guide_h;
    }

    @Override
    protected void initView() {
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            imgWifiV.setVisibility(View.VISIBLE);
        else
            imgWifi.setVisibility(View.VISIBLE);
    }

    @Override
    protected void initData() {
    }
}
