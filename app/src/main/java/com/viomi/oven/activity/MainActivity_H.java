package com.viomi.oven.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.net.wifi.WifiManager;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.viomi.oven.AppParam;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.adapter.TubatuAdapter1;
import com.viomi.oven.enumType.ModelType;
import com.viomi.oven.manager.AccountManager;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.service.BackgroudService;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.NetworkUtils;
import com.viomi.oven.util.ToastUtil;
import com.viomi.oven.view.ClipViewPager;
import com.viomi.oven.view.DotsView;
import com.viomi.oven.view.ScalePageTransformer;
import com.viomi.oven.view.WaveView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity_H extends BaseActivity {
    //UI
    @BindView(R.id.imgLight)
    ImageView imgLight;
    @BindView(R.id.imgPannel)
    ImageView imgPannel;
    @BindView(R.id.rlContainer)
    RelativeLayout rlContainer;
    @BindView(R.id.viewpager)
    ClipViewPager viewpager;
    @BindView(R.id.dots)
    DotsView dots;
    @BindView(R.id.waveView)
    WaveView waveView;

    //参数
    private WifiManager wifiManager;
    private Intent backgroudServiceintent;
    private TubatuAdapter1 mTubatuAdapter1;
    //
    public static final int MSG_SHOW_WAVE = 1000;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main_h;
    }

    @Override
    protected void onResume() {
        super.onResume();
        initParams();
        sendToHandlerDelayed(MSG_SHOW_WAVE, 1000);
    }

    public void initParams() {
        Global.backMainInitHawkParams();
    }

    @Override
    protected void initView() {
        rlContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return viewpager.dispatchTouchEvent(event);
            }
        });
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                LogUtils.d(TAG, " onPageScrolled position is:" + position + " positionOffset is:" + positionOffset + " positionOffsetPixels is:" + positionOffsetPixels);
                waveView.stop();
                if (positionOffsetPixels == 0)
                    mTubatuAdapter1.setCurPos(position);
            }

            @Override
            public void onPageSelected(int position) {
                LogUtils.d(TAG, "the onPageSelected is:" + position);
                dots.dotSelect(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                LogUtils.d(TAG, " onPageScrollStateChanged is:" + state);
                sendToHandlerDelayed(MSG_SHOW_WAVE, 1000);
            }
        });
        viewpager.setOnItemClick(new ClipViewPager.ItemClickCallBack() {
            @Override
            public void click(int pos) {
                if (pos == 0) {
                    if (!NetworkUtils.isAvailable()) {
                        ToastUtil.show("Wifi未连接");
                    } else {
                        HawkParams.setModelType(ModelType.MODEL_SCAN);
                        ScannerActivity.actionStart(MainActivity_H.this,
                                false, ScannerActivity.EXTRA_LASER_LINE_MODE_0, ScannerActivity.EXTRA_SCAN_MODE_0, false
                                , false, false);
                    }
                } else if (pos == 1) {
                    HawkParams.setModelType(ModelType.MODEL_SMART);
                    startActivity(new Intent(mContext, FoodMenuActivity.class));
                } else if (pos == 2) {
                    HawkParams.setModelType(ModelType.MODEL_SPECIAL);
                    HawkParams.setFoodName("专业烹饪");
                    startActivity(new Intent(mContext, SpecialCookingActivity_H.class));
                } else if (pos == 3) {
                    if (AccountManager.getViomiUser(mContext) == null) {
                        startActivity(new Intent(mContext, ScanLoginActivity.class));
                    } else {
                        HawkParams.setModelType(ModelType.MODEL_MY_SET);
                        startActivity(new Intent(mContext, FoodMenuActivity.class));
                    }
                } else if (pos == 4) {
                    startActivity(new Intent(mContext, SettingActivity.class));
                } else if (pos == 5) {
                    startActivity(new Intent(mContext, MoreActivity.class));
                } else if (pos == 6) {
                    if (!NetworkUtils.isAvailable()) {
                        ToastUtil.show("Wifi未连接");
                    } else {
                        if (AccountManager.getViomiUser(mContext) == null) {
                            startActivity(new Intent(mContext, ScanLoginActivity.class));
                        } else {
                            startActivity(new Intent(mContext, OneKeyBuyActivity.class));
                        }
                    }
                }
            }
        });

        waveView.setDuration(6000);
        waveView.setInitialRadius(200);
        waveView.setStyle(Paint.Style.FILL);
        waveView.setColor(getResources().getColor(R.color.wave_color));
        //waveView.setColor(Color.GRAY);
        waveView.setInterpolator(new LinearOutSlowInInterpolator());
    }

    @Override
    protected void initData() {
        //开机自动连wifi
        wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);
        initService();
        //
        viewpager.setPageTransformer(true, new ScalePageTransformer());
        viewpager.setPageMargin(0);
        //
        mTubatuAdapter1 = new TubatuAdapter1(this);
        viewpager.setAdapter(mTubatuAdapter1);
        //

        List<Integer> list1 = new ArrayList<>();
        list1.add(R.drawable.model_scan1);
        list1.add(R.drawable.model_smart1);
        list1.add(R.drawable.model_special1);
        list1.add(R.drawable.model_my_set1);
        list1.add(R.drawable.model_setting1);
        list1.add(R.drawable.model_more1);
        list1.add(R.drawable.model_buy);

        List<String> names = new ArrayList<>(Arrays.asList("扫码烹饪", "智能菜谱", "专业烹饪", "我的菜谱", "设置", "更多", "一键购"));

        dots.setCount(list1.size());
        //设置OffscreenPageLimit
        viewpager.setOffscreenPageLimit(Math.min(list1.size(), 6));
        //mPagerAdapter.addAll(list);
        mTubatuAdapter1.addAll(list1, names);
        viewpager.setCurrentItem(1);
        waveView.start();
    }

    @OnClick({R.id.imgPannel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgPannel:
                if (!SerialManager.getInstance().sendCmdControlPanelSwitch(true)) {
                    //showToast("设置失败");
                }
                break;
        }
    }

    private void initService() {
        backgroudServiceintent = new Intent(this, BackgroudService.class);
        startService(backgroudServiceintent);
    }

    private void stopService() {
        stopService(backgroudServiceintent);
    }

    @Override
    public void dealTheMsg(Message msg) {
        super.dealTheMsg(msg);
        if (msg.what == MSG_SHOW_WAVE) {
            waveView.start();
            imgLight.setSelected(AppParam.isLightOn);
        }
    }

    @Override
    protected void onPause() {
        waveView.stop();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        stopService();
        super.onDestroy();
    }
}
