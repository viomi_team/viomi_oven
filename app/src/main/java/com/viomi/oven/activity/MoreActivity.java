package com.viomi.oven.activity;

import android.content.Intent;
import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.adapter.RAdapterMoreModel;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.dialog.WarnInfoDialog;
import com.viomi.oven.enumType.FromType;
import com.viomi.oven.enumType.SteamBakeType;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Ljh on 2018/1/3
 * 更多
 */

public class MoreActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgWifi)
    ImageView imgWifi;
    @BindView(R.id.imgWifiV)
    ImageView imgWifiV;
    @BindView(R.id.recycle)
    RecyclerView recycle;
    WarnInfoDialog mWarnInfoDialog;
    //参数
    List<SteamBakeType> datas = new ArrayList<>();
    RAdapterMoreModel mRAdapterMoreModel;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_more;
    }

    @Override
    protected void initView() {
        mWarnInfoDialog = new WarnInfoDialog(mContext);
    }

    @Override
    protected void initData() {
        HawkParams.setCookFrom(FromType.FROM_OTHER.value);
        /*
        datas.add(SteamBakeType.STEAM);
        //
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            datas.add(SteamBakeType.BAKE);
        else datas.add(SteamBakeType.BAKE_6);
        //
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            datas.add(SteamBakeType.STEAM_BAKE);
        else datas.add(SteamBakeType.STEAM_BAKE_1);
        */
        //
        datas.add(SteamBakeType.FERMENTATION);
        datas.add(SteamBakeType.HOT_FOOD);
        datas.add(SteamBakeType.THAW);
        datas.add(SteamBakeType.DISINFECTION);
        datas.add(SteamBakeType.CLEAN);
        datas.add(SteamBakeType.PREHEATING);
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V2)) {
            datas.add(SteamBakeType.CHUGOU);
            datas.add(SteamBakeType.KEEP_WARM);
        }
        mRAdapterMoreModel = new RAdapterMoreModel(mContext, datas);
        mRAdapterMoreModel.setCallBack(new RAdapterMoreModel.MoreModelCallBack() {
            @Override
            public void clicked(int pos) {
                if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) {
                    MoreModeRunActivity.actionStart(mContext, datas.get(pos).value);
                } else {
                    //其中清洁模式任何参数都不调
                    if (datas.get(pos) == SteamBakeType.CHUGOU) {//除垢模式
                        startActivity(new Intent(mContext, ChugouActivity.class));
                    } else {
                        ChangeParamActivity.actionStart(mContext, datas.get(pos).value);
                    }
                }
            }
        });
        int colume = DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? 2 : 4;
        //ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(this, colume);
        //gridLayoutManager.setScrollEnabled(false);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, colume);
        recycle.setLayoutManager(gridLayoutManager);
        recycle.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) {
                    outRect.set(35, 38, 35, 38);
                } else {
                    outRect.set(40, 30, 40, 30);
                }
            }
        });
        recycle.setAdapter(mRAdapterMoreModel);
        //需要除垢
        if (HawkParams.isNeedChugou() && DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V2)) {
            mWarnInfoDialog.showInfo("蒸发盘已累计工作满100小时，请将柠檬酸除垢剂（粉）与水按照20g:1L的比例加入水箱后进行除垢");
        }
    }
}
