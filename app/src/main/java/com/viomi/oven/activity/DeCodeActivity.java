//package com.viomi.oven.activity;
//
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.os.Bundle;
//import android.view.WindowManager;
//
//import com.google.zxing.Result;
//import com.google.zxing.client.result.ParsedResultType;
//import com.mylhyl.zxing.scanner.decode.QRDecode;
//import com.viomi.oven.util.LogUtils;
//
///**
// * 单击解析图片
// */
//public class DeCodeActivity extends BasicScannerActivity {
//    private static final String TAG = "DeCodeActivity";
//    @Override
//    void onResultActivity(Result result, ParsedResultType type, Bundle bundle) {
//        LogUtils.d(TAG," the result = "+result.getText());
//        switch (type) {
//            case ADDRESSBOOK:
//                //AddressBookActivity.gotoActivity(DeCodeActivity.this, bundle);
//                break;
//            case PRODUCT:
//                //BarcodeActivity.gotoActivity(DeCodeActivity.this, bundle);
//                break;
//            case ISBN:
//                //BarcodeActivity.gotoActivity(DeCodeActivity.this, bundle);
//                break;
//            case URI:
//                //UriActivity.gotoActivity(DeCodeActivity.this, bundle);
//                break;
//            case TEXT:
//                //TextActivity.gotoActivity(DeCodeActivity.this, bundle);
//                break;
//            case GEO:
//                break;
//            case TEL:
//                break;
//            case SMS:
//                break;
//        }
//        dismissProgressDialog();
//        finish();
//    }
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        Bundle extras = getIntent().getExtras();
//        if (extras != null) {
//            byte[] bytes = extras.getByteArray("bytes");
//            if (bytes != null && bytes.length > 0) {
//                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
//                if (bitmap != null) {
//                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//                    showProgressDialog();
//                    QRDecode.decodeQR(bitmap, this);
//                }
//            }
//        }
//    }
//}
