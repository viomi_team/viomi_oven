package com.viomi.oven.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Message;
import android.os.SystemClock;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.bean.CheckScanResult;
import com.viomi.oven.bean.LoginQrCode;
import com.viomi.oven.bean.ViomiUser;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.interfaces.AppCallback;
import com.viomi.oven.manager.AccountManager;
import com.viomi.oven.manager.DeviceManager;
import com.viomi.oven.util.DataProcessUtil;
import com.viomi.oven.util.GsonTools;
import com.viomi.oven.util.ImgUtil;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.NetworkUtils;
import com.viomi.oven.util.PhoneUtil;
import com.viomi.oven.util.StringUtil;
import com.viomi.oven.util.ToastUtil;
import com.viomi.oven.util.http.MyStringCallback;
import com.zhy.http.okhttp.OkHttpUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;

/**
 * Created by Ljh on 2018/1/3
 * 扫码登录
 */

public class ScanLoginActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvWarn)
    TextView tvWarn;
    @BindView(R.id.imgWifiV)
    ImageView imgWifiV;
    @BindView(R.id.imgWifi)
    ImageView imgWifi;
    @BindView(R.id.logoViomi)
    ImageView logoViomi;
    @BindView(R.id.imgLight)
    ImageView imgLight;
    @BindView(R.id.tvUseInfo)
    TextView tvUseInfo;
    @BindView(R.id.imgQr)
    ImageView imgQr;
    @BindView(R.id.rlImgQr)
    RelativeLayout rlImgQr;
    @BindView(R.id.llQr)
    LinearLayout llQr;
    @BindView(R.id.tvDowmInfo)
    TextView tvDowmInfo;
    @BindView(R.id.tvRefresh)
    TextView tvRefresh;
    private ProgressDialog progressDialog = null;
    //参数
    private CheckThread checkThread;
    private AppCallback<String> mBindCallback;

    private String mClientId;
    private ViomiUser mTempViomiUser;

    private static final int MSG_WHAT_CHECK_LOGIN = 2;
    private static final int MSG_WHAT_CHECK_LOGIN_FAIL = 4;
    private static final int MSG_WHAT_LOGIN_FAIL = 6;
    private static final int MSG_WHAT_LOGIN_SUCCESS = 7;
    private static final int MSG_WHAT_LOGIN_TIMEOUT = 8;

    public static void actionStartForResult(Context context, int reqCode) {
        Intent intent = new Intent(context, ScanLoginActivity.class);
        ((Activity) context).startActivityForResult(intent, reqCode);
    }


    @Override
    protected int getLayoutRes() {
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            return R.layout.activity_scan_login;
        else
            return R.layout.activity_scan_login_h;
    }

    @Override
    protected void initView() {
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            imgWifiV.setVisibility(View.VISIBLE);
        else
            imgWifiV.setVisibility(View.VISIBLE);
        tvDowmInfo.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
        tvUseInfo.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
    }

    @Override
    protected void initData() {
        if (NetworkUtils.isAvailable())
            httpGetQr();
        else {
            reFreshTvWarn(false);
            logoViomi.setVisibility(View.GONE);
            imgQr.setImageResource(R.drawable.net_error);
        }
    }

    public void reFreshTvWarn(boolean ok) {
        tvWarn.setText(ok ? "使用手机端，云米商城\nAPP扫码登录" : "加载异常，请重新刷新");
    }

    public void httpGetQr() {
        showLoading();
        OkHttpUtils
                .get()
                .url(AppStaticConfig.URL_CREATOR_QR)
                .addParams("type", "1")
                .addParams("clientID", mClientId = DataProcessUtil.formatMac(PhoneUtil.getMiIdentify().mac) + System.currentTimeMillis() / 1000)
                .tag(this)
                .build()
                .execute(new MyStringCallback() {

                    @Override
                    public void onSuccess(String result) {
                        hideLoading();
                        if (checkThread != null) {
                            checkThread.interrupt();
                            checkThread = null;
                        }
                        LoginQrCode loginQrCode = GsonTools.ReGetClassItem(result, LoginQrCode.class);
                        if (loginQrCode.getCode() == AppStaticConfig.HTTP_SUCCESS) {
                            reFreshTvWarn(true);
                            //logoViomi.setVisibility(View.VISIBLE);
                            ImgUtil.showImage(loginQrCode.getResult(), imgQr);
                            //启动线程查询是否扫描成功
                            checkThread = new CheckThread((ScanLoginActivity) mContext);
                            checkThread.start();
                        } else {//提示重新获取
                            reFreshTvWarn(false);
                            logoViomi.setVisibility(View.GONE);
                            imgQr.setImageResource(R.drawable.net_error);
                        }
                    }

                    @Override
                    public void onFail(int errorCode) {
                        hideLoading();
                        reFreshTvWarn(false);
                        logoViomi.setVisibility(View.GONE);
                        imgQr.setImageResource(R.drawable.net_error);
                    }
                });
    }

    public void httpCheckLogin() {
        JSONObject paramsJson = new JSONObject();
        try {
            paramsJson.put("clientID", mClientId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        OkHttpUtils
                .postString()
                .url(AppStaticConfig.URL_CHECK_LOGIN_STATUS)
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .content(paramsJson.toString())
                .tag("httpCheckLogin")
                .build()
                .execute(new MyStringCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if (mContext == null || ((Activity) mContext).isFinishing())
                            return;
                        CheckScanResult mCheckScanResult = GsonTools.ReGetClassItem(result, CheckScanResult.class);
                        if (mCheckScanResult != null && mCheckScanResult.getCode() == AppStaticConfig.HTTP_SUCCESS && mCheckScanResult.checkDataOk()) {
                            mTempViomiUser = AccountManager.parserViomiUser(mCheckScanResult.getAppendAttr());
                            if (mTempViomiUser != null) {
                                if (checkThread != null) {
                                    checkThread.interrupt();
                                    checkThread = null;
                                }
                                mTempViomiUser.setToken(mCheckScanResult.getToken());
                                mTempViomiUser.setUserCode(mCheckScanResult.getLoginData().getUserCode());
                                HawkParams.setUserId(mCheckScanResult.getLoginData().getUserId());
                                LogUtils.d(TAG, "viomiUser success，token=" + mTempViomiUser.getToken() + ",usercode=" + mTempViomiUser.getUserCode());
                                if (mBindCallback == null) {
                                    mBindCallback = new AppCallback<String>() {
                                        @Override
                                        public void onSuccess(String data) {
                                            if (!StringUtil.isEmpty(data))
                                                LogUtils.d(TAG, "bind onSuccess" + data);
                                            if (mTempViomiUser.getType().equals("ios")) {
                                                HawkParams.setAndroidScanLogin(false);
                                            } else {
                                                HawkParams.setAndroidScanLogin(true);
                                            }
                                            sendToHandler(MSG_WHAT_LOGIN_SUCCESS);
                                        }

                                        @Override
                                        public void onFail(int errorCode, String msg) {
                                            LogUtils.d(TAG, "bind fail!errorCode=" + errorCode + ",msg=" + msg);
                                            sendToHandler(MSG_WHAT_LOGIN_FAIL);
                                        }
                                    };
                                }
                                DeviceManager.getInstance().reBindDevice(mTempViomiUser.getMiId(), mBindCallback);
                                progressDialog = ProgressDialog.show(ScanLoginActivity.this, "请稍候...", "授权登录中...", true, true);
                                sendToHandlerDelayed(MSG_WHAT_LOGIN_TIMEOUT, 60000);
                            }
                        }
                    }

                    @Override
                    public void onFail(int errorCode) {

                    }
                });
    }

    @Override
    public void dealTheMsg(Message msg) {
        super.dealTheMsg(msg);
        switch (msg.what) {
            case MSG_WHAT_CHECK_LOGIN:
                if (NetworkUtils.isAvailable())
                    httpCheckLogin();
                break;
            case MSG_WHAT_LOGIN_FAIL:
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                tvRefresh.performClick();
                //checkThread = new CheckThread((ScanLoginActivity) mContext);
                //checkThread.start();
                ToastUtil.show(mContext.getString(R.string.toast_login_fail) + "!" + (String) msg.obj);
                break;
            case MSG_WHAT_LOGIN_SUCCESS:
                if (checkThread != null) {
                    checkThread.interrupt();
                    checkThread = null;
                }
                AccountManager.saveViomiUser(mContext, mTempViomiUser);//保存商城帐号信息
                HawkParams.setScanPhoneType(mTempViomiUser.getType().equals("ios") ? 1 : 0);
                HawkParams.setViomiLoginTime(System.currentTimeMillis() / 1000);
                ToastUtil.show(mContext.getString(R.string.toast_login_success));
                setResult(RESULT_OK);
                finish();
                break;
            case MSG_WHAT_LOGIN_TIMEOUT:
                ToastUtil.show(mContext.getString(R.string.toast_login_time_out));
                finish();
                break;
        }
    }

    private static class CheckThread extends Thread {
        private WeakReference<ScanLoginActivity> weakReference;

        public CheckThread(ScanLoginActivity activity) {
            this.weakReference = new WeakReference<ScanLoginActivity>(activity);
        }

        @Override
        public void run() {
            while (!isInterrupted()) {
                ScanLoginActivity mActivity = weakReference.get();
                if (mActivity != null) {
                    mActivity.sendToHandler(MSG_WHAT_CHECK_LOGIN);
                    SystemClock.sleep(3000);
                }
            }
        }
    }

    @OnClick({R.id.imgBack, R.id.tvDowmInfo, R.id.tvRefresh, R.id.tvUseInfo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvDowmInfo:
                startActivity(new Intent(mContext, DownAppShopActivity.class));
                break;
            case R.id.imgBack:
                finish();
                break;
            case R.id.tvRefresh:
                httpGetQr();
                break;
            case R.id.tvUseInfo:
                startActivity(new Intent(mContext, UseGuideActivity.class));
                break;
        }
    }

    @Override
    protected void onDestroy() {
        if (null != checkThread) {
            checkThread.interrupt();
            checkThread = null;
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        super.onDestroy();
    }
}
