package com.viomi.oven.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.viomi.oven.R;
import com.viomi.oven.adapter.WifiAdapter;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.ToastUtil;
import com.viomi.oven.util.wifimodel.Wifi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Ljh on 2017/12/16
 */

public class WifiScanActivity extends BaseActivity {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.imgWifiV)
    ImageView imgWifiV;
    @BindView(R.id.imgSwitch)
    ImageView imgSwitch;
    @BindView(R.id.wifiListview)
    ListView wifiListview;
    //
    private WifiAdapter adapter;
    private WifiManager wifiManager;
    private List<ScanResult> scanlist = new ArrayList<>();

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_wifi_scan;
    }

    @Override
    protected void initView() {
        imgWifiV.setVisibility(View.VISIBLE);
        imgWifiV.setSelected(true);
        tvTitle.setText("无线局域网");
        wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        //
        imgSwitch.setSelected(wifiManager.isWifiEnabled());
        wifiListview.setVisibility(wifiManager.isWifiEnabled() ? View.VISIBLE : View.INVISIBLE);
        adapter = new WifiAdapter(mContext, scanlist, wifiManager);
        wifiListview.setAdapter(adapter);
        wifiListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                launchWifiConnecter(scanlist.get(position));
            }
        });
    }

    @Override
    protected void initData() {
        IntentFilter filter = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        //filter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        registerReceiver(mReceiver, filter);
        if (wifiManager.isWifiEnabled()) {
            wifiManager.startScan();
            showLoading();
        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            LogUtils.w(TAG, "the action is:" + action);
            if (action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                List<ScanResult> sResults = wifiManager.getScanResults();
                Map<String, ScanResult> sMap = new HashMap<>();
                for (int i = 0; i < sResults.size(); i++) {
                    ScanResult result = sResults.get(i);
                    if (result.SSID != null && "" != result.SSID) {
                        if (sMap.containsKey(result.SSID)) {
                            String security = Wifi.ConfigSec.getScanResultSecurity(result);
                            WifiConfiguration config = Wifi.getWifiConfiguration(wifiManager, result, security);
                            WifiInfo info = wifiManager.getConnectionInfo();
                            boolean isCurrentNetwork_WifiInfo = info != null && android.text.TextUtils.equals(info.getSSID(), "\"" + result.SSID + "\"") && android.text.TextUtils.equals(info.getBSSID(), result.BSSID);
                            if (!(config != null && isCurrentNetwork_WifiInfo)) {
                                continue;
                            }
                        }
                        sMap.put(result.SSID, result);
                    }
                }

                Collection<ScanResult> values = sMap.values();
                scanlist.clear();
                scanlist.addAll(values);
                adapter.notifyDataSetChanged();
                wifiManager.startScan();
                hideLoading();
            } else if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            } else if (action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                int mWifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, 0);
                LogUtils.w(TAG, "the EXTRA_WIFI_STATE is:" + mWifiState);
                if (mWifiState == WifiManager.WIFI_STATE_ENABLED) {
                    hideLoading();
                } else if (mWifiState == WifiManager.WIFI_STATE_DISABLED) {
                    hideLoading();
                }
                LogUtils.w(TAG, "after wifiStatus:" + wifiManager.isWifiEnabled());
                imgSwitch.setSelected(wifiManager.isWifiEnabled());
                wifiListview.setVisibility(wifiManager.isWifiEnabled() ? View.VISIBLE : View.INVISIBLE);
                if (wifiManager.isWifiEnabled()) {
                    scanlist.clear();
                    adapter.notifyDataSetChanged();
                    wifiManager.startScan();
                    showLoading();
                }
            }
            //else if(action.equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION)){
            //    int linkWifiResult = intent.getIntExtra(WifiManager.EXTRA_SUPPLICANT_ERROR, 123);
            //    if (linkWifiResult == WifiManager.ERROR_AUTHENTICATING) {
            //        ToastUtil.show("密码错误");
            //    }
            //}
        }
    };

    private void launchWifiConnecter(final ScanResult hotspot) {
        Intent intent = new Intent(mContext, WifiConnectActivity.class);
        intent.putExtra("EXTRA_HOTSPOT", hotspot);
        startActivity(intent);
    }

    @OnClick(R.id.imgSwitch)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgSwitch:
                boolean curStatus = wifiManager.isWifiEnabled();
                LogUtils.w(TAG, "curWifiEnable is:" + curStatus);
                if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_DISABLING) {
                    ToastUtil.show("wifi正在关闭，请稍后");
                } else if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLING) {
                    ToastUtil.show("wifi正在打开，请稍后");
                } else if (wifiManager.getWifiState() != WifiManager.WIFI_STATE_UNKNOWN) {
                    wifiManager.setWifiEnabled(!wifiManager.isWifiEnabled());
                    showLoading();
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

}
