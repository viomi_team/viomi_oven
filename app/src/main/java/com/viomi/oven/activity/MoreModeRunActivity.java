package com.viomi.oven.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.dialog.NumberPickerDialog;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.util.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Ljh on 2018/4/17
 */
public class MoreModeRunActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvSmallTitle)
    TextView tvSmallTitle;
    @BindView(R.id.imgWifi)
    ImageView imgWifi;
    @BindView(R.id.imgWifiV)
    ImageView imgWifiV;
    @BindView(R.id.imgLight)
    ImageView imgLight;
    @BindView(R.id.tvInfo)
    TextView tvInfo;
    @BindView(R.id.tvCurTemp)
    TextView tvCurTemp;
    @BindView(R.id.tvUnit)
    TextView tvUnit;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tvUseInfo)
    TextView tvUseInfo;
    @BindView(R.id.tvTimeNick)
    TextView tvTimeNick;
    @BindView(R.id.llTime)
    LinearLayout llTime;
    @BindView(R.id.tvTemp)
    TextView tvTemp;
    @BindView(R.id.tvTempNick)
    TextView tvTempNick;
    @BindView(R.id.llTemp)
    LinearLayout llTemp;
    @BindView(R.id.llMenu)
    LinearLayout llMenu;
    @BindView(R.id.imgStart)
    ImageView imgStart;
    NumberPickerDialog mNumberPickerDialog;
    //参数
    SteamBakeType mSteamBakeType = SteamBakeType.STEAM;
    List<DeviceRunStep> mDeviceRunSteps = new ArrayList<>();
    List<String> tempData = new ArrayList<>();
    List<String> timeData = new ArrayList<>();
    boolean llTimeVisible = true, llTempVisible = true;
    int curTemp = 0, curTime = 0;

    public static void actionStart(Context context, int type) {
        Intent intent = new Intent(context, MoreModeRunActivity.class);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_more_mode_run;
    }

    @Override
    protected void initView() {
        int type = getIntent().getIntExtra("type", 0);
        for (SteamBakeType steamBakeType : SteamBakeType.values()) {
            if (steamBakeType.value == type) {
                mSteamBakeType = steamBakeType;
                break;
            }
        }
        tvTitle.setText(mSteamBakeType.name);
        tvInfo.setText("当前温度");
        tvTempNick.setText(mSteamBakeType.name+"温度");
        tvTimeNick.setText(mSteamBakeType.name+"时间");
        tvCurTemp.setText(SerialManager.getInstance().getDataReceiveInfo().getOvenTemp() + "");
        if (mSteamBakeType == SteamBakeType.PREHEATING) {
            imgStart.setImageResource(R.drawable.press_start_heat_v);
            llTimeVisible = false;
        } else if (mSteamBakeType == SteamBakeType.CLEAN) {
            llTempVisible = false;
            tvUseInfo.setText("此功能是在55℃蒸汽模式下，让腔体内壁上的污渍逐渐软化从而便于清洁。待功能结束后，请用抹布或海绵蘸取带有洗涤剂的温水擦拭内壁，清洁完成后，再将内壁擦干");
        }
        llTemp.setVisibility(llTempVisible ? View.VISIBLE : View.GONE);
        llTime.setVisibility(llTimeVisible ? View.VISIBLE : View.GONE);
        /////////////////////////////////////////////////////////////////////
        tvTemp.setText((curTemp = mSteamBakeType.defTemp) + "");
        tvTime.setText((curTime = mSteamBakeType.defTime) + "");
        mNumberPickerDialog = new NumberPickerDialog(mContext);
        mNumberPickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (mNumberPickerDialog.isChanged) {
                    if (mNumberPickerDialog.getFlag().equals("0")) {//选择温度
                        curTemp = Integer.parseInt(tempData.get(mNumberPickerDialog.getPos()).toString());
                        tvTemp.setText(curTemp + "");
                        if (curTemp > 220 && curTime > 40 && DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)
                                && (mSteamBakeType == SteamBakeType.BAKE || mSteamBakeType == SteamBakeType.STEAM_BAKE)) {
                            curTime = 40;
                            tvTime.setText(curTime + "");
                        }
                    } else {//选择时间
                        curTime = Integer.parseInt(timeData.get(mNumberPickerDialog.getPos()).toString());
                        tvTime.setText(curTime + "");
                    }
                }
            }
        });
    }

    @Override
    protected void initData() {
        mDeviceRunSteps.clear();
        mDeviceRunSteps.add(new DeviceRunStep(mSteamBakeType));
    }

    @OnClick({R.id.llTime, R.id.llTemp, R.id.imgStart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llTime:
                timeData.clear();
                mNumberPickerDialog.setFlag("1");
                int maxTime = mSteamBakeType.maxTime;
                if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)
                        && (mSteamBakeType == SteamBakeType.BAKE || mSteamBakeType == SteamBakeType.STEAM_BAKE) && curTemp > 220)
                    maxTime = 40;
                for (int i = mSteamBakeType.minTime; i <= maxTime; i++) {
                    timeData.add(i + "");
                }
                mNumberPickerDialog.update(timeData);
                mNumberPickerDialog.show(curTime + "", "分");
                break;
            case R.id.llTemp:
                tempData.clear();
                for (int i = mSteamBakeType.minTemp; i <= mSteamBakeType.maxTemp; i++) {
                    tempData.add(i + "");
                }
                mNumberPickerDialog.setFlag("0");
                mNumberPickerDialog.update(tempData);
                mNumberPickerDialog.show(curTemp + "", "℃");
                break;
            case R.id.imgStart:
                mDeviceRunSteps.get(0).setTemp(curTemp);
                mDeviceRunSteps.get(0).setTime(curTime);
                if (Global.isPrepareOk(mDeviceRunSteps)) {
                    if (SerialManager.getInstance().sendCmdStart(mDeviceRunSteps)) {
                        HawkParams.setMode(mSteamBakeType);
                        if (mSteamBakeType == SteamBakeType.PREHEATING) {
                            HawkParams.delCookStep();
                            PreHeatingActivity.actionStart(mContext, mDeviceRunSteps.get(0).getTemp());
                        } else {
                            HawkParams.setFoodName(mDeviceRunSteps.get(0).getType().name);
                            startActivity(new Intent(mContext, CookRunningActivity.class));
                        }
                        finish();
                    }
                }
                break;
        }
    }
}
