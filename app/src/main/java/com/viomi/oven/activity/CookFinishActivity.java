package com.viomi.oven.activity;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.otto.Subscribe;
import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.bean.BasicKeyInfo;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.enumType.FromType;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.manager.DeviceManager;
import com.viomi.oven.manager.VoiceManager;
import com.viomi.oven.otto.Event2PauseStatus;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.LogUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;

public class CookFinishActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvHour)
    TextView tvHour;
    @BindView(R.id.tvFinishInfo)
    TextView tvFinishInfo;
    @BindView(R.id.tvMinute)
    TextView tvMinute;
    @BindView(R.id.imgWifiV)
    ImageView imgWifiV;
    @BindView(R.id.tvFinishBack)
    TextView tvFinishBack;
    @BindView(R.id.llNutritionInfo)
    LinearLayout llNutritionInfo;
    @BindView(R.id.llBasicContent)
    LinearLayout llBasicContent;
    //参数
    List<BasicKeyInfo> mBasicKeyInfos = new ArrayList<>();
    int timeLengthMinute = 0;

    @Override
    protected int getLayoutRes() {
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            return R.layout.activity_cook_finish;
        else
            return R.layout.activity_cook_finish_h;
    }

    @Override
    protected void initView() {
        imgBack.setSelected(true);
        timeLengthMinute = HawkParams.getC_totletime() / 60;
        tvMinute.setText(timeLengthMinute % 60 + "");
        tvHour.setText(timeLengthMinute / 60 + "");
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.jump2MainActivity(mContext);
            }
        });
        tvFinishBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.jump2MainActivity(mContext);
                finish();
            }
        });
    }

    @Override
    protected void initData() {
        HawkParams.setWorkStatus(WorkStatus.STATUS_FINISH);
        //HawkParams.setCookStep(new ArrayList<DeviceRunStep>());//清空步骤
        //HawkParams.setS_temp(0);
        if (HawkParams.getMode() >= SteamBakeType.FERMENTATION.value
                && HawkParams.getMode() <= SteamBakeType.KEEP_WARM.value) {
            for (SteamBakeType temp : SteamBakeType.values()) {
                if (temp.value == HawkParams.getMode()) {
                    tvFinishInfo.setText(temp.name + "完成");
                    if (temp.name.equals("热菜"))
                        VoiceManager.getInstance().startSpeak("乐菜完成");
                    else
                        VoiceManager.getInstance().startSpeak(temp.name + "完成");
                }
            }
        } else {
            tvFinishInfo.setText("烹饪完成");
            VoiceManager.getInstance().startSpeak("上菜啦，上菜啦，上菜啦，重要的事情说三遍，上菜啦");
        }
        //
        //营养信息
        mBasicKeyInfos = HawkParams.getNutritionInfos();
        /*
        BasicKeyInfo temp = new BasicKeyInfo("测试1", "244", "g");
        mBasicKeyInfos.add(temp);
        temp = new BasicKeyInfo("测试1", "244", "g");
        mBasicKeyInfos.add(temp);
        */
        if (HawkParams.getCookFrom() == FromType.FROM_RECIPE.value && mBasicKeyInfos != null && mBasicKeyInfos.size() > 0) {//显示营养信息
            llNutritionInfo.setVisibility(View.VISIBLE);
            if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V2)) {
                llBasicContent.setVisibility(View.GONE);
                tvTitle.setText("烹饪完成");
            }
            for (int i = 0; i < mBasicKeyInfos.size(); i++) {
                LinearLayout ll_item = (LinearLayout) ((Activity) mContext).getLayoutInflater()
                        .inflate(DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? R.layout.item_nutrition_layout : R.layout.item_nutrition_layout_h,
                                null, false);
                ((TextView) ll_item.findViewById(R.id.tvValue)).setText(mBasicKeyInfos.get(i).getValue());
                ((TextView) ll_item.findViewById(R.id.tvUnit)).setText(mBasicKeyInfos.get(i).getUnit());
                ((TextView) ll_item.findViewById(R.id.tvName)).setText(mBasicKeyInfos.get(i).getName());
                llNutritionInfo.addView(ll_item);
                if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) {
                    if (i % 4 == 0 || i % 4 == 3) {
                        ll_item.findViewById(R.id.llContent).setBackgroundResource(R.drawable.nutrition_bg1);
                    } else if (i % 4 == 1) {
                        ll_item.findViewById(R.id.llContent).setBackgroundResource(R.drawable.nutrition_bg2);
                    } else if (i % 4 == 2) {
                        ll_item.findViewById(R.id.llContent).setBackgroundResource(R.drawable.nutrition_bg3);
                    }
                } else {
                    if (i % 5 == 0) {
                        ll_item.findViewById(R.id.llContent).setBackgroundResource(R.drawable.nutrition_v_bg1);
                    } else if (i % 5 == 1) {
                        ll_item.findViewById(R.id.llContent).setBackgroundResource(R.drawable.nutrition_v_bg2);
                    } else if (i % 5 == 2) {
                        ll_item.findViewById(R.id.llContent).setBackgroundResource(R.drawable.nutrition_v_bg3);
                    } else if (i % 5 == 3) {
                        ll_item.findViewById(R.id.llContent).setBackgroundResource(R.drawable.nutrition_v_bg4);
                    } else if (i % 5 == 4) {
                        ll_item.findViewById(R.id.llContent).setBackgroundResource(R.drawable.nutrition_v_bg5);
                    }
                }
            }
            DeviceManager.getInstance().sendOnceRecord(HawkParams.getCookeStartTime() / 1000, HawkParams.getFoodName(), timeLengthMinute * 60, HawkParams
                    .getNutritionInfos2String(), HawkParams.getC_temps(), HawkParams.getWeight());
            if (HawkParams.getFirstScanTime() == 0)
                HawkParams.setFirstScanTime((int) ((new Date()).getTime() / 1000));
        }
    }

    @Subscribe
    public void receiveEvent(Event2PauseStatus event) {
        LogUtils.d("receiver event from is" + event.getBusFrom() + " the time is:");
        if (HawkParams.getWorkStatus() == WorkStatus.STATUS_FINISH.value) {
            if (event.getPauseStatus() == 2) {
                Global.jump2MainActivity(mContext);
            }
        }
    }
}
