package com.viomi.oven.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.view.LongClickImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ljh on 2018/1/9
 */
public class ChangeParamActivity extends BaseActivity {
    //UI
    private TextView tvTitle;
    private LongClickImageView imgDelTime;
    private TextView tvTime;
    private TextView tvTimeNick;
    private LongClickImageView imgAddTime;
    private LongClickImageView imgDelTemp;
    private TextView tvTemp;
    private TextView tvTempNick;
    private LongClickImageView imgAddTemp;
    private ImageView imgStart;
    private LinearLayout llTime;
    private LinearLayout llTemp;
    //参数
    int layout = R.layout.activity_change1params;
    SteamBakeType mSteamBakeType = SteamBakeType.STEAM;
    List<DeviceRunStep> mDeviceRunSteps = new ArrayList<>();
    boolean llTimeVisible = true, llTempVisible = true;
    int curTemp = 0, curTime = 0;

    public static void actionStart(Context context, int type) {
        Intent intent = new Intent(context, ChangeParamActivity.class);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutRes() {
        int type = getIntent().getIntExtra("type", 0);
        layout = (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) ? R.layout.activity_change1params_v : R.layout.activity_change1params;
        if (type == SteamBakeType.STEAM.value) {//蒸汽
            mSteamBakeType = SteamBakeType.STEAM;
            layout = (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) ? R.layout.activity_change2params_v : R.layout.activity_change2params;
        } else if (type == SteamBakeType.BAKE.value) {//热风烤
            mSteamBakeType = SteamBakeType.BAKE;
            layout = (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) ? R.layout.activity_change2params_v : R.layout.activity_change2params;
        } else if (type == SteamBakeType.STEAM_BAKE.value) {//蒸烤
            mSteamBakeType = SteamBakeType.STEAM_BAKE;
            layout = (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) ? R.layout.activity_change2params_v : R.layout.activity_change2params;
        } else if (type == SteamBakeType.FERMENTATION.value) {//发酵
            mSteamBakeType = SteamBakeType.FERMENTATION;
            layout = (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) ? R.layout.activity_change2params_v : R.layout.activity_change2params;
        } else if (type == SteamBakeType.HOT_FOOD.value) {//热菜
            mSteamBakeType = SteamBakeType.HOT_FOOD;
            layout = (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) ? R.layout.activity_change2params_v : R.layout.activity_change2params;
        } else if (type == SteamBakeType.THAW.value) {//解冻
            mSteamBakeType = SteamBakeType.THAW;
            layout = (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) ? R.layout.activity_change2params_v : R.layout.activity_change2params;
        } else if (type == SteamBakeType.KEEP_WARM.value) {//保温
            mSteamBakeType = SteamBakeType.KEEP_WARM;
            layout = (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) ? R.layout.activity_change2params_v : R.layout.activity_change2params;
        } else if (type == SteamBakeType.CHUGOU.value) {//除垢(台面式无)  加热到100℃，烤箱灯不亮。按操作流程
            mSteamBakeType = SteamBakeType.CHUGOU;
            llTimeVisible = false;
        } else if (type == SteamBakeType.DISINFECTION.value) {//高温消毒
            mSteamBakeType = SteamBakeType.DISINFECTION;
            layout = (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) ? R.layout.activity_change2params_v : R.layout.activity_change2params;
        } else if (type == SteamBakeType.CLEAN.value) {//清洁
            mSteamBakeType = SteamBakeType.CLEAN;
            llTempVisible = false;
        } else if (type == SteamBakeType.PREHEATING.value) {//预热
            mSteamBakeType = SteamBakeType.PREHEATING;
            llTimeVisible = false;
        } else if (type == SteamBakeType.BAKE_6.value) {//嵌入式热风烤
            mSteamBakeType = SteamBakeType.BAKE_6;
            layout = (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) ? R.layout.activity_change2params_v : R.layout.activity_change2params;
        } else if (type == SteamBakeType.STEAM_BAKE_1.value) {//嵌入式蒸烤
            mSteamBakeType = SteamBakeType.STEAM_BAKE_1;
            layout = (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) ? R.layout.activity_change2params_v : R.layout.activity_change2params;
        }
        mDeviceRunSteps.clear();
        mDeviceRunSteps.add(new DeviceRunStep(mSteamBakeType));
        return layout;
    }

    @Override
    protected void initView() {
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText(mSteamBakeType.name);
        tvTempNick = (TextView) findViewById(R.id.tvTempNick);
        tvTimeNick = (TextView) findViewById(R.id.tvTimeNick);
        tvTempNick.setText(mSteamBakeType.name + "温度");
        tvTimeNick.setText(mSteamBakeType.name + "时间");
        imgDelTime = (LongClickImageView) findViewById(R.id.imgDelTime);
        tvTime = (TextView) findViewById(R.id.tvTime);
        imgAddTime = (LongClickImageView) findViewById(R.id.imgAddTime);
        imgStart = (ImageView) findViewById(R.id.imgStart);
        imgDelTemp = (LongClickImageView) findViewById(R.id.imgDelTemp);
        tvTemp = (TextView) findViewById(R.id.tvTemp);
        imgAddTemp = (LongClickImageView) findViewById(R.id.imgAddTemp);
        llTemp = (LinearLayout) findViewById(R.id.llTemp);
        llTime = (LinearLayout) findViewById(R.id.llTime);
        llTemp.setVisibility(llTempVisible ? View.VISIBLE : View.GONE);
        llTime.setVisibility(llTimeVisible ? View.VISIBLE : View.GONE);
        /////////////////////////////////////////////////////////////////////
        imgDelTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.d(TAG, "imgDelTime onClick");
                timeDel();
            }
        });
        imgAddTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.d(TAG, "imgAddTime onClick");
                timeAdd();
            }
        });
        imgDelTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.d(TAG, "imgDelTemp onClick");
                tempDel();
            }
        });
        imgAddTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.d(TAG, "imgAddTemp onClick");
                tempAdd();
            }
        });
        imgStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(mSteamBakeType == SteamBakeType.KEEP_WARM){
                //    showToast("功能未完善");
                //    return;
                //}

                mDeviceRunSteps.get(0).setTemp(curTemp);
                mDeviceRunSteps.get(0).setTime(curTime);
                if (Global.isPrepareOk(mDeviceRunSteps)) {
                    if (SerialManager.getInstance().sendCmdStart(mDeviceRunSteps)) {
                        HawkParams.setMode(mSteamBakeType);
                        if (mSteamBakeType == SteamBakeType.PREHEATING) {
                            HawkParams.delCookStep();
                            PreHeatingActivity.actionStart(mContext, mDeviceRunSteps.get(0).getTemp());
                        } else {
                            HawkParams.setFoodName(mDeviceRunSteps.get(0).getType().name);
                            startActivity(new Intent(mContext, CookRunningActivity.class));
                        }
                        finish();
                    }
                }
            }
        });
    }

    @Override
    protected void initData() {
        tvTemp.setText((curTemp = mSteamBakeType.defTemp) + "");
        tvTime.setText((curTime = mSteamBakeType.defTime) + "");
    }

    public void timeDel() {
        if (curTime - 1 >= mSteamBakeType.minTime)
            curTime--;
        tvTime.setText(curTime + "");
    }

    public void timeAdd() {
        if (curTime + 1 <= mSteamBakeType.maxTime)
            curTime++;
        tvTime.setText(curTime + "");
    }

    public void tempDel() {
        if (curTemp - 1 >= mSteamBakeType.minTemp)
            curTemp--;
        tvTemp.setText(curTemp + "");
    }

    public void tempAdd() {
        if (curTemp + 1 <= mSteamBakeType.maxTemp)
            curTemp++;
        tvTemp.setText(curTemp + "");
    }
}
