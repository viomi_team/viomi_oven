package com.viomi.oven.activity;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Message;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.squareup.otto.Subscribe;
import com.tencent.bugly.beta.Beta;
import com.viomi.oven.HawkParams;
import com.viomi.oven.MiIndentify;
import com.viomi.oven.R;
import com.viomi.oven.dialog.UpLoadDialog;
import com.viomi.oven.enumType.DownFileType;
import com.viomi.oven.manager.UpgradeManager;
import com.viomi.oven.otto.EventCheckUpgradeFinish;
import com.viomi.oven.otto.EventMcuInstall;
import com.viomi.oven.util.ApkUtil;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.NetworkUtils;
import com.viomi.oven.util.PhoneUtil;
import com.viomi.oven.util.ToastUtil;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Ljh on 2018/.
 */

public class VersionManagerActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgWifiV)
    ImageView imgWifiV;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvNewApp)
    TextView tvNewApp;
    @BindView(R.id.tvAppVer)
    TextView tvAppVer;
    @BindView(R.id.llAppVer)
    LinearLayout llAppVer;
    @BindView(R.id.tvNewSys)
    TextView tvNewSys;
    @BindView(R.id.tvSysVer)
    TextView tvSysVer;
    @BindView(R.id.llSysVer)
    LinearLayout llSysVer;
    @BindView(R.id.tvNewMcu)
    TextView tvNewMcu;
    @BindView(R.id.tvMcuVer)
    TextView tvMcuVer;
    @BindView(R.id.llMcuVer)
    LinearLayout llMcuVer;
    @BindView(R.id.llFactoryTest)
    LinearLayout llFactoryTest;
    @BindView(R.id.llSystemSelfCheck)
    LinearLayout llSystemSelfCheck;
    @BindView(R.id.llNetAdb)
    LinearLayout llNetAdb;
    @BindView(R.id.llSysSet)
    LinearLayout llSysSet;
    @BindView(R.id.llNetInfo)
    LinearLayout llNetInfo;
    @BindView(R.id.tvMacDid)
    TextView tvMacDid;
    @BindView(R.id.soundBar)
    SeekBar soundBar;
    @BindView(R.id.llSoundBar)
    LinearLayout llSoundBar;
    @BindView(R.id.sysSoundBar)
    SeekBar sysSoundBar;
    @BindView(R.id.llSysSoundBar)
    LinearLayout llSysSoundBar;
    UpLoadDialog mUpLoadDialog;
    //
    Intent intentBro;
    int count = 0;
    AudioManager mAudioManager;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_version_manager;
    }

    @Override
    protected void initView() {
        tvTitle.setText("当前版本");
        imgWifiV.setVisibility(View.VISIBLE);
        tvAppVer.setText("v" + ApkUtil.getVersionCode());
        tvSysVer.setText(PhoneUtil.getSystemVersionCodeStr());
        tvMcuVer.setText(Global.verString(HawkParams.getMcuVer()));
        llSysVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                if (count >= 6) {
                    llFactoryTest.setVisibility(View.VISIBLE);
                    llSystemSelfCheck.setVisibility(View.VISIBLE);
                    llNetAdb.setVisibility(View.VISIBLE);
                    llSysSet.setVisibility(View.VISIBLE);
                    llSysSoundBar.setVisibility(View.VISIBLE);
                }
            }
        });
        MiIndentify miIndentify = PhoneUtil.getMiIdentify();
        LogUtils.d(TAG, "MAC:" + miIndentify.mac + "\n" + "DID:" + miIndentify.did + "\n" + "Token:" + miIndentify.token);
        tvMacDid.setText("MAC:" + miIndentify.mac + "\n" + "DID:" + miIndentify.did);
        //
        if (HawkParams.getVoiceOpenStatus())
            llSoundBar.setVisibility(View.VISIBLE);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        soundBar.setMax(mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        soundBar.setProgress(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        soundBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                //mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        sysSoundBar.setMax(mAudioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM));
        sysSoundBar.setProgress(mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM));
        sysSoundBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    protected void initData() {
        Beta.checkUpgrade(false, false);//Bugly检测是否有新版本
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtils.isAvailable()) {
            showLoading();
            UpgradeManager.getInstance().httpCheckAppVersion(false, UpgradeManager.OPERATE_MANUAL);
            UpgradeManager.getInstance().httpCheckAppVersion(true, UpgradeManager.OPERATE_MANUAL);
        }
    }

    @Override
    public void dealTheMsg(Message msg) {
        super.dealTheMsg(msg);
        switch (msg.what) {
            case 100:
                ToastUtil.show("版本已更新");
                break;
        }
    }

    @OnClick({R.id.llAppVer, R.id.llMcuVer, R.id.llSystemSelfCheck, R.id.llFactoryTest, R.id.llNetAdb, R.id.llSysSet})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llAppVer:
                if (tvNewApp.getVisibility() == View.VISIBLE) {//下载并安装
                    mUpLoadDialog = new UpLoadDialog(mContext);
                    mUpLoadDialog.setFlag(DownFileType.APP.valueString);
                    mUpLoadDialog.show();
                } else
                    ToastUtil.show("已是最新版本");
                break;
            case R.id.llMcuVer:
                if (tvNewMcu.getVisibility() == View.VISIBLE) {//下载并安装
                    mUpLoadDialog = new UpLoadDialog(mContext);
                    mUpLoadDialog.setFlag(DownFileType.MCU.valueString);
                    mUpLoadDialog.show();
                } else
                    ToastUtil.show("已是最新版本");
                break;
            case R.id.llNetAdb:
                intentBro = new Intent();
                intentBro.setAction("android.intent.action.WIFI_ADB");
                mContext.sendBroadcast(intentBro);
                break;
            case R.id.llSystemSelfCheck:
                intentBro = new Intent();
                intentBro.setAction("android.provider.Telephony.SECRET_CODE");
                intentBro.setData(Uri.parse("android_secret_code://66"));
                mContext.sendBroadcast(intentBro);
                break;
            case R.id.llFactoryTest:
                startActivity(new Intent(mContext,AgingModeActivity.class));
                break;
            case R.id.llSysSet:
                intentBro = new Intent(Settings.ACTION_SETTINGS);
                startActivity(intentBro);
                break;
        }
    }

    @Subscribe
    public void receiveEvent(EventCheckUpgradeFinish event) {
        LogUtils.d("receiver event from is" + event.getBusFrom());
        if (event.getSuccessStatus()) {//获取成功
            if (HawkParams.getOnlineAppVer() != null && HawkParams.getOnlineAppVer().getCode() > ApkUtil.getVersionCode())
                tvNewApp.setVisibility(View.VISIBLE);
            if (HawkParams.getOnlineMcuVer() != null && HawkParams.getOnlineMcuVer().getCode() > HawkParams.getMcuVer())
                tvNewMcu.setVisibility(View.VISIBLE);
        }
        hideLoading();
    }

    @Subscribe
    public void receiveEvent(EventMcuInstall event) {
        LogUtils.d("receiver event from is" + event.getBusFrom());
        if (event.getSuccessStatus() == 1) {//安装成功
            showToast("升级成功");
            mUpLoadDialog.cancel();
            tvNewMcu.setVisibility(View.GONE);
            tvMcuVer.setText("已是最新版本");
            startScreenTimer();
        } else if (event.getSuccessStatus() == 2) {//安装失败
            showToast("升级失败");
            mUpLoadDialog.cancel();
        } else if (event.getSuccessStatus() == 0) {//安装中
            LogUtils.d(TAG, "the progress =" + event.getProgress());
            mUpLoadDialog.updateProgress(event.getProgress());
        }
    }
}
