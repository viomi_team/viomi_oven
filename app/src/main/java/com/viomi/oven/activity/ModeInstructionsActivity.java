package com.viomi.oven.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.viomi.oven.R;
import com.viomi.oven.adapter.AdapterModeInstructions;
import com.viomi.oven.bean.ModeInfo;
import com.viomi.oven.enumType.SteamBakeType;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Ljh on 2018/4/8
 * 烤箱模式说明
 */
public class ModeInstructionsActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.imgLight)
    ImageView imgLight;
    @BindView(R.id.lv)
    ListView lv;
    //参数
    AdapterModeInstructions mAdapterModeInstructions;
    List<ModeInfo> mModeInfos = new ArrayList<>();

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_mode_instructions;
    }

    @Override
    protected void initView() {
        tvTitle.setText("蒸烤箱专业模式说明");
        imgLight.setVisibility(View.GONE);
    }

    @Override
    protected void initData() {
        SteamBakeType type = SteamBakeType.STEAM;
        int defTemp = type.defTemp;
        String tempRange = type.minTemp + "-" + type.maxTemp;
        String function = "若设定温度在40-89℃，顶部内管不工作；若设定温度在90℃以上，则顶部内管辅助加热，底部煮水盘同时工作，不间断。预设温度为" + defTemp + "℃，范围为" + tempRange + "℃；默认时间为20分钟，时间范围为00:01-03:00";
        String food = "-------------------------";
        ModeInfo temp = new ModeInfo(type, function, food);
        mModeInfos.add(temp);

        type = SteamBakeType.BAKE_1;
        defTemp = type.defTemp;
        tempRange = type.minTemp + "-" + type.maxTemp;
        function = "顶部外烤+下烤。预设温度为" + defTemp + "℃，范围为" + tempRange + "℃；默认时间为20分钟，时间范围为00:01-03:00";
        food = "蛋糕、酥饼、泡芙、曲奇、土豆片、薄脆蛋糕";
        temp = new ModeInfo(type, function, food);
        mModeInfos.add(temp);

        type = SteamBakeType.BAKE_2;
        defTemp = type.defTemp;
        tempRange = type.minTemp + "-" + type.maxTemp;
        function = "顶部外烤+下烤+热风风机。预设温度为" + defTemp + "℃，范围为" + tempRange + "℃；默认时间为20分钟，时间范围为00:01-03:00";
        food = "面包、烤鱼、烤鸡、叉烧、牛肉、茄子、玉米、油炸食品";
        temp = new ModeInfo(type, function, food);
        mModeInfos.add(temp);

        type = SteamBakeType.BAKE_3;
        defTemp = type.defTemp;
        tempRange = type.minTemp + "-" + type.maxTemp;
        function = "（小面积）顶部内烤+热风风机+后烤辅助。预设温度为" + defTemp + "℃，范围为" + tempRange + "℃；默认时间为20分钟，时间范围为00:01-03:00";
        food = "肉串、肉排及其它快热食品";
        temp = new ModeInfo(type, function, food);
        mModeInfos.add(temp);

        type = SteamBakeType.BAKE_4;
        defTemp = type.defTemp;
        tempRange = type.minTemp + "-" + type.maxTemp;
        function = "（大面积）顶部内烤+外烤。预设温度为" + defTemp + "℃，范围为" + tempRange + "℃；默认时间为20分钟，时间范围为00:01-03:00";
        food = "强烧烤：肉串、肉排、香肠、点心";
        temp = new ModeInfo(type, function, food);
        mModeInfos.add(temp);

        type = SteamBakeType.BAKE_5;
        defTemp = type.defTemp;
        tempRange = type.minTemp + "-" + type.maxTemp;
        function = "顶部内烤+热风风机+后烤辅助。预设温度为" + defTemp + "℃，范围为" + tempRange + "℃；默认时间为20分钟，时间范围为00:01-03:00";
        food = "开胃面点、小点心、肉馅、肉块";
        temp = new ModeInfo(type, function, food);
        mModeInfos.add(temp);

        type = SteamBakeType.BAKE_6;
        defTemp = type.defTemp;
        tempRange = type.minTemp + "-" + type.maxTemp;
        function = "后烤+热风风机。预设温度为" + defTemp + "℃，范围为" + tempRange + "℃；默认时间为20分钟，时间范围为00:01-03:00";
        food = "土豆饼、肉片、制作果脯";
        temp = new ModeInfo(type, function, food);
        mModeInfos.add(temp);

        type = SteamBakeType.BAKE_7;
        defTemp = type.defTemp;
        tempRange = type.minTemp + "-" + type.maxTemp;
        function = "下烤+热风烧烤（后烤+热风风机）。预设温度为" + defTemp + "℃，范围为" + tempRange + "℃；默认时间为20分钟，时间范围为00:01-03:00";
        food = "披萨、馅饼、糕饼";
        temp = new ModeInfo(type, function, food);
        mModeInfos.add(temp);

        type = SteamBakeType.STEAM_BAKE_1;
        defTemp = type.defTemp;
        tempRange = type.minTemp + "-" + type.maxTemp;
        function = "（大面积）顶部内烤+外烤+蒸发器内管。预设温度为" + defTemp + "℃，范围为" + tempRange + "℃；默认时间为20分钟，时间范围为00:01-03:00";
        food = "烤肉类、烤排骨、烤鱼、烤鸡翅、烤鸡腿（干一点口感）";
        temp = new ModeInfo(type, function, food);
        mModeInfos.add(temp);

        type = SteamBakeType.STEAM_BAKE_2;
        defTemp = type.defTemp;
        tempRange = type.minTemp + "-" + type.maxTemp;
        function = "（热风烧烤）后烤+热风风机+蒸发器内管。预设温度为" + defTemp + "℃，范围为" + tempRange + "℃；默认时间为20分钟，时间范围为00:01-03:00";
        food = "烤肉类、烤排骨、烤鱼、烤鸡翅、烤鸡腿";
        temp = new ModeInfo(type, function, food);
        mModeInfos.add(temp);
        //
        mAdapterModeInstructions = new AdapterModeInstructions(mContext, mModeInfos);
        lv.setAdapter(mAdapterModeInstructions);
    }
}
