//package com.viomi.oven.activity;
//
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.viomi.oven.HawkParams;
//import com.viomi.oven.R;
//import com.viomi.oven.device.DeviceRunStep;
//import com.viomi.oven.dialog.AddSpecialCookDialog;
//import com.viomi.oven.dialog.CommonConfirmDialog;
//import com.viomi.oven.dialog.NumberPickerDialog;
//import com.viomi.oven.enumType.FromType;
//import com.viomi.oven.enumType.SteamBakeType;
//import com.viomi.oven.manager.SerialManager;
//import com.viomi.oven.manager.VoiceManager;
//import com.viomi.oven.util.Global;
//import com.viomi.oven.util.LogUtils;
//import com.viomi.oven.util.ToastUtil;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.BindView;
//import butterknife.OnClick;
//
//
///***
// * Created by Ljh on 2018/1/3
// * 专业模式
// */
//public class SpecialCookingActivity_V_New1 extends BaseActivity {
//    //UI
//    @BindView(R.id.tvNum1)
//    TextView tvNum1;
//    @BindView(R.id.tvNum2)
//    TextView tvNum2;
//    @BindView(R.id.tvName)
//    TextView tvName;
//    @BindView(R.id.rlCurModel)
//    RelativeLayout rlCurModel;
//    @BindView(R.id.llTime)
//    LinearLayout llTime;
//    @BindView(R.id.llTemp)
//    LinearLayout llTemp;
//    @BindView(R.id.tvTemp)
//    TextView tvTemp;
//    @BindView(R.id.tvTime)
//    TextView tvTime;
//    @BindView(R.id.imgAppoint)
//    ImageView imgAppoint;
//    @BindView(R.id.imgBtnStart)
//    ImageView imgBtnStart;
//    @BindView(R.id.llControl)
//    LinearLayout llControl;
//    @BindView(R.id.tvModeName1)
//    TextView tvModeName1;
//    @BindView(R.id.tvModeTime1)
//    TextView tvModeTime1;
//    @BindView(R.id.tvModeTemp1)
//    TextView tvModeTemp1;
//    @BindView(R.id.llModeContent1)
//    LinearLayout llModeContent1;
//    @BindView(R.id.rlMode1)
//    RelativeLayout rlMode1;
//    @BindView(R.id.tvModeName2)
//    TextView tvModeName2;
//    @BindView(R.id.tvModeTime2)
//    TextView tvModeTime2;
//    @BindView(R.id.tvModeTemp2)
//    TextView tvModeTemp2;
//    @BindView(R.id.llModeContent2)
//    LinearLayout llModeContent2;
//    @BindView(R.id.rlMode2)
//    RelativeLayout rlMode2;
//    @BindView(R.id.imgArrow)
//    ImageView imgArrow;
//    AddSpecialCookDialog mAddSpecialCookDialog;
//    CommonConfirmDialog mCommonConfirmDialog1;
//    NumberPickerDialog mNumberPickerDialog;
//    //参数
//    List<String> tempData = new ArrayList<>();
//    List<String> timeData = new ArrayList<>();
//    List<DeviceRunStep> mDeviceRunSteps = new ArrayList<>();
//    int curShowTemp = 0, curShowTime = 0, curSelectPos = 0;
//
//    @Override
//    protected int getLayoutRes() {
//        return R.layout.activity_special_cooking_v_new1;
//    }
//
//    @Override
//    protected void initView() {
//        mNumberPickerDialog = new NumberPickerDialog(mContext);
//        mNumberPickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialog) {
//                if (mNumberPickerDialog.isChanged) {
//                    if (mNumberPickerDialog.getFlag().equals("0")) {//选择温度
//                        curShowTemp = Integer.parseInt(tempData.get(mNumberPickerDialog.getPos()).toString());
//                        tvTemp.setText(curShowTemp + "");
//                        mDeviceRunSteps.get(curSelectPos).setTemp(curShowTemp);
//                        if (curShowTemp > 220 && curShowTime > 40 && mDeviceRunSteps.get(curSelectPos).getType() != SteamBakeType.STEAM) {
//                            curShowTime = 40;
//                            mDeviceRunSteps.get(curSelectPos).setTime(curShowTime);
//                            tvTime.setText(curShowTime + "");
//                        }
//                        initShowModeView();
//                    } else {//选择时间
//                        curShowTime = Integer.parseInt(timeData.get(mNumberPickerDialog.getPos()).toString());
//                        tvTime.setText(curShowTime + "");
//                        mDeviceRunSteps.get(curSelectPos).setTime(curShowTime);
//                        initShowModeView();
//                    }
//                }
//            }
//        });
//        mCommonConfirmDialog1 = new CommonConfirmDialog(mContext);
//        mCommonConfirmDialog1.setCallBack(new CommonConfirmDialog.CommonConfirmDialogCallBack() {
//            @Override
//            public void btnOk() {//确认删除
//                mDeviceRunSteps.remove(mCommonConfirmDialog1.getPos());
//                if (mCommonConfirmDialog1.getFlag().equals("rlMode1")) {
//                    if (Integer.parseInt(rlMode1.getTag().toString()) == curSelectPos) {//rlMode1为选中
//                        if (Integer.parseInt(rlMode2.getTag().toString()) == -1) {//rlMode2无数据
//                            rlMode1.setTag(-1);
//                            curSelectPos--;
//                        }
//                        if (Integer.parseInt(rlMode2.getTag().toString()) >= mDeviceRunSteps.size())
//                            rlMode2.setTag(-1);
//                    } else {//rlMode2为选中
//                        curSelectPos--;
//                        if (Integer.parseInt(rlMode2.getTag().toString()) >= mDeviceRunSteps.size())
//                            rlMode2.setTag(-1);
//                    }
//                } else if (mCommonConfirmDialog1.getFlag().equals("rlMode2")) {
//                    if (curSelectPos == Integer.parseInt(rlMode2.getTag().toString()))//rlMode3为选中
//                        curSelectPos--;
//                    if (Integer.parseInt(rlMode2.getTag().toString()) >= mDeviceRunSteps.size())
//                        rlMode2.setTag(-1);
//                }
//                //tag更新
//                initShowModeView();
//                initShowArrow();
//                initShowContent();
//            }
//        });
//        mAddSpecialCookDialog = new AddSpecialCookDialog(mContext);
//        mAddSpecialCookDialog.setCallBack(new AddSpecialCookDialog.AddSpecialCookDialogCallBack() {
//            @Override
//            public void setCurType(boolean isAdd, SteamBakeType curType) {
//                if (isAdd) {//添加模式
//                    DeviceRunStep temp = new DeviceRunStep(curType);
//                    mDeviceRunSteps.add(temp);
//                    curSelectPos = mDeviceRunSteps.size() - 1;
//                    if (mAddSpecialCookDialog.getFlag().equals("rlMode1")) {
//                        rlMode1.setTag(curSelectPos);
//                        rlMode2.setTag(-1);
//                    } else if (mAddSpecialCookDialog.getFlag().equals("rlMode2")) {
//                        rlMode2.setTag(curSelectPos);
//                    }
//                    //tag更新
//                    initShowModeView();
//                    initShowArrow();
//                    initShowContent();
//                } else {//更改模式
//                    mDeviceRunSteps.get(curSelectPos).setType(curType);
//                    initShowModeView();
//                    initShowContent();
//                }
//            }
//        });
//        /////////////////////////
//        rlMode1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (v.getTag().toString().equals("-1")) {//添加
//                    mAddSpecialCookDialog.setFlag("rlMode1");
//                    mAddSpecialCookDialog.show();
//                    mAddSpecialCookDialog.setSelectType(null);
//                } else {//选中
//                    curSelectPos = Integer.parseInt(v.getTag().toString());
//                    initShowModeView();
//                    initShowContent();
//                }
//            }
//        });
//        rlMode2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (v.getTag().toString().equals("-1")) {//添加
//                    mAddSpecialCookDialog.setFlag("rlMode2");
//                    mAddSpecialCookDialog.show();
//                    mAddSpecialCookDialog.setSelectType(null);
//                } else {//选中
//                    curSelectPos = Integer.parseInt(v.getTag().toString());
//                    initShowModeView();
//                    initShowContent();
//                }
//            }
//        });
//        rlMode1.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                if (mDeviceRunSteps.size() == 1)//只剩一个时不可删
//                    return true;
//                int posTag = Integer.parseInt(v.getTag().toString());
//                if (posTag == -1)//为增加按钮时
//                    return true;
//                //删除
//                mCommonConfirmDialog1.setFlag("rlMode1");
//                mCommonConfirmDialog1.setPos(posTag);
//                mCommonConfirmDialog1.show();
//                return true;
//            }
//        });
//
//        rlMode2.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                if (mDeviceRunSteps.size() == 1)//只剩一个时不可删
//                    return true;
//                int posTag = Integer.parseInt(v.getTag().toString());
//                if (posTag == -1)//为增加按钮时
//                    return true;
//                //删除
//                mCommonConfirmDialog1.setFlag("rlMode2");
//                mCommonConfirmDialog1.setPos(posTag);
//                mCommonConfirmDialog1.show();
//                return true;
//            }
//        });
//    }
//
//    @Override
//    protected void initData() {
//        HawkParams.setFoodName("");
//        HawkParams.setCookFrom(FromType.FROM_SPECIAL.value);
//        //
//        DeviceRunStep temp = new DeviceRunStep(SteamBakeType.STEAM);
//        mDeviceRunSteps.add(temp);
//        curSelectPos = 0;
//        rlMode1.setTag(0);
//        rlMode2.setTag(-1);
//        initShowModeView();
//        initShowArrow();
//        initShowContent();
//    }
//
//    public void initShowModeView() {
//        LogUtils.d(TAG, "curSelectPos is:" + curSelectPos);
//        //rlMode1
//        int dataSource1 = Integer.parseInt(rlMode1.getTag().toString());
//        LogUtils.d(TAG, "dataSource1 is:" + dataSource1);
//        if (dataSource1 == -1) {//显示添加图标
//            llModeContent1.setVisibility(View.INVISIBLE);
//            tvNum1.setVisibility(View.INVISIBLE);
//            rlMode2.setTag(-1);
//        } else {//显示内容
//            llModeContent1.setVisibility(View.VISIBLE);
//            tvNum1.setVisibility(View.VISIBLE);
//            tvNum1.setText((dataSource1 + 1) + "");
//            llModeContent1.setSelected(curSelectPos == dataSource1);
//            tvModeName1.setText(mDeviceRunSteps.get(dataSource1).getType().name);
//            tvModeTemp1.setText(mDeviceRunSteps.get(dataSource1).getTemp() + "");
//            tvModeTime1.setText(mDeviceRunSteps.get(dataSource1).getTime() + "");
//        }
//        //rlMode2
//        int dataSource2 = Integer.parseInt(rlMode2.getTag().toString());
//        LogUtils.d(TAG, "dataSource2 is:" + dataSource2);
//        if (dataSource1 == -1) {//rlMode为增加符号时，rlMode2不可见
//            rlMode2.setVisibility(View.INVISIBLE);
//            return;
//        } else
//            rlMode2.setVisibility(View.VISIBLE);
//        if (dataSource2 == -1) {//显示添加图标
//            tvNum2.setVisibility(View.INVISIBLE);
//            llModeContent2.setVisibility(View.INVISIBLE);
//        } else {//显示内容
//            tvNum2.setVisibility(View.VISIBLE);
//            tvNum2.setText((dataSource2 + 1) + "");
//            llModeContent2.setVisibility(View.VISIBLE);
//            llModeContent2.setSelected(curSelectPos == dataSource2);
//            tvModeName2.setText(mDeviceRunSteps.get(dataSource2).getType().name);
//            tvModeTemp2.setText(mDeviceRunSteps.get(dataSource2).getTemp() + "");
//            tvModeTime2.setText(mDeviceRunSteps.get(dataSource2).getTime() + "");
//        }
//    }
//
//    public void initShowArrow() {
//        imgArrow.setVisibility(mDeviceRunSteps.size() <= 1 ? View.INVISIBLE : View.VISIBLE);
//        imgArrow.setSelected(rlMode1.getTag().toString().equals("0") ? false : true);//0向下，-1和2向上
//    }
//
//    public void initShowContent() {
//        tvName.setText(mDeviceRunSteps.get(curSelectPos).getType().name);
//        curShowTemp = mDeviceRunSteps.get(curSelectPos).getTemp();
//        tvTemp.setText(curShowTemp + "");
//        //
//        curShowTime = mDeviceRunSteps.get(curSelectPos).getTime();
//        tvTime.setText(curShowTime + "");
//    }
//
//    public boolean isParamsOk() {
//        int timeLength = 0, highTempLength = 0;
//        for (DeviceRunStep deviceRunStep : mDeviceRunSteps) {
//            timeLength += deviceRunStep.getTime();
//            if (deviceRunStep.getTemp() > 220 && (deviceRunStep.getMode() == SteamBakeType.BAKE.value || deviceRunStep.getMode() == SteamBakeType.STEAM_BAKE.value)) {
//                highTempLength += deviceRunStep.getTime();
//                //专业模式下（烤、蒸烤叠加），当设定温度在221-230℃，连续运行总时间不能超过40 min
//                if (highTempLength > 40) {
//                    ToastUtil.show("在大于220度高温状态不可以连续工作超过40min");
//                    return false;
//                }
//            } else highTempLength = 0;
//        }
//        //判断门和水箱是否正常
//        return Global.isPrepareOk(mDeviceRunSteps);
//    }
//
//    @OnClick({R.id.imgArrow, R.id.rlCurModel, R.id.imgBtnStart, R.id.imgAppoint, R.id.llTemp, R.id.llTime})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.imgArrow:
//                if (view.isSelected()) {//当前是向上，即在第二页,上翻到第一页
//                    view.setSelected(false);
//                    rlMode1.setTag(0);
//                    rlMode2.setTag(1);
//                    curSelectPos = 0;
//                    initShowModeView();
//                    initShowArrow();
//                    initShowContent();
//                } else {//当前是向下，即在第一页,下翻到第二页
//                    view.setSelected(true);
//                    if (mDeviceRunSteps.size() == 2) {
//                        rlMode1.setTag(-1);
//                        rlMode2.setTag(-1);
//                    } else if (mDeviceRunSteps.size() == 3) {
//                        curSelectPos = 2;
//                        rlMode1.setTag(2);
//                        rlMode2.setTag(-1);
//                    } else {
//                        curSelectPos = 2;
//                        rlMode1.setTag(2);
//                        rlMode2.setTag(3);
//                    }
//                    initShowModeView();
//                    initShowArrow();
//                    initShowContent();
//                }
//                break;
//            case R.id.rlCurModel://选择蒸烤
//                if (curSelectPos != Integer.parseInt(rlMode1.getTag().toString()) && curSelectPos != Integer.parseInt(rlMode2.getTag().toString())) {
//                    return;
//                }
//                mAddSpecialCookDialog.show();
//                mAddSpecialCookDialog.setSelectType(mDeviceRunSteps.get(curSelectPos).getType());
//                break;
//            case R.id.imgAppoint:
//                if (isParamsOk()) {
//                    HawkParams.setCookStep(mDeviceRunSteps);
//                    startActivity(new Intent(mContext, SelectAppointTimeActivity.class));
//                }
//                break;
//            case R.id.imgBtnStart://确认开始
//                if (isParamsOk()) {
//                    if (SerialManager.getInstance().sendCmdStart(mDeviceRunSteps)) {
//                        VoiceManager.getInstance().startSpeak("现在开始烹饪");
//                        startActivity(new Intent(mContext, CookRunningActivity.class));
//                        finish();
//                    }
//                }
//                break;
//            case R.id.llTemp:
//                tempData.clear();
//                for (int i = mDeviceRunSteps.get(curSelectPos).getType().minTemp; i <= mDeviceRunSteps.get(curSelectPos).getType().maxTemp; i++) {
//                    tempData.add(i + "");
//                }
//                mNumberPickerDialog.setFlag("0");
//                mNumberPickerDialog.update(tempData);
//                mNumberPickerDialog.show(curShowTemp + "", "℃");
//                break;
//            case R.id.llTime:
//                timeData.clear();
//                mNumberPickerDialog.setFlag("1");
//                int maxTime = mDeviceRunSteps.get(curSelectPos).getType().maxTime;
//                if (mDeviceRunSteps.get(curSelectPos).getType() != SteamBakeType.STEAM && curShowTemp > 220)
//                    maxTime = 40;
//                for (int i = mDeviceRunSteps.get(curSelectPos).getType().minTime; i <= maxTime; i++) {
//                    timeData.add(i + "");
//                }
//                mNumberPickerDialog.update(timeData);
//                mNumberPickerDialog.show(curShowTime + "", "分");
//                break;
//        }
//    }
//}