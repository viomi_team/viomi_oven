package com.viomi.oven.activity;

import android.os.Bundle;

import com.viomi.oven.R;
import com.viomi.oven.util.Global;

public class WelcomeActivity extends BaseActivity {
    //UI

    //参数

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        someInit();
        jumpTo();
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        //        WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_welcome;
        //return 0;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
    }

    public void someInit() {//部分内置初始化数据(菜谱)
    }

    public void jumpTo() {
        Global.jump2MainActivity(mContext);
        finish();
    }
}
