package com.viomi.oven.activity;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.R;
import com.viomi.oven.adapter.AdapterFoodItem;
import com.viomi.oven.bean.BaseResult;
import com.viomi.oven.bean.FoodCard;
import com.viomi.oven.dialog.OneKeyBuyDialog;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.ToastUtil;
import com.viomi.oven.util.http.HttpReCallback;
import com.viomi.oven.util.http.MyStringCallback;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.RequestBody;

public class OneKeyBuyActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvNum)
    TextView tvNum;
    @BindView(R.id.imgCard)
    ImageView imgCard;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tvCurPage)
    TextView tvCurPage;
    @BindView(R.id.tvTotalPage)
    TextView tvTotalPage;
    OneKeyBuyDialog mOneKeyBuyDialog;
    //参数
    AdapterFoodItem mAdapterFoodItem;
    int totalAmount = 0;
    List<FoodCard> items = new ArrayList<>();

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_one_key_buy;
    }

    @Override
    protected void initView() {
        mOneKeyBuyDialog = new OneKeyBuyDialog(mContext);
        imgCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvNum.getVisibility() == View.VISIBLE)
                    startActivity(new Intent(mContext, ShoppingCartActivity.class));
                else
                    showToast("购物车为空");
            }
        });
        tvTitle.setText("一键购");
    }

    @Override
    protected void initData() {
        httpGetFoodList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        httpGetShopCart();
    }

    public void httpGetFoodList() {
        showLoading();
        OkHttpUtils
                .get()
                .url(AppStaticConfig.URL_ONE_KEY_SHOP_LIST)
                .tag(this)
                .build()
                .execute(new HttpReCallback<BaseResult<List<FoodCard>>>() {
                    @Override
                    public void onSuccess(BaseResult<List<FoodCard>> result) {
                        if (result.getCode() == 100) {
                            items = result.getResult();
                            if (items == null)
                                ToastUtil.show("数据解析异常");
                            else {
                                initShow();
                            }
                        }
                        hideLoading();
                    }

                    @Override
                    public void onFail(int errorCode) {
                        hideLoading();
                    }
                });
    }

    public void initShow() {
        tvCurPage.setText("1");
        tvTotalPage.setText("/" + items.size());
        mAdapterFoodItem = new AdapterFoodItem(items);
        viewPager.setAdapter(mAdapterFoodItem);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                LogUtils.d(TAG, "the position = " + position + " positionOffset = " + positionOffset + " positionOffsetPixels = " + positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                LogUtils.d(TAG, "the onPageSelected = " + position);
                tvCurPage.setText(position + 1 + "");
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                LogUtils.d(TAG, "the onPageScrollStateChanged = " + state);
            }
        });
        mAdapterFoodItem.setCallBack(new AdapterFoodItem.AdapterFoodItemCallBack() {
            @Override
            public void onBuyClick(int pos, boolean isBuy) {
                if (isBuy) {//查看详情
                    showLoading();
                    mOneKeyBuyDialog.setFlag(items.get(pos).getSkuId());
                    mOneKeyBuyDialog.show();
                    hideLoading();
                } else {
                    httpAddShopCart(items.get(pos).getSkuId());
                }
            }
        });
    }

    public void httpGetShopCart() {
        showLoading();
        OkHttpUtils
                .get()
                .url(AppStaticConfig.getShopCartList())
                .tag(this)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        hideLoading();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        String result = response.substring(response.indexOf("{", 1), response.lastIndexOf("}"));
                        try {
                            int code = new JSONObject(result).getInt("code");
                            if (code == 100) {
                                totalAmount = new JSONObject(result).getJSONObject("result").getInt("totalAmount");
                                if (totalAmount > 0) {
                                    tvNum.setText(totalAmount + "");
                                    tvNum.setVisibility(View.VISIBLE);
                                }
                            } else showToast(new JSONObject(result).getString("desc"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            totalAmount = 0;
                            tvNum.setVisibility(View.GONE);
                            //ToastUtil.show("数据解析异常");
                        }
                        hideLoading();
                    }
                });
    }

    public void httpAddShopCart(String skuId) {
        showLoading();
        OkHttpUtils
                .put()
                .url(AppStaticConfig.addFoodItem(skuId))
                .requestBody(RequestBody.create(null, ""))//
                .tag(this)
                .build()
                .execute(new MyStringCallback() {
                    @Override
                    public void onSuccess(String result) {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getJSONObject("mobBaseRes").getInt("code") == 100) {
                                totalAmount++;
                                tvNum.setText(totalAmount + "");
                                tvNum.setVisibility(View.VISIBLE);
                                showToast("添加成功");
                            } else
                                showToast(jsonObject.getJSONObject("mobBaseRes").getString("desc"));
                        } catch (JSONException e) {
                            showToast("数据解析异常");
                            e.printStackTrace();
                        }
                        hideLoading();
                    }

                    @Override
                    public void onFail(int errorCode) {
                        hideLoading();
                    }
                });
    }
}
