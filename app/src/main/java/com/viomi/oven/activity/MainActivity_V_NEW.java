package com.viomi.oven.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.wifi.WifiManager;
import android.os.Message;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.view.View;
import android.widget.ImageView;

import com.viomi.oven.AppParam;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.enumType.ModelType;
import com.viomi.oven.manager.AccountManager;
import com.viomi.oven.service.BackgroudService;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.NetworkUtils;
import com.viomi.oven.util.ToastUtil;
import com.viomi.oven.util.ViewUtil;
import com.viomi.oven.view.TurnplateView;
import com.viomi.oven.view.WaveView;

import butterknife.BindView;

public class MainActivity_V_NEW extends BaseActivity {
    //UI
    @BindView(R.id.imgWifiV)
    ImageView imgWifiV;
    @BindView(R.id.imgLight)
    ImageView imgLight;
    @BindView(R.id.waveView)
    WaveView waveView;
    @BindView(R.id.turnPlateView)
    TurnplateView turnPlateView;
    //参数
    private WifiManager wifiManager;
    private Intent backgroudServiceintent;
    //
    public static final int MSG_SHOW_WAVE = 1000;
    //圆心 直径980（-134，690）  直径780（-234，680）
    private Bitmap[] icons;
    private String[] iconNames;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main_v_new;
    }

    @Override
    protected void onResume() {
        super.onResume();
        initParams();
        sendToHandlerDelayed(MSG_SHOW_WAVE, 1000);
    }

    public void initParams() {
        Global.backMainInitHawkParams();
    }

    @Override
    protected void initView() {
        imgWifiV.setVisibility(View.VISIBLE);
        waveView.setDuration(6000);
        waveView.setInitialRadius(80);
        waveView.setMaxRadius(400);
        waveView.setSpeed(600);
        waveView.setStyle(Paint.Style.FILL);
        waveView.setColor(getResources().getColor(R.color.wave_color));
        waveView.setInterpolator(new LinearOutSlowInInterpolator());
        loadBitmaps();
        turnPlateView.initTurnPlateView(icons, iconNames, 7, 32, new TurnplateView.OnTurnplateListener() {
            @Override
            public void onPointTouch(int position) {
                LogUtils.d(TAG, "positionListenerMethod" + position);
                if (position == 3) {
                    if (!NetworkUtils.isAvailable()) {
                        ToastUtil.show("Wifi未连接");
                    } else {
                        HawkParams.setModelType(ModelType.MODEL_SCAN);
                        ScannerActivity.actionStart(MainActivity_V_NEW.this,
                                false, ScannerActivity.EXTRA_LASER_LINE_MODE_0, ScannerActivity.EXTRA_SCAN_MODE_0, false
                                , false, false);
                    }
                } else if (position == 2) {
                    HawkParams.setModelType(ModelType.MODEL_SMART);
                    startActivity(new Intent(mContext, FoodMenuActivity.class));
                } else if (position == 4) {
                    HawkParams.setModelType(ModelType.MODEL_SPECIAL);
                    startActivity(new Intent(mContext, SpecialCookingActivity_V_New3.class));
                } else if (position == 0) {
                    if (AccountManager.getViomiUser(mContext) == null) {
                        showToast("请先登录");
                        startActivity(new Intent(mContext, ScanLoginActivity.class));
                    } else {
                        HawkParams.setModelType(ModelType.MODEL_MY_SET);
                        startActivity(new Intent(mContext, FoodMenuActivity.class));
                    }
                } else if (position == 5) {
                    startActivity(new Intent(mContext, SettingActivity.class));
                } else if (position == 1) {
                    startActivity(new Intent(mContext, MoreActivity.class));
                } else if (position == 6) {
                    if (!NetworkUtils.isAvailable()) {
                        ToastUtil.show("Wifi未连接");
                    } else {
                        if (AccountManager.getViomiUser(mContext) == null) {
                            startActivity(new Intent(mContext, ScanLoginActivity.class));
                        } else {
                            startActivity(new Intent(mContext, OneKeyBuyActivity.class));
                        }
                    }
                }
            }

            @Override
            public void onMove(boolean move) {
                if (move)
                    waveView.stop();
                else waveView.start();
            }
        });

        turnPlateView.setChooseBn(3);
    }

    @Override
    protected void initData() {
        //开机自动连wifi
        wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);
        initService();
    }

    private void initService() {
        backgroudServiceintent = new Intent(this, BackgroudService.class);
        startService(backgroudServiceintent);
    }

    private void stopService() {
        stopService(backgroudServiceintent);
    }

    @Override
    public void dealTheMsg(Message msg) {
        super.dealTheMsg(msg);
        if (msg.what == MSG_SHOW_WAVE) {
            waveView.start();
            imgLight.setSelected(AppParam.isLightOn);
            //if(AppParam.mcuStatus == McuStatus.MCU_WORKING)
            //    SerialManager.getInstance().sendCmdOver();
            //if(SerialManager.getInstance().getDataReceiveInfo().curTimeL !=0 || SerialManager.getInstance().getDataReceiveInfo().curTimeH != 0){
            //    SerialManager.getInstance().sendCmdOver();
            //}
        }
    }

    @Override
    protected void onPause() {
        waveView.stop();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        stopService();
        super.onDestroy();
    }

    /**
     * 加载大图标
     */
    public void loadBitmaps() {
        icons = new Bitmap[7];
        icons[0] = ViewUtil.Drawable2Bitmap(this, R.drawable.v_mode_myset_new_press_v);
        icons[1] = ViewUtil.Drawable2Bitmap(this, R.drawable.v_mode_more_new_press_v);
        icons[2] = ViewUtil.Drawable2Bitmap(this, R.drawable.v_mode_smart_new_press_v);
        icons[3] = ViewUtil.Drawable2Bitmap(this, R.drawable.v_mode_scan_new_press_v);
        icons[4] = ViewUtil.Drawable2Bitmap(this, R.drawable.v_mode_special_new_press_v);
        icons[5] = ViewUtil.Drawable2Bitmap(this, R.drawable.v_mode_setting_new_press_v);
        icons[6] = ViewUtil.Drawable2Bitmap(this, R.drawable.v_mode_buy_new_press_v);

        iconNames = new String[7];
        iconNames[0] = "我的菜谱";
        iconNames[1] = "更多";
        iconNames[2] = "智能菜谱";
        iconNames[3] = "扫码烹饪";
        iconNames[4] = "专业烹饪";
        iconNames[5] = "设置";
        iconNames[6] = "一键购";
    }
}
