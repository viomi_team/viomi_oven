package com.viomi.oven.activity;

import android.os.Message;
import android.view.View;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.view.LockView;

import butterknife.BindView;

public class ScreenTimeActivity extends BaseActivity {
    //UI
    @BindView(R.id.v1)
    View v1;
    @BindView(R.id.lockView1)
    LockView lockView1;
    //参数
    public static final int MSG_SCREEN_OFF = 1000;

    @Override
    protected int getLayoutRes() {
        return DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1) ? R.layout.activity_screen_time : R.layout.activity_screen_time_h;
    }

    @Override
    protected void onResume() {
        super.onResume();
        sendToHandlerDelayed(MSG_SCREEN_OFF, 60000);
    }

    @Override
    public void dealTheMsg(Message msg) {
        super.dealTheMsg(msg);
        if (msg.what == MSG_SCREEN_OFF) {
            if(isFinishing())
                return;
        }
    }

    @Override
    protected void initView() {
        findViewById(R.id.root).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!HawkParams.getLockScreenStatus()) {
                    finish();
                }
            }
        });
        lockView1.setVisibility(HawkParams.getLockScreenStatus() ? View.VISIBLE : View.GONE);
        v1.setVisibility(HawkParams.getLockScreenStatus() ? View.GONE : View.VISIBLE);
    }

    @Override
    protected void initData() {
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
