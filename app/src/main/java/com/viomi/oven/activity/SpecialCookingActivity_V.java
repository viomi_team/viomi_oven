//package com.viomi.oven.activity;
//
//import android.content.Intent;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import com.viomi.oven.HawkParams;
//import com.viomi.oven.R;
//import com.viomi.oven.adapter.AdapterSpecialStep;
//import com.viomi.oven.device.DeviceRunStep;
//import com.viomi.oven.dialog.AddSpecialCookDialog;
//import com.viomi.oven.dialog.CommonConfirmDialog;
//import com.viomi.oven.enumType.FromType;
//import com.viomi.oven.enumType.SteamBakeType;
//import com.viomi.oven.manager.SerialManager;
//import com.viomi.oven.manager.VoiceManager;
//import com.viomi.oven.util.ToastUtil;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.BindView;
//import butterknife.OnClick;
//
//
///***
// * Created by Ljh on 2018/1/3
// * 专业模式
// */
//public class SpecialCookingActivity_V extends BaseActivity {
//    //UI
//    @BindView(R.id.imgBack)
//    ImageView imgBack;
//    @BindView(R.id.tvName)
//    TextView tvName;
//    @BindView(R.id.llCurModel)
//    LinearLayout llCurModel;
//    @BindView(R.id.unGvStep)
//    ListView unGvStep;
//    @BindView(R.id.imgAdd)
//    ImageView imgAdd;
//    @BindView(R.id.tvTemp)
//    TextView tvTemp;
//    @BindView(R.id.tvTime)
//    TextView tvTime;
//    @BindView(R.id.imgAppoint)
//    ImageView imgAppoint;
//    @BindView(R.id.imgBtnStart)
//    ImageView imgBtnStart;
//    @BindView(R.id.llControl)
//    LinearLayout llControl;
//    AddSpecialCookDialog mAddSpecialCookDialog;
//    CommonConfirmDialog mCommonConfirmDialog1;
//    //参数
//    List<DeviceRunStep> mDeviceRunSteps = new ArrayList<>();
//    AdapterSpecialStep mAdapterSpecialStep;
//    int curShowTemp = 0, curShwoTime = 0;
//
//    @Override
//    protected int getLayoutRes() {
//        return R.layout.activity_special_cooking_v;
//    }
//
//    @Override
//    protected void initView() {
//        unGvStep.setVisibility(View.VISIBLE);
//        imgBack.setSelected(true);
//        mCommonConfirmDialog1 = new CommonConfirmDialog(mContext);
//        mCommonConfirmDialog1.setCallBack(new CommonConfirmDialog.CommonConfirmDialogCallBack() {
//            @Override
//            public void btnOk() {
//                mDeviceRunSteps.remove(mCommonConfirmDialog1.getPos());
//                mAdapterSpecialStep.notifyDataSetChanged();
//                updateContent();
//                imgAdd.setVisibility(View.VISIBLE);
//            }
//        });
//        mAddSpecialCookDialog = new AddSpecialCookDialog(mContext);
//        mAddSpecialCookDialog.setCallBack(new AddSpecialCookDialog.AddSpecialCookDialogCallBack() {
//            @Override
//            public void setCurType(boolean isAdd, SteamBakeType curType) {
//                if (isAdd) {//添加模式
//                    DeviceRunStep temp = new DeviceRunStep(curType);
//                    mDeviceRunSteps.add(temp);
//                    mAdapterSpecialStep.setCurPos(mDeviceRunSteps.size() - 1);
//                    imgAdd.setVisibility(mDeviceRunSteps.size() >= 4 ? View.GONE : View.VISIBLE);
//                    updateContent();
//                } else {//更改模式
//                    mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setType(curType);
//                    mAdapterSpecialStep.notifyDataSetChanged();
//                    updateContent();
//                }
//            }
//        });
//        //
//        unGvStep.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                if (mAdapterSpecialStep.curPos != position) {
//                    mAdapterSpecialStep.setCurPos(position);
//                    updateContent();
//                }
//            }
//        });
//        unGvStep.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                if (mDeviceRunSteps.size() <= 1) {
//                    //ToastUtil.show("至少保留一个");
//                    return true;
//                }
//                mCommonConfirmDialog1.setPos(position);
//                mCommonConfirmDialog1.show();
//                return true;
//            }
//        });
//
//        imgBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!mAdapterSpecialStep.editStatus) {
//                    onBackPressed();
//                }
//            }
//        });
//    }
//
//    @Override
//    protected void initData() {
//        HawkParams.setFoodName("");
//        HawkParams.setCookFrom(FromType.FROM_SPECIAL.value);
//        //
//        DeviceRunStep temp = new DeviceRunStep(SteamBakeType.STEAM);
//        mDeviceRunSteps.add(temp);
//        mAdapterSpecialStep = new AdapterSpecialStep(mContext, mDeviceRunSteps);
//        unGvStep.setAdapter(mAdapterSpecialStep);
//        updateContent();
//        changeShowStatus();
//    }
//
//    public void changeShowStatus() {
//        imgAdd.setVisibility((mAdapterSpecialStep.editStatus || mDeviceRunSteps.size() >= 4) ? View.GONE : View.VISIBLE);
//    }
//
//    public void updateContent() {
//        curShowTemp = mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getTemp();
//        tvName.setText(mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().name);
//        tvTemp.setText(mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getTemp() + "");
//        //
//        curShwoTime = mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getTime();
//        tvTime.setText(mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getTime() + "");
//    }
//
//    public void tempDel() {
//        if (curShowTemp - 1 < mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().minTemp)
//            return;
//        else {
//            curShowTemp--;
//            tvTemp.setText(curShowTemp + "");
//            mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setTemp(curShowTemp);
//            mAdapterSpecialStep.notifyDataSetChanged();
//        }
//    }
//
//    public void tempAdd() {
//        if (curShowTemp + 1 > mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().maxTemp)
//            return;
//        else {
//            curShowTemp++;
//            if (mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType() != SteamBakeType.STEAM) {//40-199度01-180 min,,200-215度1-60 min，，216-230度1-40 min
//                if (curShowTemp >= 220 && curShwoTime > 40)
//                    curShwoTime = 40;
//                mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setTime(curShwoTime);
//                tvTime.setText(curShwoTime + "");
//            }
//            tvTemp.setText(curShowTemp + "");
//            mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setTemp(curShowTemp);
//            mAdapterSpecialStep.notifyDataSetChanged();
//        }
//    }
//
//    public void timeDel() {
//        if (curShwoTime - 1 < mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().minTime)
//            return;
//        else {
//            curShwoTime--;
//            tvTime.setText(curShwoTime + "");
//            mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setTime(curShwoTime);
//            mAdapterSpecialStep.notifyDataSetChanged();
//        }
//    }
//
//    public void timeAdd() {
//        if (curShwoTime + 1 > mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType().maxTime)
//            return;
//        else {
//            curShwoTime++;
//            if (mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType() != SteamBakeType.STEAM) {//40-199度01-180 min,,200-215度1-60 min，，216-230度1-40 min
//                if (curShowTemp > 199 && curShowTemp < 216 && curShwoTime > 60)
//                    curShwoTime = 60;
//                else if (curShowTemp >= 216 && curShwoTime > 40)
//                    curShwoTime = 40;
//            }
//            tvTime.setText(curShwoTime + "");
//            mDeviceRunSteps.get(mAdapterSpecialStep.curPos).setTime(curShwoTime);
//            mAdapterSpecialStep.notifyDataSetChanged();
//        }
//    }
//
//    @OnClick({R.id.imgAdd, R.id.llCurModel, R.id.imgBtnStart, R.id.imgAppoint, R.id.imgAddTemp, R.id.imgDelTemp, R.id.imgAddTime, R.id.imgDelTime})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.imgAdd:
//                mAddSpecialCookDialog.show();
//                mAddSpecialCookDialog.setSelectType(null);
//                break;
//            case R.id.llCurModel://选择蒸烤
//                if (mAdapterSpecialStep.editStatus)
//                    return;
//                mAddSpecialCookDialog.show();
//                mAddSpecialCookDialog.setSelectType(mDeviceRunSteps.get(mAdapterSpecialStep.curPos).getType());
//                break;
//            case R.id.imgAppoint:
//                HawkParams.setCookStep(mDeviceRunSteps);
//                startActivity(new Intent(mContext, SelectAppointTimeActivity.class));
//                break;
//            case R.id.imgBtnStart://确认开始
//                int timeLength = 0;
//                for (DeviceRunStep deviceRunStep : mDeviceRunSteps) {
//                    timeLength += deviceRunStep.getTime();
//                }
//                if (timeLength == 0) {
//                    ToastUtil.show("参数异常");
//                    return;
//                }
//                if (SerialManager.getInstance().sendCmdStart(mDeviceRunSteps)) {
//                    VoiceManager.getInstance().startSpeak("现在开始烹饪");
//                    startActivity(new Intent(mContext, CookRunningActivity.class));
//                    finish();
//                }
//                break;
//            case R.id.imgAddTemp:
//                tempAdd();
//                break;
//            case R.id.imgDelTemp:
//                tempDel();
//                break;
//            case R.id.imgAddTime:
//                timeAdd();
//                break;
//            case R.id.imgDelTime:
//                timeDel();
//                break;
//        }
//    }
//}
