package com.viomi.oven.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.bean.BaseResult;
import com.viomi.oven.bean.BasicKeyInfo;
import com.viomi.oven.bean.FoodDetail;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.enumType.FromType;
import com.viomi.oven.enumType.ModelType;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.manager.AccountManager;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.ImgUtil;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.StringUtil;
import com.viomi.oven.util.ToastUtil;
import com.viomi.oven.util.http.HttpReCallback;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Ljh on 2018/1/3
 * 一键烹饪
 */
public class OneKeyCookingActivity_H extends BaseActivity {
    //UI
    @BindView(R.id.imgWifiV)
    ImageView imgWifiV;
    @BindView(R.id.imgFood)
    ImageView imgFood;
    @BindView(R.id.tvBuy)
    TextView tvBuy;
    @BindView(R.id.tvType)
    TextView tvType;
    @BindView(R.id.tvFoodName)
    TextView tvFoodName;
    @BindView(R.id.tvOk)
    TextView tvOk;
    @BindView(R.id.tvInfo)
    TextView tvInfo;
    @BindView(R.id.llInfo)
    LinearLayout llInfo;
    @BindView(R.id.llBasic)
    LinearLayout llBasic;
    @BindView(R.id.llNutrition)
    LinearLayout llNutrition;
    @BindView(R.id.llDetail)
    LinearLayout llDetail;
    @BindView(R.id.llMaterials)
    LinearLayout llMaterials;
    @BindView(R.id.tvMaterials)
    TextView tvMaterials;
    @BindView(R.id.tvFactoryName)
    TextView tvFactoryName;
    @BindView(R.id.llFactoryName)
    LinearLayout llFactoryName;
    @BindView(R.id.tvFactoryAddr)
    TextView tvFactoryAddr;
    @BindView(R.id.llFactoryAddr)
    LinearLayout llFactoryAddr;
    @BindView(R.id.llCompose)
    LinearLayout llCompose;
    @BindView(R.id.llCompose1)
    LinearLayout llCompose1;
    @BindView(R.id.llCompose2)
    LinearLayout llCompose2;
    //参数
    FoodDetail mFoodDetail = new FoodDetail();
    List<DeviceRunStep> preHeating = new ArrayList<>();
    List<DeviceRunStep> mDeviceRunSteps = new ArrayList<>();
    String foodId = "";

    public static void actionStart(Context context, String foodId) {
        Intent intent = new Intent(context, OneKeyCookingActivity_H.class);
        intent.putExtra("foodId", foodId);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_one_key_cooking_h;
    }

    @Override
    protected void initView() {
        imgWifiV.setVisibility(View.VISIBLE);
    }

    @Override
    protected void initData() {
        foodId = getIntent().getStringExtra("foodId");
        //foodId = "viomi-oven-food:id:2";
        if (StringUtil.isEmpty(foodId)) {
            if (HawkParams.getModelType() == ModelType.MODEL_MY_SET.value)
                mFoodDetail = HawkParams.getMySetFoodList().get(HawkParams.getSelectFoodPos());
            else
                mFoodDetail = HawkParams.getSmartFoodList().get(HawkParams.getSelectFoodPos());
            initShowAndData();
            //菜谱
            tvType.setText("菜谱名称:");
            tvBuy.setVisibility(View.GONE);
            llFactoryAddr.setVisibility(View.GONE);
            llFactoryName.setVisibility(View.GONE);
        } else {
            //食材
            tvType.setText("食材名称:");
            httpGetFoodDetail(foodId);
        }
    }

    public void httpGetFoodDetail(String foodId) {
        showLoading();
        OkHttpUtils
                .get()
                .url(AppStaticConfig.URL_FOOD_QR_DETAIL + foodId)
                .tag(this)
                .build()
                .execute(new HttpReCallback<BaseResult<FoodDetail>>() {
                    @Override
                    public void onSuccess(BaseResult<FoodDetail> result) {
                        if (result.getCode() == 100) {
                            mFoodDetail = result.getResult();
                            if (mFoodDetail == null)
                                ToastUtil.show("数据解析异常");
                            initShowAndData();
                        }else if(result.getCode() == 902){
                            showToast("请扫描云米食材");
                            finish();
                        }
                        hideLoading();
                    }

                    @Override
                    public void onFail(int errorCode) {
                        hideLoading();
                    }
                });
    }

    public void initShowAndData() {
        if (mFoodDetail == null)
            return;
        if (mFoodDetail.getImgUrl().size() > 0) {
            ImgUtil.showDefinedImage(mFoodDetail.getImgUrl().get(0), imgFood, R.color.transparent);
        } else if (mFoodDetail.getImgsId().size() > 0) {
            ImgUtil.showDefinedImage(mFoodDetail.getImgsId().get(0), imgFood, R.color.transparent);
        }
        //else
        //    ImgUtil.showDefinedImage(R.drawable.default_food, imgFood, R.drawable.default_food);
        tvFoodName.setText(mFoodDetail.getName());
        //生产商和生产地址
        tvFactoryName.setText(mFoodDetail.getManufacture().getName());
        tvFactoryAddr.setText(mFoodDetail.getManufacture().getAddress());
        //用料
        if(!StringUtil.isEmpty(mFoodDetail.getMaterial())){
            llMaterials.setVisibility(View.VISIBLE);
            tvMaterials.setText(mFoodDetail.getMaterial());
        }
        //烹饪方式
        String info = "";
        for (int i = 0; i < mFoodDetail.getRecipeGuideList().size(); i++) {
            if (i == 0)
                info = mFoodDetail.getRecipeGuideList().get(i).getGuideOrder() + "、" + mFoodDetail.getRecipeGuideList().get(i).getGuideDesc();
            else info += "\n" + mFoodDetail.getRecipeGuideList().get(i).getGuideOrder() + "、" + mFoodDetail.getRecipeGuideList().get(i).getGuideDesc();
        }
        tvInfo.setText(info);
        //基本信息
        List<BasicKeyInfo> mBasicInfos = mFoodDetail.getBasicInfo();
        for (int i = 0; i < mBasicInfos.size(); i++) {//商品基本信息
            llDetail.setVisibility(View.VISIBLE);
            TextView textView = new TextView(mContext);
            textView.setText(mBasicInfos.get(i).getName() + ":  " + mBasicInfos.get(i).getValue() + mBasicInfos.get(i).getUnit());
            textView.setTextSize(Global.dip2px(mContext, 26));
            textView.setTextColor(Color.WHITE);
            textView.setGravity(Gravity.CENTER_VERTICAL);
            textView.getPaint().setTextSize(Global.dip2px(mContext, 26));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 80);
            params.leftMargin = 40;
            llBasic.addView(textView, params);
            if (mBasicInfos.get(i).getName().equals("产品重量")) {
                HawkParams.setWeight(mBasicInfos.get(i).getValue());
            }
        }
        //营养信息
        List<BasicKeyInfo> mNutritionInfos = mFoodDetail.getNutritionInfo();
        for (int i = 0; i < mNutritionInfos.size(); i++) {//商品营养信息
            llDetail.setVisibility(View.VISIBLE);
            TextView textView = new TextView(mContext);
            textView.setText(mNutritionInfos.get(i).getName() + ":  " + mNutritionInfos.get(i).getValue() + mNutritionInfos.get(i).getUnit());
            textView.setTextSize(Global.dip2px(mContext, 26));
            textView.getPaint().setTextSize(Global.dip2px(mContext, 26));
            textView.setTextColor(Color.WHITE);
            textView.setGravity(Gravity.CENTER_VERTICAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 80);
            params.leftMargin = 40;
            llNutrition.addView(textView, params);
        }
        HawkParams.setNutritionInfos(mNutritionInfos);
        LogUtils.d(TAG, HawkParams.getNutritionInfos2String());
        //配料名称
        List<BasicKeyInfo> mIngredients = mFoodDetail.getIngredients();
        for (int i = 0; i < mIngredients.size(); i++) {//配料
            llDetail.setVisibility(View.VISIBLE);
            TextView textView = new TextView(mContext);
            textView.setText(mIngredients.get(i).getName());
            textView.setTextSize(Global.dip2px(mContext, 26));
            textView.getPaint().setTextSize(Global.dip2px(mContext, 26));
            textView.setTextColor(Color.WHITE);
            textView.setGravity(Gravity.CENTER_VERTICAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 80);
            params.leftMargin = 40;
            if (i % 2 == 0) {
                llCompose1.addView(textView, params);
            } else {
                llCompose2.addView(textView, params);
            }
        }
        //烹饪信息
        mDeviceRunSteps.clear();
        preHeating.clear();
        for (DeviceRunStep temp : mFoodDetail.getRecipeDirectionList()) {
            DeviceRunStep step = new DeviceRunStep(SteamBakeType.STEAM);
            step.setMode(temp.getMode());
            step.setTemp(temp.getTemp());
            step.setTime(temp.getTime());
            step.setStop(temp.getStop());
            step.setStoptip(temp.getStoptip());
            if (temp.getMode() == SteamBakeType.PREHEATING.value) {
                preHeating.add(step);
            } else {
                mDeviceRunSteps.add(Global.compatibleChange(step));
            }
        }
    }

    @OnClick({R.id.tvBuy, R.id.tvOk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvBuy:
                if (AccountManager.getViomiUser(mContext) == null) {
                    ToastUtil.show("请先登录");
                    startActivity(new Intent(mContext, ScanLoginActivity.class));
                } else {
                    startActivity(new Intent(mContext, OneKeyBuyActivity.class));
                }
                break;
            case R.id.tvOk:
                if (preHeating.size() > 0) {//有预热
                    if (Global.isPrepareOk(preHeating)) {
                        if (SerialManager.getInstance().sendCmdStart(preHeating)) {
                            HawkParams.setFoodName(mFoodDetail.getName());
                            HawkParams.setCookFrom(FromType.FROM_RECIPE.value);
                            HawkParams.setCookStep(mDeviceRunSteps);
                            PreHeatingActivity.actionStart(mContext, preHeating.get(0).getTemp());
                            finish();
                        }
                    }
                } else if (mDeviceRunSteps.size() > 0) {//直接烹饪
                    if (Global.isPrepareOk(mDeviceRunSteps)) {
                        if (SerialManager.getInstance().sendCmdStart(mDeviceRunSteps)) {
                            HawkParams.setFoodName(mFoodDetail.getName());
                            HawkParams.setCookFrom(FromType.FROM_RECIPE.value);
                            //VoiceManager.getInstance().startSpeak("现在开始烹饪" + mFoodDetail.getName());
                            startActivity(new Intent(mContext, CookRunningActivity.class));
                        }
                    }
                } else
                    showToast("缺乏烹饪数据！");
                break;
        }
    }
}
