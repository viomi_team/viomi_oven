package com.viomi.oven.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.otto.Subscribe;
import com.viomi.oven.AppParam;
import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.R;
import com.viomi.oven.bean.BespeakdishBean;
import com.viomi.oven.bean.CurveRunStep;
import com.viomi.oven.bean.ModesBean;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.dialog.CommonConfirmDialog;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.manager.SerialManager;
import com.viomi.oven.otto.Event2PauseStatus;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.GsonTools;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.view.CurveRunningView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Ljh on 2018/1/3
 * 专业模式的预约和普通模式的预览页面(普通模式可能还包含预热环节)
 */
public class AppointPreviewActivity extends BaseActivity {
    //UI
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgLight)
    ImageView imgLight;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvPreHeating)
    TextView tvPreHeating;
    @BindView(R.id.tvAppointTime)
    TextView tvAppointTime;
    @BindView(R.id.llAppointTitle)
    LinearLayout llAppointTitle;
    @BindView(R.id.llTotalTime1)
    LinearLayout llTotalTime1;
    @BindView(R.id.curveView)
    CurveRunningView curveView;
    @BindView(R.id.tvTotalTime)
    TextView tvTotalTime;
    @BindView(R.id.tvTotalTime1)
    TextView tvTotalTime1;
    @BindView(R.id.tvBtn)
    TextView tvBtn;
    @BindView(R.id.tvLookDetail)
    TextView tvLookDetail;
    @BindView(R.id.rlPointContent)
    RelativeLayout rlPointContent;
    @BindView(R.id.root)
    LinearLayout root;
    CommonConfirmDialog mCommonConfirmDialog;
    //参数
    boolean isAppoint = false;//true预约模式，false普通模式预览
    private String previewTitle = "蒸鱼";
    int appointTime = 0, timeLength = 0;//appointTime为秒级时间戳
    List<CurveRunStep> mCurveRunSteps = new ArrayList<>();
    List<DeviceRunStep> mDeviceRunSteps = new ArrayList<>();
    List<DeviceRunStep> preHeating = new ArrayList<>();
    private IntentFilter intentFilter;

    private TimeChangeReceiver timeChangeReceiver;
    //
    private static final int showCurve = 1000;

    public static void actionStart(Context context, boolean isAppoint) {
        Intent intent = new Intent(context, AppointPreviewActivity.class);
        intent.putExtra("isAppoint", isAppoint);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutRes() {
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            return R.layout.activity_appoint_preview_v;
        else
            return R.layout.activity_appoint_preview_h;
    }

    @Override
    protected void initView() {
        isAppoint = getIntent().getBooleanExtra("isAppoint", false);
        previewTitle = HawkParams.getFoodName();
        appointTime = HawkParams.getAppointTime();
        mDeviceRunSteps = HawkParams.getCookStep();
        //
        curveView.setShowBgPoint(false);
        if (mDeviceRunSteps.size() > 0 && mDeviceRunSteps.get(0).getType() == SteamBakeType.PREHEATING) {
            tvPreHeating.setVisibility(View.VISIBLE);
            tvPreHeating.setText("此菜谱需先预热到" + mDeviceRunSteps.get(0).getTemp() + "℃");
            tvBtn.setBackgroundResource(R.drawable.press_start_heat_v);
            preHeating.clear();
            preHeating.add(mDeviceRunSteps.get(0));
            mDeviceRunSteps.remove(0);
        }
        if (mDeviceRunSteps.size() == 0) {
            showToast("烹饪数据异常");
            finish();
        }
        //
        imgBack.setVisibility(isAppoint ? View.GONE : View.VISIBLE);
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1)) {
            tvBtn.setBackgroundResource(isAppoint ? R.drawable.pressed_cancle_appoint_v_new : R.drawable.pressed_start_v_new);
        } else
            tvBtn.setBackgroundResource(isAppoint ? R.drawable.cancle_appoint_yellow : R.drawable.img_start_yellow);
        tvTitle.setText(previewTitle);
        if (isAppoint) {
            tvTitle.setText(DateFormat.format("HH:mm", new Date()).toString());
        }
        //tvTitle.setVisibility(isAppoint ? View.GONE : View.VISIBLE);
        llAppointTitle.setVisibility(isAppoint ? View.VISIBLE : View.GONE);
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            llTotalTime1.setVisibility(isAppoint ? View.GONE : View.VISIBLE);
        tvLookDetail.setVisibility(isAppoint ? View.GONE : View.VISIBLE);
        int appointHour = ((appointTime / (60 * 60) % (24)) + 8) % 24;
        int appointMin = (appointTime / 60 % 60);
        tvAppointTime.setText(appointHour + ":" + (appointMin < 10 ? ("0" + appointMin) : appointMin));
        mCommonConfirmDialog = new CommonConfirmDialog(mContext);
        mCommonConfirmDialog.setTitle("是否确认取消预约");
        mCommonConfirmDialog.setCallBack(new CommonConfirmDialog.CommonConfirmDialogCallBack() {
            @Override
            public void btnOk() {
                //取消预约的逻辑
                Global.jump2MainActivity(mContext);
                finish();
            }
        });
        if (findViewById(R.id.rlStep) != null) {
            RelativeLayout rlStep = (RelativeLayout) findViewById(R.id.rlStep);
            rlStep.removeAllViews();
            for (int i = 0; i < mDeviceRunSteps.size(); i++) {
                LinearLayout ll_item = (LinearLayout) ((Activity) mContext).getLayoutInflater()
                        .inflate(R.layout.adapter_special_step_show, null, false);
                ((TextView) ll_item.findViewById(R.id.tvModel)).setText((mDeviceRunSteps.size() > 1 ? (i + 1) + "、" : "") + mDeviceRunSteps.get(i).getType()
                        .name);
                ((TextView) ll_item.findViewById(R.id.tvTemp)).setText(mDeviceRunSteps.get(i).getTemp() + "℃");
                ((TextView) ll_item.findViewById(R.id.tvTime)).setText(mDeviceRunSteps.get(i).getTime() + "min");
                ((ImageView) ll_item.findViewById(R.id.imgModel)).setImageResource(mDeviceRunSteps.get(i).getType().s_drawable);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(200, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (i == 1) {
                    params.setMargins(270, 0, 0, 0);
                } else if (i == 2) {
                    params.setMargins(0, 160, 0, 0);
                } else if (i == 3) {
                    params.setMargins(270, 160, 0, 0);
                }
                rlStep.addView(ll_item, params);
            }
        }
    }

    @Override
    protected void initData() {
        HawkParams.setWorkStatus(isAppoint ? WorkStatus.STATUS_YUYUEING : WorkStatus.STATUS_IDLE);
        // 模拟曲线图绘制
        sendToHandlerDelayed(showCurve, "", 500);
        for (DeviceRunStep temp : mDeviceRunSteps) {
            timeLength += temp.getTime();
        }
        tvTotalTime.setText("共" + timeLength + "min");
        tvTotalTime1.setText(timeLength + "");
        HawkParams.setC_totletime(timeLength * 60);
        if (isAppoint && appointTime > 0) {
            if ((appointTime / 60) <= (System.currentTimeMillis() / 1000 / 60)) {//预约的时间小于当前系统时间
                startCooking();
            } else {
                //预约倒计时
                timeChangeReceiver = new TimeChangeReceiver();
                intentFilter = new IntentFilter();
                intentFilter.addAction(Intent.ACTION_TIME_TICK);//每分钟变化
                registerReceiver(timeChangeReceiver, intentFilter);
            }
            //预约的菜谱信息
            BespeakdishBean bespeakdishBean = new BespeakdishBean();
            bespeakdishBean.setName(HawkParams.getFoodName());
            List<DeviceRunStep> cookStep = HawkParams.getCookStep();
            for (DeviceRunStep step : cookStep) {
                bespeakdishBean.getModes().add(new ModesBean(step.getMode(), step.getTime(), step.getTemp()));
            }
            HawkParams.setBespeakdish(GsonTools.objectToJson(bespeakdishBean));
        }
        showPoint();
    }

    //添加阶段点
    public void showPoint() {
        rlPointContent.removeAllViews();
        int timeStageLeng = 0;
        for (int i = 0; i < mDeviceRunSteps.size(); i++) {
            RelativeLayout rl_item = (RelativeLayout) ((Activity) mContext).getLayoutInflater()
                    .inflate(R.layout.item_step, null, false);
            ((TextView) rl_item.findViewById(R.id.tvStep)).setText((i + 1) + "");
            rl_item.setTag(i + "");
            timeStageLeng += mDeviceRunSteps.get(i).getTime() * 60;
            LogUtils.d(TAG, "showPoint timeLeng:" + timeStageLeng + " timeLengthSec:" + timeLength * 60);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(60, 60);
            int marginLeft = (int) (600 * 1.0 / (timeLength * 60) * timeStageLeng - 20);
            LogUtils.d(TAG, "showPoint marginLeft:" + marginLeft);
            layoutParams.setMargins(marginLeft, 0, 0, 0);
            rlPointContent.addView(rl_item, layoutParams);
        }
    }

    private List<CurveRunStep> getTempCurveStep(List<DeviceRunStep> steps) {
        List<CurveRunStep> mCurveRunSteps = new ArrayList<>();
        int curTemp = SerialManager.getInstance().getDataReceiveInfo().getOvenTemp();
        int num = (int) (steps.get(0).time * 1.0 * 40 / timeLength);
        if(num == 0)
            num = 1;
        double dashTemper = (steps.get(0).getTemp() - curTemp) * 1.0 / 3;
        //0
        for (int i = 0; i < num; i++) {
            if (dashTemper > 0) {
                if ((curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)) > steps.get(0).getTemp())
                    mCurveRunSteps.add(new CurveRunStep(steps.get(0).getTemp() + 1 * (i % 2)));
                else
                    mCurveRunSteps.add(new CurveRunStep(curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)));
            } else {
                if ((curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)) < steps.get(0).getTemp())
                    mCurveRunSteps.add(new CurveRunStep(steps.get(0).getTemp() + 1 * (i % 2)));
                else
                    mCurveRunSteps.add(new CurveRunStep(curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)));
            }
        }
        //1
        if (steps.size() > 1) {
            num = (int) (steps.get(1).time * 1.0 * 40 / timeLength);
            curTemp = (int) mCurveRunSteps.get(mCurveRunSteps.size() - 1).getTemp();
            dashTemper = (steps.get(1).getTemp() - curTemp) * 1.0 / 3;
            if(num == 0)
                num = 1;
            for (int i = 0; i < num; i++) {
                if (dashTemper > 0) {
                    if ((curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)) > steps.get(1).getTemp())
                        mCurveRunSteps.add(new CurveRunStep(steps.get(1).getTemp() + 1 * (i % 2)));
                    else
                        mCurveRunSteps.add(new CurveRunStep(curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)));
                } else {
                    if ((curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)) < steps.get(1).getTemp())
                        mCurveRunSteps.add(new CurveRunStep(steps.get(1).getTemp() + 1 * (i % 2)));
                    else
                        mCurveRunSteps.add(new CurveRunStep(curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)));
                }
            }
        }
        //
        if (steps.size() > 2) {
            num = (int) (steps.get(2).time * 1.0 * 40 / timeLength);
            curTemp = (int) mCurveRunSteps.get(mCurveRunSteps.size() - 1).getTemp();
            dashTemper = (steps.get(2).getTemp() - curTemp) * 1.0 / 3;
            if(num == 0)
                num = 1;
            for (int i = 0; i < num; i++) {
                if (dashTemper > 0) {
                    if ((curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)) > steps.get(2).getTemp())
                        mCurveRunSteps.add(new CurveRunStep(steps.get(2).getTemp() + 1 * (i % 2)));
                    else
                        mCurveRunSteps.add(new CurveRunStep(curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)));
                } else {
                    if ((curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)) < steps.get(2).getTemp())
                        mCurveRunSteps.add(new CurveRunStep(steps.get(2).getTemp() + 1 * (i % 2)));
                    else
                        mCurveRunSteps.add(new CurveRunStep(curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)));
                }
            }
        }
        if (steps.size() > 3) {
            num = (int) (steps.get(3).time * 1.0 * 40 / timeLength);
            curTemp = (int) mCurveRunSteps.get(mCurveRunSteps.size() - 1).getTemp();
            dashTemper = (steps.get(3).getTemp() - curTemp) * 1.0 / 3;
            if(num == 0)
                num = 1;
            for (int i = 0; i < num; i++) {
                if (dashTemper > 0) {
                    if ((curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)) > steps.get(3).getTemp())
                        mCurveRunSteps.add(new CurveRunStep(steps.get(3).getTemp() + 1 * (i % 2)));
                    else
                        mCurveRunSteps.add(new CurveRunStep(curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)));
                } else {
                    if ((curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)) < steps.get(3).getTemp())
                        mCurveRunSteps.add(new CurveRunStep(steps.get(3).getTemp() + 1 * (i % 2)));
                    else
                        mCurveRunSteps.add(new CurveRunStep(curTemp + i * dashTemper * (0.8 + (i + 1) * 0.1)));
                }
            }
        }
        while (mCurveRunSteps.size() < 40) {
            mCurveRunSteps.add(new CurveRunStep(mCurveRunSteps.get(mCurveRunSteps.size() - 1).getTemp()));
        }
        return mCurveRunSteps;
    }

    class TimeChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case Intent.ACTION_TIME_TICK:
                    //每过一分钟 触发
                    imgLight.setSelected(AppParam.isLightOn);
                    tvTitle.setText(DateFormat.format("HH:mm", new Date()).toString());
                    LogUtils.d(TAG, "1 min passed");
                    //int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                    //int minute = Calendar.getInstance().get(Calendar.MINUTE);
                    if ((appointTime / 60) <= (System.currentTimeMillis() / 1000 / 60)) {//预约的时间小于当前系统时间
                        startCooking();
                    }
                    break;
            }
        }
    }

    @Override
    public void dealTheMsg(Message msg) {
        super.dealTheMsg(msg);
        if (msg.what == showCurve) {
            mCurveRunSteps.clear();
            /*
            //curveView.setShowMaxCount(mDeviceRunSteps.size() + 1);
            //获取当前烤箱温度(作为曲线起始温度)
            DeviceParamsGet paramsGet = SerialManager.getInstance().getDataReceiveInfo();
            mCurveRunSteps.add(new CurveRunStep(paramsGet.getOvenTemp()));
            //绘制模拟曲线
            for (int i = 0; i < mDeviceRunSteps.size(); i++) {//温度值取20到260
                CurveRunStep temp = new CurveRunStep(mDeviceRunSteps.get(i).temp);
                mCurveRunSteps.add(temp);
            }
            */
            mCurveRunSteps = getTempCurveStep(mDeviceRunSteps);
            curveView.updata(mCurveRunSteps);
        }
    }

    @OnClick({R.id.tvBtn, R.id.tvLookDetail})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvBtn:
                if (isAppoint) {//取消预约
                    mCommonConfirmDialog.show();
                } else {//普通模式，开始烹饪或者开始预热
                    startCooking();
                }
                break;
            case R.id.tvLookDetail:
                if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
                    startActivity(new Intent(mContext, OneKeyCookingActivity_V.class));
                else
                    startActivity(new Intent(mContext, OneKeyCookingActivity_H.class));
                break;
        }
    }

    public void startCooking() {
        if (preHeating.size() > 0) {//需要预热
            if (Global.isPrepareOk(preHeating)) {
                if (SerialManager.getInstance().sendCmdStart(preHeating)) {
                    HawkParams.setCookStep(mDeviceRunSteps);
                    PreHeatingActivity.actionStart(mContext, preHeating.get(0).getTemp());
                    finish();
                }
            }
        } else {
            if (Global.isPrepareOk(mDeviceRunSteps)) {
                if (SerialManager.getInstance().sendCmdStart(mDeviceRunSteps)) {
                    //VoiceManager.getInstance().startSpeak("现在开始烹饪" + HawkParams.getFoodName());
                    startActivity(new Intent(mContext, CookRunningActivity.class));
                    finish();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (timeChangeReceiver != null)
            unregisterReceiver(timeChangeReceiver);
        super.onDestroy();
    }

    @Subscribe
    public void receiveEvent(Event2PauseStatus event) {
        LogUtils.d("receiver event from is" + event.getBusFrom() + " the time is:");
        if (HawkParams.getWorkStatus() == WorkStatus.STATUS_YUYUEING.value && isAppoint) {
            if (event.getPauseStatus() == 2) {//结束预约指令
                if (isAppoint) {
                    Global.jump2MainActivity(mContext);
                }
            }
        }
    }
}
