package com.viomi.oven;

import com.viomi.oven.enumType.McuStatus;

/**
 * Created by Ljh on 2017/12/20.
 * 全局变量
 */

public class AppParam {
    public static boolean isLightOn = false;//灯状态
    public static boolean isDoorOpen = false;//门状态
    public static boolean isNoWater = false;//水箱状态
    public static boolean isConnectErr = false;//发送通讯异常
    public static boolean isPannelOn = false;
    public static McuStatus mcuStatus = McuStatus.MCU_IDLE;//0空闲   1运行中    2暂停挂起状态     3升级状态
}


