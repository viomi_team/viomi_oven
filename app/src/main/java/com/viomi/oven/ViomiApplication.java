package com.viomi.oven;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.multidex.MultiDex;

import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.NoEncryption;
import com.tencent.bugly.Bugly;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.manager.VoiceManager;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.http.MyLogInterceptor;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.https.HttpsUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class ViomiApplication extends Application implements
        Thread.UncaughtExceptionHandler {
    private static final String TAG = "ViomiApplication";
    public static ViomiApplication gContext;
    public static Handler gMainHandler;

    public static ViomiApplication getContext() {
        return gContext;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        gContext = this;
        gMainHandler = new Handler();
        Bugly.init(getApplicationContext(), AppStaticConfig.TYPE_DEBUG ? "ba1ae0a0d3" : "cd6daa717a", AppStaticConfig.TYPE_DEBUG);
        Bugly.setAppChannel(getApplicationContext(), DeviceConfig.MODEL);
        //File cacheFile = new File(gContext.getCacheDir(), "[缓存目录]");
        //Cache cache = new Cache(cacheFile, 1024 * 1024 * 80); //80Mb
        HttpsUtils.SSLParams sslParams = HttpsUtils.getSslSocketFactory(null, null, null);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager)
                .addInterceptor(new MyLogInterceptor("HttpReceiver", true))
                .connectTimeout(60000L, TimeUnit.MILLISECONDS)
                .readTimeout(60000L, TimeUnit.MILLISECONDS)
                .writeTimeout(60000L, TimeUnit.MILLISECONDS)
                //.addNetworkInterceptor(new CacheInterceptor())
                // .cache(cache)
                //其他配置
                .build();

        OkHttpUtils.initClient(okHttpClient);
        //
        //if (isMainProcess()) {
        //}
        //LogCat开关
        //LogUtils.setDebugEnable(AppStaticConfig.TYPE_DEBUG);
        // Hawk初始化
        Hawk.init(this).setEncryption(new NoEncryption()).build();

        VoiceManager.getInstance().init(this);
    }

    /**
     * 获得进程的名字
     *
     * @param context
     * @return 进程号
     */
    public static String getProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager.getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }

    public boolean isMainProcess() {
        return getPackageName().equals(getProcessName(getApplicationContext()));
    }

    public static void runAsync(Runnable r) {
        gMainHandler.post(r);
    }

    public static void runAsyncDelayed(Runnable r, long delayMillis) {
        gMainHandler.postDelayed(r, delayMillis);
    }

    private String getErrorInfo(Throwable arg1) {
        Writer writer = new StringWriter();
        PrintWriter pw = new PrintWriter(writer);
        arg1.printStackTrace(pw);
        pw.close();
        String error = writer.toString();
        return error;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable arg1) {
        try {
            LogUtils.d("信息：uncaughtException: " + getErrorInfo(arg1));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}
