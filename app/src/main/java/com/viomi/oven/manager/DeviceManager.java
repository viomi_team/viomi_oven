package com.viomi.oven.manager;

import android.content.Context;
import android.content.Intent;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.content.LocalBroadcastManager;

import com.viomi.defined.device.ViomiOven;
import com.viomi.defined.service.OvenService;
import com.viomi.oven.AppParam;
import com.viomi.oven.HawkParams;
import com.viomi.oven.MiIndentify;
import com.viomi.oven.ViomiApplication;
import com.viomi.oven.activity.AppointPreviewActivity;
import com.viomi.oven.activity.ChugouActivity;
import com.viomi.oven.activity.CookRunningActivity;
import com.viomi.oven.activity.PreHeatingActivity;
import com.viomi.oven.bean.AppSetStatus;
import com.viomi.oven.bean.BasicKeyInfo;
import com.viomi.oven.broadcast.BroadcastAction;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.device.MiotDeviceInfo;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.interfaces.AppCallback;
import com.viomi.oven.otto.BusProvider;
import com.viomi.oven.otto.Event2PauseStatus;
import com.viomi.oven.util.ApkUtil;
import com.viomi.oven.util.Global;
import com.viomi.oven.util.GsonTools;
import com.viomi.oven.util.IpUtils;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.PhoneUtil;
import com.xiaomi.miot.host.manager.MiotDeviceConfig;
import com.xiaomi.miot.host.manager.MiotHostManager;
import com.xiaomi.miot.typedef.device.DiscoveryType;
import com.xiaomi.miot.typedef.error.MiotError;
import com.xiaomi.miot.typedef.exception.MiotException;
import com.xiaomi.miot.typedef.listener.CompletedListener;
import com.xiaomi.miot.typedef.listener.OnBindListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ljh on 2017/1/11.
 */

public class DeviceManager {
    private static final String TAG = DeviceManager.class.getSimpleName();
    private static DeviceManager INSTANCE;
    private ViomiOven mDevice;
    private AppCallback<String> mBindCallback;

    public static DeviceManager getInstance() {
        synchronized (DeviceManager.class) {
            if (INSTANCE == null) {
                synchronized (DeviceManager.class) {
                    if (INSTANCE == null) {
                        INSTANCE = new DeviceManager();
                    }
                }
            }
        }
        return INSTANCE;
    }

    /***
     * 设备是否已绑定
     * @return
     */
    public boolean isDeviceBind() {
        return HawkParams.isDeviceBindFlag();
    }

    /***
     * 设备绑定状态设置
     * @param flag
     */
    public void setDeviceBindFlag(boolean flag) {
        HawkParams.setDeviceBindFlag(flag);
    }


    /***
     *初始化设备服务
     * @param context
     */
    public void initDevice(Context context, final String miId, final AppCallback<String> callback) {
        LogUtils.d(TAG, "initDevice");
        mBindCallback = callback;
        try {
            MiotHostManager.getInstance().bind(context, new CompletedListener() {
                @Override
                public void onSucceed() {
                    LogUtils.d(TAG, "bind onSucceed");

                    try {
                        MiotHostManager.getInstance().start();
                    } catch (MiotException e) {
                        LogUtils.e(TAG, "img_start_yellow fail!,msg=" + e.getMessage());
                        e.printStackTrace();
                    }

                    try {
                        creatDevice(miId);
                    } catch (MiotException e) {
                        LogUtils.e(TAG, "creatDevice fail!,msg=" + e.getMessage());
                        e.printStackTrace();
                        mBindCallback.onFail(-4, "creatDevice fail!");
                    }
                }

                @Override
                public void onFailed(MiotError miotError) {
                    LogUtils.e(TAG, "bind failed: " + miotError);
                    mBindCallback.onFail(-2, "bind fail!");
                }
            });
        } catch (MiotException e) {
            String error = e.getMessage();
            LogUtils.e(TAG, "getErrorCode:" + e.getErrorCode() + ",getMessage:" + e.getMessage());
            e.printStackTrace();

        }
    }

    /***
     * 解除设备连接
     */

    public boolean unBindDevice(final Context context, final AppCallback<String> callback) {
        LogUtils.d(TAG, "unBindDevice");
        try {
            LogUtils.d(TAG, "unBindDevice reset device!");
            MiotHostManager.getInstance().reset(new CompletedListener() {
                @Override
                public void onSucceed() {
                    AccountManager.deleteViomiUser(context);
                    setDeviceBindFlag(false);
                    Intent intent = new Intent(BroadcastAction.ACTION_DEVICE_UNBIND);
                    LocalBroadcastManager.getInstance(ViomiApplication.getContext()).sendBroadcast(intent);
                    callback.onSuccess("");
                }

                @Override
                public void onFailed(MiotError miotError) {
                    LogUtils.e(TAG, "unBindDevice reset error,msg=" + miotError.getMessage());
                    callback.onFail(-1, miotError.getMessage());
                }
            });
        } catch (MiotException e) {
            LogUtils.e(TAG, "stopBindDevice error,msg=" + e.getMessage());
            e.printStackTrace();

            return false;
        }
        return true;
    }

    /***
     * 设备切换绑定账号
     */
    public boolean reBindDevice(final String userId, final AppCallback<String> callback) {
        LogUtils.d(TAG, "reBindDevice");
        mBindCallback = callback;
        try {
            MiotHostManager.getInstance().reset(new CompletedListener() {
                @Override
                public void onSucceed() {
                    LogUtils.i(TAG, "reBindDevice  reset succss!");
                    try {
                        creatDevice(userId);
                    } catch (MiotException e) {
                        LogUtils.e(TAG, "reBindDevice creatDevice fail!,msg=" + e.getMessage());
                        e.printStackTrace();
                        mBindCallback.onFail(-7, "reBindDevice creatDevice error!");
                    }
                }

                @Override
                public void onFailed(MiotError miotError) {
                    LogUtils.e(TAG, "reBindDevice reset error,msg=" + miotError.getMessage());
                    mBindCallback.onFail(-5, "reset device fail!");
                }
            });
        } catch (MiotException e) {
            LogUtils.e(TAG, "stopBindDevice error,msg=" + e.getMessage());
            e.printStackTrace();
            mBindCallback.onFail(-6, "reset device error!");
            return false;
        }
        return true;
    }

    /***
     * 停止服务
     * @param context
     */
    public boolean unBindService(Context context) {
        LogUtils.d(TAG, "unBindService");
        try {
            MiotHostManager.getInstance().stop();
            MiotHostManager.getInstance().unbind(context);
            return true;
        } catch (MiotException e) {
            e.printStackTrace();
        } catch (Exception e) {
            LogUtils.e(TAG, "unbind error,msg=" + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    private void bindRegister() {
        try {
            LogUtils.d(TAG, "registerBindListener");
            MiotHostManager.getInstance().registerBindListener(new OnBindListener() {
                @Override
                public void onBind() {
                    LogUtils.d(TAG, "device bind!");
                    setDeviceBindFlag(true);
                    Intent intent = new Intent(BroadcastAction.ACTION_DEVICE_BIND);
                    LocalBroadcastManager.getInstance(ViomiApplication.getContext()).sendBroadcast(intent);
                }

                @Override
                public void onUnBind() {
                    LogUtils.e(TAG, "device unbind!");
                    setDeviceBindFlag(false);
                    Intent intent = new Intent(BroadcastAction.ACTION_DEVICE_UNBIND);
                    LocalBroadcastManager.getInstance(ViomiApplication.getContext()).sendBroadcast(intent);
                }
            }, new CompletedListener() {
                @Override
                public void onSucceed() {
                    LogUtils.d(TAG, "device bind success!");
                }

                @Override
                public void onFailed(MiotError miotError) {
                    LogUtils.e(TAG, "device bind fail!msg=" + miotError.getMessage());
                }
            });
        } catch (MiotException e) {
            LogUtils.e(TAG, "registerBindListener,msg=" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void creatDevice(String userid) throws MiotException {
        MiIndentify miIndentify = PhoneUtil.getMiIdentify();
        LogUtils.d(TAG, "did=" + miIndentify.did + ",mac=" + miIndentify.mac + ",token=" + miIndentify.token);
        MiotDeviceInfo info = new MiotDeviceInfo();
        info.deviceId = miIndentify.did;
        info.macAddress = miIndentify.mac;
        info.miotToken = miIndentify.token;
        info.miotInfo = getMiotInfo(userid);
        LogUtils.d(TAG, "info.miotInfo=" + info.miotInfo);
        initDevice(info, userid);

    }

    private void initDevice(MiotDeviceInfo info, final String userid) throws MiotException {
        LogUtils.d(TAG, "initDevice");
        /**
         * 1. Initialize Configuration
         */
        MiotDeviceConfig config = new MiotDeviceConfig();
        config.addDiscoveryType(DiscoveryType.MIOT);
        config.friendlyName("ViomiOven");
        config.deviceId(info.deviceId);
        config.macAddress(info.macAddress);
        config.manufacturer("viomiOven");
        config.modelName(DeviceConfig.MODEL);//绑定model
        config.miotToken(info.miotToken);
        config.miotInfo(info.miotInfo);

        /**
         * 2. Create Device
         */
        mDevice = new ViomiOven(config);

        /**
         * 3. set Action Handler, setter & getter for property
         */
        OvenService.ActionHandler actionHandler = new OvenService.ActionHandler() {
            @Override
            public void onsetbespeakdish(String bespeakdish) {
                LogUtils.d(TAG, "onsetbespeakdish:" + bespeakdish);
            }

            @Override
            public void onsetstatus(String status) {
                LogUtils.i(TAG, "length is:" + status.length() + "    status:" + status);
                //
                if (HawkParams.getWorkStatus() == WorkStatus.STATUS_IDLE.value
                        || HawkParams.getWorkStatus() == WorkStatus.STATUS_FINISH.value) {
                    if (status != null && status.length() > 0) {
                        AppSetStatus appSetStatus = GsonTools.getClassItem(status, AppSetStatus.class);
                        //LogUtils.d(TAG,"AppSetStatus json is:"+GsonTools.objectToJson(appSetStatus));
                        HawkParams.setFoodId(appSetStatus.getDish().getId());
                        if (appSetStatus.getStatus() == WorkStatus.STATUS_YUYUEING.value && appSetStatus.getPrepareTime() > 0) {//预约烹饪
                            HawkParams.setNutritionInfos(new ArrayList<BasicKeyInfo>());
                            Intent intent = new Intent();
                            intent.setClass(ViomiApplication.getContext(), AppointPreviewActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            //数据
                            intent.putExtra("isAppoint", true);
                            HawkParams.setFoodName(appSetStatus.getDish().getName());
                            HawkParams.setAppointTime(appSetStatus.getPrepareTime());//
                            HawkParams.setCookStep(appSetStatus.getDish().getModes());
                            HawkParams.setCookFrom(appSetStatus.getFrom());
                            //
                            ViomiApplication.getContext().startActivity(intent);
                        } else if (appSetStatus.getStatus() == WorkStatus.STATUS_WORKING.value) {///立即烹饪
                            HawkParams.setNutritionInfos(appSetStatus.getDish().getInfo());
                            HawkParams.setBespeakdish("");
                            if (appSetStatus.getDish().getModes().get(0).getMode() == SteamBakeType.PREHEATING.value) {////第一步为预热
                                List<DeviceRunStep> preHeating = new ArrayList<>();
                                preHeating.add(appSetStatus.getDish().getModes().get(0));
                                appSetStatus.getDish().getModes().remove(0);
                                if (Global.isPrepareOk(preHeating)) {
                                    if (SerialManager.getInstance().sendCmdStart(preHeating)) {
                                        HawkParams.setFoodName(appSetStatus.getDish().getName());
                                        HawkParams.setCookStep(appSetStatus.getDish().getModes());
                                        HawkParams.setCookFrom(appSetStatus.getFrom());
                                        Intent intent = new Intent();
                                        intent.setClass(ViomiApplication.getContext(), PreHeatingActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.putExtra("will2Temp", preHeating.get(0).getTemp());
                                        ViomiApplication.getContext().startActivity(intent);
                                    }
                                }
                            } else {//立即烹饪
                                if (Global.isPrepareOk(appSetStatus.getDish().getModes())) {
                                    if (SerialManager.getInstance().sendCmdStart(appSetStatus.getDish().getModes())) {
                                        HawkParams.setFoodName(appSetStatus.getDish().getName());
                                        HawkParams.setCookFrom(appSetStatus.getFrom());
                                        Intent intent = new Intent();
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        HawkParams.setFoodName(appSetStatus.getDish().getName());
                                        if (appSetStatus.getDish().getModes().get(0).getMode() == SteamBakeType.CHUGOU.value) {
                                            intent.setClass(ViomiApplication.getContext(), ChugouActivity.class);
                                            intent.putExtra("step", 1);
                                            ViomiApplication.getContext().startActivity(intent);
                                        } else {
                                            intent.setClass(ViomiApplication.getContext(), CookRunningActivity.class);
                                            ViomiApplication.getContext().startActivity(intent);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onsetc_time(int c_time) {
                LogUtils.i(TAG, "onsetc_time:" + c_time);
            }

            @Override
            public void onsetdescaling(String descaling) {
                LogUtils.i(TAG, "onsetdescaling:" + descaling);
            }

            @Override
            public void onsetdishsVer(int dishsVer) {////菜谱信息通过云端同步，手机、设备通过这个字段判断是否向云端更新
                LogUtils.i(TAG, "onsetdishsVer:" + dishsVer);
                HawkParams.setOnLineMySetDishVer(dishsVer);
            }

            @Override
            public void onsetlight(int light) {
                LogUtils.i(TAG, "onsetlight:" + light);
                SerialManager.getInstance().sendCmdLightSwitch(light == 1);
            }

            @Override
            public void onsetdoor(int door) {
                LogUtils.i(TAG, "onsetdoor:" + door);
            }

            @Override
            public void onsetc_dishdata(String c_dishdata) {
                LogUtils.i(TAG, "onsetc_dishdata:" + c_dishdata);
            }

            @Override
            public void onsetpauseStatus(int pauseStatus) {
                LogUtils.i(TAG, "onsetpauseStatus:" + pauseStatus);
                BusProvider.getInstance().postEvent(new Event2PauseStatus(TAG, pauseStatus));
            }

            @Override
            public void onsetc_totletime(int c_totletime) {
                LogUtils.i(TAG, "onsetc_totletime:" + c_totletime);
            }

            @Override
            public void onsetc_dishname(String c_dishname) {
                LogUtils.i(TAG, "length is:" + c_dishname.length() + "onsetc_dishname:" + c_dishname);
            }

            @Override
            public void onsetdishinfo(String dishinfo) {
                LogUtils.i(TAG, "onsetdishinfo:" + dishinfo);
            }

            @Override
            public void onsetfrom(int from) {
                LogUtils.i(TAG, "onsets_temp:" + from);
            }

            @Override
            public void onseterror(int error) {
                LogUtils.i(TAG, "onseterror:" + error);
            }

            @Override
            public void onsets_temp(int s_temp) {
                LogUtils.i(TAG, "onsets_temp:" + s_temp);
            }

            @Override
            public void onsetfirstScan(int firstScan) {
                LogUtils.i(TAG, "onsetfirstScan:" + firstScan);
            }

            @Override
            public void onsetc_temp(int c_temp) {
                LogUtils.i(TAG, "onsetc_temp:" + c_temp);
            }

            @Override
            public void onsetc_temps(String c_temps) {
                LogUtils.i(TAG, "onsetc_temps:" + c_temps);
            }

            @Override
            public void onsetwater(int water) {
                LogUtils.i(TAG, "onsetwater:" + water);
            }

            @Override
            public void onsetbespeakStamp(int bespeakStamp) {
                LogUtils.i(TAG, "onsetbespeakStamp:" + bespeakStamp);
            }

            @Override
            public void onsetmode(int mode) {
                LogUtils.i(TAG, "onsetmode:" + mode);
            }
        };
        //
        OvenService.PropertyGetter propertyGetter = new OvenService.PropertyGetter() {


            @Override
            public String getstatus() {
                LogUtils.d(TAG, "getstatus:" + HawkParams.getWorkStatus());
                return HawkParams.getWorkStatus() + "";
            }

            @Override
            public int getmode() {
                LogUtils.d(TAG, "getmode:" + HawkParams.getMode());
                return HawkParams.getMode();
            }

            @Override
            public int getfrom() {
                LogUtils.d(TAG, "getfrom:" + HawkParams.getCookFrom());
                return HawkParams.getCookFrom();
            }

            @Override
            public int getpauseStatus() {
                LogUtils.d(TAG, "getpauseStatus:" + HawkParams.getPauseStatus());
                return HawkParams.getPauseStatus();
            }

            @Override
            public int gets_temp() {//当前工作模式设置的温度
                LogUtils.d(TAG, "gets_temp:" + HawkParams.getS_temp());
                return HawkParams.getS_temp();
            }

            @Override
            public String getc_dishname() {//当前(上次)烹饪菜名
                LogUtils.d(TAG, "getc_dishname:" + HawkParams.getFoodName());
                //return "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789";
                return HawkParams.getFoodName();
            }

            @Override
            public String getc_dishdata() {
                LogUtils.d(TAG, "getc_dishdata:");
                return HawkParams.getCookModesBeanString();
            }

            @Override
            public int getc_temp() {//当前(上次)温度
                LogUtils.d(TAG, "getc_temp:" + HawkParams.getC_temp());
                return HawkParams.getC_temp();
            }

            @Override
            public int getc_totletime() {//当前(上次)总烹饪时间
                LogUtils.d(TAG, "getc_totletime:" + HawkParams.getC_totletime());
                return HawkParams.getC_totletime();
            }

            @Override
            public int getc_time() {//当前(上次)已烹饪时间
                LogUtils.d(TAG, "getc_time:" + HawkParams.getC_time());
                return HawkParams.getC_time();
            }

            @Override
            public String getc_temps() {//实时曲线数据
                LogUtils.d(TAG, "getc_temps:" + HawkParams.getC_temps() + " the size is:" + HawkParams.getC_temps().length());
                return HawkParams.getC_temps();
            }

            @Override
            public String getdishinfo() {
                LogUtils.d(TAG, "getdishinfo:" + HawkParams.getNutritionInfos2String());
                return HawkParams.getNutritionInfos2String();
            }

            @Override
            public String getdescaling() {
                LogUtils.d(TAG, "getdishinfo:" + HawkParams.getDescaling());
                return HawkParams.getDescaling()+"";
            }

            @Override
            public String getbespeakdish() {
                return HawkParams.getBespeakdish();
            }

            @Override
            public int getbespeakStamp() {
                LogUtils.d(TAG, "getbespeakStamp:" + HawkParams.getAppointTime());
                return HawkParams.getAppointTime();
            }

            @Override
            public int getfirstScan() {
                LogUtils.d(TAG, "getfirstScan:" + HawkParams.getFirstScanTime());
                return HawkParams.getFirstScanTime();
            }

            @Override
            public int getdishsVer() {
                LogUtils.d(TAG, "getdishsVer:" + HawkParams.getOffLineMySetDishVer());
                return HawkParams.getOffLineMySetDishVer();
            }

            @Override
            public int geterror() {
                LogUtils.d(TAG, "geterror");
                return SerialManager.getInstance().latestError;
            }

            @Override
            public int getdoor() {
                LogUtils.d(TAG, "getdoor");
                return AppParam.isDoorOpen ? 1 : 0;
            }

            @Override
            public int getlight() {
                LogUtils.d(TAG, " getlight:" + (AppParam.isLightOn ? 1 : 0));
                return AppParam.isLightOn ? 1 : 0;
            }

            @Override
            public int getwater() {
                LogUtils.d(TAG, "getwater");
                return AppParam.isNoWater ? 0 : 1;
            }
        };
        //只作打印，无其他用处
        OvenService.PropertySetter propertySetter = new OvenService.PropertySetter() {

            @Override
            public void setstatus(String value) {
                LogUtils.d(TAG, "setstatus:" + value);

            }

            @Override
            public void setmode(int value) {
                LogUtils.d(TAG, "setmode:" + value);

            }

            @Override
            public void setfrom(int value) {
                LogUtils.d(TAG, "setfrom:" + value);

            }

            @Override
            public void setpauseStatus(int value) {
                LogUtils.d(TAG, "setpauseStatus:" + value);
            }

            @Override
            public void sets_temp(int value) {
                LogUtils.d(TAG, "sets_temp:" + value);
            }

            @Override
            public void setc_dishname(String value) {
                LogUtils.d(TAG, "setc_dishname:" + value);
            }

            @Override
            public void setc_dishdata(String value) {
                LogUtils.d(TAG, "setc_dishdata:" + value);
            }

            @Override
            public void setc_temp(int value) {
                LogUtils.d(TAG, "setc_temp:" + value);
            }

            @Override
            public void setc_totletime(int value) {
                LogUtils.d(TAG, "setc_totletime:" + value);
            }

            @Override
            public void setc_time(int value) {
                LogUtils.d(TAG, "setc_time:" + value);
            }

            @Override
            public void setc_temps(String value) {
                LogUtils.d(TAG, "setc_temps:" + value);
            }

            @Override
            public void setdishinfo(String value) {
                LogUtils.d(TAG, "setdishinfo:" + value);
            }

            @Override
            public void setdescaling(String value) {
                LogUtils.d(TAG, "setdescaling:" + value);
            }

            @Override
            public void setbespeakdish(String value) {
                LogUtils.d(TAG, "setbespeakdish:" + value);
            }

            @Override
            public void setbespeakStamp(int value) {
                LogUtils.d(TAG, "setbespeakStamp:" + value);
            }

            @Override
            public void setfirstScan(int value) {
                LogUtils.d(TAG, "setfirstScan:" + value);
            }

            @Override
            public void setdishsVer(int value) {
                LogUtils.d(TAG, "setdishsVer:" + value);
            }

            @Override
            public void seterror(int value) {
                LogUtils.d(TAG, "seterror:" + value);
            }

            @Override
            public void setdoor(int value) {
                LogUtils.d(TAG, "setdoor:" + value);
            }

            @Override
            public void setlight(int value) {
                LogUtils.d(TAG, "setlight:" + value);
            }

            @Override
            public void setwater(int value) {
                LogUtils.d(TAG, "setwater:" + value);
            }
        };

        mDevice.OvenService().setHandler(actionHandler, propertyGetter, propertySetter);

        /**
         * 4. Start
         */
        LogUtils.d(TAG, " mDevice start");
        mDevice.start(new CompletedListener() {
            @Override
            public void onSucceed() {
                LogUtils.d(TAG, "mDevice.start onSucceed");
                if (userid != null) {
                    setDeviceBindFlag(true);
                }
                mBindCallback.onSuccess(null);
            }

            @Override
            public void onFailed(MiotError miotError) {
                LogUtils.e(TAG, "mDevice.start onFailed: " + miotError);
                mBindCallback.onFail(-5, "mDevice start onFailed!,msg=" + miotError.getMessage());
            }
        });

        //注册监听设备状态
        bindRegister();
    }

    /***
     * 异常发生事件
     */
    public void sendFaultHappen(int error) {

        String method = "event.fault_happen";
        String params = "[" + error + "]";
        LogUtils.e(TAG, "method=" + method + ",params=" + params);
        if (mDevice != null) {
            try {
                mDevice.send(method, params);
            } catch (MiotException e) {
                e.printStackTrace();
            }
        }
    }

    /***
     * 异常恢复事件
     */
    public void sendFaultFix(int error) {
        String method = "event.fault_fixed";
        String params = "[" + error + "]";
        LogUtils.i(TAG, "method=" + method + ",params=" + params);
        if (mDevice != null) {
            try {
                mDevice.send(method, params);
            } catch (MiotException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param startCookTime 开始烹饪时间戳ms
     * @param dishName      菜名
     * @param useTime       本次烹饪时长
     * @param dishInfo      烹饪菜谱的营养信息
     * @param tempInfo      烹饪过程的40个温度点
     */
    public void sendOnceRecord(long startCookTime, String dishName, int useTime, String dishInfo, String tempInfo, String weight) {
        String method = "event.cook_record";
        String params = "[" + startCookTime + "," + dishName + "," + useTime + "," + dishInfo + "," + tempInfo + "," + weight + "]";
        LogUtils.i(TAG, "method=" + method + ",params=" + params);
        if (mDevice != null) {
            try {
                mDevice.send(method, params);
            } catch (MiotException e) {
                e.printStackTrace();
            }
        }
    }


    private void send() throws MiotException {
        String method = "event.fault_happen";
        String params = "[1]";
        mDevice.send(method, params);
    }

    /***
     *设定设备信息
     * @return
     */
    private String getMiotInfo(String userId) {
        WifiManager wifiMng = (WifiManager) ViomiApplication.getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wi = wifiMng.getConnectionInfo();
        DhcpInfo di = wifiMng.getDhcpInfo();
        JSONObject jo = new JSONObject();

        try {
            jo.put("method", "_internal.info");
            jo.put("partner_id", "");
            JSONObject jop = new JSONObject();
            jop.put("hw_ver", "Android");
            jop.put("fw_ver", ApkUtil.getVersion());
            JSONObject jopa = new JSONObject();
            jopa.put("ssid", wi.getSSID());
            jopa.put("bssid", wi.getBSSID());
            jop.put("ap", jopa);
            JSONObject jopn = new JSONObject();
            jopn.put("localIp", IpUtils.intToIp(di.ipAddress));
            jopn.put("mask", IpUtils.intToIp(di.netmask));
            jopn.put("gw", IpUtils.intToIp(di.gateway));
            jop.put("netif", jopn);
            if (userId != null) {
                jop.put("uid", userId);
            }
            jo.put("params", jop);
            return jo.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }


}
