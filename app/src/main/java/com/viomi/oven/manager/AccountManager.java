package com.viomi.oven.manager;

import android.content.Context;

import com.viomi.oven.ViomiApplication;
import com.viomi.oven.bean.ViomiUser;
import com.viomi.oven.util.FileUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by young2 on 2016/9/7.
 */
public class AccountManager {
    public final static String ViomiUserFile = "ViomiUser.dat";

    public static final int BIND_NOT_YET = 0;//未绑定
    public static final int BIND_BY_VIOMI = 1;//由云米app绑定
    public static final int BIND_BY_MIJIA = 2;//由米家app绑定


    /***
     * 账号绑定的类型
     * @return
     */
    public static int getBindStatus() {
        if (DeviceManager.getInstance().isDeviceBind()) {
            ViomiUser viomiUser = AccountManager.getViomiUser(ViomiApplication.getContext());
            if (viomiUser != null) {
                return BIND_BY_VIOMI;
            } else {
                return BIND_BY_MIJIA;
            }
        } else {
            return BIND_NOT_YET;
        }
    }


    public static void saveViomiUser(Context context, ViomiUser user) {
        FileUtil.saveObject(context, ViomiUserFile, user);
    }

    /***
     * 获取云米app授权账号信息
     * @param context
     * @return
     */
    public static ViomiUser getViomiUser(Context context) {
        ViomiUser user;

        try {
            user = (ViomiUser) FileUtil.getObject(context, ViomiUserFile);
            if (user != null) {
                user.setToken(formatNull(user.getToken()));
                user.setMiId(formatNull(user.getMiId()));
                user.setHeadImg(formatNull(user.getHeadImg()));
                user.setAccount(formatNull(user.getAccount()));
                user.setUserCode(formatNull(user.getUserCode()));
                user.setMobile(formatNull(user.getMobile()));
                user.setNickname(formatNull(user.getNickname()));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return user;
    }

    public static void deleteViomiUser(Context context) {
        FileUtil.saveObject(context, ViomiUserFile, null);
    }

    public static ViomiUser parserViomiUser(String json) {

        try {
            ViomiUser viomiUser = new ViomiUser();
            JSONObject jsonObject = new JSONObject(json);
            JSONObject xiaomiJson = jsonObject.getJSONObject("xiaomi");
            viomiUser.setMiId(xiaomiJson.getString("miId"));
            viomiUser.setType(xiaomiJson.getString("type"));
            viomiUser.setNickname(jsonObject.optString("nikeName"));
            viomiUser.setHeadImg(jsonObject.optString("headImg"));
            viomiUser.setAccount(jsonObject.getString("account"));
            viomiUser.setMobile(jsonObject.optString("mobile"));
            viomiUser.setCid(jsonObject.optInt("cid"));
            viomiUser.setGender(jsonObject.optInt("gender"));
            return viomiUser;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String formatNull(String input) {
        if (input == null || input.equals("null")) {
            return null;
        }
        return input;
    }
}
