package com.viomi.oven.manager;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.bean.AppUpgradeMsg;
import com.viomi.oven.bean.McuUpgradeProgress;
import com.viomi.oven.bean.UpgradeProgress;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.enumType.DownFileType;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.interfaces.ProgressCallback;
import com.viomi.oven.otto.BusProvider;
import com.viomi.oven.otto.EventMcuInstall;
import com.viomi.oven.otto.EventCheckUpgradeFinish;
import com.viomi.oven.util.ApkUtil;
import com.viomi.oven.util.FileUtil;
import com.viomi.oven.util.GsonTools;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.NetworkUtils;
import com.viomi.oven.util.ToastUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import okhttp3.Call;

import static com.viomi.oven.enumType.DownFileType.APP;
import static com.viomi.oven.enumType.DownFileType.MCU;

/**
 * Created by Ljh on 2017/12/29.
 */

public class UpgradeManager {
    private final static String TAG = "UpgradeManager";
    private static UpgradeManager INSTANCE = null;
    public final static int OPERATE_MANUAL = 0;//人工检测升级，要UI界面
    public final static int OPERATE_AUTO = 1;//后台自动检测升级，不需要UI
    private Context mContext;

    //private UpgradeProgress mAppUpgradeProgress;
    private McuUpgradeProgress mMcuUpgradeProgress;

    public static UpgradeManager getInstance() {
        synchronized (UpgradeManager.class) {
            if (INSTANCE == null) {
                synchronized (UpgradeManager.class) {
                    if (INSTANCE == null) {
                        INSTANCE = new UpgradeManager();
                    }
                }
            }
        }
        return INSTANCE;
    }

    public UpgradeManager() {
        //mAppUpgradeProgress = new UpgradeProgress();
        mMcuUpgradeProgress = new McuUpgradeProgress();
    }

    public void init(Context context) {
        mContext = context;
    }

    /***
     * 获取app升级状态
     * @return
     */
    //public UpgradeProgress getAppUpgradeProgress() {
    //    return mAppUpgradeProgress;
    //}

    /***
     * 获取Mcu升级状态
     * @return
     */
    public UpgradeProgress getMcuUpgradeProgress() {
        return mMcuUpgradeProgress;
    }

    public void httpCheckAppVersion(final boolean isApp, final int operatType) {//com.viomi.oven viomi.oven.v1  viomi.oven.v2  + ".debug"
        String pkg = isApp ? ApkUtil.getPackageName() : (DeviceConfig.MODEL + ".mcu");
        if (AppStaticConfig.TYPE_DEBUG)
            pkg += ".debug";
        OkHttpUtils
                .get()
                .url(AppStaticConfig.URL_CHECK_APP_VER + pkg)
                .build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                if (!NetworkUtils.isAvailable() && operatType == OPERATE_MANUAL) {
                    ToastUtil.show("网络异常");
                    LogUtils.d("HttpReceiver", "errorCode -9996 网络错误");
                }
                BusProvider.getInstance().postEvent(new EventCheckUpgradeFinish(TAG, false));
            }

            @Override
            public void onResponse(String response, int id) {
                try {
                    List<AppUpgradeMsg> appUpgradeMsgs = GsonTools.getClassItemList(new JSONObject(response).getString("data"), AppUpgradeMsg.class);
                    if (isApp && appUpgradeMsgs.size() > 0) {//服务器App版本
                        HawkParams.setOnlineAppVer(appUpgradeMsgs.get(0));
                        if (appUpgradeMsgs.get(0).getCode() > ApkUtil.getVersionCode() && operatType == OPERATE_AUTO) {//比当前app版本高，自动进行更新
                            downLoadApp(APP, null);
                        }
                    } else if (!isApp && appUpgradeMsgs.size() > 0) {//服务器Mcu版本
                        HawkParams.setOnlineMcuVer(appUpgradeMsgs.get(0));
                        if (appUpgradeMsgs.get(0).getCode() > HawkParams.getMcuVer() && operatType == OPERATE_AUTO) {//比当前MCU版本高，自动进行更新
                            downLoadApp(MCU, null);
                        }
                    }
                    BusProvider.getInstance().postEvent(new EventCheckUpgradeFinish(TAG, true));
                } catch (Exception e) {
                    BusProvider.getInstance().postEvent(new EventCheckUpgradeFinish(TAG, false));
                    e.printStackTrace();
                }
            }
        });
    }

    public void downLoadApp(final DownFileType type, final ProgressCallback callback) {
        AppUpgradeMsg msg;
        String filePath = "";
        if (type == APP) {
            msg = HawkParams.getOnlineAppVer();
            filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + String.format("/app%d.apk", msg.getCode());
            LogUtils.d(TAG, "APP filePath = " + filePath);
            if (FileUtil.isExists(filePath)) {//直接安装
                if (callback != null)
                    callback.onSuccess("");
                installApk(filePath);
                return;
            }
        } else {
            msg = HawkParams.getOnlineMcuVer();
            filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + String.format("/mcu%d.bin", msg.getCode());
            LogUtils.d(TAG, "MCU filePath = " + filePath);
            if (FileUtil.isExists(filePath)) {//直接安装
                if (callback != null)
                    callback.onSuccess("");
                mcuDownloadProgress(filePath);
                return;
            }
        }
        final String tempName = ((type == APP) ? String.format("/app%d", msg.getCode()) : String.format("/mcu%d", msg.getCode())) + ".tmp";
        LogUtils.d(TAG, "tempName = " + tempName);
        OkHttpUtils//
                .get()//
                .url(msg.getUrl())//
                .tag(this)
                .build()//
                .execute(new FileCallBack(Environment.getExternalStorageDirectory().getAbsolutePath(), tempName)//
                {
                    @Override
                    public void inProgress(float progress, long total, int id) {
                        LogUtils.d(TAG, " the progress is:" + progress + " the total is:" + total + " the id is:" + id);
                        if (callback != null)
                            callback.onProgress((int) (progress * 100));
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        LogUtils.d("onError :" + e.getMessage());
                        HawkParams.setWorkStatus(WorkStatus.STATUS_IDLE);
                        if (callback != null)
                            callback.onFail(0, null);
                    }

                    @Override
                    public void onResponse(File file, int id) {
                        LogUtils.d(TAG, "onResponse :" + file.getAbsolutePath());
                        if (callback != null)
                            callback.onSuccess("");
                        if (type == APP) {
                            file.renameTo(new File(Environment.getExternalStorageDirectory().getAbsolutePath(), tempName.replace(".tmp", ".apk")));
                            installApk(Environment.getExternalStorageDirectory().getAbsolutePath() + tempName.replace(".tmp", ".apk"));
                        } else if (type == MCU) {
                            file.renameTo(new File(Environment.getExternalStorageDirectory().getAbsolutePath(), tempName.replace(".tmp", ".bin")));
                            mcuDownloadProgress(Environment.getExternalStorageDirectory().getAbsolutePath() + tempName.replace(".tmp", ".bin"));
                        }
                        LogUtils.d(TAG, "after :" + file.getAbsolutePath() + " file size is:" + file.length());
                    }
                });
    }

    private void installApk(final String path) {
        LogUtils.d(TAG, "APK installApk path is:" + path);
        ToastUtil.show("升级中");
        HawkParams.setWorkStatus(WorkStatus.STATUS_IDLE);
        //静默安装
        Intent mIntent = new Intent();
        mIntent.setAction("android.intent.action.SILENCE_INSTALL");
        mIntent.putExtra("apkPath", path);
        mContext.sendBroadcast(mIntent);
    }

    /***
     * mcu安装升级处理
     * @param path 安装路径
     */
    private void mcuDownloadProgress(final String path) {
        LogUtils.d(TAG, "MCU mcuDownloadProgress the path is:" + path);
        //开始升级
        new Thread(new Runnable() {
            @Override
            public void run() {
                mMcuUpgradeProgress.progress = 0;
                SerialManager.getInstance().upgradeFirmware(path, new ProgressCallback() {
                    @Override
                    public void onSuccess(Object data) {
                        BusProvider.getInstance().postEvent(new EventMcuInstall(TAG, 1));
                    }

                    @Override
                    public void onFail(int errorCode, String msg) {
                        BusProvider.getInstance().postEvent(new EventMcuInstall(TAG, 2));
                    }

                    @Override
                    public void onProgress(int progress) {
                        LogUtils.d(TAG, "the mcu up progress is:" + progress);
                        mMcuUpgradeProgress.progress = progress;
                        BusProvider.getInstance().postEvent(new EventMcuInstall(TAG, 0, progress));
                    }
                });
            }
        }).start();
    }
}
