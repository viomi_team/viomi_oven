package com.viomi.oven.manager;

import android.os.SystemClock;

import com.mediatek.factorymode.serial.jniSERIAL;
import com.viomi.oven.AppParam;
import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.HawkParams;
import com.viomi.oven.bean.CurveRunStep;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.device.DeviceParamsGet;
import com.viomi.oven.device.DeviceParamsSet;
import com.viomi.oven.device.DeviceRunStep;
import com.viomi.oven.device.SerialParser;
import com.viomi.oven.enumType.McuStatus;
import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.enumType.WorkStatus;
import com.viomi.oven.interfaces.ProgressCallback;
import com.viomi.oven.otto.BusProvider;
import com.viomi.oven.otto.EventUpgradeCurve;
import com.viomi.oven.util.DataProcessUtil;
import com.viomi.oven.util.FileUtil;
import com.viomi.oven.util.GsonTools;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.ToastUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.viomi.oven.device.SerialInfo.*;
import static com.viomi.oven.device.SerialInfo.ErrCode.ER_CONNECT;
import static com.viomi.oven.device.SerialInfo.ErrCode.ER_DOOR;
import static com.viomi.oven.device.SerialInfo.ErrCode.ER_LACK_WATER;

/**
 * 串口通讯管理
 * Created by Ljh on 2018/1/05.
 */

public class SerialManager {
    private static final String TAG = SerialManager.class.getSimpleName();
    //
    private jniSERIAL mSerialPort;
    private DeviceParamsGet mDeviceParamsGet = new DeviceParamsGet();//接收数据
    private DeviceParamsSet mDeviceParamsSet = new DeviceParamsSet();//发送数据
    private int[] mDataReceive;//上一次接受数据
    private long mTimeReceive;//上一次接收数据时间戳
    private WriteThread mWriteThread = null;//发Check指令

    private int mCommunicateCount = 0;//通讯计时
    private boolean mCommunicateErrorFlag = false;//通讯异常是否已发生，true为发生通讯异常，false为通讯正常
    private boolean needUpdateGraph = false;//发送工作进度
    public static int latestError = 0;//异常判断位
    private List<String> latestErrList = new ArrayList<>();
    public static String latestErrInfo = "";
    private int[] checkData = new int[]{0xff, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0xAA};//
    //--------------------------------------小米MIIO指令，bootLoader升级用------------------------------------//////////
    private int[] GET_DOWN = new int[]{0x67, 0x65, 0x74, 0x5F, 0x64, 0x6F, 0x77, 0x6E, 0x0D};
    private int[] DOWN_UPDATE_FW = new int[]{0x64, 0x6F, 0x77, 0x6E, 0x20, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x5F, 0x66, 0x77, 0x0D};
    private int[] READY = new int[]{0x72, 0x65, 0x73, 0x75, 0x6C, 0x74, 0x20, 0x22, 0x72, 0x65, 0x61, 0x64, 0x79, 0x22, 0x0D};
    private int[] OK = new int[]{0x6F, 0x6B, 0x0D};
    //
    private String STR_GET_DOWN = DataProcessUtil.bytesToHexString(GET_DOWN);
    private String STR_DOWN_UPDATE_FW = DataProcessUtil.bytesToHexString(DOWN_UPDATE_FW);
    private String STR_READY = DataProcessUtil.bytesToHexString(READY);
    private String STR_OK = DataProcessUtil.bytesToHexString(OK);
    //----------------------------------------------------------------------------------------------------//////////
    private static final int READ_DELAY = 100;//100ms内返回数据包
    private static final int WRITE_DELAY = 500;//1s改成0.5s主动发一次
    private static final int COUNT_LIMIT = 3;//发送错误计数
    private static final int CHECK_COUNT_LIMIT = 10;//
    //
    private static SerialManager INSTANCE;

    public static SerialManager getInstance() {
        synchronized (SerialManager.class) {
            if (INSTANCE == null) {
                synchronized (SerialManager.class) {
                    if (INSTANCE == null) {
                        INSTANCE = new SerialManager();
                    }
                }
            }
        }
        return INSTANCE;
    }

    /***
     * 打开串口
     *@param model 型号
     */
    public void open(String model) {
        mSerialPort = new jniSERIAL();
        if (mSerialPort.no_serial_lib) {
            LogUtils.e(TAG, "open no_serial_lib");
            return;
        }

        int baudrate = 115200;
        int result = mSerialPort.init(mSerialPort, baudrate, 8, 'n', 1);

        LogUtils.w(TAG, "open serial ,baudrate=" + baudrate + " result=" + result);
        if (mWriteThread == null) {
            mWriteThread = new WriteThread();
            mWriteThread.start();
        }
    }

    /**
     * 关闭串口
     */
    public void close() {
        if (mWriteThread != null) {
            mWriteThread.interrupt();
            mWriteThread = null;
        }
        if (mSerialPort != null) {
            if (!mSerialPort.no_serial_lib) {
                mSerialPort.exit();
            }
        }
    }

    private String readBootSerial() {
        SystemClock.sleep(READ_DELAY);
        int size = mSerialPort.serial_read(mSerialPort);
        mTimeReceive = System.currentTimeMillis();
        int[] data = mSerialPort.rd_data;//取回的data为固定长度32，填充的无效字节为00
        mDataReceive = mSerialPort.rd_data;
        if (data == null || size <= 0) {
            LogUtils.e(TAG, "readBootSerial size 0");
            return "";
        }
        String receiveStr = DataProcessUtil.bytesToHexString(data, size);
        LogUtils.i(TAG, "readBootSerial length:" + size + " data=" + receiveStr);
        return receiveStr;
    }

    /**
     * 读取串口数据,100ms内返回
     */
    private synchronized void readSerial() {
        SystemClock.sleep(READ_DELAY);
        mCommunicateErrorFlag = true;
        mCommunicateCount++;
        int size = mSerialPort.serial_read(mSerialPort);
        mTimeReceive = System.currentTimeMillis();
        int[] data = mSerialPort.rd_data;//取回的data为固定长度32，填充的无效字节为00
        mDataReceive = mSerialPort.rd_data;
        if (data == null || size <= 0) {
            LogUtils.e(TAG, "readSerial size 0");
            return;
        }
        LogUtils.i(TAG, "readSerial length:" + size + " data=" + DataProcessUtil.bytesToHexString(data, size));
        if (size == 20 && data[1] == 15 && data[2] == 0x00) {//check类指令
            boolean result = SerialParser.parserGet(data, size, mDeviceParamsGet);
            LogUtils.i(TAG, "result=" + result + ",mDataReceiveInfo:" + mDeviceParamsGet.toString());
            if (result) {
                mCommunicateCount = 0;
                mCommunicateErrorFlag = false;//异常解决
                if (AppParam.isConnectErr) {
                    AppParam.isConnectErr = false;
                    DeviceManager.getInstance().sendFaultFix(ER_CONNECT.value);
                }
                //状态更新
                AppParam.isNoWater = mDeviceParamsGet.getIsNoWater();
                AppParam.isLightOn = mDeviceParamsGet.getIsLightOn();
                AppParam.isDoorOpen = mDeviceParamsGet.getIsDoorOpen();
                AppParam.isPannelOn = mDeviceParamsGet.getIsPannelOpen();
                AppParam.mcuStatus = mDeviceParamsGet.getMcuState();
                HawkParams.setMcuVer(mDeviceParamsGet.getMcuVer());
                compareError(mDeviceParamsGet.curErr);
                updateRunningGraph(mDeviceParamsGet);//停止和暂停状态是否更新
            }
        } else if ((size - 5) % 3 == 0 && size >= 8 && (data[1] % 3 == 0) && data[2] == 0x01) {//烹饪启动指令
            boolean result = SerialParser.parserStartCmd(data, mDeviceParamsSet);
            if (result) {
                mCommunicateCount = 0;
                mCommunicateErrorFlag = false;//异常解决
                if (AppParam.isConnectErr) {
                    AppParam.isConnectErr = false;
                    DeviceManager.getInstance().sendFaultFix(ER_CONNECT.value);
                }
            }
        } else {//MCU升级状态类指令
            if (DataProcessUtil.bytesToHexString(data, size).contains(STR_GET_DOWN)) {//一直收到GET_DOWN指令则MCU为升级状态
                LogUtils.w(TAG, "MCU等待升级中……");
                HawkParams.setMcuVer(0);
                AppParam.mcuStatus = McuStatus.MCU_UPGRADE;
            }
        }
    }

    /**
     * @param mDeviceParamsGet
     */
    public void updateRunningGraph(DeviceParamsGet mDeviceParamsGet) {
        if (needUpdateGraph || (HawkParams.getDescaling() > 0 && HawkParams.getDescaling() < 4)) {
            int stage = mDeviceParamsGet.curStage;
            if (stage == AppStaticConfig.COOKING_FINISH_STAGE)
                needUpdateGraph = false;
            //if(HawkParams.getDescaling() > 0 && HawkParams.getDescaling() < 4)//除垢特殊处理
            //    needUpdateGraph = true;
            if (stage != 0 && HawkParams.getCookStep() != null && stage <= HawkParams.getCookStep().size() && HawkParams.getCookStep().size() > 0
                    && !HawkParams.getPreheating()) {
                HawkParams.setS_temp(HawkParams.getCookStep().get(stage - 1).getTemp());
                HawkParams.setMode(HawkParams.getCookStep().get(stage - 1).getMode());
            }
            int timeSec = mDeviceParamsGet.getTimeSec();
            HawkParams.setC_time(timeSec);
            int temp = mDeviceParamsGet.getTemp();
            HawkParams.setC_temp(temp);
            BusProvider.getInstance().postEvent(new EventUpgradeCurve(TAG, new CurveRunStep(stage, timeSec, temp/*, waterOk, connectOk*/)));
        }
    }

    /***
     * 异常发生和修复，上报小米后端
     * @param error
     */
    private void compareError(int error) {
        if (error != latestError) {//异常状态有变动
            latestErrList.clear();
            latestErrInfo = "";
            for (ErrCode e : ErrCode.values()) {
                if ((error & e.value) == e.value) {//存在该异常
                    latestErrList.add(e.errInfo);
                    if ((latestError & e.value) != e.value)//上次未上传该异常
                        DeviceManager.getInstance().sendFaultHappen(e.value);
                } else {//不存在该异常或该异常已修复
                    if ((latestError & e.value) == e.value)//上次有报该异常，本次已修复
                        DeviceManager.getInstance().sendFaultFix(e.value);
                }
            }
            latestError = error;
            if (latestErrList.size() == 1) {
                latestErrInfo = latestErrList.get(0);
                if ((!latestErrInfo.equals(ER_DOOR.errInfo)) && (!latestErrInfo.equals(ER_LACK_WATER.errInfo)))
                    latestErrInfo += "\n若仍然无法解决，请联系售后服务中心，售后电话400-100-2632。";
            } else if (latestErrList.size() > 1) {
                for (int i = 0; i < latestErrList.size(); i++) {
                    latestErrInfo += (i + 1) + latestErrList.get(i);
                    if (i != latestErrList.size() - 1)
                        latestErrInfo += "\n";
                }
                latestErrInfo += "\n若仍然无法解决，请联系售后服务中心，售后电话400-100-2632。";
            }
        }
    }

    /***
     * 固件升级,需在子线程
     * @param file 文件路径
     * @return
     */
    public boolean upgradeFirmware(String file, ProgressCallback callback) {
        if (file == null) {
            LogUtils.e(TAG, "upgradeFirmware fail,file null!");
            callback.onFail(0, "upgradeFirmware fail,file null!");
            return false;
        }
        if (mSerialPort.no_serial_lib) {
            DeviceManager.getInstance().sendFaultHappen(ER_CONNECT.value);
            callback.onFail(0, "upgradeFirmware no_serial_lib");
            return false;
        }
        if (mSerialPort == null) {
            open(DeviceConfig.MODEL);
        }
        if (!sendCmdReset()) ;
        //return false;
        //
        callback.onProgress(0);
        if (mWriteThread != null)
            mWriteThread.pauseThread();
        while (mWriteThread.isSendChecking) {
            LogUtils.e(TAG, "upgradeFirmware mWriteThread.isSendChecking just wait");
        }
        //开始启动升级
        boolean result = upgradeFirmwareStep1();
        if (!result) {
            callback.onFail(0, "upgradeFirmwareStep1 failed");
            LogUtils.e(TAG, "upgradeFirmwareStep1 fail !");
            mWriteThread.resumeThread();
            callback.onFail(0, "upgradeFirmwareStep1 fail !");
            return false;
        }
        callback.onProgress(20);
        //
        result = upgradeFirmwareXmodem(file, callback);
        if (!result) {
            LogUtils.e(TAG, "upgradeFirmwareXmodem fail !");
            callback.onFail(0, "upgradeFirmwareXmodem failed");
            mWriteThread.resumeThread();
            return false;
        }
        callback.onProgress(98);
        //
        result = upgradeFirmwareStep3();
        if (!result) {
            LogUtils.e(TAG, "upgradeFirmwareStep3 fail !");
            callback.onFail(0, "upgradeFirmwareStep3 failed");
            mWriteThread.resumeThread();
            return false;
        } else callback.onSuccess("");
        mWriteThread.resumeThread();
        return true;
    }

    /***
     * 升级步骤1，发送升级指令，开始升级
     * 只发一次
     * @return
     */
    public boolean upgradeFirmwareStep1() {
        //readBootSerial();
        int count = 0;
        while (true) {
            //SystemClock.sleep(WRITE_DELAY);
            if (readBootSerial().contains(STR_GET_DOWN)) {

                //if (count > 1) {
                LogUtils.w(TAG, "SERIAL_JNI contains STR_GET_DOWN");
                break;
                //}
            }
            count++;
            LogUtils.d(TAG, "read STR_GET_DOWN count = " + count);
        }
        count = 0;
        while (true) {//下发down update_fw命令
            if (sendAndReadUpgrade(DOWN_UPDATE_FW).contains(STR_READY)) {
                LogUtils.d(TAG, "SERIAL_JNI contains STR_READY");
                break;
            }
            count++;
            LogUtils.d(TAG, "read STR_READY count = " + count);
        }
        ///*
        mSerialPort.wr_data = OK;//下发OK命令
        LogUtils.i(TAG, "write upgradeFirmwareStep1,data=" + DataProcessUtil.bytesToHexString(OK));
        int leng = mSerialPort.serial_write(mSerialPort, mSerialPort.wr_data.length);
        SystemClock.sleep(WRITE_DELAY);
        if (leng != OK.length) {
            return false;
        } else {
            for (int i = 0; i < 2; i++) {
                readBootSerial();
            }
            return true;
        }
    }

    public boolean upgradeFirmwareXmodem(String file, ProgressCallback callback) {
        if (file == null) {
            return false;
        }
        byte[] fileBytes;
        try {
            fileBytes = FileUtil.readFileByBytes(file);
        } catch (IOException e) {
            e.printStackTrace();
            LogUtils.e(TAG, "readFileByBytes error!msg= " + e.getMessage());
            fileBytes = null;
            return false;
        }
        if (fileBytes == null || fileBytes.length <= 1024) {
            LogUtils.e(TAG, "readFileByBytes file error!");
            fileBytes = null;
            return false;
        }

        int index = 0;
        int perLength = 128;//每次发送字节数
        int count = fileBytes.length / perLength;
        int remain = fileBytes.length % perLength;
        if (remain > 0) {
            count += 1;
        }
        LogUtils.w(TAG, "need sendPage count = " + count + " remain = " + remain);
        byte[] bytes = new byte[3 + perLength + 1];
        int checkSum = 0;
        for (int i = 0; i < count; i++) {
            callback.onProgress(i * 100 / count);
            Arrays.fill(bytes, (byte) 0x1A);
            bytes[0] = XMODEM_SOH;
            bytes[1] = (byte) ((i + 1) % 256);
            bytes[2] = (byte) (~(bytes[1]));
            if ((remain > 0) && (i == count - 1)) {
                System.arraycopy(fileBytes, index, bytes, 3, remain);
                index += remain;
            } else {
                System.arraycopy(fileBytes, index, bytes, 3, perLength);
                index += perLength;
            }
            checkSum = 0;
            for (int j = 0; j < 128; j++) {
                checkSum += bytes[3 + j];
            }
            bytes[3 + perLength] = (byte) (checkSum & 0xff);
            LogUtils.d(TAG, "upgradeFirmwareXmodem page:" + i + " checkSum:" + checkSum + " =" + DataProcessUtil.bytesToHexString(bytes));
            if (!pageSend(bytes)) {
                LogUtils.e(TAG, "upgradeFirmwareXmodem fail!page index=" + i);
                fileBytes = null;
                bytes = null;
                return false;
            }
        }
        LogUtils.w(TAG, "upgradeFirmwareXmodem finsih");
        fileBytes = null;
        bytes = null;
        return true;
    }

    /***
     * bin文件每页发送数据，失败的话重发10次
     * @param bytes
     * @return
     */
    private boolean pageSend(byte[] bytes) {
        boolean result = false;
        for (int i = 0; i < 10; i++) {
            result = perPageSend(bytes);
            if (result) {
                LogUtils.w(TAG, "pageSend success");
                return true;
            }
        }
        return false;
    }

    /***
     * bin文件数据单页发送
     * @param bytes
     * @return
     */
    private boolean perPageSend(byte[] bytes) {
        boolean result = cmdWrite(bytes);
        if (!result) {
            LogUtils.e(TAG, "perPageSend cmdWrite,data fail");
            return false;
        }
        SystemClock.sleep(READ_DELAY);
        int size = mSerialPort.serial_read(mSerialPort);
        mTimeReceive = System.currentTimeMillis();
        int[] data = mSerialPort.rd_data;//取回的data为固定长度32，填充的无效字节为00
        mDataReceive = mSerialPort.rd_data;
        if (data == null || size <= 0) {
            LogUtils.e(TAG, "readSerial size 0");
            return false;
        }
        LogUtils.i(TAG, "perPageSend readSerial back length:" + size + " data=" + DataProcessUtil.bytesToHexString(data, size));
        if ((byte) data[0] == XMODEM_ACK) {
            LogUtils.d(TAG, "perPageSend ACK");
            return true;
        } else if ((byte) data[0] == XMODEM_NAK) {
            LogUtils.e(TAG, "perPageSend NAK");
            return false;
        }
        return false;
    }

    /***
     * 升级步骤3，检测升级成功
     * 只发一次
     * @return
     */
    public boolean upgradeFirmwareStep3() {
        readBootSerial();
        //for (int i = 0; i < 2; i++) {
        sendAndReadUpgrade(XMODEM_EOT_INT);
        LogUtils.e(TAG, "upgradeFirmwareStep3");
        //}
        return true;
    }

    public boolean sendCmdLightSwitch(boolean swithOn) {
        LogUtils.d(TAG, "ceshi 切换灯开关" + swithOn);
        mDeviceParamsSet.cmd = swithOn ? 0x11 : 0x10;
        boolean isOk = sendCmd(mDeviceParamsSet);
        if (!isOk) {
            ToastUtil.show("设置失败");
        }
        return isOk;
    }

    private boolean sendCmdReset() {
        LogUtils.d(TAG, "重启MCU");
        mDeviceParamsSet.cmd = 0x21;
        boolean isOk = sendCmd(mDeviceParamsSet);
        if (!isOk) {
            ToastUtil.show("设置失败");
        }
        return isOk;
    }

    public boolean sendCmdControlPanelSwitch(boolean swithOn) {
        LogUtils.d(TAG, "切换嵌入式蒸烤面板开关" + swithOn);
        //mDeviceParamsSet.cmd = swithOn ? 0x13 : 0x12;//无0x12指令（预留）
        mDeviceParamsSet.cmd = 0x13;
        boolean isOk = sendCmd(mDeviceParamsSet);
        if (!isOk) {
            ToastUtil.show("设置失败");
        }
        return isOk;
    }

    /**
     * 开始蒸烤
     */
    public boolean sendCmdStart(List<DeviceRunStep> mDeviceRunSteps) {
        LogUtils.d(TAG, "开始运行指令 sendCmdStart" + GsonTools.listToJsonString(mDeviceRunSteps) + "    " + mDeviceRunSteps.toString() + " haseCode" + mDeviceRunSteps.hashCode());
        if (mDeviceRunSteps == null)
            return false;
        mDeviceParamsSet.cmd = 0x01;
        mDeviceParamsSet.mDeviceRunSteps = mDeviceRunSteps;
        boolean isOk = sendCmd(mDeviceParamsSet);
        if (isOk) {
            needUpdateGraph = true;
            if (mDeviceRunSteps.size() > 0) {
                HawkParams.setWorkStatus(WorkStatus.STATUS_WORKING);//工作状态
                HawkParams.setMode(mDeviceRunSteps.get(0).getMode());//工作模式
                HawkParams.setPauseStatus(0);//暂停状态为正常
                HawkParams.setS_temp(mDeviceRunSteps.get(0).getTemp());//当前工作模式设置的温度
                if (mDeviceRunSteps.get(0).getMode() != SteamBakeType.PREHEATING.value) {//非预热
                    //LogUtils.d(TAG,"setCookStep = "+GsonTools.listToJsonString(mDeviceRunSteps)+"    "+mDeviceRunSteps.toString()+" haseCode"+mDeviceRunSteps.hashCode());
                    HawkParams.setCookStep(mDeviceRunSteps);//烹饪步骤和总烹饪时长
                    HawkParams.setPreheating(false);
                } else {//预热
                    HawkParams.setPreheating(true);
                }
            }
        } else
            ToastUtil.show("设置失败");
        return isOk;
    }

    /**
     * 结束蒸烤
     */
    public boolean sendCmdOver() {
        LogUtils.d(TAG, "发送结束指令 sendCmdOver");
        mDeviceParamsSet.cmd = 0x03;//关灯，结束
        boolean isOk = sendCmd(mDeviceParamsSet);
        if (isOk) {
            needUpdateGraph = false;
            HawkParams.setPauseStatus(1);
            HawkParams.setWorkStatus(WorkStatus.STATUS_IDLE);
        } else {
            ToastUtil.show("设置失败");
        }
        return isOk;
    }

    /**
     * 暂停蒸烤
     */
    public boolean sendCmdStop() {
        LogUtils.d(TAG, "发送暂停指令 sendCmdStop");
        mDeviceParamsSet.cmd = 0x02;//关灯，暂停
        boolean result = sendCmd(mDeviceParamsSet);
        if (result)
            HawkParams.setPauseStatus(1);
        else
            ToastUtil.show("设置失败");
        return result;
    }

    public boolean sendCmdResumeRun() {
        LogUtils.d(TAG, "发送恢复运行指令 sendCmdResumeRun");
        mDeviceParamsSet.cmd = 0x04;//关灯，恢复运行
        boolean result = sendCmd(mDeviceParamsSet);
        if (result)
            HawkParams.setPauseStatus(0);
        else
            ToastUtil.show("设置失败");
        return result;
    }

    /**
     * 老化测试
     */
    public boolean sendCmdAgingTest() {
        LogUtils.d(TAG, "老化模式");
        mDeviceParamsSet.cmd = 0x20;
        boolean isOk = sendCmd(mDeviceParamsSet);
        if (isOk) {
            needUpdateGraph = true;
            HawkParams.setWorkStatus(WorkStatus.STATUS_WORKING);//工作状态
            HawkParams.setPauseStatus(0);//暂停状态为正常
        } else
            ToastUtil.show("设置失败");
        return isOk;
    }

    /**
     * 老化界面中的排水处理
     *
     * @return
     */
    public boolean sendCmdAgingDrainage() {
        LogUtils.d(TAG, "老化模式排水处理");
        mDeviceParamsSet.cmd = 0x22;
        boolean isOk = sendCmd(mDeviceParamsSet);
        if (!isOk) {
            ToastUtil.show("设置失败");
        }
        return isOk;
    }

    /**
     * @param deviceParamsSet
     * @return true为命令发送并设置成功，false为失败
     */
    public boolean sendCmd(DeviceParamsSet deviceParamsSet) {
        //考虑开子线程？？
        if (mWriteThread != null)
            mWriteThread.pauseThread();//暂停check
        while (mWriteThread.isSendChecking) {
            LogUtils.e(TAG, "sendCmd mWriteThread.isSendChecking just wait");
        }
        int count = COUNT_LIMIT;//命令发送3次失败则报异常
        while (count > 0) {
            if (cmdWrite(deviceParamsSet)) {
                readSerial();
                //判断接收是否正常
                if (!mCommunicateErrorFlag) {//通讯正常
                    break;
                }
            }
            count--;
        }
        //通讯异常
        if (count == 0) {
            DeviceManager.getInstance().sendFaultHappen(ER_CONNECT.value);
        } else {
            mDeviceParamsSet = deviceParamsSet;
        }
        //恢复check循环
        mWriteThread.resumeThread();
        return (count != 0);
    }

    private String sendAndReadUpgrade(int[] data) {
        mSerialPort.wr_data = data;
        LogUtils.i(TAG, "write sendAndReadUpgrade,data=" + DataProcessUtil.bytesToHexString(data));
        int leng = mSerialPort.serial_write(mSerialPort, mSerialPort.wr_data.length);
        if (leng != data.length) {
            return "";
        } else {
            SystemClock.sleep(WRITE_DELAY * 3);
            return readBootSerial();
        }
    }

    private void sendCheck() {
        if (mSerialPort == null) {
            open(DeviceConfig.MODEL);
            if (mSerialPort.no_serial_lib) {
                AppParam.isConnectErr = true;
                DeviceManager.getInstance().sendFaultHappen(ER_CONNECT.value);
                return;
            }
        }
        if (mSerialPort.no_serial_lib) {
            AppParam.isConnectErr = true;
            DeviceManager.getInstance().sendFaultHappen(ER_CONNECT.value);
            return;
        }
        mSerialPort.wr_data = checkData;
        LogUtils.i(TAG, "write sendCheck,data=" + DataProcessUtil.bytesToHexString(checkData));
        int leng = mSerialPort.serial_write(mSerialPort, mSerialPort.wr_data.length);
        if (leng != checkData.length) {
            mCommunicateCount++;
        } else {
            readSerial();
        }
        if (mCommunicateCount >= CHECK_COUNT_LIMIT) {//传输失败则报错
            DeviceManager.getInstance().sendFaultHappen(ER_CONNECT.value);
            AppParam.isConnectErr = true;
        }
    }

    /***
     * 写控制命令
     * @param deviceParamsSet 蒸烤箱发送数据
     */
    public boolean cmdWrite(DeviceParamsSet deviceParamsSet) {
        if (deviceParamsSet == null) {
            LogUtils.e(TAG, "cmdWrite null");
            return false;
        }
        if (mSerialPort.no_serial_lib) {
            return false;
        }
        if (mSerialPort == null) {
            open(DeviceConfig.MODEL);
            if (mSerialPort.no_serial_lib) {
                return false;
            }
        }
        int[] data = SerialParser.parserSend(deviceParamsSet);
        if (data == null || data.length == 0) {
            LogUtils.e(TAG, "cmdWrite params parser fail!");
            return false;
        }
        mSerialPort.wr_data = data;
        LogUtils.w(TAG, "write cmdWrite,data=" + DataProcessUtil.bytesToHexString(data));
        int leng = mSerialPort.serial_write(mSerialPort, mSerialPort.wr_data.length);
        if (leng != data.length) {
            LogUtils.e(TAG, "cmdWrite length fail!leng=" + leng);
            return false;
        }
        return true;
    }

    /***
     * 写字节数据,升级时用
     * @param bytes
     */
    public boolean cmdWrite(byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            LogUtils.e(TAG, "cmdWrite bytes null");
            return false;
        }
        if (bytes.length > mSerialPort.MAX_WRITE_LENGTH) {//溢出
            LogUtils.e(TAG, "cmdWrite bytes overflow！");
            return false;
        }
        if (mSerialPort.no_serial_lib) {
            return false;
        }
        mSerialPort.wr_data = new int[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            mSerialPort.wr_data[i] = bytes[i];
        }
        if (bytes.length < 256) {
            LogUtils.d(TAG, "SERIAL_JNI cmdWrite bytes,data=" + DataProcessUtil.bytesToHexString(bytes));
        }
        int leng = mSerialPort.serial_write(mSerialPort, mSerialPort.wr_data.length);
        if (leng != bytes.length) {
            LogUtils.e(TAG, "cmdWrite bytes length fail!leng=" + leng);
            return false;
        }
        return true;
    }

    public DeviceParamsGet getDataReceiveInfo() {
        return mDeviceParamsGet;
    }

    public void upDateDeviceParams(DeviceParamsSet deviceParamsSet) {
        this.mDeviceParamsSet = deviceParamsSet;
    }

    public DeviceParamsSet getDataSendInfo() {
        return mDeviceParamsSet;
    }

    /***
     * 返回上一次获取的串口数据信息
     * @return
     */
    public String getDataReceiveStr() {
        return mTimeReceive + ":" + DataProcessUtil.bytesToHexString(mDataReceive);
    }

    public boolean isWorkInCheck() {
        return !mWriteThread.pause;
    }

    class WriteThread extends Thread {
        private final Object lock = new Object();
        public boolean pause = false;//true串口为非checking工作模式（升级或发送cmd），false为发送check模式
        public boolean isSendChecking = false;

        /**
         * 调用这个方法实现暂停线程
         */
        void pauseThread() {
            LogUtils.d(TAG, "WriteThread pauseThread");
            pause = true;
        }

        /**
         * 调用这个方法实现恢复线程的运行
         */
        void resumeThread() {
            LogUtils.d(TAG, "WriteThread resumeThread");
            pause = false;
            synchronized (lock) {
                lock.notifyAll();
            }
        }

        /**
         * 注意：这个方法只能在run方法里调用，不然会阻塞主线程，导致页面无响应
         */
        void onPause() {
            synchronized (lock) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public synchronized void run() {
            super.run();
            while (!isInterrupted()) {
                while (pause) {
                    isSendChecking = false;
                    onPause();
                }
                try {
                    SystemClock.sleep(WRITE_DELAY);
                    if (!pause) {
                        isSendChecking = true;
                        sendCheck();
                        isSendChecking = false;
                    } else
                        isSendChecking = false;
                } catch (Exception e) {
                    LogUtils.e(TAG, "serial read fail!msg: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }
}
