package com.viomi.oven.manager;

import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;

import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.Setting;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SpeechUtility;
import com.iflytek.cloud.SynthesizerListener;
import com.viomi.oven.HawkParams;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.StringUtil;

/**
 * Created by young2 on 2017/2/15.
 */

public class VoiceManager {

    private final static String TAG = VoiceManager.class.getSimpleName();


    private Context mContext;
    private static VoiceManager INSTANCE;
    private SpeechSynthesizer mTts;    // 语音合成对象
    public static String voicerPeople = "xiaoyan";    // 默认云端发音人

    AudioManager mAudioManager = null;


    public static VoiceManager getInstance() {
        synchronized (VoiceManager.class) {
            if (INSTANCE == null) {
                synchronized (VoiceManager.class) {
                    if (INSTANCE == null) {
                        INSTANCE = new VoiceManager();
                    }
                }
            }
        }
        return INSTANCE;
    }


    public void init(Context context) {

        mContext = context;
        mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        try {
            SpeechUtility.createUtility(context, "appid=5a80f5f4");
            ttsInit(context);
        } catch (Exception e) {
            Log.e(TAG, "init error");
        }
        Setting.setShowLog(true);
    }

    public void close() {

        if (mTts != null) {
            mTts.stopSpeaking();
            mTts.destroy();
        }
    }

    /***
     * 语音合成初始化
     */
    private void ttsInit(Context context) {
        // 初始化合成对象
        mTts = SpeechSynthesizer.createSynthesizer(context, new InitListener() {
            @Override
            public void onInit(int code) {
                Log.d(TAG, "TtsInitListener init() code = " + code);
                if (code != ErrorCode.SUCCESS) {
                    Log.e(TAG, "TtsInitListener init() fail !");
                }
            }
        });

        // 清空参数
        mTts.setParameter(SpeechConstant.PARAMS, null);
        //设置合成

        //设置使用云端引擎
        mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
        //设置发音人
        mTts.setParameter(SpeechConstant.VOICE_NAME, voicerPeople);

        //设置合成情感
        mTts.setParameter(SpeechConstant.EMOT, "neutral");//neutral, happy, sad, angry
        //设置合成语速
        mTts.setParameter(SpeechConstant.SPEED, "50");
        //设置合成音调
        mTts.setParameter(SpeechConstant.PITCH, "50");
        //设置合成音量
        mTts.setParameter(SpeechConstant.VOLUME, "100");
        //设置播放器音频流类型
        mTts.setParameter(SpeechConstant.STREAM_TYPE, "3");

        // 设置播放合成音频打断音乐播放，默认为true
        mTts.setParameter(SpeechConstant.KEY_REQUEST_FOCUS, "true");

        // 设置音频保存路径，保存音频格式支持pcm、wav，设置路径为sd卡请注意WRITE_EXTERNAL_STORAGE权限
        // 注：AUDIO_FORMAT参数语记需要更新版本才能生效
        mTts.setParameter(SpeechConstant.AUDIO_FORMAT, "wav");

    }

    /***
     * 开始语音合成
     * @param text
     */
    public void startSpeak(String text) {
        LogUtils.d(TAG, "startSpeak:" + text);
        if (HawkParams.getVoiceOpenStatus() && !StringUtil.isEmpty(text)) {
            int code = mTts.startSpeaking(text, mTtsListener);
            if (code != ErrorCode.SUCCESS) {
                Log.e(TAG, "startSpeaking fail !code=" + code);
                //showTip("语音合成失败,错误码: " + code);
            }
        }
    }

    /**
     * 合成回调监听。
     */
    private SynthesizerListener mTtsListener = new SynthesizerListener() {

        @Override
        public void onSpeakBegin() {
        }

        @Override
        public void onBufferProgress(int i, int i1, int i2, String s) {
        }

        @Override
        public void onSpeakPaused() {
        }

        @Override
        public void onSpeakResumed() {
        }

        @Override
        public void onSpeakProgress(int i, int i1, int i2) {
        }

        @Override
        public void onCompleted(SpeechError speechError) {
            LogUtils.d(TAG, "mTtsListener onCompleted");

        }

        @Override
        public void onEvent(int i, int i1, int i2, Bundle bundle) {
        }
    };


    /**
     * 停止语音合成
     *
     * @param enable
     */
    public void stopSpeak(boolean enable) {
        if (mTts != null && mTts.isSpeaking()) {
            mTts.stopSpeaking();
        }
    }


}
