package com.viomi.oven.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.viomi.oven.R;
import com.viomi.oven.bean.CurveRunStep;
import com.viomi.oven.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.MeasureSpec.AT_MOST;

/**
 * Created by Ljh on 2018/1/3
 * 参考 https://github.com/duboAndroid/FinancialCustomerView
 **/
public class CurveRunningView extends View {
    /**
     * 默认参数及常量
     */
    public static final String TAG = CurveRunningView.class.getSimpleName();
    Context mContext;
    //控件默认宽高。当控件的宽高设置为wrap_content时会采用该参数进行默认的设置（单位：sp）
    public static final float DEF_WIDTH = 1280;
    public static final float DEF_HIGHT = 180;
    //可见的显示的条数，屏幕上显示的并不是所有的数据，只是部分数据，这个数据就是“可见的条数”
    int mShownMaxCount = 40;//采样点数
    int lineNum = 12;
    float curveStartX = 100, curveEndX = 1200;//绘图的有效区间
    int curveStartTemp = -30, curveEndTemp = 250;//温度区间
    /**
     * 各种画笔及其参数
     */
    //画笔:正在加载中
    Paint mLoadingPaint;
    final float mLoadingTextSize = 28;
    final String mLoadingText = "数据加载中，请稍后";
    final String refreText = "此曲线为模拟曲线，仅供参考";
    //是否显示loading,在入场的时候，数据还没有加载时进行显示。逻辑判断使用，不可更改
    boolean mDrawLoadingPaint = true;

    //画笔:内部xy轴虚线
    Paint mInnerXyPaint;
    float mInnerXyLineWidth = 3;
    int mInnerXyLineColor;
    //是否是虚线，可更改
    boolean mIsInnerXyLineDashed = true;

    //画笔:折线图
    Paint mBrokenLinePaint;
    float mBrokenLineWidth = 3;
    int mBrokenLineColor;
    //是否是虚线。可更改
    boolean mIsBrokenLineDashed = false;

    //画笔:折线图阴影,折线图阴影的处理方式采用一个画笔两个Path进行处理
    Paint mBrokenLineBgPaint;
    //折线下面的浅蓝色
    int mBrokenLineBgColor;
    //透明度，可更改
    int mAlpha = 40;
    boolean showBgPoint = true;

    //画笔:最后一个小圆点的半径。对于是否显示小圆点，根据PullType.PULL_RIGHT_STOP判断
    Paint mDotPaint;
    float mDotRadius = 15;
    int mDotColor;

    Paint mDotPaint1;//内圆
    float mDotRadius1 = 12;
    int mDotColor1;
    /**
     * 其它属性
     */
    //控件宽高，会在onMeasure中进行测量。
    int mWidth;//屏幕总宽1280dp,13等分（98*13+6或99*13-7）
    int mHeight;
    //上下左右padding，这里不再采用系统属性padding，因为用户容易忘记设置padding,直接在这里更改即可。
    float mPaddingTop = 0;
    float mPaddingBottom = 0;
    //float mPaddingLeft = 0;

    // 注意，遵循取前不取后，因此mEndIndex这个点不应该取到,但是mBeginIndex会取到。
    //数据开始位置，数据集合的起始位置
    int mBeginIndex = 0;
    //数据的结束位置，这里之所以定义结束位置，因为数据可能会小于mShownMaxCount。
    int mEndIndex = 0;
    //数据集合
    List<CurveRunStep> mCurveRunStepList;

    //每一个x、y轴的一个单元的的宽和高
    float mPerX;//每个采样点间X轴的间隔
    float mPerY;

    public CurveRunningView(Context context) {
        this(context, null);
    }

    public CurveRunningView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
        mContext = context;
        initAttrs(attrs);
    }

    public CurveRunningView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initAttrs(attrs);
    }

    public void setCurveStartTemp(int curveStartTemp) {
        this.curveStartTemp = curveStartTemp;
    }

    public void setCurveEndTemp(int curveEndTemp) {
        this.curveEndTemp = curveEndTemp;
    }

    public void setShowMaxCount(int count) {
        this.mShownMaxCount = count;
    }

    public int getShowMaxxCount() {
        return mShownMaxCount;
    }

    public void setShowBgPoint(boolean showBgPoint) {
        this.showBgPoint = showBgPoint;
    }

    protected void initAttrs(AttributeSet attrs) {
        TypedArray typedArray = mContext.obtainStyledAttributes(attrs, R.styleable.CurveView);
        curveStartX = typedArray.getDimensionPixelSize(R.styleable.CurveView_curve_startx, 100);
        curveEndX = typedArray.getDimensionPixelSize(R.styleable.CurveView_curve_endx, 1200);
        curveStartTemp = typedArray.getDimensionPixelSize(R.styleable.CurveView_curve_start_temp, -30);
        curveEndTemp = typedArray.getDimensionPixelSize(R.styleable.CurveView_curve_end_temp, 250);
        //mPaddingLeft = typedArray.getDimensionPixelSize(R.styleable.CurveView_inner_line_pad_left, 0);
        lineNum = typedArray.getInteger(R.styleable.CurveView_inner_line_num, 12);
        typedArray.recycle();

        //加载颜色和字符串资源
        loadDefAttrs();
        //初始化画笔
        initLoadingPaint();
        initInnerXyPaint();
        initBrokenLinePaint();
        initBrokenLineBgPaint();
        initDotPaint();
    }

    protected void loadDefAttrs() {
        //数据源
        mCurveRunStepList = new ArrayList<>(mShownMaxCount);
        //颜色
        mInnerXyLineColor = getColor(R.color.color_timeSharing_innerXyDashColor);
        mBrokenLineColor = getColor(R.color.white);
        mDotColor = getColor(R.color.white);
        mDotColor1 = getColor(R.color.yellow_f8);
        mBrokenLineBgColor = getColor(R.color.color_curve_line_bg);
    }

    protected void initLoadingPaint() {
        mLoadingPaint = new Paint();
        mLoadingPaint.setColor(getColor(R.color.white));
        mLoadingPaint.setTextSize(mLoadingTextSize);
        mLoadingPaint.setAntiAlias(true);
    }

    protected void initInnerXyPaint() {
        mInnerXyPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mInnerXyPaint.setColor(mInnerXyLineColor);
        mInnerXyPaint.setStrokeWidth(mInnerXyLineWidth);
        mInnerXyPaint.setStyle(Paint.Style.STROKE);
        if (mIsInnerXyLineDashed) {
            setLayerType(LAYER_TYPE_SOFTWARE, null);
            mInnerXyPaint.setPathEffect(new DashPathEffect(new float[]{10, 5}, 0));
        }
    }

    protected void initBrokenLinePaint() {
        mBrokenLinePaint = new Paint();
        mBrokenLinePaint.setColor(mBrokenLineColor);
        mBrokenLinePaint.setStrokeWidth(mBrokenLineWidth);
        mBrokenLinePaint.setStyle(Paint.Style.STROKE);
        mBrokenLinePaint.setAntiAlias(true);

        if (mIsBrokenLineDashed) {
            setLayerType(LAYER_TYPE_SOFTWARE, null);
            mBrokenLinePaint.setPathEffect(new DashPathEffect(new float[]{5, 5}, 0));
        }
    }

    protected void initBrokenLineBgPaint() {
        mBrokenLineBgPaint = new Paint();
        mBrokenLineBgPaint.setColor(mBrokenLineBgColor);
        mBrokenLineBgPaint.setStyle(Paint.Style.FILL);
        mBrokenLineBgPaint.setAntiAlias(true);
        mBrokenLineBgPaint.setAlpha(mAlpha);
    }

    protected void initDotPaint() {
        mDotPaint = new Paint();
        mDotPaint.setColor(mDotColor);
        mDotPaint.setStyle(Paint.Style.FILL);
        mDotPaint.setAntiAlias(true);
        mDotPaint.setShadowLayer(22, 0, 0, R.color.yellow_f8);

        mDotPaint1 = new Paint();
        mDotPaint1.setColor(mDotColor1);
        mDotPaint1.setStyle(Paint.Style.FILL);
        mDotPaint1.setAntiAlias(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthSpecMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSpecSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSpecSize = MeasureSpec.getSize(heightMeasureSpec);
        if (widthSpecMode == AT_MOST && heightSpecMode == AT_MOST) {
            setMeasuredDimension((int) DEF_WIDTH, (int) DEF_HIGHT);
        } else if (widthSpecMode == AT_MOST) {
            setMeasuredDimension((int) DEF_WIDTH, heightSpecSize);
        } else if (heightSpecMode == AT_MOST) {
            setMeasuredDimension(widthSpecSize, (int) DEF_HIGHT);
        } else {
            setMeasuredDimension(widthSpecSize, heightSpecSize);
        }
        mWidth = getMeasuredWidth();
        mHeight = getMeasuredHeight();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mCurveRunStepList == null || mCurveRunStepList.isEmpty()) {
            showLoadingPaint(canvas);
            return;
        }
        drawInnerXy(canvas);
        drawBrokenLine(canvas);
        //showReferenceWarn(canvas);
    }

    protected void showLoadingPaint(Canvas canvas) {
        if (!mDrawLoadingPaint) return;
        //这里特别注意，x轴的起始点要减去文字宽度的一半
        canvas.drawText(mLoadingText, mWidth / 2 - mLoadingPaint.measureText(mLoadingText) / 2,
                mHeight / 2, mLoadingPaint);
    }

    protected void showReferenceWarn(Canvas canvas) {
        if (!showBgPoint) {
            //canvas.drawText(refreText, 60, 60, mLoadingPaint);
        }
    }

    /**
     * 屏幕总宽度1280等分成13份
     * 12条曲线以100为间隔
     *
     * @param canvas
     */
    protected void drawInnerXy(Canvas canvas) {
        //绘制y轴虚线
        //double perWidth = (mWidth - mPaddingLeft - mPaddingRight) / 13;
        double perWidth = 100;
        for (int i = 0; i < lineNum; i++) {
            canvas.drawLine((float) (curveStartX + perWidth * i), mPaddingTop + (i % 2 == 0 ? 0 : 74),
                    (float) (curveStartX + perWidth * i), mHeight - mPaddingBottom,
                    mInnerXyPaint);
        }
    }

    protected void drawBrokenLine(Canvas canvas) {
        //先画第一个点
        CurveRunStep firstStep = mCurveRunStepList.get(0);
        Path path = new Path();
        Path path2 = new Path();
        //这里需要说明一下，x轴的起始点，其实需要加上mPerX，但是加上之后不是从起始位置开始，不好看。
        // 同理，for循环内x轴其实需要(i+1)。现在这样处理，最后会留一点空隙，其实挺好看的。
        float floatY = (float) (mHeight - mPaddingBottom - mPerY * (firstStep.temp - curveStartTemp));
        //记录下位置信息
        firstStep.floatX = curveStartX;
        firstStep.floatY = floatY;
        path.moveTo(firstStep.floatX, firstStep.floatY);
        if (showBgPoint)
            path2.moveTo(firstStep.floatX, firstStep.floatY);

        for (int i = mBeginIndex; i <= mEndIndex; i++) {
            if (i == 0)
                continue;
            CurveRunStep step = mCurveRunStepList.get(i);
            //注意这个 mPerX * (i-mBeginIndex)，而不是mPerX * (i)
            float floatX2 = firstStep.floatX + mPerX * (i - mBeginIndex);
            float floatY2 = (float) (mHeight - mPaddingBottom - mPerY * (step.temp - curveStartTemp));
            //记录下位置信息
            step.floatX = floatX2;
            step.floatY = floatY2;
            /////
            //float controlY = mCurveRunStepList.get(i - 1).floatY;
            float controlY = (mCurveRunStepList.get(i - 1).floatY + floatY2) / 2;
            float controlX = (mCurveRunStepList.get(i - 1).floatX + floatX2) / 2;

            //path.cubicTo(controlX, controlY, controlX, floatY2, floatX2, floatY2);
            //if (showBgPoint)
            //    path2.cubicTo(controlX, controlY, controlX, floatY2, floatX2, floatY2);

            path.lineTo(floatX2, floatY2);
            if (showBgPoint)
                path2.lineTo(floatX2, floatY2);

            //折线下方阴影
            if (i == mEndIndex && showBgPoint) {
                //最右端绘制小圆点
                //canvas.drawCircle(floatX2, floatY2, mDotRadius, mDotPaint);
                //canvas.drawCircle(floatX2, floatY2, mDotRadius1, mDotPaint1);
                //在这里把path圈起来，添加阴影。特别注意，这里处理下方阴影和折线边框。采用两个画笔和两个Path处理的，
                // 貌似没有一个Paint可以同时绘制边框和填充色。
                path2.lineTo(floatX2, mHeight - mPaddingBottom);
                path2.lineTo(firstStep.floatX, mHeight - mPaddingBottom);
                path2.close();
            }
        }
        canvas.drawPath(path, mBrokenLinePaint);
        if (showBgPoint)
            canvas.drawPath(path2, mBrokenLineBgPaint);
        //画起始圆点
        canvas.drawCircle(mCurveRunStepList.get(mBeginIndex).floatX, firstStep.floatY, mDotRadius - 5, mDotPaint);
        canvas.drawCircle(mCurveRunStepList.get(mBeginIndex).floatX, firstStep.floatY, mDotRadius1 - 5, mDotPaint1);
        //画结束圆点
        if (mEndIndex > 0) {
            canvas.drawCircle(mCurveRunStepList.get(mEndIndex).floatX, mCurveRunStepList.get(mEndIndex).floatY, mDotRadius, mDotPaint);
            canvas.drawCircle(mCurveRunStepList.get(mEndIndex).floatX, mCurveRunStepList.get(mEndIndex).floatY, mDotRadius1, mDotPaint1);
        }
    }

    protected int getColor(@ColorRes int colorId) {
        return getResources().getColor(colorId);
    }

    /**
     * 数据设置入口
     *
     * @param curveRunStepList
     */
    public void updata(List<CurveRunStep> curveRunStepList) {
        if (curveRunStepList == null || curveRunStepList.isEmpty()) {
            ToastUtil.show("数据异常");
            //Toast.makeText(mContext, "数据异常", Toast.LENGTH_SHORT).show();
            Log.e(TAG, "updata: 数据异常");
            return;
        }
        mCurveRunStepList = curveRunStepList;
        //寻找开始位置和结束位置
        seekBeginAndEndByNewer();

        //寻找边界和计算单元数据大小
        seekAndCalculateCellData();
    }

    /**
     * 起始位置为0
     */
    protected void seekBeginAndEndByNewer() {
        mBeginIndex = 0;
        mEndIndex = ((mCurveRunStepList.size() > mShownMaxCount) ? mShownMaxCount : mCurveRunStepList.size()) - 1;
    }

    /**
     * 寻找边界和计算单元数据大小。寻找:x轴开始位置数据和结束位置的model、y轴的最大数据和最小数据对应的model；
     * 计算x/y轴数据单元大小
     */
    protected void seekAndCalculateCellData() {
        mPerX = (curveEndX - curveStartX) / (mShownMaxCount - 1);//
        mPerY = (float) ((mHeight - mPaddingTop - mPaddingBottom) / (curveEndTemp - curveStartTemp));//

        //刷新界面
        invalidate();
    }
}
