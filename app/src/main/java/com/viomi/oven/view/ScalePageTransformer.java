package com.viomi.oven.view;

import android.os.Build;
import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by HanHailong on 15/9/27.
 */
public class ScalePageTransformer implements ViewPager.PageTransformer {

    //public static final float MAX_SCALE = 1.2f;
    //public static final float MIN_SCALE = 0.9f;

    public static final float MAX_SCALE = 1.0f;
    public static final float MIN_SCALE = 0.70f;

    @Override
    public void transformPage(View page, float position) {

        if (position < -1) {//-4  -3   -2
            position = -1;  //-4  -3   -2  -1 全部定为 -1
        } else if (position > 1) {///2   3   4
            position = 1;   //4    3    2   1 全部定为  1
        }

        float tempScale = position < 0 ? 1 + position : 1 - position;//-1和1时tempScale为0     中间位置tempScale为1

        float slope = (MAX_SCALE - MIN_SCALE) / 1;
        //一个公式
        float scaleValue = MIN_SCALE + tempScale * slope;//中间位置为MAX_SCALE，其他位置为MIN_SCALE
        page.setScaleX(scaleValue);
        page.setScaleY(scaleValue);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            page.getParent().requestLayout();
        }
    }
}
