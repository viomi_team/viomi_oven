package com.viomi.oven.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.viomi.oven.R;
import com.viomi.oven.util.LogUtils;

/**
 * Created by Ljh on 2018/1/5.
 */

public class SeekView extends FrameLayout {
    private static final String TAG = "SeekView";
    private LinearLayout root;
    private TextView tvCurNum;
    private TextView tvNum1;
    private TextView tvNum2;
    private TextView tvNum5;
    private TextView tvNum4;
    private SeekBar seekBar;
    private TextView tvUnit;
    //
    int seekMax = 300;
    int seekMin = 50;
    int seekNum = 120;
    int seekProgressMax = 100;
    String seekUnit = "min";
    OnSeekChange callBack;

    public SeekView(Context context) {
        super(context);
        init(context, null);
    }

    public SeekView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public SeekView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void setOnSeekChangeListener(OnSeekChange callBack) {
        this.callBack = callBack;
    }

    private void init(Context context, AttributeSet attrs) {
        LayoutInflater.from(getContext()).inflate(R.layout.view_seek, this);
        root = (LinearLayout) findViewById(R.id.root);
        tvCurNum = (TextView) findViewById(R.id.tvCurNum);
        tvNum1 = (TextView) findViewById(R.id.tvNum1);
        tvNum2 = (TextView) findViewById(R.id.tvNum2);
        tvNum5 = (TextView) findViewById(R.id.tvNum5);
        tvNum4 = (TextView) findViewById(R.id.tvNum4);
        seekBar = (SeekBar) findViewById(R.id.seekBar);

        tvUnit = (TextView) findViewById(R.id.tvUnit);
        //
        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.SeekView);
            seekMax = ta.getInt(R.styleable.SeekView_seekMax, 300);
            seekMin = ta.getInt(R.styleable.SeekView_seekMin, 50);
            seekNum = ta.getInt(R.styleable.SeekView_seekNum, 120);
            if (seekNum < seekMin)
                seekNum = seekMin;
            seekUnit = ta.getString(R.styleable.SeekView_seekUnit);
            ta.recycle();
        }
        //
        initialize(seekMax, seekMin, seekNum, seekUnit);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                LogUtils.d(TAG, "the onProgressChanged progress is:" + progress);
                setSeekNum(progress + seekMin);
                if (callBack != null)
                    callBack.seekChange(progress + seekMin);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        root.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return seekBar.dispatchTouchEvent(event);
            }
        });
    }

    public void initialize(int seekMax, int seekMin, int seekNum, String seekUnit) {
        this.seekMin = seekMin;
        this.seekMax = seekMax;
        this.seekNum = seekNum < seekMin ? seekMin : seekNum;
        this.seekUnit = seekUnit;
        tvUnit.setText(seekUnit);
        setSeekNum(seekNum);
        seekProgressMax = seekMax - seekMin;
        seekBar.setMax(seekProgressMax);
        seekBar.setProgress(seekNum - seekMin);
    }

    public int getSeekNum() {
        return Integer.parseInt(tvCurNum.getText().toString());
    }

    public void setSeekBar(int seekNum){
        seekBar.setProgress(seekNum - seekMin);
    }

    public void setSeekNum(int seekNum) {
        if (seekNum < seekMin) {
            seekNum = seekMin;
        }
        if (seekNum > seekMax)
            seekNum = seekMax;
        tvCurNum.setText("" + seekNum);
        tvNum2.setText(seekNum == seekMin ? "" : seekNum - 2 + "");
        tvNum1.setText(seekNum == seekMin ? "" : seekNum - 4 + "");
        tvNum4.setText(seekNum == seekMax ? "" : seekNum + 2 + "");
        tvNum5.setText(seekNum == seekMax ? "" : seekNum + 4 + "");
    }

    public interface OnSeekChange {
        void seekChange(int num);
    }
}
