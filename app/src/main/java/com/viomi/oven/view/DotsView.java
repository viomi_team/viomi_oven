package com.viomi.oven.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.viomi.oven.R;

/**
 * Created by Ljh on 18/1/31.
 */
public class DotsView extends LinearLayout {
    private int mCount = 0;
    private float mSize = 20, mSpace = 18;
    private int mBackground = 0;

    public DotsView(Context context) {
        super(context);
    }

    public DotsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public DotsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }


    public void dotSelect(int i) {
        if (0 > i || i >= mCount) {
            return;
        }
        unSelectAll();
        getChildAt(i).setSelected(true);
    }

    public void setCount(int count) {
        if (0 != mCount) {
            removeDot();
        }
        mCount = count <= 0 ? 0 : count;
        initDots();
    }

    private void unSelectAll() {
        for (int i = 0; i < getChildCount(); ++i) {
            getChildAt(i).setSelected(false);
        }
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.DotsView);
        mSize = typedArray.getDimensionPixelSize(R.styleable.DotsView_dot_size, 0);
        mSpace = typedArray.getDimensionPixelSize(R.styleable.DotsView_dot_space, 0);
        mBackground = typedArray.getResourceId(R.styleable.DotsView_dot_background, R.drawable.select_dot);
        typedArray.recycle();
    }

    private void removeDot() {
        removeAllViews();
    }

    private void initDots() {
        setOrientation(HORIZONTAL);
        for (int i = 0; i < mCount; ++i) {
            addView(createDot(i, mCount));
        }
        invalidate();
    }

    private ImageView createDot(int index, int mCount) {
        ImageView imageView = new ImageView(getContext());
        imageView.setImageResource(mBackground);
        if (index != mCount - 1) {
            imageView.setPadding(0, 0, (int) mSpace, 0);
        }
        return imageView;
    }
}
