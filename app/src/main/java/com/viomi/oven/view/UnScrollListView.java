package com.viomi.oven.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListView;

/**
 * Created by Ljh on 2018/3/4.
 */
public class UnScrollListView extends ListView {
    private int mPosition;

    public UnScrollListView(Context context) {
        super(context);
    }

    public UnScrollListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UnScrollListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    //@Override
    //public boolean onTouchEvent(MotionEvent ev) {
    //    if(ev.getAction() == MotionEvent.ACTION_MOVE){
    //        super.onTouchEvent(ev);
    //        return true;
    //    }
    //    return super.onTouchEvent(ev);
    //}
    //
    //@Override
    //public boolean onInterceptTouchEvent(MotionEvent ev) {
    //    if(ev.getAction() == MotionEvent.ACTION_MOVE){
    //        //super.onTouchEvent(ev);
    //        return true;
    //    }
    //    return super.onInterceptTouchEvent(ev);
    //}

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        if(ev.getAction()==MotionEvent.ACTION_MOVE)
            return true;
        return super.dispatchTouchEvent(ev);
    }
    /**
    * 重写该方法，达到使ListView适应ScrollView的效果
    */
    //@Override
    //protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    //    int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,MeasureSpec.AT_MOST);
    //    super.onMeasure(widthMeasureSpec, expandSpec);
    //}
}
