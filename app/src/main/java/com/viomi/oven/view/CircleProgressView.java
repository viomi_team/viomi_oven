package com.viomi.oven.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.viomi.oven.util.LogUtils;

/**
 * Created by Ljh on 2018/4/26
 * 参考https://blog.csdn.net/lintax/article/details/52454171
 */
public class CircleProgressView extends View {
    private static final String TAG = "CircleProgressView";
    Paint mPaint;
    RectF rect;
    int progressValue = 0;
    int[] mColors = new int[]{0xFFFF374F, 0xFFFF6C4C, 0xFFFFBE4E};

    public CircleProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CircleProgressView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void init() {
        mPaint = new Paint();
        rect = new RectF();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        LogUtils.i(TAG, "log: w=" + width + " h=" + height);
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int mWidth = getMeasuredWidth();
        int mHeight = getMeasuredHeight();
        LogUtils.d(TAG, "mWidth = " + mWidth + " mHeight = " + mHeight);
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth((float) mWidth / 10);
        mPaint.setStyle(Paint.Style.STROKE);
        //mPaint.setStrokeCap(Paint.Cap.ROUND);
        //mPaint.setColor(Color.TRANSPARENT);

        rect.set(20, 20, mWidth - 20, mHeight - 20);
        //canvas.drawArc(rect, 0, 360, false, mPaint);
        //mPaint.setColor(Color.BLACK);

        float section = ((float) progressValue) / 100;
        int count = mColors.length;
        int[] colors = new int[count];
        System.arraycopy(mColors, 0, colors, 0, count);

        LinearGradient shader = new LinearGradient(0, 0, mWidth, mHeight, mColors, null, Shader.TileMode.CLAMP);
        mPaint.setShader(shader);

        canvas.drawArc(rect, -90, section * 360, false, mPaint);
    }

    public void setProgressValue(int progressValue) {
        this.progressValue = progressValue;
        Log.i("customView", "log: progressValue=" + progressValue);
        postInvalidate();
    }

    public void setColors(int[] colors) {
        mColors = colors;
        Log.i("customView", "log: progressValue=" + progressValue);
    }
}
