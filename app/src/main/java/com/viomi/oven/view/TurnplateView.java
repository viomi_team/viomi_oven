package com.viomi.oven.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.ViewUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ljh on 2018/3/10.
 */
public class TurnplateView extends View implements OnTouchListener {
    private static final String TAG = "TurnplateView";
    private OnTurnplateListener onTurnplateListener;
    RectF oval = new RectF(-624, 200, 356, 1180);//圆所在矩形坐标，圆心 直径980（-134，690）
    boolean isMoving = false;
    private int Distance = 30;
    /**
     * 文本画笔
     */
    private Paint paintTxt = new Paint();
    /**
     * 画圆的画笔
     */
    private Paint paintCircle = new Paint();
    /**
     * 图标数组
     */
    private Bitmap[] bigIcons;
    /**
     * 文本数组
     */
    private String[] iconNames;
    /**
     * 点对象数组
     */
    private List<Point> points;
    /**
     * 图标总个数
     */
    private int ponit_num;
    /**
     * 圆心坐标
     */
    public int mPointX = 0, mPointY = 0;
    /**
     * 圆半径
     */
    private int mRadius = 0;
    /**
     * 图标间隔的角度
     */
    private int mDegreeDelta;
    /**
     * 换算缓存图标
     */
    private int tempDegree = 0;
    /**
     * 被选中的图标
     */
    private int chooseBtn = 0;
    /**
     * 上下文
     */
    private Context context;
    /**
     * 背景圆的半径
     */
    public int backgroudCircleRadius;

    /**
     * 起始点角度
     */
    public int startPointAngle = 0;

    /**
     * 点击时间down的X坐标
     */
    private int downX, downY;

    public TurnplateView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public TurnplateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        paintTxt.setColor(Color.parseColor("#FFFFFF"));
        paintTxt.setAntiAlias(true);
        paintCircle.setStrokeWidth(3);
        paintCircle.setAntiAlias(true);
        paintCircle.setColor(Color.parseColor("#FF9306"));
        paintCircle.setStyle(Paint.Style.STROKE);
    }

    public void initTurnPlateView(Bitmap[] bigIcons, String[] iconNames, int ponit_num, int divideAngle, OnTurnplateListener listener) {
        this.bigIcons = bigIcons;
        this.iconNames = iconNames;
        this.ponit_num = ponit_num;
        this.mPointX = -134;
        this.mPointY = 690;
        this.backgroudCircleRadius = 370;//(980/2-120)
        this.mRadius = backgroudCircleRadius + bigIcons[0].getHeight() / 2;
        this.mDegreeDelta = divideAngle;
        this.onTurnplateListener = listener;
        initPoints();
        computeCoordinates();
    }

    /**
     * 初始化每个点的坐标
     */
    private void initPoints() {
        points = new ArrayList<>();
        Point point;
        int angle = startPointAngle - mDegreeDelta * 3;
        for (int index = 0; index < ponit_num; index++) {
            point = new Point();
            point.angle = angle;
            if (point.angle > 360) {
                point.angle -= 360;
            } else if (point.angle < 0) {
                point.angle += 360;
            }
            angle += mDegreeDelta;
            point.bitmap = bigIcons[index];
            point.flag = index;
            points.add(point);
        }
    }

    /**
     * 计算每个点的坐标
     */
    private void computeCoordinates() {
        LogUtils.d(TAG, "计算每个点的坐标computeCoordinates");
        Point point;
        for (int index = 0; index < ponit_num; index++) {
            point = points.get(index);//points[index];
            point.x = mPointX + (float) (mRadius * Math.cos(point.angle * Math.PI / 180));
            point.y = mPointY + (float) (mRadius * Math.sin(point.angle * Math.PI / 180));
            //LogUtils.d(TAG, "computeCoordinates index is:" + index + " angle:" + point.angle + " x:" + point.x + " y:" + point.y);
        }
    }

    /**
     * 根据移动坐标计算每个点的角度
     *
     * @param x
     * @param y
     * @return
     */
    private boolean resetPointAngle(float x, float y) {
        int degree = computeMigrationAngle(x, y);
        LogUtils.d(TAG, "根据移动坐标计算每个点的角度resetPointAngle x:" + x + " y:" + y + " degree = " + degree);
        for (int index = 0; index < ponit_num; index++) {
            points.get(index).angle += degree;
            if (points.get(index).angle > 360) {
                points.get(index).angle -= 360;
            } else if (points.get(index).angle < 0) {
                points.get(index).angle += 360;
            }
        }
        //第一个图标滑动到了屏幕可显示的第一个位置下方
        int tempFirstAngle = points.get(0).angle;
        int tempLastAngle = points.get(ponit_num - 1).angle;
        if (getAngleTo0(tempFirstAngle) < mDegreeDelta * 2 - 5) {
            Point tempP = points.get(ponit_num - 1);
            points.remove(ponit_num - 1);
            tempP.angle = tempFirstAngle - mDegreeDelta;
            points.add(0, tempP);
        } else if (getAngleTo0(tempLastAngle) < mDegreeDelta * 2 - 5) {
            Point tempP = points.get(0);
            points.remove(0);
            tempP.angle = tempLastAngle + mDegreeDelta;
            points.add(tempP);
        }


        //int tempAngle = points.get(ponit_num - 1).angle + degree;//图标顺时针排列，最后一图标坐标范围为startAngle~startAngle+mDegreeDelta * (ponit_num - 1)
        //if (tempAngle > 360) {
        //    tempAngle -= 360;
        //} else if (tempAngle < 0) {
        //    tempAngle += 360;
        //}
        //LogUtils.d(TAG, "tempAngle = " + tempAngle);
        //if (tempAngle > mDegreeDelta * (ponit_num - 1)) {//超过限定边界
        //    LogUtils.w(TAG, " tempAngle" + tempAngle);
        //    return false;
        //}
        //for (int index = 0; index < ponit_num; index++) {
        //    points.get(index).angle += degree;
        //    if (points.get(index).angle > 360) {
        //        points.get(index).angle -= 360;
        //    } else if (points.get(index).angle < 0) {
        //        points.get(index).angle += 360;
        //    }
        //}
        return true;
    }

    /**
     * 根据移动的角度计算每个点的角度
     *
     * @param degree 顺时针旋转为正，反之为负
     * @return
     */
    private boolean resetPointAngleDiff(int degree) {
        LogUtils.d(TAG, "根据移动的角度计算每个点的角度resetPointAngleDiff:" + degree);
        //int tempAngle = points.get(ponit_num - 1).angle + degree;
        //if (tempAngle > 360) {
        //    tempAngle -= 360;
        //} else if (tempAngle < 0) {
        //    tempAngle += 360;
        //}
        //LogUtils.d(TAG, "tempAngle = " + tempAngle);
        //if (tempAngle > mDegreeDelta * (ponit_num - 1) + 5) {
        //    LogUtils.w(TAG, " tempAngle" + tempAngle);
        //    return false;
        //}
        for (int index = 0; index < ponit_num; index++) {
            points.get(index).angle += degree;
            if (points.get(index).angle > 360) {
                points.get(index).angle -= 360;
            } else if (points.get(index).angle < 0) {
                points.get(index).angle += 360;
            }
        }
        return true;
    }

    /**
     * 计算每个点的角度
     *
     * @param x
     * @param y
     * @return
     */
    private int computeMigrationAngle(float x, float y) {
        int a = 0;
        float distance = (float) Math.sqrt(((x - mPointX) * (x - mPointX) + (y - mPointY) * (y - mPointY)));
        int degree = (int) (Math.acos((x - mPointX) / distance) * 180 / Math.PI);
        LogUtils.d(TAG, "计算每个点的角度computeMigrationAngle  x:" + x + " y:" + y + " degree:" + degree);
        if (y < mPointY) {
            degree = -degree;
        }
        if (tempDegree != 0) {
            a = degree - tempDegree;//移动的角度
        }
        tempDegree = degree;
        return a;
    }

    /**
     * 判断点击图标
     *
     * @param event
     */
    private void switchScreen(MotionEvent event) {
        for (Point point : points) {
            float distance = (float) Math.sqrt(((event.getX() - point.x) * (event.getX() - point.x) + (event.getY() - point.y) * (event.getY() - point.y)));
            Log.d(TAG, "计算点击的位置与各个元点的距离switchScreen distance is: " + distance);
            if (distance < 160) {
                LogUtils.i(TAG, "onPointTouch :" + point.flag);
                onTurnplateListener.onPointTouch(point.flag);
                break;
            }
        }
    }

    /**
     * 手动设置被选中的图标
     *
     * @param chooseIndex
     */
    public void setChooseBn(int chooseIndex) {
        //chooseBtn = chooseIndex;
        //resetChoiceIndex();
    }

    /**
     * 手势
     */
    ///*
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                isMoving = false;
                downX = (int) event.getX();
                downY = (int) event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                if ((Math.abs(downX - event.getX()) > Distance) || (Math.abs(downY - event.getY()) > Distance)) {
                    isMoving = true;
                    onTurnplateListener.onMove(isMoving);
                }
                if (!resetPointAngle(event.getX(), event.getY())) {
                    LogUtils.w(TAG, "resetPointAngle");
                    break;
                }
                computeCoordinates();
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                int upX = (int) event.getX();
                int upY = (int) event.getY();
                //如果按下和抬起x距离小于10像素，认为是点击，否则是滑动
                if ((Math.abs(downX - upX) <= Distance) && (Math.abs(downY - upY) <= Distance) && !isMoving) {
                    // 点击执行
                    isMoving = false;
                    switchScreen(event);
                    break;
                }
                isMoving = false;
                onTurnplateListener.onMove(isMoving);
                tempDegree = 0;
                //getNearestIndex();
                //int diff = 0 - points.get(chooseBtn).angle;
                int diff = getDiff();
                if (!resetPointAngleDiff(diff)) {
                    break;
                }
                computeCoordinates();
                invalidate();
                break;
            case MotionEvent.ACTION_CANCEL:
                break;
        }
        return true;
    }
//*/

    /**
     * 根据图标属性绘制图标
     */
    @Override
    public void onDraw(Canvas canvas) {
        // 画弧线
        //canvas.drawArc(oval, points.get(0).angle, (ponit_num - 1) * mDegreeDelta, false, paintCircle);
        //画图标和文本
        for (int index = 0; index < ponit_num; index++) {
            LogUtils.d(TAG, "onDraw index = " + index + " angle=" + points.get(index).angle + " x=" + points.get(index).x + " y=" + points.get(index).y);
            drawInCenter(canvas, index, points.get(index).x, points.get(index).y, points.get(index).flag, points.get(index).angle);
        }
    }

    /**
     * 根据图标属性绘制图标和文本
     */
    void drawInCenter(Canvas canvas, int index, float left, float top, int flag, int angle) {
        int dAngle = Math.abs(angle);//离0度角的绝对值，图标大小从startPointAngle依次是240 180 120 60，文本最大40
        if (angle > 180)
            dAngle = 360 - angle;
        int size = (int) (240 - 60.0 * dAngle / mDegreeDelta);
        int textSize = (int) (40 - 8.0 * dAngle / mDegreeDelta);
        if (size > 0) {
            LogUtils.d(TAG, "drawInCenter size is:" + size + " the dAngle is:" + dAngle);
            Bitmap tempBitMap = ViewUtil.ScaledBitmap(points.get(index).bitmap, size, size);
            canvas.drawBitmap(tempBitMap, left - tempBitMap.getWidth() / 2, top - tempBitMap.getHeight() / 2, null);
            if (!isMoving) {
                paintTxt.setTextSize(textSize);
                canvas.drawText(iconNames[flag], left + tempBitMap.getWidth() / 2 + 40, top + textSize / 2, paintTxt);
            }
        }
    }

    /**
     * 图标属性类
     *
     * @author aa
     */
    class Point {
        int flag;
        Bitmap bitmap;
        int angle;
        float x;
        float y;
    }

    public interface OnTurnplateListener {
        public void onPointTouch(int flag);

        public void onMove(boolean move);
    }

    @Override
    public boolean onTouch(View arg0, MotionEvent arg1) {
        return false;
    }

    /**
     * 计算距离0度最近的图标
     */
    private void getNearestIndex() {
        int diff = getAngleTo0(Math.abs(points.get(0).angle));
        chooseBtn = 0;
        for (int i = 1; i < ponit_num; i++) {
            if (diff > getAngleTo0(Math.abs(points.get(i).angle))) {
                diff = getAngleTo0(Math.abs(points.get(i).angle));
                chooseBtn = points.get(i).flag;
            }
        }
        LogUtils.d(TAG, "chooseBtn = " + chooseBtn);
    }

    public int getDiff() {
        int absDiff = getAngleTo0(Math.abs(points.get(0).angle));
        int diff = 0;
        chooseBtn = 0;
        for (int i = 1; i < ponit_num; i++) {
            if (absDiff > getAngleTo0(Math.abs(points.get(i).angle))) {
                absDiff = getAngleTo0(Math.abs(points.get(i).angle));
                chooseBtn = points.get(i).flag;
                diff = 0 - points.get(i).angle;
            }
        }
        LogUtils.d(TAG, "chooseBtn = " + chooseBtn);
        return diff;
    }

    /**
     * 距离startPointAngle的角度，startPointAngle为0
     *
     * @param angle
     * @return
     */
    private int getAngleTo0(int angle) {
        if (angle > 360) {
            angle -= 360;
        } else if (angle < 0) {
            angle += 360;
        }
        if (angle > 180)
            angle = 360 - angle;
        return angle;
    }

    /**
     * 移动图标后复位
     */
    //private void resetChoiceIndex() {
    //    LogUtils.d(TAG, "resetChoiceIndex");
    //    int diff = 0 - points.get(chooseBtn).angle;
    //    if (!resetPointAngleDiff(diff)) {
    //        LogUtils.d(TAG, "resetChoiceIndex 势超过了边界就强制复位");
    //        // 若手势超过了边界就强制复位
    //        int diffReset = 0 - points.get(0).angle;
    //        resetPointAngleDiff(diffReset);
    //    }
    //    computeCoordinates();
    //    invalidate();
    //}
}