package com.viomi.oven.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import com.viomi.oven.util.LogUtils;

/**
 * Created by HanHailong on 15/9/27.
 */
public class ClipViewPager extends ViewPager {
    private static final String TAG = "ClipViewPager";
    //默认距离
    private final static float DISTANCE = 10;
    private float downX;
    private float downY;
    ItemClickCallBack callBack;
    ItemLongClickCallBack mItemLongClickCallBack;

    //是否移动了
    private boolean isMoved;
    private boolean isLongClicked = false, disAbleClick = false;
    //长按的runnable
    private Runnable mLongPressRunnable;
    //

    public ClipViewPager(Context context) {
        super(context);
        initLongPressRunnable();
    }

    public ClipViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLongPressRunnable();
    }

    public void initLongPressRunnable() {
        mLongPressRunnable = new Runnable() {
            @Override
            public void run() {
                isLongClicked = true;
                performLongClick();
            }
        };
    }

    public void setDisAbleOtherClick(boolean disAbleClick) {
        this.disAbleClick = disAbleClick;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        int curX = (int) ev.getX();
        int curY = (int) ev.getY();
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            downX = ev.getX();
            downY = ev.getY();
            isMoved = false;
            isLongClicked = false;
            postDelayed(mLongPressRunnable, ViewConfiguration.getLongPressTimeout());
        } else if (ev.getAction() == MotionEvent.ACTION_UP) {

            float upX = ev.getX();
            float upY = ev.getY();
            //如果 up的位置和down 的位置 距离 > 设置的距离,则事件继续传递,不执行下面的点击切换事件
            if (Math.abs(upX - downX) > DISTANCE || Math.abs(upY - downY) > DISTANCE) {
                LogUtils.d(TAG, " > DISTANCE");
                removeCallbacks(mLongPressRunnable);
                return super.dispatchTouchEvent(ev);
            }
            //点击切换事件
            if (isLongClicked && disAbleClick) {//长按后禁止响应点击事件

            } else {
                View view = viewOfClickOnScreen(ev);
                if (view != null) {
                    int index = (Integer) view.getTag();
                    LogUtils.d(TAG, "viewOfClickOnScreen pos is:" + index);
                    if (callBack != null)
                        callBack.click(index);
                    else {
                        if (getCurrentItem() != index) {
                            setCurrentItem(index);
                        }
                    }
                }
            }
            removeCallbacks(mLongPressRunnable);
        } else if (ev.getAction() == MotionEvent.ACTION_MOVE) {
            if (!isMoved) {
                if (Math.abs(downX - curX) > DISTANCE
                        || Math.abs(downY - curY) > DISTANCE) {
                    //移动超过阈值，则表示移动了
                    isMoved = true;
                    removeCallbacks(mLongPressRunnable);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * @param ev
     * @return
     */
    private View viewOfClickOnScreen(MotionEvent ev) {
        int childCount = getChildCount();
        int currentIndex = getCurrentItem();
        int[] location = new int[2];
        for (int i = 0; i < childCount; i++) {
            View v = getChildAt(i);
            int position = (Integer) v.getTag();
            v.getLocationOnScreen(location);
            int minX = location[0];
            int minY = location[1];

            int maxX = location[0] + v.getWidth();
            int maxY = location[1] + v.getHeight();

            if (position < currentIndex) {
                maxX -= v.getWidth() * (1 - ScalePageTransformer.MIN_SCALE) * 0.5 + v.getWidth() * (Math.abs(1 - ScalePageTransformer.MAX_SCALE)) * 0.5;
                minX -= v.getWidth() * (1 - ScalePageTransformer.MIN_SCALE) * 0.5 + v.getWidth() * (Math.abs(1 - ScalePageTransformer.MAX_SCALE)) * 0.5;
            } else if (position == currentIndex) {
                minX += v.getWidth() * (Math.abs(1 - ScalePageTransformer.MAX_SCALE));
            } else if (position > currentIndex) {
                maxX -= v.getWidth() * (Math.abs(1 - ScalePageTransformer.MAX_SCALE)) * 0.5;
                minX -= v.getWidth() * (Math.abs(1 - ScalePageTransformer.MAX_SCALE)) * 0.5;
            }
            float x = ev.getRawX();
            float y = ev.getRawY();
            LogUtils.d(TAG, " MotionEvent ACTION_UP getRawX is:" + ev.getRawX() + " getRawY is:" + ev.getRawY());
            if (y > 600 || y < 116)//限制横屏MainActivity的有效点击区域
                return null;
            if ((x > minX && x < maxX) && (y > minY && y < maxY)) {
                return v;
            }
        }
        return null;
    }

    public void setOnItemClick(ItemClickCallBack callBack) {
        this.callBack = callBack;
    }

    public interface ItemClickCallBack {
        void click(int pos);
    }

    public void setOnItemLongClick(ItemLongClickCallBack callBack) {
        this.mItemLongClickCallBack = callBack;
    }

    public interface ItemLongClickCallBack {
        void longClick(int pos);
    }
}
