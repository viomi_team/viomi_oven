package com.viomi.oven.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.R;
import com.viomi.oven.device.DeviceConfig;
import com.viomi.oven.util.LogUtils;


/**
 * 锁屏左右滑动控件
 * Created by jiangjieqiang on 2017/1/10.
 */

public class LockView extends RelativeLayout {
    private static final String TAG = "LockView";
    //
    private ImageView homeImageView;         //home
    private int mx, my;
    private int top;
    private int bottom;
    private int left;
    private int right;
    private Context mContext;
    private double moveDistance = 0;

    private float homeDownX, homeDownY;

    public LockView(Context context) {
        super(context);
        mContext = context;
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            LayoutInflater.from(context).inflate(R.layout.layout_lock_view, this);
        else
            LayoutInflater.from(context).inflate(R.layout.layout_lock_view_h, this);
        initViews();
        addListener();
    }

    public LockView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mContext = context;
        if (DeviceConfig.MODEL.equals(AppStaticConfig.VIOMI_OVEN_V1))
            LayoutInflater.from(context).inflate(R.layout.layout_lock_view, this);
        else
            LayoutInflater.from(context).inflate(R.layout.layout_lock_view_h, this);
        initViews();
        addListener();
    }

    private void addListener() {
        homeImageView.setOnTouchListener(new OnTouchListener() {
            //private int height;
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        homeDownX = event.getRawX();
                        homeDownY = event.getRawY();
                        //
                        Log.i(TAG, "event.getRawX()" + event.getRawX() + "event.getRawY()" + event.getRawY());
                        Log.i(TAG, "event.getX()" + event.getX() + "event.getY()" + event.getY());
                        //height = (int) (event.getRawY() - 50);
                        left = v.getLeft();
                        right = v.getRight();
                        top = v.getTop();
                        bottom = v.getBottom();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        mx = (int) (event.getRawX());
                        my = (int) (event.getRawY());
                        LogUtils.d(TAG, "the my = " + my + " the homeDownY = " + homeDownY);
                        LogUtils.d(TAG, "the distance is = " + (moveDistance = Math.sqrt(Math.abs(homeDownX - mx) * Math.abs(homeDownX - mx) + Math.abs(homeDownY - my) * Math.abs(homeDownY - my))));
                        v.layout((int) (mx - homeDownX + left), (int) (my - homeDownY + top), (int) (mx - homeDownX + right), (int) (my - homeDownY + bottom));
                        if (moveDistance > 100 && homeDownY - my > -10) {
                            ((Activity) mContext).finish();
                        }
                        break;

                    case MotionEvent.ACTION_UP:
                        v.layout(left, top, right, bottom);
                        break;
                }
                return true;
            }

        });
    }

    private void initViews() {
        homeImageView = (ImageView) findViewById(R.id.image_lock_view_drag);
    }
}
