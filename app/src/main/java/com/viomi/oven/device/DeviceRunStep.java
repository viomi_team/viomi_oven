package com.viomi.oven.device;

import com.viomi.oven.enumType.SteamBakeType;
import com.viomi.oven.util.LogUtils;

import java.io.Serializable;

/**
 * loadWay
 * 1、台面式：
 * bit0：	预留，固定为0关闭
 * bit1：	预留，固定为0关闭
 * bit2：	预留，固定为0关闭
 * bit3：	预留，固定为0关闭
 * bit4：	后方（后部）加热管，功率为：1300W.    =1开，=0关闭
 * bit5：	热风风机，功率为： 25W。              =1开，=1关闭
 * bit6：	蒸汽发生器（蒸发盘）内管，功率为：800W.   =1开，=0关闭
 * bit7：	预留，固定为0关闭
 * <p>
 * 2、嵌入式：整机最大功率为2800W，负载需要计算功率总和，再确定允许打开哪一路负载。
 * bit0：	上方（顶部）加热管内管，功率为：750W。  =1开，=0关闭
 * bit1：	上方（顶部）加热管外管，功率为：1200W。  =1开，=0关闭
 * bit2：	下方（底部）加热管，功率为：800W。    =1开，=0关闭
 * bit3：	下方（底部）煮水盘，功率为：400W。    =1开，=0关闭
 * bit4：	后方（后部）加热管，功率为：1900W。   =1开，=0关闭
 * bit5：	热风风机，功率为：25W                 =1开，=0关闭
 * bit6：	蒸汽发生器（蒸发盘）内管，功率为：800W。 =1开，=0关闭
 * bit7：	蒸汽发生器（蒸发盘）外管，功率为：1200W。=1开，=0关闭
 */

public class DeviceRunStep implements Serializable {
    private static final String TAG = "DeviceRunStep";
    private static final long serialVersionUID = 2889651631893639303L;
    /////////////////////////////////////////////////////////////////////////////
    private int mode = 0;//0为未定义模式
    //
    public int temp = 0;//设定温度，范围0~255
    public int time = 0;//设定时间，范围0~255,单位分钟
    private int stop = 0;//1暂停，0正常,中间是否暂停提示用户加料之列操作

    private String stoptip = "";//暂停时的语音提示

    public SteamBakeType type = SteamBakeType.NO_MODE;//1为蒸  2为烤  3为蒸烤

    public DeviceRunStep(SteamBakeType type) {
        setType(type);
    }

    public DeviceRunStep(int mode, int time, int temp) {
        setMode(mode);
        setTime(time);
        setTemp(temp);
    }

    public DeviceRunStep(int mode, int time, int temp, int stop, String stoptip) {
        setMode(mode);
        setTime(time);
        setTemp(temp);
        this.stop = stop;
        this.stoptip = stoptip;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        if (time < type.minTime || time > type.maxTime)
            time = type.minTime;
        this.time = time;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        if (temp < type.minTemp || temp > type.maxTemp)
            temp = type.minTemp;
        this.temp = temp;
    }

    public SteamBakeType getType() {
        if (type == null) {
            if (mode == 0)
                type = SteamBakeType.STEAM;
            else {
                for (SteamBakeType temp : SteamBakeType.values()) {
                    if (temp.value == mode) {
                        LogUtils.d(TAG, "setMode:" + mode + " the setType is:" + temp.name);
                        type = temp;
                        //if (this.temp == 0)
                        //    setTemp(type.defTemp);
                        //if (this.time == 0)
                        //    setTime(type.defTime);
                        break;
                    }
                }
            }
        }
        return type;
    }

    public void setType(SteamBakeType type) {
        this.type = type;
        this.mode = type.value;
        setTemp(type.defTemp);
        setTime(type.defTime);
    }

    public int getMode() {
        if (mode == 0)
            mode = type.value;
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
        for (SteamBakeType temp : SteamBakeType.values()) {
            if (temp.value == mode) {
                LogUtils.d(TAG, "setMode:" + mode + " the setType is:" + temp.name);
                this.type = temp;
                setTemp(type.defTemp);
                setTime(type.defTime);
                break;
            }
        }
    }

    public int getStop() {
        return stop;
    }

    public void setStop(int stop) {
        this.stop = stop;
    }

    public String getStoptip() {
        if(stoptip == null)
            stoptip = "";
        return stoptip;
    }

    public void setStoptip(String stoptip) {
        this.stoptip = stoptip;
    }
}
