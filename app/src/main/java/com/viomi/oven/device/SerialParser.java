package com.viomi.oven.device;

import com.viomi.oven.util.LogUtils;

import static com.viomi.oven.device.SerialInfo.DATA_HEADER;
import static com.viomi.oven.device.SerialInfo.DATA_TAIL;

/**
 * Created by Ljh on 2018/1/4.
 */

public class SerialParser {
    private final static String TAG = SerialParser.class.getSimpleName();

    /***
     * 串口常规通讯接收数据（check类指令）
     * @param data 数据
     * @param para
     * @return
     */
    public static boolean parserGet(int[] data, int length, DeviceParamsGet para) {
        byte[] bytes = new byte[length - 3];
        for (int i = 0; i < bytes.length; i++) {//拷贝出数据长度、命令字节、数据字节，用以计算CRC校验
            bytes[i] = (byte) (data[i + 1]);
        }
        byte crc8 = (byte) CRC8.findCrc(bytes);
        LogUtils.d(TAG, " the crc8 is:" + crc8);
        LogUtils.d(TAG, " the data[0] is:" + data[0] + "  " + "the 0xff & DATA_HEADER is:" + (0xff & DATA_HEADER)
                + "\ndata[length - 1] is:" + data[length - 1] + "  " + "the 0xff & DATA_TAIL is:" + (0xff & DATA_TAIL)
                + "\ndata[1] is:" + data[1] + "  " + "length - 5 is:" + (length - 5)
                + "\ndata[length - 2] is:" + data[length - 2] + "  " + "the 0xff & crc8 is:" + (0xff & crc8));
        if (data[0] == (0xff & DATA_HEADER)
                && data[length - 1] == (0xff & DATA_TAIL)
                && data[1] == length - 5
            /*&& data[length - 2] == (0xff & crc8)*/) {//包头和包尾、数据长度、CRC校验位
            para.dataLength = data[1];
            para.curCmd = data[2];
            para.curOvenTempH = data[3];
            para.curOvenTempL = data[4];
            para.curTimeH = data[5];
            para.curTimeL = data[6];
            para.curStage = data[7];
            para.curStatus = data[8];
            para.curErr = data[9] * 256 + data[10];//错误码占用8到11共4byte，实际使用后两位
            para.curBakeTempH = data[11];
            para.curBakeTempL = data[12];
            para.curStempTemp = data[13];
            para.curBoilTemp = data[14];
            para.electricityValue = data[15];
            para.mcuVer = data[length - 3];
            para.crc8 = data[length - 2];
            return true;
        }
        return false;
    }

    /**
     * 校验控制板返回的data和发送的mDeviceParamsSet是否一致
     *
     * @param data
     * @param mDeviceParamsSet
     * @return
     */
    public static boolean parserStartCmd(int[] data, DeviceParamsSet mDeviceParamsSet) {
        //TODO LJH
        return true;
    }

    public static int[] parserSend(DeviceParamsSet para) {
        if (para == null)
            return new int[0];
        int length = 5 + (para.mDeviceRunSteps.size() > 0 ? para.mDeviceRunSteps.size() : 1) * 3;
        int[] data = new int[length];
        data[0] = DATA_HEADER & 0xFF;//包头
        data[1] = (para.mDeviceRunSteps.size() > 0 ? para.mDeviceRunSteps.size() : 1) * 3;//数据长度
        data[2] = para.cmd;//命令字节
        if (para.mDeviceRunSteps.size() > 0) {
            for (int i = 0; i < para.mDeviceRunSteps.size(); i++) {
                data[3 + i * 3] = para.mDeviceRunSteps.get(i).getType().value;
                data[4 + i * 3] = para.mDeviceRunSteps.get(i).getTemp();
                data[5 + i * 3] = para.mDeviceRunSteps.get(i).getTime();
            }
        } else {//空负载情况下，填充一组空数据
            data[3] = data[4] = data[5] = 0;
        }
        byte[] bytes = new byte[length - 3];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) (data[i + 1] & 0xff);
        }
        int crc8 = CRC8.findCrc(bytes);
        data[length - 2] = crc8;//CRC8校验
        data[length - 1] = DATA_TAIL & 0xFF;
        return data;
    }
}
