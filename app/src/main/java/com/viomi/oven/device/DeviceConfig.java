package com.viomi.oven.device;

import com.viomi.oven.AppStaticConfig;
import com.viomi.oven.ViomiApplication;
import com.viomi.oven.util.LogUtils;
import com.viomi.oven.util.PhoneUtil;
import com.viomi.oven.util.StringUtil;
import com.viomi.oven.util.SystemPropertiesProxy;

/**
 * Created by Ljh on 2018/1/11.
 */

public class DeviceConfig {
    private static final String TAG = "DeviceConfig";
    //7C49EB0F4281|85396801|YfSP4rnmihSiIP7y
    //7C49EB0F4280 85396800 snGnEYs2W9GmfzaF
    //34CE00939F6A|58067022|Vv13kaA34bVskC9F
    //did=58067021,mac=34:CE:00:93:9F:69,token=jej2hCNNU0sB0gGA
    //自用
    //public static String MODEL = AppStaticConfig.VIOMI_OVEN_V1;//产品型号
    //public final static String DefaultDeviceId = "85396800";
    //public final static String DefaultMac = "7C:49:EB:0F:42:80";
    //public final static String DefaultMiotToken = "snGnEYs2W9GmfzaF";

    //给测试用
    public static String MODEL = getModel();//产品型号
    //public static String MODEL = AppStaticConfig.VIOMI_OVEN_V1;//产品型号
    public final static String DefaultDeviceId = "85396801";
    public final static String DefaultMac = "7C:49:EB:0F:42:81";
    public final static String DefaultMiotToken = "YfSP4rnmihSiIP7y";

    private static String getModel() {
        if (PhoneUtil.getSystemVersionCodeStr().equals("YUNMI.ULX005_V.PB6803.050_V1.0_DEMO")) {
            LogUtils.d(TAG, "VIOMI_OVEN_V1");
            return AppStaticConfig.VIOMI_OVEN_V1;
        } else if (PhoneUtil.getSystemVersionCodeStr().equals("YUNMI.ULX005_H.PB6803.050_V1.0_DEMO")) {
            LogUtils.d(TAG, "VIOMI_OVEN_V2");
            return AppStaticConfig.VIOMI_OVEN_V2;
        } else {
            String type = SystemPropertiesProxy.get(ViomiApplication.getContext(), "ro.viomi.type");
            if (!StringUtil.isEmpty(type)) {
                if (SystemPropertiesProxy.get(ViomiApplication.getContext(), "ro.viomi.type").equals("viomi_oven_ulx006")) {//竖屏：viomi_oven_ulx006
                    LogUtils.d(TAG, "VIOMI_OVEN_V1");
                    return AppStaticConfig.VIOMI_OVEN_V1;
                } else if (SystemPropertiesProxy.get(ViomiApplication.getContext(), "ro.viomi.type").equals("viomi_oven_ulx005")) {//横屏：viomi_oven_ulx005
                    LogUtils.d(TAG, "VIOMI_OVEN_V2");
                    return AppStaticConfig.VIOMI_OVEN_V2;
                }
            }
        }
        //return AppStaticConfig.VIOMI_OVEN_V1;
        return AppStaticConfig.VIOMI_OVEN_V2;
    }

    //private static String getModel() {
    //    String type = SystemPropertiesProxy.get(ViomiApplication.getContext(), "ro.viomi.type");
    //    if (SystemPropertiesProxy.get(ViomiApplication.getContext(), "ro.viomi.type").equals("viomi_oven_ulx006")) {//竖屏：viomi_oven_ulx006
    //        LogUtils.d(TAG, "VIOMI_OVEN_V1");
    //        return AppStaticConfig.VIOMI_OVEN_V1;
    //    } else if (SystemPropertiesProxy.get(ViomiApplication.getContext(), "ro.viomi.type").equals("viomi_oven_ulx005")) {//横屏：viomi_oven_ulx005
    //        LogUtils.d(TAG, "VIOMI_OVEN_V2");
    //        return AppStaticConfig.VIOMI_OVEN_V2;
    //    }
    //    return AppStaticConfig.VIOMI_OVEN_V1;
    //}
}

