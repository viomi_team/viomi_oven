package com.viomi.oven.device;

import com.viomi.oven.device.SerialInfo.OvenMode;
import com.viomi.oven.enumType.McuStatus;
import com.viomi.oven.util.LogUtils;

/**
 * Created by Ljh on 2017/12/14.
 */
public class DeviceParamsGet {
    public int dataLength = 0;//数据长度
    public int curCmd = 0;//命令字节
    public int curOvenTempH = 0;//腔体温度高位 0~65535度。（放大了10倍的值，显示屏显示时，需要除以10）
    public int curOvenTempL = 0;//腔体温度低位
    public int curTimeH = 0;//当前已运行时间高位,单位为s（0~65535）
    public int curTimeL = 0;//当前已运行时间低位
    public int curStage = 0;//当前阶段，0~5,0为未启动，1~4为蒸烤阶段，11为结束蒸烤动作
    public int curStatus = 0;//状态位,bit0门信号，bit1水箱信号，bit2嵌入式蒸烤箱面板，bit3炉灯
    public int curErr = 0;
    public int curBakeTempH = 0;//底部烤温度高位
    public int curBakeTempL = 0;//底部烤温度低位
    public int curStempTemp = 0;//蒸发盘温度
    public int curBoilTemp = 0;//煮水盘温度
    public int electricityValue = 0;//电流值
    public int mcuVer = 0;//MCU固件版本
    public int crc8 = 0;

    public boolean isLightOn() {
        return true;
    }

    public OvenMode getMode() {
        return OvenMode.MODE_TEMP;
    }

    public int getRunningTime() {
        return curTimeH * 256 + curTimeL;
    }

    public int getOvenTemp() {
        return (curOvenTempH * 256 + curOvenTempL) / 10;
    }

    public int getBakeTemp() {
        return (curBakeTempH * 256 + curBakeTempL);
    }

    public int getSteamTemp() {
        return curStempTemp;
    }

    public int getBoilTemp() {
        return curBoilTemp;
    }

    public int getMcuVer() {
        return mcuVer;
    }

    public double getElectricityValue() {
        return electricityValue * 0.1;
    }

    public boolean getIsDoorOpen() {//=1门关闭，=0门打开
        return (!((curStatus & 0b00000001) == 0b00000001));
    }

    public boolean getIsNoWater() {//=1有水，=0无水
        return (!((curStatus & 0b00000010) == 0b00000010));
    }

    public boolean getIsPannelOpen() {
        return ((curStatus & 0b00000100) == 0b00000100);
    }

    public boolean getIsLightOn() {
        return ((curStatus & 0b00001000) == 0b00001000);
    }

    public McuStatus getMcuState() {
        int status = curStatus >> 6;
        LogUtils.d("DeviceParamsGet", "getMcuState = " + status);
        if (status == 0)
            return McuStatus.MCU_IDLE;
        else if (status == 1)
            return McuStatus.MCU_WORKING;
        else if (status == 2)
            return McuStatus.MCU_SUSPENDED;
        else return McuStatus.MCU_IDLE;
    }

    public int getTimeSec() {
        return curTimeH * 256 + curTimeL;
    }

    public int getTemp() {
        return (curOvenTempH * 256 + curOvenTempL) / 10;
    }

    public String toString() {
        String result = "dataLength=" + dataLength + ",curCmd=" + curCmd + ",--curTimeH=" + curTimeH + ",curTimeL=" + curTimeL + " runTime = " + getRunningTime()
                + "\n,--curOvenTempH=" + curOvenTempH + ",curOvenTempL=" + curOvenTempL + " OvenTemp = " + getOvenTemp()
                + "\n,--curBakeTempH=" + curBakeTempH + ",curBakeTempL=" + curBakeTempL + " BakeTemp = " + getBakeTemp()
                + "\n,--curStempTemp=" + curStempTemp + ",curBoilTemp=" + curBoilTemp + " BoilTemp = " + getBoilTemp()
                + "\n,--curErr=" + curErr + ",curStage=" + curStage + ",curStatus=" + curStatus + ",crc8=" + crc8 + ",mcuVer=" + mcuVer + "mcu status=" +
                (curStatus >> 6);
        return result;
    }
}
