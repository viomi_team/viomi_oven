package com.viomi.oven.device;

/**
 * Created by Ljh on 2017/1/4.
 */

public class SerialInfo {
    //普通数据
    public static final byte DATA_HEADER = (byte) 0xff;//包头
    public static final byte DATA_TAIL = (byte) 0xAA;//包尾
    //
    public static final byte NORMAL_STATUS = 0;
    public static final byte CLOSE_STATUS = 0x01;
    public static final byte UPGRADE_START = (byte) 0xa5;//开始升级
    public static final byte UPGRADE_SEND_SUCCESS = 0x02;//功能位，升级发送数据位成功
    public static final byte UPGRADE_SEND_FAIL = 0x03;//功能位，升级发送数据位失败
    public static final byte UPGRADE_END = (byte) 0xa6;//升级完成
    //xmodem
    public static final byte XMODEM_SOH = (byte) 0x01;
    public static final byte XMODEM_EOT = (byte) 0x04;
    public static final byte XMODEM_ACK = (byte) 0x06;
    public static final byte XMODEM_NAK = (byte) 0x15;
    public static final byte XMODEM_CAN = (byte) 0x18;
    //
    public static final int[] XMODEM_EOT_INT = new int[]{0x04};
    //命令字节
    public static final int LIGHT_ON = (0x01 << 7);
    public static final int INSPECTION = (0x01 << 2);//商检、老化处理

    //命令字节
    public enum OvenMode {//命令运行模式
        MODE_TEMP(0x00),//App文件
        MODE_OVER_ROAST(0x01),//MCU文件
        MODE_STOP_ROAST(0x02),//MCU文件
        MODE_RUN_ROAST(0x03);//其他
        public int value;
        public String valueString;

        OvenMode(int value) {
            this.value = value;
            valueString = Integer.toString(value);
        }
    }

    //error_code
    public enum ErrCode {
        ER_DOOR(0x0001, "门打开"),//门打开异常，指在蒸烤过程中强行打开了门的异常，此位需要上报异常。
        ER_CONNECT(0x0002, ""),//通讯异常
        ER_OVEN_NTC(0x0004, "腔体温度传感器故障；请断电重启产品"),//腔体温度NTC开短路异常
        ER_EVAPORATING_HIGH_TEMP(0x0008, "蒸发盘温度过高，请停止加热；等待一段时间带蒸发盘冷却后再断电重启产品"),//蒸发盘高温
        ER_EVAPORATING_NTC(0x0010, "蒸发盘温度传感器故障；请断电重启产品"),//蒸发盘温度NTC开短路异常
        ER_EVAPORATING_LOW_TEMPER(0x0020, "蒸发盘加热功能异常，请断电重启产品"),//蒸发盘低温
        ER_BOIL_NTC(0x0040, "煮水盘温度传感器故障；请断电重启产品"),//煮水盘温度NTC开短路异常
        ER_BOIL_HEATING(0x0080, "煮水盘加热异常"),//煮水盘加热异常
        ER_BAKE_NTC(0x0100, "底部温度传感器故障；请断电重启产品"),//底部烤温度NTC开短路异常
        ER_BAKE_HEATING(0x0200, ""),//未定义
        ER_LACK_WATER(0x0400, "水箱水量不足，已影响您的使用，请给水箱加水以恢复蒸汽功能(建议使用纯净水)"),//缺水异常
        ER_OVEN_LOW_TEMPER(0x0800, "腔体加热功能异常，请断电重启产品"),//腔体低温异常
        ER_OVEN_HIGH_TEMPER(0x1000, "内腔温度过高，请停止加热；打开炉门待腔体温度降低后再断电重启产品");//腔体高温异常
        public int value;
        public String valueString;
        public String errInfo;

        ErrCode(int value, String errInfo) {
            this.value = value;
            valueString = Integer.toString(value);
            this.errInfo = errInfo;
        }
    }
}
