package com.viomi.oven.device;

public class MiotDeviceInfo {

    public String deviceId;
    public String macAddress;
    public String miotToken;
    public String miotInfo;
}
