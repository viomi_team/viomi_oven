package com.viomi.oven.device;

import com.viomi.oven.device.SerialInfo.OvenMode;

import java.util.ArrayList;
import java.util.List;

import static com.viomi.oven.device.SerialInfo.INSPECTION;
import static com.viomi.oven.device.SerialInfo.LIGHT_ON;
import static com.viomi.oven.device.SerialInfo.OvenMode.*;

/**
 * Created by Ljh on 2017/12/14.
 */

public class DeviceParamsSet {
    public int cmd = 0x64;
    public List<DeviceRunStep> mDeviceRunSteps = new ArrayList<>();

    public void setLightOn(boolean on) {
        if (on)
            cmd = cmd | LIGHT_ON;
        else
            cmd = cmd & (~LIGHT_ON);
    }

    public void setInspectionOn(boolean on) {
        if (on)
            cmd = cmd | INSPECTION;
        else
            cmd = cmd & (~INSPECTION);
    }

    public void setMode(OvenMode mode) {
        if (mode == MODE_TEMP)
            cmd = cmd | MODE_TEMP.value;
        else if (mode == MODE_OVER_ROAST)
            cmd = cmd | MODE_OVER_ROAST.value;
        else if (mode == MODE_STOP_ROAST)
            cmd = cmd | MODE_STOP_ROAST.value;
        else
            cmd = cmd | MODE_RUN_ROAST.value;
    }

    public void addStep(DeviceRunStep step) {
        mDeviceRunSteps.add(step);
    }

    public void removeStep(int pos) {
        if (mDeviceRunSteps.size() > pos)
            mDeviceRunSteps.remove(pos);
    }

    public int getCmd() {
        return cmd;
    }

    public void setCmd(int cmd) {
        this.cmd = cmd;
    }

    public List<DeviceRunStep> getDeviceRunSteps() {
        return mDeviceRunSteps;
    }

    public void setDeviceRunSteps(List<DeviceRunStep> deviceRunSteps) {
        mDeviceRunSteps = deviceRunSteps;
    }

    public String toString() {
        String result = "cmd=" + cmd;
        for (int i = 0; i < mDeviceRunSteps.size(); i++) {
            result += ",     step" + i + " temp=" + mDeviceRunSteps.get(i).getTemp()
                    + " timeMinute=" + mDeviceRunSteps.get(i).getTime();
        }
        return result;
    }

    public String toShortString() {
        String result = "";
        return result;
    }

}
