package com.viomi.oven.enumType;

/**
 * Created by Ljh on 2018/2/22
 */
public enum McuStatus {//
    MCU_IDLE(0),//空闲状态
    MCU_WORKING(1),//烹饪中
    MCU_SUSPENDED(2),//暂停挂起状态
    MCU_UPGRADE(3);//升级状态

    public int value;
    public String valueString;

    McuStatus(int value) {
        this.value = value;
        valueString = Integer.toString(value);
    }
}
