package com.viomi.oven.enumType;

/**
 * Created by Ljh on 2018/1/8
 */
public enum ModelType {//
    MODEL_SPECIAL(0),//专业模式
    MODEL_SMART(1),//智能菜谱
    MODEL_SCAN(2),//扫码烹饪
    MODEL_MY_SET(3),//我的菜谱
    MODEL_OTHER(4);//其他

    public int value;
    public String valueString;

    ModelType(int value) {
        this.value = value;
        valueString = Integer.toString(value);
    }
}
