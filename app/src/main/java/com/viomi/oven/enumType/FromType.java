package com.viomi.oven.enumType;

/**
 * Created by Ljh on 2018/2/24
 * app要根据不同的命令来源，显示不同的信息
 */
public enum FromType {//
    FROM_RECIPE(0),//菜谱
    FROM_SPECIAL(1),//专业烹饪
    FROM_OTHER(2);//其他模式

    public int value;
    public String valueString;

    FromType(int value) {
        this.value = value;
        valueString = Integer.toString(value);
    }
}
