package com.viomi.oven.enumType;

/**
 * Created by Ljh on 2018/2/22
 */
public enum WorkStatus {//
    STATUS_IDLE(0),//空闲状态
    STATUS_WORKING(1),//烹饪中
    STATUS_YUYUEING(2),//预约进行ing
    STATUS_FINISH(3),//烹饪完成
    STATUS_UPGRADE(4);//升级状态

    public int value;
    public String valueString;

    WorkStatus(int value) {
        this.value = value;
        valueString = Integer.toString(value);
    }
}
