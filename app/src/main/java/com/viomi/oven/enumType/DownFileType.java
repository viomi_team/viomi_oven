package com.viomi.oven.enumType;

/**
 * Created by Ljh on 2017/12/29
 */
public enum DownFileType {//下载文件
    APP(0),//App文件
    MCU(1),//MCU文件
    OTHER(2);//其他

    public int value;
    public String valueString;

    DownFileType(int value) {
        this.value = value;
        valueString = Integer.toString(value);
    }
}
