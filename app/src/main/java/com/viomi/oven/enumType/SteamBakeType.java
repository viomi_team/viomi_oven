package com.viomi.oven.enumType;

import com.viomi.oven.R;

/**
 * Created by Ljh on 2018/1/8
 * loadWay
 * 1、台面式：
 * bit0：	预留，固定为0关闭
 * bit1：	预留，固定为0关闭
 * bit2：	预留，固定为0关闭
 * bit3：	预留，固定为0关闭
 * bit4：	后方（后部）加热管，功率为：1300W.    =1开，=0关闭
 * bit5：	热风风机，功率为： 25W。              =1开，=1关闭
 * bit6：	蒸汽发生器（蒸发盘）内管，功率为：800W.   =1开，=0关闭
 * bit7：	预留，固定为0关闭
 * <p>
 * 2、嵌入式：整机最大功率为2800W，负载需要计算功率总和，再确定允许打开哪一路负载。
 * bit0：	上方（顶部）加热管内管，功率为：750W。  =1开，=0关闭
 * bit1：	上方（顶部）加热管外管，功率为：1200W。  =1开，=0关闭
 * bit2：	下方（底部）加热管，功率为：800W。    =1开，=0关闭
 * bit3：	下方（底部）煮水盘，功率为：400W。    =1开，=0关闭
 * bit4：	后方（后部）加热管，功率为：1900W。   =1开，=0关闭
 * bit5：	热风风机，功率为：25W                 =1开，=0关闭
 * bit6：	蒸汽发生器（蒸发盘）内管，功率为：800W。 =1开，=0关闭
 * bit7：	蒸汽发生器（蒸发盘）外管，功率为：1200W。=1开，=0关闭
 */
public enum SteamBakeType {//
    NO_MODE(0),//无
    STEAM(1),//蒸汽
    BAKE(2),//热风烤
    STEAM_BAKE(3),//蒸烤组合
    FERMENTATION(4),//发酵
    HOT_FOOD(5),//热菜
    THAW(6),//解冻
    CHUGOU(7),//除垢
    DISINFECTION(8),//高温消毒
    CLEAN(9),//清洁
    PREHEATING(10),//预热
    KEEP_WARM(11),//保温
    //嵌入式
    BAKE_1(33),//烤1
    BAKE_2(34),//烤2
    BAKE_3(35),//烤3
    BAKE_4(36),//烤4
    BAKE_5(37),//烤5
    BAKE_6(38),//烤6
    BAKE_7(39),//烤7
    STEAM_BAKE_1(49),//蒸烤1
    STEAM_BAKE_2(50),//蒸烤2
    STEAM_BAKE_3(51),//蒸烤3
    STEAM_BAKE_4(52),//蒸烤4
    STEAM_BAKE_5(53),//蒸烤5
    STEAM_BAKE_6(54);//蒸烤6
    //
    public int value = 1;//默认蒸汽
    public String name = "";
    //public int loadWay = 0;
    //
    public int minTime = 0;//分钟
    public int maxTime = 0;
    public int defTime = 0;
    //
    public int minTemp = 0;//温度
    public int maxTemp = 0;
    public int defTemp = 0;
    //drawable
    public int drawable = 0;
    public int l_drawable = 0;
    public int h_drawable = 0;
    public int s_drawable = 0;


    SteamBakeType(int value) {
        this.value = value;
        if (value == 1) {
            name = "蒸汽";//温度 时间都可调，蒸汽（95℃以上热风辅助）
            defTime = 20;
            defTemp = 100;
            minTemp = 40;
            maxTemp = 130;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_steam_press;
            l_drawable = R.drawable.large_steam;
            h_drawable = R.drawable.normal_steam;
            s_drawable = R.drawable.small_steam;
            //loadWay = 0b01100000;
        } else if (value == 2) {
            name = "热风烤";//温度 时间都可调
            defTime = 20;
            defTemp = 180;
            minTemp = 40;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_hot_air_press;
            l_drawable = R.drawable.large_refeng_bake;
            h_drawable = R.drawable.normal_refeng_bake;
            s_drawable = R.drawable.small_refeng_bake;
            //loadWay = 0b00110000;
        } else if (value == 3) {
            name = "蒸烤";//温度 时间都可调
            defTime = 20;
            defTemp = 180;
            minTemp = 100;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_steam_roast_press;
            //loadWay = 0b01110000;
        } else if (value == 4) {//蒸汽
            name = "发酵";//温度 时间都可调
            defTime = 60;
            defTemp = 40;
            minTemp = 35;
            maxTemp = 60;
            minTime = 1;
            maxTime = 720;
            drawable = R.drawable.other_fermentation_press;
            //loadWay = 0b01000000;
        } else if (value == 5) {//蒸汽
            name = "热菜";//    时间温度可调
            defTime = 10;
            defTemp = 100;
            minTemp = 50;
            maxTemp = 100;
            minTime = 1;
            maxTime = 60;//说明书未说明？
            drawable = R.drawable.other_hot_food_press;
            //loadWay = 0b01000000;
        } else if (value == 6) {//蒸汽
            name = "解冻";//    时间可调，温度可调
            defTime = 20;
            defTemp = 50;
            minTemp = 40;
            maxTemp = 60;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_thaw_press;
            //loadWay = 0b01000000;
        } else if (value == 7) {//蒸汽，  台式无除垢，嵌入式有
            name = "除垢";//暂无说明
            defTime = 60;
            defTemp = 100;
            minTemp = 80;
            maxTemp = 115;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_descaling_press;
            //loadWay = 0b01000000;
        } else if (value == 8) {//蒸汽+热风，？？？？？
            name = "高温消毒";// 时间可调,温度可调
            defTime = 20;//
            defTemp = 130;
            minTemp = 100;
            maxTemp = 130;
            minTime = 1;
            maxTime = 60;
            drawable = R.drawable.other_disinfection_press;
            //loadWay = 0b01110000;
        } else if (value == 9) {//蒸汽 ？？？？？
            name = "清洁";//时间可调
            defTime = 30;
            defTemp = 55;
            minTemp = 55;
            maxTemp = 55;
            minTime = 20;
            maxTime = 180;
            drawable = R.drawable.other_clean_press;
            //loadWay = 0b01000000;
        } else if (value == 10) {//热风，加热到指定温度即结束工作
            name = "预热";//温度可调
            defTime = 180;
            defTemp = 100;
            minTemp = 40;
            maxTemp = 230;
            minTime = 180;
            maxTime = 180;
            drawable = R.drawable.other_preheating_press;
            //loadWay = 0b00110000;
        } else if (value == 11) {//保温
            name = "保温";//温度可调
            defTime = 20;
            defTemp = 60;
            minTemp = 60;
            maxTemp = 80;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_keep_warm_press;
            //loadWay = 0b00110000;
        } else if (value == 33) {//烤1
            name = "焙烤";//温度可调
            defTime = 20;
            defTemp = 180;
            minTemp = 40;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_hot_air_press;
            l_drawable = R.drawable.large_hongpei;
            h_drawable = R.drawable.normal_hongpei;
            s_drawable = R.drawable.small_hongpei;
            //loadWay = 0b00110000;
        } else if (value == 34) {//烤2
            name = "风焙烤";//温度可调
            defTime = 20;
            defTemp = 160;
            minTemp = 40;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_hot_air_press;
            l_drawable = R.drawable.large_feng_hongpei;
            h_drawable = R.drawable.normal_feng_hongpei;
            s_drawable = R.drawable.small_feng_hongpei;
            //loadWay = 0b00110000;
        } else if (value == 35) {//烤3
            name = "烧烤";//温度可调
            defTime = 20;
            defTemp = 220;
            minTemp = 40;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_hot_air_press;
            l_drawable = R.drawable.large_bake;
            h_drawable = R.drawable.normal_bake;
            s_drawable = R.drawable.small_bake;
            //loadWay = 0b00110000;
        } else if (value == 36) {//烤4
            name = "强烧烤";//温度可调
            defTime = 20;
            defTemp = 220;
            minTemp = 40;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_hot_air_press;
            l_drawable = R.drawable.large_qiang_bake;
            h_drawable = R.drawable.normal_qiang_bake;
            s_drawable = R.drawable.small_qiang_bake;
            //loadWay = 0b00110000;
        } else if (value == 37) {//烤5
            name = "风扇烤";//温度可调
            defTime = 20;
            defTemp = 210;
            minTemp = 40;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_hot_air_press;
            l_drawable = R.drawable.large_fengshan_bake;
            h_drawable = R.drawable.normal_fengshan_bake;
            s_drawable = R.drawable.small_fengshan_bake;
            //loadWay = 0b00110000;
        } else if (value == 38) {//烤6
            name = "热风烤";//温度可调
            defTime = 20;
            defTemp = 180;
            minTemp = 40;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_hot_air_press;
            l_drawable = R.drawable.large_refeng_bake;
            h_drawable = R.drawable.normal_refeng_bake;
            s_drawable = R.drawable.small_refeng_bake;
            //loadWay = 0b00110000;
        } else if (value == 39) {//烤7
            name = "披萨烤";//温度可调
            defTime = 20;
            defTemp = 220;
            minTemp = 40;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_preheating_press;
            l_drawable = R.drawable.large_pisha_bake;
            h_drawable = R.drawable.normal_pisha_bake;
            s_drawable = R.drawable.small_pisha_bake;
            //loadWay = 0b00110000;
        } else if (value == 49) {//蒸烤1
            name = "嫩烤";//温度可调
            defTime = 20;
            defTemp = 220;
            minTemp = 100;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_steam_roast_press_v;
            l_drawable = R.drawable.large_neng_bake;
            h_drawable = R.drawable.normal_neng_bake;
            s_drawable = R.drawable.small_neng_bake;
            //loadWay = 0b00110000;
        } else if (value == 50) {//蒸烤2
            name = "脆烤";//温度可调
            defTime = 20;
            defTemp = 180;
            minTemp = 100;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_steam_roast_press_v;
            l_drawable = R.drawable.large_cui_bake;
            h_drawable = R.drawable.normal_cui_bake;
            s_drawable = R.drawable.small_cui_bake;
            //loadWay = 0b00110000;
        } else if (value == 51) {//蒸烤3
            name = "蒸烤3";//温度可调
            defTime = 20;
            defTemp = 180;
            minTemp = 40;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_steam_roast_press_v;
            //loadWay = 0b00110000;
        } else if (value == 52) {//蒸烤4
            name = "蒸烤4";//温度可调
            defTime = 20;
            defTemp = 180;
            minTemp = 40;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_steam_roast_press_v;
            //loadWay = 0b00110000;
        } else if (value == 53) {//蒸烤5
            name = "蒸烤5";//温度可调
            defTime = 20;
            defTemp = 180;
            minTemp = 40;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_steam_roast_press_v;
            //loadWay = 0b00110000;
        } else if (value == 54) {//蒸烤6
            name = "蒸烤6";//温度可调
            defTime = 20;
            defTemp = 180;
            minTemp = 40;
            maxTemp = 230;
            minTime = 1;
            maxTime = 180;
            drawable = R.drawable.other_steam_roast_press_v;
            //loadWay = 0b00110000;
        }
    }
}
