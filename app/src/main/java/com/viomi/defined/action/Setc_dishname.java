package com.viomi.defined.action;

import com.viomi.defined.ViomiDefined;
import com.viomi.defined.property.C_dishname;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class Setc_dishname extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setc_dishname.toActionType();

    public Setc_dishname() {
        super(TYPE);

        super.addArgument(C_dishname.TYPE.toString());
    }
}