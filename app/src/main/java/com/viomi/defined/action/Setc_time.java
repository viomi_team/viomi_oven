package com.viomi.defined.action;

import com.viomi.defined.ViomiDefined;
import com.viomi.defined.property.Status;
import com.viomi.defined.property.Mode;
import com.viomi.defined.property.From;
import com.viomi.defined.property.PauseStatus;
import com.viomi.defined.property.S_temp;
import com.viomi.defined.property.C_dishname;
import com.viomi.defined.property.C_dishdata;
import com.viomi.defined.property.C_temp;
import com.viomi.defined.property.C_totletime;
import com.viomi.defined.property.C_time;
import com.viomi.defined.property.C_temps;
import com.viomi.defined.property.Dishinfo;
import com.viomi.defined.property.Descaling;
import com.viomi.defined.property.Bespeakdish;
import com.viomi.defined.property.BespeakStamp;
import com.viomi.defined.property.FirstScan;
import com.viomi.defined.property.DishsVer;
import com.viomi.defined.property.Error;
import com.viomi.defined.property.Door;
import com.viomi.defined.property.Light;
import com.viomi.defined.property.Water;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class Setc_time extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setc_time.toActionType();

    public Setc_time() {
        super(TYPE);

        super.addArgument(C_time.TYPE.toString());
    }
}