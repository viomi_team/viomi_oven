package com.viomi.defined.action;

import com.viomi.defined.ViomiDefined;
import com.viomi.defined.property.C_dishdata;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class Setc_dishdata extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setc_dishdata.toActionType();

    public Setc_dishdata() {
        super(TYPE);

        super.addArgument(C_dishdata.TYPE.toString());
    }
}