package com.viomi.defined.action;

import com.viomi.defined.ViomiDefined;
import com.viomi.defined.property.Status;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class Setstatus extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setstatus.toActionType();

    public Setstatus() {
        super(TYPE);

        super.addArgument(Status.TYPE.toString());
    }
}