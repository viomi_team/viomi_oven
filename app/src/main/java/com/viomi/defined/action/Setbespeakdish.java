package com.viomi.defined.action;

import com.viomi.defined.ViomiDefined;
import com.viomi.defined.property.Bespeakdish;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class Setbespeakdish extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setbespeakdish.toActionType();

    public Setbespeakdish() {
        super(TYPE);

        super.addArgument(Bespeakdish.TYPE.toString());
    }
}