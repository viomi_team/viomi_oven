package com.viomi.defined.action;

import com.viomi.defined.ViomiDefined;
import com.viomi.defined.property.BespeakStamp;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetbespeakStamp extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setbespeakStamp.toActionType();

    public SetbespeakStamp() {
        super(TYPE);

        super.addArgument(BespeakStamp.TYPE.toString());
    }
}