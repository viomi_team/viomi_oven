package com.viomi.defined.action;

import com.viomi.defined.ViomiDefined;
import com.viomi.defined.property.Water;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class Setwater extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setwater.toActionType();

    public Setwater() {
        super(TYPE);

        super.addArgument(Water.TYPE.toString());
    }
}