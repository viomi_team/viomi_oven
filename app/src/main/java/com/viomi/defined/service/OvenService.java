package com.viomi.defined.service;

import android.util.Log;

import com.viomi.defined.ViomiDefined;
import com.viomi.defined.action.*;
import com.viomi.defined.property.*;
import com.viomi.defined.property.Error;
import com.xiaomi.miot.typedef.data.value.Vint;
import com.xiaomi.miot.typedef.data.value.Vstring;
import com.xiaomi.miot.typedef.device.Action;
import com.xiaomi.miot.typedef.device.ActionInfo;
import com.xiaomi.miot.typedef.device.operable.ServiceOperable;
import com.xiaomi.miot.typedef.error.MiotError;
import com.xiaomi.miot.typedef.property.Property;
import com.xiaomi.miot.typedef.urn.ServiceType;

public class OvenService extends ServiceOperable {

    public static final ServiceType TYPE = ViomiDefined.Service.OvenService.toServiceType();
    private static final String TAG = "OvenService";

    public OvenService(boolean hasOptionalProperty) {
        super(TYPE);

        super.addProperty(new Status());
        super.addProperty(new Mode());
        super.addProperty(new From());
        super.addProperty(new PauseStatus());
        super.addProperty(new S_temp());
        super.addProperty(new C_dishname());
        super.addProperty(new C_dishdata());
        super.addProperty(new C_temp());
        super.addProperty(new C_totletime());
        super.addProperty(new C_time());
        super.addProperty(new C_temps());
        super.addProperty(new Dishinfo());
        super.addProperty(new Descaling());
        super.addProperty(new Bespeakdish());
        super.addProperty(new BespeakStamp());
        super.addProperty(new FirstScan());
        super.addProperty(new DishsVer());
        super.addProperty(new Error());
        super.addProperty(new Door());
        super.addProperty(new Light());
        super.addProperty(new Water());

        if (hasOptionalProperty) {
        }

        super.addAction(new Setbespeakdish());
        super.addAction(new Setstatus());
        super.addAction(new Setc_time());
        super.addAction(new Setdescaling());
        super.addAction(new SetdishsVer());
        super.addAction(new Setlight());
        super.addAction(new Setdoor());
        super.addAction(new Setc_dishdata());
        super.addAction(new SetpauseStatus());
        super.addAction(new Setc_totletime());
        super.addAction(new Setc_dishname());
        super.addAction(new Setdishinfo());
        super.addAction(new Setfrom());
        super.addAction(new Seterror());
        super.addAction(new Sets_temp());
        super.addAction(new SetfirstScan());
        super.addAction(new Setc_temp());
        super.addAction(new Setc_temps());
        super.addAction(new Setwater());
        super.addAction(new SetbespeakStamp());
        super.addAction(new Setmode());
    }

    /**
     * Properties
     */
    public Status status() {
        Property p = super.getProperty(Status.TYPE);
        if (p != null) {
            if (p instanceof Status) {
                return (Status) p;
            }
        }

        return null;
    }
    public Mode mode() {
        Property p = super.getProperty(Mode.TYPE);
        if (p != null) {
            if (p instanceof Mode) {
                return (Mode) p;
            }
        }

        return null;
    }
    public From from() {
        Property p = super.getProperty(From.TYPE);
        if (p != null) {
            if (p instanceof From) {
                return (From) p;
            }
        }

        return null;
    }
    public PauseStatus pauseStatus() {
        Property p = super.getProperty(PauseStatus.TYPE);
        if (p != null) {
            if (p instanceof PauseStatus) {
                return (PauseStatus) p;
            }
        }

        return null;
    }
    public S_temp s_temp() {
        Property p = super.getProperty(S_temp.TYPE);
        if (p != null) {
            if (p instanceof S_temp) {
                return (S_temp) p;
            }
        }

        return null;
    }
    public C_dishname c_dishname() {
        Property p = super.getProperty(C_dishname.TYPE);
        if (p != null) {
            if (p instanceof C_dishname) {
                return (C_dishname) p;
            }
        }

        return null;
    }
    public C_dishdata c_dishdata() {
        Property p = super.getProperty(C_dishdata.TYPE);
        if (p != null) {
            if (p instanceof C_dishdata) {
                return (C_dishdata) p;
            }
        }

        return null;
    }
    public C_temp c_temp() {
        Property p = super.getProperty(C_temp.TYPE);
        if (p != null) {
            if (p instanceof C_temp) {
                return (C_temp) p;
            }
        }

        return null;
    }
    public C_totletime c_totletime() {
        Property p = super.getProperty(C_totletime.TYPE);
        if (p != null) {
            if (p instanceof C_totletime) {
                return (C_totletime) p;
            }
        }

        return null;
    }
    public C_time c_time() {
        Property p = super.getProperty(C_time.TYPE);
        if (p != null) {
            if (p instanceof C_time) {
                return (C_time) p;
            }
        }

        return null;
    }
    public C_temps c_temps() {
        Property p = super.getProperty(C_temps.TYPE);
        if (p != null) {
            if (p instanceof C_temps) {
                return (C_temps) p;
            }
        }

        return null;
    }
    public Dishinfo dishinfo() {
        Property p = super.getProperty(Dishinfo.TYPE);
        if (p != null) {
            if (p instanceof Dishinfo) {
                return (Dishinfo) p;
            }
        }

        return null;
    }
    public Descaling descaling() {
        Property p = super.getProperty(Descaling.TYPE);
        if (p != null) {
            if (p instanceof Descaling) {
                return (Descaling) p;
            }
        }

        return null;
    }
    public Bespeakdish bespeakdish() {
        Property p = super.getProperty(Bespeakdish.TYPE);
        if (p != null) {
            if (p instanceof Bespeakdish) {
                return (Bespeakdish) p;
            }
        }

        return null;
    }
    public BespeakStamp bespeakStamp() {
        Property p = super.getProperty(BespeakStamp.TYPE);
        if (p != null) {
            if (p instanceof BespeakStamp) {
                return (BespeakStamp) p;
            }
        }

        return null;
    }
    public FirstScan firstScan() {
        Property p = super.getProperty(FirstScan.TYPE);
        if (p != null) {
            if (p instanceof FirstScan) {
                return (FirstScan) p;
            }
        }

        return null;
    }
    public DishsVer dishsVer() {
        Property p = super.getProperty(DishsVer.TYPE);
        if (p != null) {
            if (p instanceof DishsVer) {
                return (DishsVer) p;
            }
        }

        return null;
    }
    public Error error() {
        Property p = super.getProperty(Error.TYPE);
        if (p != null) {
            if (p instanceof Error) {
                return (Error) p;
            }
        }

        return null;
    }
    public Door door() {
        Property p = super.getProperty(Door.TYPE);
        if (p != null) {
            if (p instanceof Door) {
                return (Door) p;
            }
        }

        return null;
    }
    public Light light() {
        Property p = super.getProperty(Light.TYPE);
        if (p != null) {
            if (p instanceof Light) {
                return (Light) p;
            }
        }

        return null;
    }
    public Water water() {
        Property p = super.getProperty(Water.TYPE);
        if (p != null) {
            if (p instanceof Water) {
                return (Water) p;
            }
        }

        return null;
    }

    /**
     * Actions
     */
    public Setbespeakdish setbespeakdish(){
        Action a = super.getAction(Setbespeakdish.TYPE);
        if (a != null) {
            if (a instanceof Setbespeakdish) {
                return (Setbespeakdish) a;
            }
        }

        return null;
    }
    public Setstatus setstatus(){
        Action a = super.getAction(Setstatus.TYPE);
        if (a != null) {
            if (a instanceof Setstatus) {
                return (Setstatus) a;
            }
        }

        return null;
    }
    public Setc_time setc_time(){
        Action a = super.getAction(Setc_time.TYPE);
        if (a != null) {
            if (a instanceof Setc_time) {
                return (Setc_time) a;
            }
        }

        return null;
    }
    public Setdescaling setdescaling(){
        Action a = super.getAction(Setdescaling.TYPE);
        if (a != null) {
            if (a instanceof Setdescaling) {
                return (Setdescaling) a;
            }
        }

        return null;
    }
    public SetdishsVer setdishsVer(){
        Action a = super.getAction(SetdishsVer.TYPE);
        if (a != null) {
            if (a instanceof SetdishsVer) {
                return (SetdishsVer) a;
            }
        }

        return null;
    }
    public Setlight setlight(){
        Action a = super.getAction(Setlight.TYPE);
        if (a != null) {
            if (a instanceof Setlight) {
                return (Setlight) a;
            }
        }

        return null;
    }
    public Setdoor setdoor(){
        Action a = super.getAction(Setdoor.TYPE);
        if (a != null) {
            if (a instanceof Setdoor) {
                return (Setdoor) a;
            }
        }

        return null;
    }
    public Setc_dishdata setc_dishdata(){
        Action a = super.getAction(Setc_dishdata.TYPE);
        if (a != null) {
            if (a instanceof Setc_dishdata) {
                return (Setc_dishdata) a;
            }
        }

        return null;
    }
    public SetpauseStatus setpauseStatus(){
        Action a = super.getAction(SetpauseStatus.TYPE);
        if (a != null) {
            if (a instanceof SetpauseStatus) {
                return (SetpauseStatus) a;
            }
        }

        return null;
    }
    public Setc_totletime setc_totletime(){
        Action a = super.getAction(Setc_totletime.TYPE);
        if (a != null) {
            if (a instanceof Setc_totletime) {
                return (Setc_totletime) a;
            }
        }

        return null;
    }
    public Setc_dishname setc_dishname(){
        Action a = super.getAction(Setc_dishname.TYPE);
        if (a != null) {
            if (a instanceof Setc_dishname) {
                return (Setc_dishname) a;
            }
        }

        return null;
    }
    public Setdishinfo setdishinfo(){
        Action a = super.getAction(Setdishinfo.TYPE);
        if (a != null) {
            if (a instanceof Setdishinfo) {
                return (Setdishinfo) a;
            }
        }

        return null;
    }
    public Setfrom setfrom(){
        Action a = super.getAction(Setfrom.TYPE);
        if (a != null) {
            if (a instanceof Setfrom) {
                return (Setfrom) a;
            }
        }

        return null;
    }
    public Seterror seterror(){
        Action a = super.getAction(Seterror.TYPE);
        if (a != null) {
            if (a instanceof Seterror) {
                return (Seterror) a;
            }
        }

        return null;
    }
    public Sets_temp sets_temp(){
        Action a = super.getAction(Sets_temp.TYPE);
        if (a != null) {
            if (a instanceof Sets_temp) {
                return (Sets_temp) a;
            }
        }

        return null;
    }
    public SetfirstScan setfirstScan(){
        Action a = super.getAction(SetfirstScan.TYPE);
        if (a != null) {
            if (a instanceof SetfirstScan) {
                return (SetfirstScan) a;
            }
        }

        return null;
    }
    public Setc_temp setc_temp(){
        Action a = super.getAction(Setc_temp.TYPE);
        if (a != null) {
            if (a instanceof Setc_temp) {
                return (Setc_temp) a;
            }
        }

        return null;
    }
    public Setc_temps setc_temps(){
        Action a = super.getAction(Setc_temps.TYPE);
        if (a != null) {
            if (a instanceof Setc_temps) {
                return (Setc_temps) a;
            }
        }

        return null;
    }
    public Setwater setwater(){
        Action a = super.getAction(Setwater.TYPE);
        if (a != null) {
            if (a instanceof Setwater) {
                return (Setwater) a;
            }
        }

        return null;
    }
    public SetbespeakStamp setbespeakStamp(){
        Action a = super.getAction(SetbespeakStamp.TYPE);
        if (a != null) {
            if (a instanceof SetbespeakStamp) {
                return (SetbespeakStamp) a;
            }
        }

        return null;
    }
    public Setmode setmode(){
        Action a = super.getAction(Setmode.TYPE);
        if (a != null) {
            if (a instanceof Setmode) {
                return (Setmode) a;
            }
        }

        return null;
    }

    /**
     * PropertyGetter
     */
    public interface PropertyGetter {
        String getstatus();

        int getmode();

        int getfrom();

        int getpauseStatus();

        int gets_temp();

        String getc_dishname();

        String getc_dishdata();

        int getc_temp();

        int getc_totletime();

        int getc_time();

        String getc_temps();

        String getdishinfo();

        String getdescaling();

        String getbespeakdish();

        int getbespeakStamp();

        int getfirstScan();

        int getdishsVer();

        int geterror();

        int getdoor();

        int getlight();

        int getwater();

    }

    /**
     * PropertySetter
     */
    public interface PropertySetter {
        void setstatus(String value);

        void setmode(int value);

        void setfrom(int value);

        void setpauseStatus(int value);

        void sets_temp(int value);

        void setc_dishname(String value);

        void setc_dishdata(String value);

        void setc_temp(int value);

        void setc_totletime(int value);

        void setc_time(int value);

        void setc_temps(String value);

        void setdishinfo(String value);

        void setdescaling(String value);

        void setbespeakdish(String value);

        void setbespeakStamp(int value);

        void setfirstScan(int value);

        void setdishsVer(int value);

        void seterror(int value);

        void setdoor(int value);

        void setlight(int value);

        void setwater(int value);

    }

    /**
     * ActionsHandler
     */
    public interface ActionHandler {
        void onsetbespeakdish(String bespeakdish);
        void onsetstatus(String status);
        void onsetc_time(int c_time);
        void onsetdescaling(String descaling);
        void onsetdishsVer(int dishsVer);
        void onsetlight(int light);
        void onsetdoor(int door);
        void onsetc_dishdata(String c_dishdata);
        void onsetpauseStatus(int pauseStatus);
        void onsetc_totletime(int c_totletime);
        void onsetc_dishname(String c_dishname);
        void onsetdishinfo(String dishinfo);
        void onsetfrom(int from);
        void onseterror(int error);
        void onsets_temp(int s_temp);
        void onsetfirstScan(int firstScan);
        void onsetc_temp(int c_temp);
        void onsetc_temps(String c_temps);
        void onsetwater(int water);
        void onsetbespeakStamp(int bespeakStamp);
        void onsetmode(int mode);
    }


    private MiotError onsetbespeakdish(ActionInfo action) {
        String bespeakdish = ((Vstring) action.getArgumentValue(Bespeakdish.TYPE)).getValue();
        actionHandler.onsetbespeakdish(bespeakdish);

        return MiotError.OK;
    }
    private MiotError onsetstatus(ActionInfo action) {
        String status = ((Vstring) action.getArgumentValue(Status.TYPE)).getValue();
        actionHandler.onsetstatus(status);

        return MiotError.OK;
    }
    private MiotError onsetc_time(ActionInfo action) {
        int c_time = ((Vint) action.getArgumentValue(C_time.TYPE)).getValue();
        actionHandler.onsetc_time(c_time);

        return MiotError.OK;
    }
    private MiotError onsetdescaling(ActionInfo action) {
        String descaling = ((Vstring) action.getArgumentValue(Descaling.TYPE)).getValue();
        actionHandler.onsetdescaling(descaling);

        return MiotError.OK;
    }
    private MiotError onsetdishsVer(ActionInfo action) {
        int dishsVer = ((Vint) action.getArgumentValue(DishsVer.TYPE)).getValue();
        actionHandler.onsetdishsVer(dishsVer);

        return MiotError.OK;
    }
    private MiotError onsetlight(ActionInfo action) {
        int light = ((Vint) action.getArgumentValue(Light.TYPE)).getValue();
        actionHandler.onsetlight(light);

        return MiotError.OK;
    }
    private MiotError onsetdoor(ActionInfo action) {
        int door = ((Vint) action.getArgumentValue(Door.TYPE)).getValue();
        actionHandler.onsetdoor(door);

        return MiotError.OK;
    }
    private MiotError onsetc_dishdata(ActionInfo action) {
        String c_dishdata = ((Vstring) action.getArgumentValue(C_dishdata.TYPE)).getValue();
        actionHandler.onsetc_dishdata(c_dishdata);

        return MiotError.OK;
    }
    private MiotError onsetpauseStatus(ActionInfo action) {
        int pauseStatus = ((Vint) action.getArgumentValue(PauseStatus.TYPE)).getValue();
        actionHandler.onsetpauseStatus(pauseStatus);

        return MiotError.OK;
    }
    private MiotError onsetc_totletime(ActionInfo action) {
        int c_totletime = ((Vint) action.getArgumentValue(C_totletime.TYPE)).getValue();
        actionHandler.onsetc_totletime(c_totletime);

        return MiotError.OK;
    }
    private MiotError onsetc_dishname(ActionInfo action) {
        String c_dishname = ((Vstring) action.getArgumentValue(C_dishname.TYPE)).getValue();
        actionHandler.onsetc_dishname(c_dishname);

        return MiotError.OK;
    }
    private MiotError onsetdishinfo(ActionInfo action) {
        String dishinfo = ((Vstring) action.getArgumentValue(Dishinfo.TYPE)).getValue();
        actionHandler.onsetdishinfo(dishinfo);

        return MiotError.OK;
    }
    private MiotError onsetfrom(ActionInfo action) {
        int from = ((Vint) action.getArgumentValue(From.TYPE)).getValue();
        actionHandler.onsetfrom(from);

        return MiotError.OK;
    }
    private MiotError onseterror(ActionInfo action) {
        int error = ((Vint) action.getArgumentValue(Error.TYPE)).getValue();
        actionHandler.onseterror(error);

        return MiotError.OK;
    }
    private MiotError onsets_temp(ActionInfo action) {
        int s_temp = ((Vint) action.getArgumentValue(S_temp.TYPE)).getValue();
        actionHandler.onsets_temp(s_temp);

        return MiotError.OK;
    }
    private MiotError onsetfirstScan(ActionInfo action) {
        int firstScan = ((Vint) action.getArgumentValue(FirstScan.TYPE)).getValue();
        actionHandler.onsetfirstScan(firstScan);

        return MiotError.OK;
    }
    private MiotError onsetc_temp(ActionInfo action) {
        int c_temp = ((Vint) action.getArgumentValue(C_temp.TYPE)).getValue();
        actionHandler.onsetc_temp(c_temp);

        return MiotError.OK;
    }
    private MiotError onsetc_temps(ActionInfo action) {
        String c_temps = ((Vstring) action.getArgumentValue(C_temps.TYPE)).getValue();
        actionHandler.onsetc_temps(c_temps);

        return MiotError.OK;
    }
    private MiotError onsetwater(ActionInfo action) {
        int water = ((Vint) action.getArgumentValue(Water.TYPE)).getValue();
        actionHandler.onsetwater(water);

        return MiotError.OK;
    }
    private MiotError onsetbespeakStamp(ActionInfo action) {
        int bespeakStamp = ((Vint) action.getArgumentValue(BespeakStamp.TYPE)).getValue();
        actionHandler.onsetbespeakStamp(bespeakStamp);

        return MiotError.OK;
    }
    private MiotError onsetmode(ActionInfo action) {
        int mode = ((Vint) action.getArgumentValue(Mode.TYPE)).getValue();
        actionHandler.onsetmode(mode);

        return MiotError.OK;
    }

    /**
     * Handle actions invocation & properties operation
     */
    private ActionHandler actionHandler;
    private PropertyGetter propertyGetter;
    private PropertySetter propertySetter;

    public void setHandler(ActionHandler handler, PropertyGetter getter, PropertySetter setter) {
        actionHandler = handler;
        propertyGetter = getter;
        propertySetter = setter;
    }

    @Override
    public MiotError onSet(Property property) {
        Log.e(TAG, "onSet");

        if (propertySetter == null) {
            return super.onSet(property);
        }

        ViomiDefined.Property p = ViomiDefined.Property.valueOf(property.getDefinition().getType());
        switch (p) {
            case status:
                propertySetter.setstatus(((Vstring) property.getCurrentValue()).getValue());
                break;
            case mode:
                propertySetter.setmode(((Vint) property.getCurrentValue()).getValue());
                break;
            case from:
                propertySetter.setfrom(((Vint) property.getCurrentValue()).getValue());
                break;
            case pauseStatus:
                propertySetter.setpauseStatus(((Vint) property.getCurrentValue()).getValue());
                break;
            case s_temp:
                propertySetter.sets_temp(((Vint) property.getCurrentValue()).getValue());
                break;
            case c_dishname:
                propertySetter.setc_dishname(((Vstring) property.getCurrentValue()).getValue());
                break;
            case c_dishdata:
                propertySetter.setc_dishdata(((Vstring) property.getCurrentValue()).getValue());
                break;
            case c_temp:
                propertySetter.setc_temp(((Vint) property.getCurrentValue()).getValue());
                break;
            case c_totletime:
                propertySetter.setc_totletime(((Vint) property.getCurrentValue()).getValue());
                break;
            case c_time:
                propertySetter.setc_time(((Vint) property.getCurrentValue()).getValue());
                break;
            case c_temps:
                propertySetter.setc_temps(((Vstring) property.getCurrentValue()).getValue());
                break;
            case dishinfo:
                propertySetter.setdishinfo(((Vstring) property.getCurrentValue()).getValue());
                break;
            case descaling:
                propertySetter.setdescaling(((Vstring) property.getCurrentValue()).getValue());
                break;
            case bespeakdish:
                propertySetter.setbespeakdish(((Vstring) property.getCurrentValue()).getValue());
                break;
            case bespeakStamp:
                propertySetter.setbespeakStamp(((Vint) property.getCurrentValue()).getValue());
                break;
            case firstScan:
                propertySetter.setfirstScan(((Vint) property.getCurrentValue()).getValue());
                break;
            case dishsVer:
                propertySetter.setdishsVer(((Vint) property.getCurrentValue()).getValue());
                break;
            case error:
                propertySetter.seterror(((Vint) property.getCurrentValue()).getValue());
                break;
            case door:
                propertySetter.setdoor(((Vint) property.getCurrentValue()).getValue());
                break;
            case light:
                propertySetter.setlight(((Vint) property.getCurrentValue()).getValue());
                break;
            case water:
                propertySetter.setwater(((Vint) property.getCurrentValue()).getValue());
                break;

            default:
                return MiotError.IOT_RESOURCE_NOT_EXIST;
        }

        return MiotError.OK;
    }

    @Override
    public MiotError onGet(Property property) {
        Log.e(TAG, "onGet");

        if (propertyGetter == null) {
            return super.onGet(property);
        }

        ViomiDefined.Property p = ViomiDefined.Property.valueOf(property.getDefinition().getType());
        switch (p) {
            case status:
                property.setValue(propertyGetter.getstatus());
                break;
            case mode:
                property.setValue(propertyGetter.getmode());
                break;
            case from:
                property.setValue(propertyGetter.getfrom());
                break;
            case pauseStatus:
                property.setValue(propertyGetter.getpauseStatus());
                break;
            case s_temp:
                property.setValue(propertyGetter.gets_temp());
                break;
            case c_dishname:
                property.setValue(propertyGetter.getc_dishname());
                break;
            case c_dishdata:
                property.setValue(propertyGetter.getc_dishdata());
                break;
            case c_temp:
                property.setValue(propertyGetter.getc_temp());
                break;
            case c_totletime:
                property.setValue(propertyGetter.getc_totletime());
                break;
            case c_time:
                property.setValue(propertyGetter.getc_time());
                break;
            case c_temps:
                property.setValue(propertyGetter.getc_temps());
                break;
            case dishinfo:
                property.setValue(propertyGetter.getdishinfo());
                break;
            case descaling:
                property.setValue(propertyGetter.getdescaling());
                break;
            case bespeakdish:
                property.setValue(propertyGetter.getbespeakdish());
                break;
            case bespeakStamp:
                property.setValue(propertyGetter.getbespeakStamp());
                break;
            case firstScan:
                property.setValue(propertyGetter.getfirstScan());
                break;
            case dishsVer:
                property.setValue(propertyGetter.getdishsVer());
                break;
            case error:
                property.setValue(propertyGetter.geterror());
                break;
            case door:
                property.setValue(propertyGetter.getdoor());
                break;
            case light:
                property.setValue(propertyGetter.getlight());
                break;
            case water:
                property.setValue(propertyGetter.getwater());
                break;

            default:
                return MiotError.IOT_RESOURCE_NOT_EXIST;
        }

        return MiotError.OK;
    }

    @Override
    public MiotError onAction(ActionInfo action) {
        Log.e(TAG, "onAction: " + action.getType().toString());

        if (actionHandler == null) {
            return super.onAction(action);
        }

        ViomiDefined.Action a = ViomiDefined.Action.valueOf(action.getType());
        switch (a) {
            case setbespeakdish:
                return onsetbespeakdish(action);
            case setstatus:
                return onsetstatus(action);
            case setc_time:
                return onsetc_time(action);
            case setdescaling:
                return onsetdescaling(action);
            case setdishsVer:
                return onsetdishsVer(action);
            case setlight:
                return onsetlight(action);
            case setdoor:
                return onsetdoor(action);
            case setc_dishdata:
                return onsetc_dishdata(action);
            case setpauseStatus:
                return onsetpauseStatus(action);
            case setc_totletime:
                return onsetc_totletime(action);
            case setc_dishname:
                return onsetc_dishname(action);
            case setdishinfo:
                return onsetdishinfo(action);
            case setfrom:
                return onsetfrom(action);
            case seterror:
                return onseterror(action);
            case sets_temp:
                return onsets_temp(action);
            case setfirstScan:
                return onsetfirstScan(action);
            case setc_temp:
                return onsetc_temp(action);
            case setc_temps:
                return onsetc_temps(action);
            case setwater:
                return onsetwater(action);
            case setbespeakStamp:
                return onsetbespeakStamp(action);
            case setmode:
                return onsetmode(action);

            default:
                Log.e(TAG, "invalid action: " + a);
                break;
        }

        return MiotError.IOT_RESOURCE_NOT_EXIST;
    }
}