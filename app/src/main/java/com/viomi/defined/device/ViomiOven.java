/* This file is auto-generated.*/

package com.viomi.defined.device;

import com.viomi.defined.service.OvenService;
import com.xiaomi.miot.host.manager.MiotDeviceConfig;
import com.xiaomi.miot.host.manager.MiotHostManager;
import com.xiaomi.miot.typedef.device.operable.DeviceOperable;
import com.xiaomi.miot.typedef.exception.MiotException;
import com.xiaomi.miot.typedef.listener.CompletedListener;
import com.xiaomi.miot.typedef.urn.DeviceType;

public class ViomiOven extends DeviceOperable {

    private static final DeviceType DEVICE_TYPE = new DeviceType("Viomi", "ViomiOven", "1");

    private OvenService _OvenService = new OvenService(false);

    public ViomiOven(MiotDeviceConfig config) {
        super(DEVICE_TYPE);
        super.setDiscoveryTypes(config.discoveryTypes());
        super.setFriendlyName(config.friendlyName());
        super.setDeviceId(config.deviceId());
        super.setMacAddress(config.macAddress());
        super.setManufacturer(config.manufacturer());
        super.setModelName(config.modelName());
        super.setMiotToken(config.miotToken());
        super.setMiotInfo(config.miotInfo());
        super.addService(_OvenService);
        super.initializeInstanceID();
    }

    public OvenService OvenService() {
        return _OvenService;
    }

    public void start(CompletedListener listener) throws MiotException {
        MiotHostManager.getInstance().register(this, listener, this);
    }

    public void stop(CompletedListener listener) throws MiotException {
        MiotHostManager.getInstance().unregister(this, listener);
    }

    public void sendEvents() throws MiotException {
        MiotHostManager.getInstance().sendEvent(super.getChangedProperties());
    }

    public void send(String method, String params) throws MiotException {
        MiotHostManager.getInstance().send(method, params);
    }
}