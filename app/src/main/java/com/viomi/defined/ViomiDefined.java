package com.viomi.defined;

import android.util.Log;

import com.xiaomi.miot.typedef.urn.ActionType;
import com.xiaomi.miot.typedef.urn.PropertyType;
import com.xiaomi.miot.typedef.urn.ServiceType;

public class ViomiDefined {

    private static final String TAG = "ViomiDefined";
    private static final String DOMAIN = "Viomi";
    private static final String _UUID = "-0000-1000-2000-000000AABBCC";

    private ViomiDefined() {
    }

    /**
     * Properties
     * urn:Viomi:property:status:0000
     * urn:Viomi:property:mode:0000
     * urn:Viomi:property:from:0000
     * urn:Viomi:property:pauseStatus:0000
     * urn:Viomi:property:s_temp:0000
     * urn:Viomi:property:c_dishname:0000
     * urn:Viomi:property:c_dishdata:0000
     * urn:Viomi:property:c_temp:0000
     * urn:Viomi:property:c_totletime:0000
     * urn:Viomi:property:c_time:0000
     * urn:Viomi:property:c_temps:0000
     * urn:Viomi:property:dishinfo:0000
     * urn:Viomi:property:descaling:0000
     * urn:Viomi:property:bespeakdish:0000
     * urn:Viomi:property:bespeakStamp:0000
     * urn:Viomi:property:firstScan:0000
     * urn:Viomi:property:dishsVer:0000
     * urn:Viomi:property:error:0000
     * urn:Viomi:property:door:0000
     * urn:Viomi:property:light:0000
     * urn:Viomi:property:water:0000
     */
    public enum Property {
        Undefined(0),
        status(1),
        mode(2),
        from(3),
        pauseStatus(4),
        s_temp(5),
        c_dishname(6),
        c_dishdata(7),
        c_temp(8),
        c_totletime(9),
        c_time(10),
        c_temps(11),
        dishinfo(12),
        descaling(13),
        bespeakdish(14),
        bespeakStamp(15),
        firstScan(16),
        dishsVer(17),
        error(18),
        door(19),
        light(20),
        water(21);

        private int value;

        Property(int value) {
            this.value = value;
        }

        public static Property valueOf(int value) {
            for (Property c : values()) {
                if (c.value() == value) {
                    return c;
                }
            }

            Log.e(TAG, "invalid value: " + value);

            return Undefined;
        }

        public static Property valueOf(PropertyType type) {
            if (!type.getDomain().equals(DOMAIN)) {
                return Undefined;
            }

            for (Property c : values()) {
                if (c.toString().equals(type.getSubType())) {
                    return c;
                }
            }

            return Undefined;
        }

        public int value() {
            return value;
        }

        public PropertyType toPropertyType() {
            return new PropertyType(DOMAIN, this.toString(), toShortUUID());
        }

        public String toUUID() {
            return String.format("%08X%s", value, _UUID);
        }

        public String toShortUUID() {
            return String.format("%04X", value);
        }
    }

    /**
     * Actions
     * urn:Viomi:action:setbespeakdish:0000
     * urn:Viomi:action:setstatus:0000
     * urn:Viomi:action:setc_time:0000
     * urn:Viomi:action:setdescaling:0000
     * urn:Viomi:action:setdishsVer:0000
     * urn:Viomi:action:setlight:0000
     * urn:Viomi:action:setdoor:0000
     * urn:Viomi:action:setc_dishdata:0000
     * urn:Viomi:action:setpauseStatus:0000
     * urn:Viomi:action:setc_totletime:0000
     * urn:Viomi:action:setc_dishname:0000
     * urn:Viomi:action:setdishinfo:0000
     * urn:Viomi:action:setfrom:0000
     * urn:Viomi:action:seterror:0000
     * urn:Viomi:action:sets_temp:0000
     * urn:Viomi:action:setfirstScan:0000
     * urn:Viomi:action:setc_temp:0000
     * urn:Viomi:action:setc_temps:0000
     * urn:Viomi:action:setwater:0000
     * urn:Viomi:action:setbespeakStamp:0000
     * urn:Viomi:action:setmode:0000
     * ...
     */
    public enum Action {
        Undefined(0),
        setbespeakdish(1),
        setstatus(2),
        setc_time(3),
        setdescaling(4),
        setdishsVer(5),
        setlight(6),
        setdoor(7),
        setc_dishdata(8),
        setpauseStatus(9),
        setc_totletime(10),
        setc_dishname(11),
        setdishinfo(12),
        setfrom(13),
        seterror(14),
        sets_temp(15),
        setfirstScan(16),
        setc_temp(17),
        setc_temps(18),
        setwater(19),
        setbespeakStamp(20),
        setmode(21);

        private int value;

        Action(int value) {
            this.value = value;
        }

        public static Action valueOf(int value) {
            for (Action c : values()) {
                if (c.value() == value) {
                    return c;
                }
            }

            Log.e(TAG, "invalid value: " + value);

            return Undefined;
        }

        public static Action valueOf(ActionType type) {
            if (! type.getDomain().equals(DOMAIN)) {
                return Undefined;
            }

            for (Action v : values()) {
                if (v.toString().equals(type.getSubType())) {
                    return v;
                }
            }

            return Undefined;
        }

        public int value() {
            return value;
        }

        public ActionType toActionType() {
            return new ActionType(DOMAIN, this.toString(), toShortUUID());
        }

        public String toUUID() {
            return String.format("%08X%s", value, _UUID);
        }

        public String toShortUUID() {
            return String.format("%04X", value);
        }
    }

    /**
     * Servics
     * urn:Viomi:service:OvenService:0000
     */
    public enum Service {
        Undefined(0),
        OvenService(1);

        private int value;

        Service(int value) {
            this.value = value;
        }

        public static Service valueOf(int value) {
            for (Service c : values()) {
                if (c.value() == value) {
                    return c;
                }
            }

            Log.e(TAG, "invalid value: " + value);

            return Undefined;
        }

        public static Service valueOf(ServiceType type) {
            if (! type.getDomain().equals(DOMAIN)) {
                return Undefined;
            }

            for (Service v : values()) {
                if (v.toString().equals(type.getSubType())) {
                    return v;
                }
            }

            return Undefined;
        }

        public int value() {
            return value;
        }

        public ServiceType toServiceType() {
            return new ServiceType(DOMAIN, this.toString(), toShortUUID());
        }

        public String toUUID() {
            return String.format("%08X%s", value, _UUID);
        }

        public String toShortUUID() {
            return String.format("%04X", value);
        }
    }
}